.class public Lru/andrey/notepad/BuyActivity;
.super Landroid/app/Activity;
.source "BuyActivity.java"


# static fields
.field static arr:Lru/andrey/notepad/BoughtArrayAdapter;

.field public static item:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/BuyModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final EDIT_MENU_ID:I

.field add:Landroid/widget/Button;

.field color:I

.field delMode:Z

.field dh:Lru/andrey/notepad/DataBaseHelper;

.field isnew:Z

.field itemName:Landroid/widget/EditText;

.field lastIndex:I

.field listName:Ljava/lang/String;

.field lv:Landroid/widget/ListView;

.field num:I

.field uniID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    .line 45
    iput-boolean v1, p0, Lru/andrey/notepad/BuyActivity;->isnew:Z

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/andrey/notepad/BuyActivity;->delMode:Z

    .line 54
    iput v1, p0, Lru/andrey/notepad/BuyActivity;->EDIT_MENU_ID:I

    .line 39
    return-void
.end method

.method public static getDrawable(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 407
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 408
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 410
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static refresh()V
    .locals 1

    .prologue
    .line 532
    sget-object v0, Lru/andrey/notepad/BuyActivity;->arr:Lru/andrey/notepad/BoughtArrayAdapter;

    invoke-virtual {v0}, Lru/andrey/notepad/BoughtArrayAdapter;->notifyDataSetChanged()V

    .line 534
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 402
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 403
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v11, 0x7f05004a

    const/4 v8, 0x1

    .line 132
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050046

    invoke-virtual {p0, v10}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_0

    .line 134
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ShopList/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 135
    .local v7, "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 136
    .local v3, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 137
    iget v9, p0, Lru/andrey/notepad/BuyActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 138
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 139
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 140
    .local v5, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x493e0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 142
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 232
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :goto_0
    return v8

    .line 144
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050047

    invoke-virtual {p0, v10}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_1

    .line 146
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ShopList/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 147
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 148
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 149
    iget v9, p0, Lru/andrey/notepad/BuyActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 150
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 151
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 152
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0xdbba0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 154
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto :goto_0

    .line 156
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050048

    invoke-virtual {p0, v10}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_2

    .line 158
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ShopList/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 159
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 160
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 161
    iget v9, p0, Lru/andrey/notepad/BuyActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 162
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 163
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 164
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x16e360

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 166
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 168
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050049

    invoke-virtual {p0, v10}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_3

    .line 170
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ShopList/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 171
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 172
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 173
    iget v9, p0, Lru/andrey/notepad/BuyActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 174
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 175
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 176
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x36ee80

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 178
    iget-object v9, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 180
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {p0, v11}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_4

    .line 182
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 183
    .local v1, "dialog":Landroid/app/Dialog;
    const v9, 0x7f030016

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setContentView(I)V

    .line 184
    invoke-virtual {v1, v11}, Landroid/app/Dialog;->setTitle(I)V

    .line 185
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 186
    const v9, 0x7f070044

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/DatePicker;

    .line 187
    .local v2, "dp":Landroid/widget/DatePicker;
    const v9, 0x7f070045

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TimePicker;

    .line 188
    .local v6, "tp":Landroid/widget/TimePicker;
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 189
    const v9, 0x7f070028

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 190
    .local v4, "save":Landroid/widget/Button;
    const v9, 0x7f070019

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 192
    .local v0, "cancel":Landroid/widget/Button;
    new-instance v9, Lru/andrey/notepad/BuyActivity$3;

    invoke-direct {v9, p0, v2, v6, v1}, Lru/andrey/notepad/BuyActivity$3;-><init>(Lru/andrey/notepad/BuyActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    new-instance v9, Lru/andrey/notepad/BuyActivity$4;

    invoke-direct {v9, p0, v1}, Lru/andrey/notepad/BuyActivity$4;-><init>(Lru/andrey/notepad/BuyActivity;Landroid/app/Dialog;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 230
    .end local v0    # "cancel":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v2    # "dp":Landroid/widget/DatePicker;
    .end local v4    # "save":Landroid/widget/Button;
    .end local v6    # "tp":Landroid/widget/TimePicker;
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const v2, 0x7f030004

    invoke-virtual {p0, v2}, Lru/andrey/notepad/BuyActivity;->setContentView(I)V

    .line 62
    const v2, 0x7f070015

    invoke-virtual {p0, v2}, Lru/andrey/notepad/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/AdView;

    .line 63
    .local v1, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 64
    invoke-static {p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 65
    const v2, 0x7f070028

    invoke-virtual {p0, v2}, Lru/andrey/notepad/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lru/andrey/notepad/BuyActivity;->add:Landroid/widget/Button;

    .line 66
    const v2, 0x7f070017

    invoke-virtual {p0, v2}, Lru/andrey/notepad/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lru/andrey/notepad/BuyActivity;->lv:Landroid/widget/ListView;

    .line 67
    const v2, 0x7f070016

    invoke-virtual {p0, v2}, Lru/andrey/notepad/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lru/andrey/notepad/BuyActivity;->itemName:Landroid/widget/EditText;

    .line 69
    iget-object v2, p0, Lru/andrey/notepad/BuyActivity;->add:Landroid/widget/Button;

    new-instance v3, Lru/andrey/notepad/BuyActivity$1;

    invoke-direct {v3, p0}, Lru/andrey/notepad/BuyActivity$1;-><init>(Lru/andrey/notepad/BuyActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    invoke-virtual {p0}, Lru/andrey/notepad/BuyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 86
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "new"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lru/andrey/notepad/BuyActivity;->isnew:Z

    .line 87
    iget-boolean v2, p0, Lru/andrey/notepad/BuyActivity;->isnew:Z

    if-nez v2, :cond_0

    .line 89
    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    .line 90
    const-string v2, "color"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lru/andrey/notepad/BuyActivity;->color:I

    .line 91
    const-string v2, "body"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iput-object v2, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    .line 92
    iget-object v2, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v3, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lru/andrey/notepad/DataBaseHelper;->getShops(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    .line 93
    new-instance v2, Lru/andrey/notepad/BoughtArrayAdapter;

    sget-object v3, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v3}, Lru/andrey/notepad/BoughtArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v2, Lru/andrey/notepad/BuyActivity;->arr:Lru/andrey/notepad/BoughtArrayAdapter;

    .line 94
    iget-object v2, p0, Lru/andrey/notepad/BuyActivity;->lv:Landroid/widget/ListView;

    sget-object v3, Lru/andrey/notepad/BuyActivity;->arr:Lru/andrey/notepad/BoughtArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 104
    :goto_0
    iget-object v2, p0, Lru/andrey/notepad/BuyActivity;->lv:Landroid/widget/ListView;

    new-instance v3, Lru/andrey/notepad/BuyActivity$2;

    invoke-direct {v3, p0}, Lru/andrey/notepad/BuyActivity$2;-><init>(Lru/andrey/notepad/BuyActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 114
    return-void

    .line 99
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    .line 100
    new-instance v2, Lru/andrey/notepad/BoughtArrayAdapter;

    sget-object v3, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v3}, Lru/andrey/notepad/BoughtArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v2, Lru/andrey/notepad/BuyActivity;->arr:Lru/andrey/notepad/BoughtArrayAdapter;

    .line 101
    iget-object v2, p0, Lru/andrey/notepad/BuyActivity;->lv:Landroid/widget/ListView;

    sget-object v3, Lru/andrey/notepad/BuyActivity;->arr:Lru/andrey/notepad/BoughtArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 119
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 120
    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 121
    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 122
    const v0, 0x7f050047

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 123
    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 124
    const v0, 0x7f050049

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 125
    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 126
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10
    .param p1, "id"    # I

    .prologue
    const/4 v9, -0x1

    .line 416
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 418
    sget-object v0, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    iget v1, p0, Lru/andrey/notepad/BuyActivity;->lastIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/andrey/notepad/BuyModel;

    .line 419
    .local v3, "bm":Lru/andrey/notepad/BuyModel;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 420
    .local v6, "alert":Landroid/app/AlertDialog$Builder;
    const v0, 0x7f050044

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 421
    invoke-virtual {p0}, Lru/andrey/notepad/BuyActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000d

    const/4 v8, 0x0

    invoke-virtual {v0, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 422
    .local v7, "v":Landroid/view/View;
    const v0, 0x7f070016

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 423
    .local v2, "name":Landroid/widget/EditText;
    invoke-virtual {v3}, Lru/andrey/notepad/BuyModel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 424
    const v0, 0x7f07002d

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 425
    .local v5, "count":Landroid/widget/EditText;
    invoke-virtual {v3}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v0

    if-eq v0, v9, :cond_0

    .line 427
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 429
    :cond_0
    const v0, 0x7f070032

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 430
    .local v4, "price":Landroid/widget/EditText;
    invoke-virtual {v3}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v0

    if-eq v0, v9, :cond_1

    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 434
    :cond_1
    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 435
    const v0, 0x7f050029

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v0, Lru/andrey/notepad/BuyActivity$8;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lru/andrey/notepad/BuyActivity$8;-><init>(Lru/andrey/notepad/BuyActivity;Landroid/widget/EditText;Lru/andrey/notepad/BuyModel;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v6, v8, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 456
    const v0, 0x7f05002a

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lru/andrey/notepad/BuyActivity$9;

    invoke-direct {v1, p0}, Lru/andrey/notepad/BuyActivity$9;-><init>(Lru/andrey/notepad/BuyActivity;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 464
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 466
    .end local v2    # "name":Landroid/widget/EditText;
    .end local v3    # "bm":Lru/andrey/notepad/BuyModel;
    .end local v4    # "price":Landroid/widget/EditText;
    .end local v5    # "count":Landroid/widget/EditText;
    .end local v6    # "alert":Landroid/app/AlertDialog$Builder;
    .end local v7    # "v":Landroid/view/View;
    :goto_0
    return-object v0

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 239
    const/16 v0, 0x64

    const v1, 0x7f05003f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 241
    iget-boolean v0, p0, Lru/andrey/notepad/BuyActivity;->isnew:Z

    if-nez v0, :cond_1

    .line 243
    const/16 v0, 0x66

    const v1, 0x7f050026

    invoke-virtual {p0, v1}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 244
    const/16 v0, 0x68

    const v1, 0x7f050037

    invoke-virtual {p0, v1}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 245
    const/16 v0, 0x69

    const v1, 0x7f05004b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 247
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    const/16 v0, 0x6a

    const v1, 0x7f05005f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 250
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    const/16 v0, 0x6b

    const v1, 0x7f050060

    invoke-virtual {p0, v1}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 255
    :cond_1
    const/16 v0, 0x67

    const v1, 0x7f050027

    invoke-virtual {p0, v1}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 257
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 539
    const/4 v4, 0x4

    if-ne p1, v4, :cond_2

    .line 542
    iget-boolean v4, p0, Lru/andrey/notepad/BuyActivity;->isnew:Z

    if-nez v4, :cond_1

    .line 544
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ShopList/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 545
    .local v3, "value":Ljava/lang/String;
    new-instance v1, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v1}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 546
    .local v1, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v1, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 547
    iget v4, p0, Lru/andrey/notepad/BuyActivity;->num:I

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 548
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 549
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 550
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 551
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v5, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lru/andrey/notepad/DataBaseHelper;->remove(Ljava/lang/String;)V

    .line 552
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v4, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_0

    .line 556
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v1}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 557
    sget-object v4, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 558
    invoke-virtual {p0}, Lru/andrey/notepad/BuyActivity;->finish()V

    .line 565
    .end local v0    # "i":I
    .end local v1    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v3    # "value":Ljava/lang/String;
    :goto_1
    const/4 v4, 0x1

    .line 567
    :goto_2
    return v4

    .line 554
    .restart local v0    # "i":I
    .restart local v1    # "om":Lru/andrey/notepad/ObjectModel;
    .restart local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v3    # "value":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    sget-object v4, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/BuyModel;

    invoke-virtual {v5, v4}, Lru/andrey/notepad/DataBaseHelper;->addBuy(Lru/andrey/notepad/BuyModel;)V

    .line 552
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 563
    .end local v0    # "i":I
    .end local v1    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lru/andrey/notepad/BuyActivity;->showSave()V

    goto :goto_1

    .line 567
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 286
    new-instance v7, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 287
    .local v7, "intent":Landroid/content/Intent;
    const/high16 v11, 0x80000

    invoke-virtual {v7, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 288
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f050029

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 290
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 291
    .local v1, "alert":Landroid/app/AlertDialog$Builder;
    new-instance v6, Landroid/widget/EditText;

    invoke-direct {v6, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 292
    .local v6, "input":Landroid/widget/EditText;
    iget-object v11, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v6, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 293
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 294
    const v11, 0x7f05002b

    invoke-virtual {p0, v11}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 295
    const v11, 0x7f050029

    invoke-virtual {p0, v11}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lru/andrey/notepad/BuyActivity$5;

    invoke-direct {v12, p0, v6}, Lru/andrey/notepad/BuyActivity$5;-><init>(Lru/andrey/notepad/BuyActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 312
    const v11, 0x7f05002a

    invoke-virtual {p0, v11}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lru/andrey/notepad/BuyActivity$6;

    invoke-direct {v12, p0}, Lru/andrey/notepad/BuyActivity$6;-><init>(Lru/andrey/notepad/BuyActivity;)V

    invoke-virtual {v1, v11, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 319
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 396
    .end local v1    # "alert":Landroid/app/AlertDialog$Builder;
    .end local v6    # "input":Landroid/widget/EditText;
    :cond_0
    :goto_0
    const/4 v11, 0x1

    return v11

    .line 321
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f050026

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 323
    iget-object v11, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v12, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lru/andrey/notepad/DataBaseHelper;->removeObject(Ljava/lang/String;)V

    .line 324
    const/4 v11, 0x1

    iput-boolean v11, p0, Lru/andrey/notepad/BuyActivity;->delMode:Z

    .line 325
    invoke-virtual {p0}, Lru/andrey/notepad/BuyActivity;->finish()V

    goto :goto_0

    .line 327
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f05004b

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 329
    invoke-virtual {p0}, Lru/andrey/notepad/BuyActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v11

    invoke-virtual {p0, v11}, Lru/andrey/notepad/BuyActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 330
    invoke-virtual {p0}, Lru/andrey/notepad/BuyActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v11

    invoke-virtual {p0, v11}, Lru/andrey/notepad/BuyActivity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 332
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f050027

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 334
    new-instance v2, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    const-string v12, "market://search?q=pub:Power"

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v2, v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 335
    .local v2, "browserIntent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lru/andrey/notepad/BuyActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 337
    .end local v2    # "browserIntent":Landroid/content/Intent;
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f05003f

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 339
    new-instance v5, Landroid/content/Intent;

    const-class v11, Lru/andrey/notepad/MainActivity;

    invoke-direct {v5, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 340
    .local v5, "in":Landroid/content/Intent;
    invoke-virtual {p0, v5}, Lru/andrey/notepad/BuyActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 342
    .end local v5    # "in":Landroid/content/Intent;
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f050037

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 344
    new-instance v8, Landroid/content/Intent;

    const-class v11, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v8, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 345
    .local v8, "shortcutIntent":Landroid/content/Intent;
    const-string v11, "new"

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 346
    const-string v11, "name"

    iget-object v12, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string v11, "body"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ShopList/"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, p0, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v11, "color"

    iget v12, p0, Lru/andrey/notepad/BuyActivity;->num:I

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 349
    const/high16 v11, 0x10000000

    invoke-virtual {v8, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 350
    const/high16 v11, 0x4000000

    invoke-virtual {v8, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 351
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 352
    .local v0, "addIntent":Landroid/content/Intent;
    const-string v11, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v11, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 353
    const-string v11, "android.intent.extra.shortcut.NAME"

    iget-object v12, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    const-string v11, "android.intent.extra.shortcut.ICON_RESOURCE"

    const-string v12, "icon64basket"

    invoke-static {p0, v12}, Lru/andrey/notepad/BuyActivity;->getDrawable(Landroid/content/Context;Ljava/lang/String;)I

    move-result v12

    invoke-static {p0, v12}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 355
    const-string v11, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-virtual {v0, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 358
    .end local v0    # "addIntent":Landroid/content/Intent;
    .end local v8    # "shortcutIntent":Landroid/content/Intent;
    :cond_6
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f05005f

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 360
    new-instance v4, Landroid/app/Dialog;

    invoke-direct {v4, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 361
    .local v4, "dialog":Landroid/app/Dialog;
    const v11, 0x7f030007

    invoke-virtual {v4, v11}, Landroid/app/Dialog;->setContentView(I)V

    .line 362
    const v11, 0x7f05005f

    invoke-virtual {v4, v11}, Landroid/app/Dialog;->setTitle(I)V

    .line 363
    const/4 v11, 0x0

    invoke-virtual {v4, v11}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 364
    const v11, 0x7f070016

    invoke-virtual {v4, v11}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/EditText;

    .line 365
    .local v9, "value":Landroid/widget/EditText;
    const v11, 0x7f07002d

    invoke-virtual {v4, v11}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    .line 367
    .local v10, "value2":Landroid/widget/EditText;
    const v11, 0x7f070028

    invoke-virtual {v4, v11}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 368
    .local v3, "button":Landroid/widget/Button;
    new-instance v11, Lru/andrey/notepad/BuyActivity$7;

    invoke-direct {v11, p0, v9, v10, v4}, Lru/andrey/notepad/BuyActivity$7;-><init>(Lru/andrey/notepad/BuyActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v3, v11}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 390
    .end local v3    # "button":Landroid/widget/Button;
    .end local v4    # "dialog":Landroid/app/Dialog;
    .end local v9    # "value":Landroid/widget/EditText;
    .end local v10    # "value2":Landroid/widget/EditText;
    :cond_7
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v11

    const v12, 0x7f050060

    invoke-virtual {p0, v12}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    if-ne v11, v12, :cond_0

    .line 392
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v11

    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    iget-object v12, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-interface {v11, v12, v13}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v4, 0x6b

    const/16 v3, 0x6a

    const/4 v2, 0x0

    .line 263
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 264
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 265
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 266
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 269
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/BuyActivity;->listName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 270
    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 271
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 277
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 279
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 281
    return-void
.end method

.method public showSave()V
    .locals 7

    .prologue
    .line 471
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 473
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f050033

    invoke-virtual {p0, v5}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 475
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 477
    .local v3, "linear":Landroid/widget/LinearLayout;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 479
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 481
    .local v2, "edit":Landroid/widget/EditText;
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 484
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 487
    iget-boolean v5, p0, Lru/andrey/notepad/BuyActivity;->isnew:Z

    if-eqz v5, :cond_0

    .line 488
    iget-object v5, p0, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v5}, Lru/andrey/notepad/DataBaseHelper;->getShopCount()I

    move-result v1

    .line 492
    .local v1, "count":I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ShopList "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 493
    .local v4, "names":Ljava/lang/String;
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 495
    const v5, 0x7f050029

    invoke-virtual {p0, v5}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/BuyActivity$10;

    invoke-direct {v6, p0, v1, v2}, Lru/andrey/notepad/BuyActivity$10;-><init>(Lru/andrey/notepad/BuyActivity;ILandroid/widget/EditText;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 519
    const v5, 0x7f05002a

    invoke-virtual {p0, v5}, Lru/andrey/notepad/BuyActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/BuyActivity$11;

    invoke-direct {v6, p0}, Lru/andrey/notepad/BuyActivity$11;-><init>(Lru/andrey/notepad/BuyActivity;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 527
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 528
    return-void

    .line 490
    .end local v1    # "count":I
    .end local v4    # "names":Ljava/lang/String;
    :cond_0
    iget v5, p0, Lru/andrey/notepad/BuyActivity;->num:I

    add-int/lit8 v1, v5, -0x1

    .restart local v1    # "count":I
    goto :goto_0
.end method
