.class public Lru/andrey/notepad/MainActivity$bannerSendTask;
.super Landroid/os/AsyncTask;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "bannerSendTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field responseBody:Ljava/lang/String;

.field text:Ljava/lang/String;

.field final synthetic this$0:Lru/andrey/notepad/MainActivity;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/MainActivity;)V
    .locals 1

    .prologue
    .line 752
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$bannerSendTask;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 755
    const-string v0, ""

    iput-object v0, p0, Lru/andrey/notepad/MainActivity$bannerSendTask;->text:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/MainActivity$bannerSendTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 18
    .param p1, "uri"    # [Ljava/lang/String;

    .prologue
    .line 766
    :try_start_0
    new-instance v13, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v13}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 767
    .local v13, "httpParameters":Lorg/apache/http/params/HttpParams;
    const-string v2, "ISO-8859-1"

    invoke-static {v13, v2}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 768
    const-string v2, "ISO-8859-1"

    invoke-static {v13, v2}, Lorg/apache/http/params/HttpProtocolParams;->setHttpElementCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 770
    new-instance v9, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v9, v13}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 772
    .local v9, "client":Lorg/apache/http/client/HttpClient;
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/andrey/notepad/MainActivity$bannerSendTask;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Lru/andrey/notepad/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/telephony/TelephonyManager;

    .line 773
    .local v15, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v15}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v10

    .line 775
    .local v10, "countryCode":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://deviceapp.org/api_banner/api/RegisterClick?guid=55dd-99gg-rrrr-7777-g788&banner="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerSendTask;->this$0:Lru/andrey/notepad/MainActivity;

    iget-object v3, v3, Lru/andrey/notepad/MainActivity;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&iso="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 777
    .local v17, "urlStr":Ljava/lang/String;
    new-instance v16, Ljava/net/URL;

    invoke-direct/range {v16 .. v17}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 778
    .local v16, "url":Ljava/net/URL;
    new-instance v1, Ljava/net/URI;

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getUserInfo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getPort()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    .local v1, "uris":Ljava/net/URI;
    invoke-virtual {v1}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v16

    .line 781
    new-instance v12, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 782
    .local v12, "get":Lorg/apache/http/client/methods/HttpGet;
    new-instance v14, Lru/andrey/notepad/MainActivity$bannerSendTask$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lru/andrey/notepad/MainActivity$bannerSendTask$1;-><init>(Lru/andrey/notepad/MainActivity$bannerSendTask;)V

    .line 797
    .local v14, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    invoke-interface {v9, v12, v14}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lru/andrey/notepad/MainActivity$bannerSendTask;->responseBody:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 805
    const-string v2, "suc"

    .end local v1    # "uris":Ljava/net/URI;
    .end local v9    # "client":Lorg/apache/http/client/HttpClient;
    .end local v10    # "countryCode":Ljava/lang/String;
    .end local v12    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v13    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v14    # "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    .end local v15    # "tm":Landroid/telephony/TelephonyManager;
    .end local v16    # "url":Ljava/net/URL;
    .end local v17    # "urlStr":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 800
    :catch_0
    move-exception v11

    .line 802
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 803
    const-string v2, "err"

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/MainActivity$bannerSendTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 812
    const-string v0, "err"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 817
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 759
    return-void
.end method
