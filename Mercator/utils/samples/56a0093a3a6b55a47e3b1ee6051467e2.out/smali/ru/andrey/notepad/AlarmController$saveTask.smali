.class Lru/andrey/notepad/AlarmController$saveTask;
.super Landroid/os/AsyncTask;
.source "AlarmController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/AlarmController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "saveTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/AlarmController;


# direct methods
.method constructor <init>(Lru/andrey/notepad/AlarmController;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/AlarmController$saveTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 19
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 118
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    const-string v16, "/NotepadBackup"

    invoke-virtual/range {v15 .. v16}, Lru/andrey/notepad/AlarmController;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 119
    new-instance v9, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/NotepadBackup/database.sql"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v9, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v9, "logFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 121
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v2, "buy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/BuyModel;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .local v8, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v13, "remind":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    iget-object v15, v15, Lru/andrey/notepad/AlarmController;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v15}, Lru/andrey/notepad/DataBaseHelper;->getBuy()Ljava/util/ArrayList;

    move-result-object v2

    .line 125
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    iget-object v15, v15, Lru/andrey/notepad/AlarmController;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v15}, Lru/andrey/notepad/DataBaseHelper;->getNotes()Ljava/util/ArrayList;

    move-result-object v8

    .line 126
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    iget-object v15, v15, Lru/andrey/notepad/AlarmController;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v15}, Lru/andrey/notepad/DataBaseHelper;->getReminds()Ljava/util/ArrayList;

    move-result-object v13

    .line 127
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-lt v7, v15, :cond_1

    .line 133
    const/4 v7, 0x0

    :goto_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-lt v7, v15, :cond_2

    .line 143
    const/4 v7, 0x0

    :goto_2
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-lt v7, v15, :cond_3

    .line 153
    new-instance v11, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/Notepad"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 154
    .local v11, "mdir":Ljava/io/File;
    new-instance v14, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/NotepadBackup/data.zip"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    .local v14, "zipto":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_0

    .line 160
    :try_start_0
    invoke-virtual {v14}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :cond_0
    :goto_3
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 170
    .local v6, "files1":[Ljava/io/File;
    array-length v15, v6

    new-array v5, v15, [Ljava/lang/String;

    .line 171
    .local v5, "files":[Ljava/lang/String;
    const/4 v7, 0x0

    :goto_4
    array-length v15, v6

    if-lt v7, v15, :cond_4

    .line 175
    new-instance v3, Lru/andrey/notepad/Compress;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v3, v5, v15}, Lru/andrey/notepad/Compress;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .local v3, "c":Lru/andrey/notepad/Compress;
    invoke-virtual {v3}, Lru/andrey/notepad/Compress;->zip()V

    .line 177
    const/4 v15, 0x0

    return-object v15

    .line 129
    .end local v3    # "c":Lru/andrey/notepad/Compress;
    .end local v5    # "files":[Ljava/lang/String;
    .end local v6    # "files1":[Ljava/io/File;
    .end local v11    # "mdir":Ljava/io/File;
    .end local v14    # "zipto":Ljava/io/File;
    :cond_1
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/andrey/notepad/BuyModel;

    .line 130
    .local v10, "m":Lru/andrey/notepad/BuyModel;
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "REPLACE INTO Buy(`listId`, `name`, `price`, `quantity`,`pos`,`isDone`) VALUES (\'"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lru/andrey/notepad/BuyModel;->getListId()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/BuyModel;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/BuyModel;->getPos()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/BuyModel;->getisDone()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\');"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lru/andrey/notepad/AlarmController;->appendSQLLog(Ljava/lang/String;)V

    .line 127
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 135
    .end local v10    # "m":Lru/andrey/notepad/BuyModel;
    :cond_2
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/andrey/notepad/ObjectModel;

    .line 136
    .local v10, "m":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "body":Ljava/lang/String;
    const-string v15, "\'"

    const-string v16, "\'\'"

    move-object/from16 v0, v16

    invoke-virtual {v1, v15, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v12

    .line 139
    .local v12, "name":Ljava/lang/String;
    const-string v15, "\'"

    const-string v16, "\'\'"

    move-object/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 140
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "REPLACE INTO Notes(`name`, `date`, `body`, `color`) VALUES (\'"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\');"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lru/andrey/notepad/AlarmController;->appendSQLLog(Ljava/lang/String;)V

    .line 133
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 145
    .end local v1    # "body":Ljava/lang/String;
    .end local v10    # "m":Lru/andrey/notepad/ObjectModel;
    .end local v12    # "name":Ljava/lang/String;
    :cond_3
    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/andrey/notepad/ObjectModel;

    .line 146
    .restart local v10    # "m":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    .line 147
    .restart local v1    # "body":Ljava/lang/String;
    const-string v15, "\'"

    const-string v16, "\'\'"

    move-object/from16 v0, v16

    invoke-virtual {v1, v15, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v12

    .line 149
    .restart local v12    # "name":Ljava/lang/String;
    const-string v15, "\'"

    const-string v16, "\'\'"

    move-object/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 150
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "REPLACE INTO Remind(`whentime`, `name`, `date`, `body`, `color`) VALUES (\'"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getWhen()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\',\'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v10}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\');"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lru/andrey/notepad/AlarmController;->appendSQLLog(Ljava/lang/String;)V

    .line 143
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 162
    .end local v1    # "body":Ljava/lang/String;
    .end local v10    # "m":Lru/andrey/notepad/ObjectModel;
    .end local v12    # "name":Ljava/lang/String;
    .restart local v11    # "mdir":Ljava/io/File;
    .restart local v14    # "zipto":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 165
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 173
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v5    # "files":[Ljava/lang/String;
    .restart local v6    # "files1":[Ljava/io/File;
    :cond_4
    aget-object v15, v6, v7

    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v7

    .line 171
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/AlarmController$saveTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 183
    iget-object v0, p0, Lru/andrey/notepad/AlarmController$saveTask;->this$0:Lru/andrey/notepad/AlarmController;

    invoke-virtual {v0}, Lru/andrey/notepad/AlarmController;->notifyBackup()V

    .line 184
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method
