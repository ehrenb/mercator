.class Lru/andrey/notepad/FoldersArrayAdapter$2$1;
.super Ljava/lang/Object;
.source "FoldersArrayAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FoldersArrayAdapter$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FoldersArrayAdapter$2;

.field private final synthetic val$edit:Landroid/widget/EditText;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/FoldersArrayAdapter$2;Landroid/widget/EditText;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FoldersArrayAdapter$2$1;->this$1:Lru/andrey/notepad/FoldersArrayAdapter$2;

    iput-object p2, p0, Lru/andrey/notepad/FoldersArrayAdapter$2$1;->val$edit:Landroid/widget/EditText;

    iput p3, p0, Lru/andrey/notepad/FoldersArrayAdapter$2$1;->val$position:I

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 137
    iget-object v2, p0, Lru/andrey/notepad/FoldersArrayAdapter$2$1;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    .local v1, "newname":Ljava/lang/String;
    iget-object v2, p0, Lru/andrey/notepad/FoldersArrayAdapter$2$1;->this$1:Lru/andrey/notepad/FoldersArrayAdapter$2;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/FoldersArrayAdapter$2;->access$0(Lru/andrey/notepad/FoldersArrayAdapter$2;)Lru/andrey/notepad/FoldersArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 139
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v2, p0, Lru/andrey/notepad/FoldersArrayAdapter$2$1;->this$1:Lru/andrey/notepad/FoldersArrayAdapter$2;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/FoldersArrayAdapter$2;->access$0(Lru/andrey/notepad/FoldersArrayAdapter$2;)Lru/andrey/notepad/FoldersArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/FoldersArrayAdapter;->access$1(Lru/andrey/notepad/FoldersArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lru/andrey/notepad/FoldersArrayAdapter$2$1;->val$position:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lru/andrey/notepad/DataBaseHelper;->updateFolder(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-static {}, Lru/andrey/notepad/FolderActivity;->update()V

    .line 142
    return-void
.end method
