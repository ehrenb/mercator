.class Lru/andrey/notepad/MainActivity$7;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/MainActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/MainActivity$7;)Lru/andrey/notepad/MainActivity;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 491
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 493
    new-instance v1, Landroid/app/Dialog;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-direct {v1, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 494
    .local v1, "dialog":Landroid/app/Dialog;
    const v4, 0x7f030006

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 495
    const v4, 0x7f050066

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setTitle(I)V

    .line 496
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 497
    const v4, 0x7f070016

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 498
    .local v3, "value":Landroid/widget/EditText;
    const v4, 0x7f070028

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 499
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lru/andrey/notepad/MainActivity$7$1;

    invoke-direct {v4, p0, v3, p3, v1}, Lru/andrey/notepad/MainActivity$7$1;-><init>(Lru/andrey/notepad/MainActivity$7;Landroid/widget/EditText;ILandroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 663
    .end local v0    # "button":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v3    # "value":Landroid/widget/EditText;
    :goto_0
    return-void

    .line 590
    :cond_0
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawing"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 592
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 593
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 594
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 595
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 596
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 597
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 599
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Camera"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 601
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 602
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 603
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 604
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 605
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 606
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 608
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PhotoText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 610
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 611
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 612
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 613
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 614
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 615
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 617
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GalleryText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 619
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 620
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 621
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 623
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 624
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 626
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Record"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 628
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 629
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 630
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 631
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 632
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 633
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 635
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Shop"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 637
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 638
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 639
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 640
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 641
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 642
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 644
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Video"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 646
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 647
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 648
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 649
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 650
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 651
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 655
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/AddActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 656
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 657
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 658
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 659
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 660
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$7;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
