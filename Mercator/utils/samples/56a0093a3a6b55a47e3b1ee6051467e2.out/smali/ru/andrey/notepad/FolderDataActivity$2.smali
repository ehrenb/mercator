.class Lru/andrey/notepad/FolderDataActivity$2;
.super Ljava/lang/Object;
.source "FolderDataActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderDataActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/FolderDataActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 212
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 216
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 221
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity;->search:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 223
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;

    iget-object v1, v1, Lru/andrey/notepad/FolderDataActivity;->search:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/FolderDataActivity;->find(Ljava/lang/String;)V

    .line 224
    const/4 v0, 0x1

    sput-boolean v0, Lru/andrey/notepad/FolderDataActivity;->isSearch:Z

    .line 412
    :goto_0
    return-void

    .line 228
    :cond_0
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/andrey/notepad/DataBaseHelper;->getFolderData(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    .line 229
    new-instance v0, Lru/andrey/notepad/FolderObjectArrayAdapter;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/FolderObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    .line 230
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 231
    const/4 v0, 0x0

    sput-boolean v0, Lru/andrey/notepad/FolderDataActivity;->isSearch:Z

    .line 232
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    new-instance v1, Lru/andrey/notepad/FolderDataActivity$2$1;

    invoke-direct {v1, p0}, Lru/andrey/notepad/FolderDataActivity$2$1;-><init>(Lru/andrey/notepad/FolderDataActivity$2;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method
