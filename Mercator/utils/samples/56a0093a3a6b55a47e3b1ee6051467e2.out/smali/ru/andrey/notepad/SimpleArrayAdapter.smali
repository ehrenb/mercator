.class public Lru/andrey/notepad/SimpleArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SimpleArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lru/andrey/notepad/ObjectModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Activity;

.field private final obj:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "obj":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    const v0, 0x7f030017

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 20
    iput-object p1, p0, Lru/andrey/notepad/SimpleArrayAdapter;->context:Landroid/app/Activity;

    .line 21
    iput-object p2, p0, Lru/andrey/notepad/SimpleArrayAdapter;->obj:Ljava/util/ArrayList;

    .line 22
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 33
    move-object v2, p2

    .line 34
    .local v2, "rowView":Landroid/view/View;
    if-nez v2, :cond_0

    .line 36
    iget-object v3, p0, Lru/andrey/notepad/SimpleArrayAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 37
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030017

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 38
    new-instance v0, Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;

    invoke-direct {v0}, Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;-><init>()V

    .line 39
    .local v0, "holder":Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;
    const v3, 0x7f070037

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 40
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 47
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v4, v0, Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, p0, Lru/andrey/notepad/SimpleArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v3}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    return-object v2

    .line 44
    .end local v0    # "holder":Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;

    .restart local v0    # "holder":Lru/andrey/notepad/SimpleArrayAdapter$ViewHolder;
    goto :goto_0
.end method
