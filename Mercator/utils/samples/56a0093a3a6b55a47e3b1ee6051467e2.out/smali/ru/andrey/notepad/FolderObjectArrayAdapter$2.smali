.class Lru/andrey/notepad/FolderObjectArrayAdapter$2;
.super Ljava/lang/Object;
.source "FolderObjectArrayAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderObjectArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderObjectArrayAdapter;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    iput p2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->val$position:I

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/FolderObjectArrayAdapter$2;)Lru/andrey/notepad/FolderObjectArrayAdapter;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 153
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 154
    .local v0, "builder3":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f050026

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 155
    iget-object v1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f050036

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 156
    const-string v1, "\u041e\u041a"

    new-instance v2, Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;

    iget v3, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->val$position:I

    invoke-direct {v2, p0, v3}, Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;-><init>(Lru/andrey/notepad/FolderObjectArrayAdapter$2;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 165
    iget-object v1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f05002a

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lru/andrey/notepad/FolderObjectArrayAdapter$2$2;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderObjectArrayAdapter$2$2;-><init>(Lru/andrey/notepad/FolderObjectArrayAdapter$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 172
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 173
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 174
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 175
    return-void
.end method
