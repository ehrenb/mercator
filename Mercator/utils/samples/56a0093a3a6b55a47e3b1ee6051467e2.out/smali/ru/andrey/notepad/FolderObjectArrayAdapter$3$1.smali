.class Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;
.super Ljava/lang/Object;
.source "FolderObjectArrayAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderObjectArrayAdapter$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$3;

.field private final synthetic val$edit:Landroid/widget/EditText;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderObjectArrayAdapter$3;Landroid/widget/EditText;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$3;

    iput-object p2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;->val$edit:Landroid/widget/EditText;

    iput p3, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;->val$position:I

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 213
    iget-object v2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 214
    .local v1, "newname":Ljava/lang/String;
    iget-object v2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/FolderObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter$3;)Lru/andrey/notepad/FolderObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 215
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/FolderObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter$3;)Lru/andrey/notepad/FolderObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$1(Lru/andrey/notepad/FolderObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$3$1;->val$position:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lru/andrey/notepad/DataBaseHelper;->updateFolderData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-static {}, Lru/andrey/notepad/FolderDataActivity;->update()V

    .line 217
    return-void
.end method
