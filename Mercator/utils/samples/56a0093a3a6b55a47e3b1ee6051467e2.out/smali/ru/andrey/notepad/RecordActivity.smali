.class public Lru/andrey/notepad/RecordActivity;
.super Landroid/app/Activity;
.source "RecordActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/RecordActivity$saveTask;
    }
.end annotation


# static fields
.field private static final BGCOLOR_MENU_ID:I = 0xf

.field private static final DELETE_MENU_ID:I = 0x5

.field private static final HOME_MENU_ID:I = 0xb

.field private static final IDD_LOAD_PROGRESS:I = 0x1

.field private static final MORE_MENU_ID:I = 0x7

.field private static final PASS_MENU_ID:I = 0xd

.field private static final REMIND_MENU_ID:I = 0xc

.field private static final SHARE_MENU_ID:I = 0x9

.field private static final SHORTCUT_MENU_ID:I = 0xa

.field private static final UNPASS_MENU_ID:I = 0xe


# instance fields
.field LoadProgress:Landroid/app/ProgressDialog;

.field audioTmp:Ljava/io/File;

.field bg:Landroid/graphics/Bitmap;

.field bgcolor:Ljava/lang/String;

.field bmp:Landroid/graphics/Bitmap;

.field count:I

.field delMode:Z

.field dh:Lru/andrey/notepad/DataBaseHelper;

.field erase:Z

.field inPlaying:Z

.field inRecording:Z

.field isnew:Z

.field m1:Landroid/media/MediaRecorder;

.field m2:Landroid/media/MediaPlayer;

.field name:Ljava/lang/String;

.field num:I

.field rec:Landroid/widget/Button;

.field start:Landroid/widget/Button;

.field stop:Landroid/widget/Button;

.field text:Landroid/widget/EditText;

.field time:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    iput-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->erase:Z

    .line 48
    iput-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    .line 53
    iput-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->delMode:Z

    .line 55
    iput v0, p0, Lru/andrey/notepad/RecordActivity;->count:I

    .line 64
    iput-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    .line 65
    iput-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    .line 66
    iput-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->inRecording:Z

    .line 67
    iput-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->inPlaying:Z

    .line 68
    const-string v0, "#dad07f"

    iput-object v0, p0, Lru/andrey/notepad/RecordActivity;->bgcolor:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public static getDrawable(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 601
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 602
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 604
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public checkAndCreateDirectory(Ljava/lang/String;)V
    .locals 3
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 129
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 130
    .local v0, "new_dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 134
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 625
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 626
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v11, 0x7f05004a

    const/4 v8, 0x1

    .line 152
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050046

    invoke-virtual {p0, v10}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_1

    .line 154
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 155
    .local v6, "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_0

    .line 156
    const-string v6, " "

    .line 158
    :cond_0
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 159
    .local v3, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Record/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 160
    iget v9, p0, Lru/andrey/notepad/RecordActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 161
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 162
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 163
    .local v5, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 164
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x493e0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 165
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 267
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :goto_0
    return v8

    .line 167
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050047

    invoke-virtual {p0, v10}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_3

    .line 169
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 170
    .restart local v6    # "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_2

    .line 171
    const-string v6, " "

    .line 173
    :cond_2
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 174
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Record/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 175
    iget v9, p0, Lru/andrey/notepad/RecordActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 176
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 177
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 178
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 179
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0xdbba0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 180
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto :goto_0

    .line 182
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050048

    invoke-virtual {p0, v10}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_5

    .line 184
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 185
    .restart local v6    # "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_4

    .line 186
    const-string v6, " "

    .line 188
    :cond_4
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 189
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Record/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 190
    iget v9, p0, Lru/andrey/notepad/RecordActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 191
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 192
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 193
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 194
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x16e360

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 195
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 197
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050049

    invoke-virtual {p0, v10}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_7

    .line 199
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 200
    .restart local v6    # "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_6

    .line 201
    const-string v6, " "

    .line 203
    :cond_6
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 204
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Record/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 205
    iget v9, p0, Lru/andrey/notepad/RecordActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 206
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 207
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 208
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x36ee80

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 210
    iget-object v9, p0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 212
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :cond_7
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {p0, v11}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_8

    .line 214
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 215
    .local v1, "dialog":Landroid/app/Dialog;
    const v9, 0x7f030016

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setContentView(I)V

    .line 216
    invoke-virtual {v1, v11}, Landroid/app/Dialog;->setTitle(I)V

    .line 217
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 218
    const v9, 0x7f070044

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/DatePicker;

    .line 219
    .local v2, "dp":Landroid/widget/DatePicker;
    const v9, 0x7f070045

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TimePicker;

    .line 220
    .local v7, "tp":Landroid/widget/TimePicker;
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 221
    const v9, 0x7f070028

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 222
    .local v4, "save":Landroid/widget/Button;
    const v9, 0x7f070019

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 224
    .local v0, "cancel":Landroid/widget/Button;
    new-instance v9, Lru/andrey/notepad/RecordActivity$3;

    invoke-direct {v9, p0, v2, v7, v1}, Lru/andrey/notepad/RecordActivity$3;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    new-instance v9, Lru/andrey/notepad/RecordActivity$4;

    invoke-direct {v9, p0, v1}, Lru/andrey/notepad/RecordActivity$4;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 265
    .end local v0    # "cancel":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v2    # "dp":Landroid/widget/DatePicker;
    .end local v4    # "save":Landroid/widget/Button;
    .end local v7    # "tp":Landroid/widget/TimePicker;
    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const v2, 0x7f030010

    invoke-virtual {p0, v2}, Lru/andrey/notepad/RecordActivity;->setContentView(I)V

    .line 75
    const v2, 0x7f070015

    invoke-virtual {p0, v2}, Lru/andrey/notepad/RecordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/AdView;

    .line 76
    .local v1, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 77
    const v2, 0x7f070016

    invoke-virtual {p0, v2}, Lru/andrey/notepad/RecordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    .line 78
    const v2, 0x7f070028

    invoke-virtual {p0, v2}, Lru/andrey/notepad/RecordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->rec:Landroid/widget/Button;

    .line 79
    const v2, 0x7f070019

    invoke-virtual {p0, v2}, Lru/andrey/notepad/RecordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->start:Landroid/widget/Button;

    .line 81
    invoke-virtual {p0}, Lru/andrey/notepad/RecordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "new"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    .line 83
    iget-boolean v2, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v2, :cond_0

    .line 85
    const-string v2, "color"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lru/andrey/notepad/RecordActivity;->num:I

    .line 87
    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    .line 88
    iget-object v2, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    const-string v3, "body"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 89
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "notecolor"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "#dad07f"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->bgcolor:Ljava/lang/String;

    .line 90
    iget-object v2, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "notecolor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "#dad07f"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setBackgroundColor(I)V

    .line 91
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Notepad/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".3gp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->audioTmp:Ljava/io/File;

    .line 99
    :goto_0
    const-string v2, "Test"

    iget-object v3, p0, Lru/andrey/notepad/RecordActivity;->audioTmp:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v2, p0, Lru/andrey/notepad/RecordActivity;->rec:Landroid/widget/Button;

    new-instance v3, Lru/andrey/notepad/RecordActivity$1;

    invoke-direct {v3, p0}, Lru/andrey/notepad/RecordActivity$1;-><init>(Lru/andrey/notepad/RecordActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v2, p0, Lru/andrey/notepad/RecordActivity;->start:Landroid/widget/Button;

    new-instance v3, Lru/andrey/notepad/RecordActivity$2;

    invoke-direct {v3, p0}, Lru/andrey/notepad/RecordActivity$2;-><init>(Lru/andrey/notepad/RecordActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    invoke-static {p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 123
    const-string v2, "/Notepad"

    invoke-virtual {p0, v2}, Lru/andrey/notepad/RecordActivity;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 125
    return-void

    .line 95
    :cond_0
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Notepad/temp.3gp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lru/andrey/notepad/RecordActivity;->audioTmp:Ljava/io/File;

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 139
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 140
    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 141
    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 142
    const v0, 0x7f050047

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 143
    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 144
    const v0, 0x7f050049

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 145
    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 146
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 760
    packed-switch p1, :pswitch_data_0

    .line 770
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 763
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/andrey/notepad/RecordActivity;->LoadProgress:Landroid/app/ProgressDialog;

    .line 764
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->LoadProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 765
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->LoadProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f05003b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 766
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->LoadProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f05003e

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 767
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->LoadProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 768
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->LoadProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 760
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v7, 0x37

    const/16 v6, 0x36

    const/16 v5, 0x34

    const/16 v4, 0x64

    const/4 v3, 0x0

    .line 366
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 368
    const/16 v0, 0xb

    const v1, 0x7f05003f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x31

    const/16 v2, 0x68

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 370
    iget-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v0, :cond_0

    .line 371
    const/4 v0, 0x5

    const v1, 0x7f050026

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 374
    :cond_0
    const/16 v0, 0x9

    const v1, 0x7f050025

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x6d

    invoke-interface {v0, v6, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 375
    iget-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v0, :cond_1

    .line 376
    const/16 v0, 0xa

    const v1, 0x7f050037

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v7, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 377
    :cond_1
    iget-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v0, :cond_2

    .line 378
    const/16 v0, 0xc

    const v1, 0x7f05004b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v7, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 380
    :cond_2
    iget-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v0, :cond_3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 381
    const/16 v0, 0xd

    const v1, 0x7f05005f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 382
    :cond_3
    iget-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v0, :cond_4

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 383
    const/16 v0, 0xe

    const v1, 0x7f050060

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 385
    :cond_4
    const/16 v0, 0xf

    const v1, 0x7f050065

    invoke-virtual {p0, v1}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x6d

    invoke-interface {v0, v6, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 387
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 689
    const/4 v3, 0x4

    if-ne p1, v3, :cond_2

    .line 692
    iget-boolean v3, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v3, :cond_1

    .line 694
    iget-object v3, p0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 695
    .local v2, "t":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 696
    const-string v2, " "

    .line 698
    :cond_0
    new-instance v0, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v0}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 699
    .local v0, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Record/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 700
    iget v3, p0, Lru/andrey/notepad/RecordActivity;->num:I

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 701
    iget-object v3, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 702
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm MM-dd-yyyy"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 703
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 704
    iget-object v3, p0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v3, v0}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 705
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "notecolor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity;->bgcolor:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 707
    new-instance v3, Lru/andrey/notepad/RecordActivity$saveTask;

    invoke-direct {v3, p0}, Lru/andrey/notepad/RecordActivity$saveTask;-><init>(Lru/andrey/notepad/RecordActivity;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lru/andrey/notepad/RecordActivity$saveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 708
    invoke-virtual {p0}, Lru/andrey/notepad/RecordActivity;->finish()V

    .line 715
    .end local v0    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v1    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v2    # "t":Ljava/lang/String;
    :goto_0
    const/4 v3, 0x1

    .line 717
    :goto_1
    return v3

    .line 713
    :cond_1
    invoke-virtual {p0}, Lru/andrey/notepad/RecordActivity;->showSave()V

    goto :goto_0

    .line 717
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 26
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 407
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v23

    packed-switch v23, :pswitch_data_0

    .line 596
    :pswitch_0
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v23

    :goto_0
    return v23

    .line 411
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lru/andrey/notepad/DataBaseHelper;->removeObject(Ljava/lang/String;)V

    .line 412
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lru/andrey/notepad/RecordActivity;->delMode:Z

    .line 413
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/RecordActivity;->finish()V

    .line 414
    const/16 v23, 0x1

    goto :goto_0

    .line 416
    :pswitch_2
    new-instance v6, Landroid/content/Intent;

    const-string v23, "android.intent.action.VIEW"

    const-string v24, "market://search?q=pub:Power"

    invoke-static/range {v24 .. v24}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 417
    .local v6, "browserIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lru/andrey/notepad/RecordActivity;->startActivity(Landroid/content/Intent;)V

    .line 418
    const/16 v23, 0x1

    goto :goto_0

    .line 420
    .end local v6    # "browserIntent":Landroid/content/Intent;
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/RecordActivity;->share()V

    .line 421
    const/16 v23, 0x1

    goto :goto_0

    .line 423
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/RecordActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lru/andrey/notepad/RecordActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 424
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/RecordActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lru/andrey/notepad/RecordActivity;->openContextMenu(Landroid/view/View;)V

    .line 425
    const/16 v23, 0x1

    goto :goto_0

    .line 427
    :pswitch_5
    new-instance v20, Landroid/content/Intent;

    const-class v23, Lru/andrey/notepad/RecordActivity;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 428
    .local v20, "shortcutIntent":Landroid/content/Intent;
    const-string v23, "new"

    const/16 v24, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 429
    const-string v23, "name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 430
    const-string v23, "body"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "Record/"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 431
    const-string v23, "color"

    move-object/from16 v0, p0

    iget v0, v0, Lru/andrey/notepad/RecordActivity;->num:I

    move/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 432
    const/high16 v23, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 433
    const/high16 v23, 0x4000000

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 434
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 435
    .local v5, "addIntent":Landroid/content/Intent;
    const-string v23, "android.intent.extra.shortcut.INTENT"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 436
    const-string v23, "android.intent.extra.shortcut.NAME"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    const-string v23, "android.intent.extra.shortcut.ICON_RESOURCE"

    const-string v24, "icon64record"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lru/andrey/notepad/RecordActivity;->getDrawable(Landroid/content/Context;Ljava/lang/String;)I

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 438
    const-string v23, "com.android.launcher.action.INSTALL_SHORTCUT"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lru/andrey/notepad/RecordActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 440
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 442
    .end local v5    # "addIntent":Landroid/content/Intent;
    .end local v20    # "shortcutIntent":Landroid/content/Intent;
    :pswitch_6
    new-instance v17, Landroid/app/Dialog;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 443
    .local v17, "dialog":Landroid/app/Dialog;
    const v23, 0x7f030015

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 444
    const v23, 0x7f050065

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 445
    const/16 v23, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 446
    const v23, 0x7f070028

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 447
    .local v7, "btn1":Landroid/widget/Button;
    const v23, 0x7f070019

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 448
    .local v8, "btn2":Landroid/widget/Button;
    const v23, 0x7f07001a

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    .line 449
    .local v9, "btn3":Landroid/widget/Button;
    const v23, 0x7f07001b

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 450
    .local v10, "btn4":Landroid/widget/Button;
    const v23, 0x7f07001c

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    .line 451
    .local v11, "btn5":Landroid/widget/Button;
    const v23, 0x7f07003f

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    .line 452
    .local v12, "btn6":Landroid/widget/Button;
    const v23, 0x7f070041

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    .line 453
    .local v13, "btn7":Landroid/widget/Button;
    const v23, 0x7f070042

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    .line 454
    .local v14, "btn8":Landroid/widget/Button;
    const v23, 0x7f070043

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/Button;

    .line 456
    .local v15, "btn9":Landroid/widget/Button;
    new-instance v23, Lru/andrey/notepad/RecordActivity$6;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$6;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 467
    new-instance v23, Lru/andrey/notepad/RecordActivity$7;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$7;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 478
    new-instance v23, Lru/andrey/notepad/RecordActivity$8;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$8;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    new-instance v23, Lru/andrey/notepad/RecordActivity$9;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$9;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 500
    new-instance v23, Lru/andrey/notepad/RecordActivity$10;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$10;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 511
    new-instance v23, Lru/andrey/notepad/RecordActivity$11;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$11;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 522
    new-instance v23, Lru/andrey/notepad/RecordActivity$12;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$12;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 533
    new-instance v23, Lru/andrey/notepad/RecordActivity$13;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$13;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544
    new-instance v23, Lru/andrey/notepad/RecordActivity$14;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/RecordActivity$14;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 554
    invoke-virtual/range {v17 .. v17}, Landroid/app/Dialog;->show()V

    .line 555
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 558
    .end local v7    # "btn1":Landroid/widget/Button;
    .end local v8    # "btn2":Landroid/widget/Button;
    .end local v9    # "btn3":Landroid/widget/Button;
    .end local v10    # "btn4":Landroid/widget/Button;
    .end local v11    # "btn5":Landroid/widget/Button;
    .end local v12    # "btn6":Landroid/widget/Button;
    .end local v13    # "btn7":Landroid/widget/Button;
    .end local v14    # "btn8":Landroid/widget/Button;
    .end local v15    # "btn9":Landroid/widget/Button;
    .end local v17    # "dialog":Landroid/app/Dialog;
    :pswitch_7
    new-instance v18, Landroid/app/Dialog;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 559
    .local v18, "dialog1":Landroid/app/Dialog;
    const v23, 0x7f030007

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 560
    const v23, 0x7f05005f

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 561
    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 562
    const v23, 0x7f070016

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/EditText;

    .line 563
    .local v21, "value":Landroid/widget/EditText;
    const v23, 0x7f07002d

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/EditText;

    .line 565
    .local v22, "value2":Landroid/widget/EditText;
    const v23, 0x7f070028

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    .line 566
    .local v16, "button":Landroid/widget/Button;
    new-instance v23, Lru/andrey/notepad/RecordActivity$15;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move-object/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lru/andrey/notepad/RecordActivity$15;-><init>(Lru/andrey/notepad/RecordActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/app/Dialog;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586
    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->show()V

    .line 587
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 589
    .end local v16    # "button":Landroid/widget/Button;
    .end local v18    # "dialog1":Landroid/app/Dialog;
    .end local v21    # "value":Landroid/widget/EditText;
    .end local v22    # "value2":Landroid/widget/EditText;
    :pswitch_8
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-interface/range {v23 .. v25}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 590
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 592
    :pswitch_9
    new-instance v19, Landroid/content/Intent;

    const-class v23, Lru/andrey/notepad/MainActivity;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 593
    .local v19, "in":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lru/andrey/notepad/RecordActivity;->startActivity(Landroid/content/Intent;)V

    .line 594
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 407
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_9
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_6
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v6, 0x64

    const/16 v5, 0x34

    const/16 v4, 0xe

    const/16 v3, 0xd

    const/4 v2, 0x0

    .line 393
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 394
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 395
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 396
    iget-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 398
    :cond_0
    iget-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 400
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 777
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 778
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 780
    return-void
.end method

.method public playaudio()V
    .locals 3

    .prologue
    .line 319
    iget-boolean v1, p0, Lru/andrey/notepad/RecordActivity;->inPlaying:Z

    if-nez v1, :cond_0

    .line 323
    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    .line 324
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lru/andrey/notepad/RecordActivity;->audioTmp:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 325
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 326
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 327
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/andrey/notepad/RecordActivity;->inPlaying:Z

    .line 328
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->start:Landroid/widget/Button;

    const v2, 0x7f020042

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 329
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    new-instance v2, Lru/andrey/notepad/RecordActivity$5;

    invoke-direct {v2, p0}, Lru/andrey/notepad/RecordActivity$5;-><init>(Lru/andrey/notepad/RecordActivity;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :goto_0
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "ERROR in playaudio FUNCTION"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 347
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    .line 348
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->start:Landroid/widget/Button;

    const v2, 0x7f020046

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 349
    const/4 v1, 0x0

    iput-boolean v1, p0, Lru/andrey/notepad/RecordActivity;->inPlaying:Z

    goto :goto_0
.end method

.method public share()V
    .locals 7

    .prologue
    .line 609
    const-string v4, "MyRecord.3gp"

    .line 610
    .local v4, "value":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Notepad"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 611
    .local v1, "path":Ljava/lang/String;
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->audioTmp:Ljava/io/File;

    .line 612
    .local v0, "from":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Notepad/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 613
    .local v3, "to":Ljava/io/File;
    invoke-virtual {v0, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 615
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 616
    .local v2, "share":Landroid/content/Intent;
    const-string v5, "image/png"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 617
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 618
    const-string v5, "Share Image"

    invoke-static {v2, v5}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lru/andrey/notepad/RecordActivity;->startActivity(Landroid/content/Intent;)V

    .line 620
    return-void
.end method

.method public showSave()V
    .locals 7

    .prologue
    .line 630
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 632
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f050033

    invoke-virtual {p0, v5}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 634
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 636
    .local v3, "linear":Landroid/widget/LinearLayout;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 638
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 640
    .local v2, "edit":Landroid/widget/EditText;
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 643
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 646
    iget-boolean v5, p0, Lru/andrey/notepad/RecordActivity;->isnew:Z

    if-eqz v5, :cond_0

    .line 647
    iget-object v5, p0, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v5}, Lru/andrey/notepad/DataBaseHelper;->getRecordsCount()I

    move-result v1

    .line 651
    .local v1, "count":I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Record "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 652
    .local v4, "names":Ljava/lang/String;
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 654
    const v5, 0x7f050029

    invoke-virtual {p0, v5}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/RecordActivity$16;

    invoke-direct {v6, p0, v1, v2}, Lru/andrey/notepad/RecordActivity$16;-><init>(Lru/andrey/notepad/RecordActivity;ILandroid/widget/EditText;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 675
    const v5, 0x7f05002a

    invoke-virtual {p0, v5}, Lru/andrey/notepad/RecordActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/RecordActivity$17;

    invoke-direct {v6, p0}, Lru/andrey/notepad/RecordActivity$17;-><init>(Lru/andrey/notepad/RecordActivity;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 683
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 684
    return-void

    .line 649
    .end local v1    # "count":I
    .end local v4    # "names":Ljava/lang/String;
    :cond_0
    iget v5, p0, Lru/andrey/notepad/RecordActivity;->num:I

    add-int/lit8 v1, v5, -0x1

    .restart local v1    # "count":I
    goto :goto_0
.end method

.method public startrecording()V
    .locals 3

    .prologue
    .line 274
    :try_start_0
    new-instance v1, Landroid/media/MediaRecorder;

    invoke-direct {v1}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    .line 275
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 276
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 277
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    iget-object v2, p0, Lru/andrey/notepad/RecordActivity;->audioTmp:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 278
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 279
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->prepare()V

    .line 280
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V

    .line 281
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/andrey/notepad/RecordActivity;->inRecording:Z

    .line 282
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->rec:Landroid/widget/Button;

    const v2, 0x7f020049

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :goto_0
    return-void

    .line 284
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public stopaudio()V
    .locals 3

    .prologue
    .line 308
    :try_start_0
    iget-object v1, p0, Lru/andrey/notepad/RecordActivity;->m2:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    :goto_0
    return-void

    .line 311
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "ERROR in playaudio FUNCTION"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stoprecording()V
    .locals 2

    .prologue
    .line 294
    :try_start_0
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 295
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 296
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->m1:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/andrey/notepad/RecordActivity;->inRecording:Z

    .line 298
    iget-object v0, p0, Lru/andrey/notepad/RecordActivity;->rec:Landroid/widget/Button;

    const v1, 0x7f02003d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :goto_0
    return-void

    .line 300
    :catch_0
    move-exception v0

    goto :goto_0
.end method
