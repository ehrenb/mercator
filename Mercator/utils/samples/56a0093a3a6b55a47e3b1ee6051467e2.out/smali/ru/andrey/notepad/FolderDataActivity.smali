.class public Lru/andrey/notepad/FolderDataActivity;
.super Landroid/app/Activity;
.source "FolderDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/FolderDataActivity$importTask;,
        Lru/andrey/notepad/FolderDataActivity$mainTask;,
        Lru/andrey/notepad/FolderDataActivity$saveTask;
    }
.end annotation


# static fields
.field private static final CAM_REQUEST:I = 0x0

.field private static final CAM_REQUEST2:I = 0x1

.field private static final IDD_BAD_IMPORT:I = 0x5

.field private static final IDD_BAD_PASS:I = 0x6

.field private static final IDD_IMPORT_PROGRESS:I = 0x3

.field private static final IDD_SAVE_PROGRESS:I = 0x2

.field private static final IDD_SUC_BACKUP:I = 0x3

.field private static final IDD_SUC_IMPORT:I = 0x4

.field private static final MENU_ITEM_1_ACTION:I = 0x64

.field private static final MENU_ITEM_2_ACTION:I = 0x66

.field private static final MENU_ITEM_3_ACTION:I = 0x67

.field private static final MENU_ITEM_4_ACTION:I = 0x68

.field private static final MENU_ITEM_5_ACTION:I = 0x69

.field private static final MENU_ITEM_6_ACTION:I = 0x6a

.field private static final MENU_ITEM_7_ACTION:I = 0x6b

.field private static final MENU_ITEM_8_ACTION:I = 0x6c

.field private static final SELECT_PHOTO:I = 0x2

.field static arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

.field static cambmp:Landroid/graphics/Bitmap;

.field static ctx:Landroid/content/Context;

.field static dh:Lru/andrey/notepad/DataBaseHelper;

.field static isSearch:Z

.field static lv:Landroid/widget/ListView;

.field static name:Ljava/lang/String;

.field static object:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field

.field static p:Ljava/io/File;

.field static remPos:I

.field static sobject:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final CONTEXT_MENU_ID:I

.field IMAGE_MAX_SIZE:I

.field ImportProgress:Landroid/app/ProgressDialog;

.field SaveProgress:Landroid/app/ProgressDialog;

.field archive:Ljava/io/File;

.field bg:Landroid/widget/RelativeLayout;

.field private iconContextMenu:Lru/andrey/notepad/IconContextMenu;

.field imageUri:Landroid/net/Uri;

.field lastAction:I

.field outputDir:Ljava/lang/String;

.field search:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    .line 74
    const/4 v0, 0x0

    sput-boolean v0, Lru/andrey/notepad/FolderDataActivity;->isSearch:Z

    .line 95
    const/4 v0, -0x1

    sput v0, Lru/andrey/notepad/FolderDataActivity;->remPos:I

    .line 107
    const-string v0, ""

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 76
    const/4 v0, 0x1

    iput v0, p0, Lru/andrey/notepad/FolderDataActivity;->CONTEXT_MENU_ID:I

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    .line 94
    const/16 v0, 0x400

    iput v0, p0, Lru/andrey/notepad/FolderDataActivity;->IMAGE_MAX_SIZE:I

    .line 65
    return-void
.end method

.method private ImageOperations(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "saveFilename"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 732
    :try_start_0
    invoke-virtual {p0, p2}, Lru/andrey/notepad/FolderDataActivity;->fetch(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/InputStream;

    .line 733
    .local v2, "is":Ljava/io/InputStream;
    invoke-static {v2, p3}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 744
    .end local v2    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v0

    .line 736
    :catch_0
    move-exception v1

    .line 738
    .local v1, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0

    .line 741
    .end local v1    # "e":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 743
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 598
    const/16 v2, 0x400

    new-array v0, v2, [B

    .line 600
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "read":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 604
    return-void

    .line 602
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private createDir(Ljava/io/File;)V
    .locals 3
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    .line 724
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 725
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not create dir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 726
    :cond_0
    return-void
.end method

.method private decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "f"    # Ljava/io/File;

    .prologue
    .line 819
    const-string v5, "Test"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    const/4 v0, 0x0

    .line 824
    .local v0, "b":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 825
    .local v2, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 827
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 828
    .local v1, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    invoke-static {v1, v5, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 829
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 831
    const/4 v4, 0x1

    .line 832
    .local v4, "scale":I
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v6, p0, Lru/andrey/notepad/FolderDataActivity;->IMAGE_MAX_SIZE:I

    if-gt v5, v6, :cond_0

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, p0, Lru/andrey/notepad/FolderDataActivity;->IMAGE_MAX_SIZE:I

    if-le v5, v6, :cond_1

    .line 834
    :cond_0
    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    iget v7, p0, Lru/andrey/notepad/FolderDataActivity;->IMAGE_MAX_SIZE:I

    int-to-double v7, v7

    iget v9, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v10, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    int-to-double v9, v9

    div-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->log(D)D

    move-result-wide v7

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    invoke-static {v9, v10}, Ljava/lang/Math;->log(D)D

    move-result-wide v9

    div-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    long-to-int v7, v7

    int-to-double v7, v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    double-to-int v4, v5

    .line 838
    :cond_1
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 839
    .local v3, "o2":Landroid/graphics/BitmapFactory$Options;
    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 840
    const/4 v5, 0x0

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 841
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 842
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 843
    const/16 v5, 0x4000

    new-array v5, v5, [B

    iput-object v5, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 845
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fis":Ljava/io/FileInputStream;
    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 846
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    invoke-static {v1, v5, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 847
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 851
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "o":Landroid/graphics/BitmapFactory$Options;
    .end local v3    # "o2":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "scale":I
    :goto_0
    return-object v0

    .line 849
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public static openMenu(Landroid/content/Context;Landroid/view/View;I)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "pos"    # I

    .prologue
    .line 1472
    sput p2, Lru/andrey/notepad/FolderDataActivity;->remPos:I

    move-object v0, p0

    .line 1473
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 1474
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "ctx":Landroid/content/Context;
    invoke-virtual {p0, p1}, Landroid/app/Activity;->openContextMenu(Landroid/view/View;)V

    .line 1475
    return-void
.end method

.method private unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    .locals 5
    .param p1, "zipfile"    # Ljava/util/zip/ZipFile;
    .param p2, "entry"    # Ljava/util/zip/ZipEntry;
    .param p3, "outputDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 696
    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 698
    new-instance v3, Ljava/io/File;

    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->createDir(Ljava/io/File;)V

    .line 720
    :goto_0
    return-void

    .line 702
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p3, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    .local v1, "outputFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 705
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->createDir(Ljava/io/File;)V

    .line 708
    :cond_1
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-virtual {p1, p2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 709
    .local v0, "inputStream":Ljava/io/BufferedInputStream;
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 713
    .local v2, "outputStream":Ljava/io/BufferedOutputStream;
    :try_start_0
    invoke-static {v0, v2}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 717
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 718
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_0

    .line 716
    :catchall_0
    move-exception v3

    .line 717
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 718
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 719
    throw v3
.end method

.method public static update()V
    .locals 3

    .prologue
    .line 1479
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/andrey/notepad/DataBaseHelper;->getFolderData(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    .line 1480
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1482
    sget-boolean v0, Lru/andrey/notepad/FolderDataActivity;->isSearch:Z

    if-eqz v0, :cond_0

    .line 1484
    new-instance v1, Lru/andrey/notepad/FolderObjectArrayAdapter;

    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->ctx:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-direct {v1, v0, v2}, Lru/andrey/notepad/FolderObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    .line 1492
    :goto_0
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1493
    return-void

    .line 1489
    :cond_0
    new-instance v1, Lru/andrey/notepad/FolderObjectArrayAdapter;

    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->ctx:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v1, v0, v2}, Lru/andrey/notepad/FolderObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    goto :goto_0
.end method


# virtual methods
.method public UnZip(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p1, "ziparchive"    # Ljava/io/File;
    .param p2, "directory"    # Ljava/lang/String;

    .prologue
    .line 643
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity;->archive:Ljava/io/File;

    .line 644
    iput-object p2, p0, Lru/andrey/notepad/FolderDataActivity;->outputDir:Ljava/lang/String;

    .line 648
    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    iget-object v3, p0, Lru/andrey/notepad/FolderDataActivity;->archive:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 649
    .local v2, "zipfile":Ljava/util/zip/ZipFile;
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_0

    .line 658
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :goto_1
    return-void

    .line 651
    .restart local v0    # "e":Ljava/util/Enumeration;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    .line 653
    .local v1, "entry":Ljava/util/zip/ZipEntry;
    iget-object v3, p0, Lru/andrey/notepad/FolderDataActivity;->outputDir:Ljava/lang/String;

    invoke-direct {p0, v2, v1, v3}, Lru/andrey/notepad/FolderDataActivity;->unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 656
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "entry":Ljava/util/zip/ZipEntry;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public appendSQLLog(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1185
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/NotepadBackup/database.sql"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1186
    .local v2, "logFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1190
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1201
    :cond_0
    :goto_0
    :try_start_1
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v0, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 1202
    .local v0, "buf":Ljava/io/BufferedWriter;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "#SQLITE#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1203
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V

    .line 1204
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1211
    .end local v0    # "buf":Ljava/io/BufferedWriter;
    :goto_1
    return-void

    .line 1192
    :catch_0
    move-exception v1

    .line 1195
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1206
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1209
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public checkAndCreateDirectory(Ljava/lang/String;)V
    .locals 3
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 608
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 609
    .local v0, "new_dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 611
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 613
    :cond_0
    return-void
.end method

.method public copyFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    .line 619
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    .line 621
    .local v8, "sd":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 623
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 624
    .local v10, "sourcePath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 625
    .local v7, "destinationPath":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v8, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 626
    .local v9, "source":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v8, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 627
    .local v6, "destination":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 629
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 630
    .local v1, "src":Ljava/nio/channels/FileChannel;
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 631
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 632
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 633
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    .end local v0    # "dst":Ljava/nio/channels/FileChannel;
    .end local v1    # "src":Ljava/nio/channels/FileChannel;
    .end local v6    # "destination":Ljava/io/File;
    .end local v7    # "destinationPath":Ljava/lang/String;
    .end local v8    # "sd":Ljava/io/File;
    .end local v9    # "source":Ljava/io/File;
    .end local v10    # "sourcePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 637
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public fetch(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 750
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 751
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->getContent()Ljava/lang/Object;

    move-result-object v0

    .line 752
    .local v0, "content":Ljava/lang/Object;
    return-object v0
.end method

.method protected find(Ljava/lang/String;)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1215
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1217
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1225
    new-instance v1, Lru/andrey/notepad/FolderObjectArrayAdapter;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/FolderObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    .line 1226
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1228
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$8;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$8;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1407
    return-void

    .line 1219
    :cond_0
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1221
    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1217
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 13
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 758
    invoke-super/range {p0 .. p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 759
    packed-switch p1, :pswitch_data_0

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 762
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 764
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 765
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v0

    .line 767
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 768
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 770
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 771
    .local v6, "columnIndex":I
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 772
    .local v9, "filePath":Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 773
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 774
    .local v12, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x3

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 776
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 777
    .local v8, "f":Ljava/io/File;
    invoke-direct {p0, v8}, Lru/andrey/notepad/FolderDataActivity;->decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->cambmp:Landroid/graphics/Bitmap;

    .line 778
    new-instance v11, Landroid/content/Intent;

    const-class v0, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v11, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 779
    .local v11, "intent":Landroid/content/Intent;
    const-string v0, "new"

    const/4 v3, 0x1

    invoke-virtual {v11, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 780
    const/4 v0, 0x2

    invoke-virtual {p0, v11, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 784
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "columnIndex":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "f":Ljava/io/File;
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 786
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 787
    .restart local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 788
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 789
    const/4 v0, 0x3

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 790
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    .line 791
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v12, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 792
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->p:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->cambmp:Landroid/graphics/Bitmap;

    .line 793
    new-instance v10, Landroid/content/Intent;

    const-class v0, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v10, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 794
    .local v10, "in2":Landroid/content/Intent;
    const-string v0, "new"

    const/4 v3, 0x1

    invoke-virtual {v10, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 795
    invoke-virtual {p0, v10}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 799
    .end local v10    # "in2":Landroid/content/Intent;
    .end local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    :pswitch_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 801
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 802
    .restart local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 803
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 804
    const/4 v0, 0x3

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 805
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    .line 806
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v12, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 807
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->p:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->cambmp:Landroid/graphics/Bitmap;

    .line 808
    new-instance v10, Landroid/content/Intent;

    const-class v0, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v10, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 809
    .restart local v10    # "in2":Landroid/content/Intent;
    const-string v0, "new"

    const/4 v3, 0x1

    invoke-virtual {v10, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 810
    invoke-virtual {p0, v10}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 759
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 113
    const v1, 0x7f03000b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->setContentView(I)V

    .line 114
    const v1, 0x7f070015

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/AdView;

    .line 115
    .local v0, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 116
    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    .line 125
    const v1, 0x7f070017

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    .line 126
    invoke-static {p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v1

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 127
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lru/andrey/notepad/DataBaseHelper;->getFolderData(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    .line 128
    new-instance v1, Lru/andrey/notepad/FolderObjectArrayAdapter;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/FolderObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    .line 129
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    const v1, 0x7f070016

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->search:Landroid/widget/EditText;

    .line 131
    sput-object p0, Lru/andrey/notepad/FolderDataActivity;->ctx:Landroid/content/Context;

    .line 133
    const v1, 0x7f070014

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->bg:Landroid/widget/RelativeLayout;

    .line 134
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->bg:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "color"

    const-string v4, "#dad07f"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 136
    const-string v1, "/Notepad"

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 137
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lru/andrey/notepad/UpdateService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 138
    new-instance v1, Lru/andrey/notepad/IconContextMenu;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/IconContextMenu;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    .line 139
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05002e

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020044

    const/16 v5, 0x64

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 140
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05002f

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02000b

    const/16 v5, 0x66

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 141
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050039

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020009

    const/16 v5, 0x67

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 142
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05003a

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020050

    const/16 v5, 0x68

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 143
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05003d

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02004d

    const/16 v5, 0x69

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 144
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050040

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020035

    const/16 v5, 0x6b

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 145
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050045

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020036

    const/16 v5, 0x6a

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 146
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/FolderDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050067

    invoke-virtual {p0, v3}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020037

    const/16 v5, 0x6c

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 150
    new-instance v1, Lru/andrey/notepad/FolderDataActivity$mainTask;

    invoke-direct {v1, p0}, Lru/andrey/notepad/FolderDataActivity$mainTask;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Lru/andrey/notepad/FolderDataActivity$mainTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 152
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$1;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$1;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v1, v2}, Lru/andrey/notepad/IconContextMenu;->setOnClickListener(Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;)V

    .line 207
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->search:Landroid/widget/EditText;

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$2;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$2;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 415
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$3;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$3;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 594
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1, "id"    # I

    .prologue
    const v6, 0x7f05004c

    const/4 v5, 0x3

    const v2, 0x7f050050

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 857
    if-ne p1, v3, :cond_0

    .line 859
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    const v2, 0x7f050038

    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lru/andrey/notepad/IconContextMenu;->createMenu(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v1

    .line 939
    :goto_0
    return-object v1

    .line 861
    :cond_0
    if-ne p1, v5, :cond_1

    .line 863
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->ImportProgress:Landroid/app/ProgressDialog;

    .line 864
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->ImportProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 865
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->ImportProgress:Landroid/app/ProgressDialog;

    invoke-virtual {p0, v6}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 866
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->ImportProgress:Landroid/app/ProgressDialog;

    const v2, 0x7f050054

    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 867
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->ImportProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 868
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->ImportProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 870
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 872
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->SaveProgress:Landroid/app/ProgressDialog;

    .line 873
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->SaveProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 874
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->SaveProgress:Landroid/app/ProgressDialog;

    invoke-virtual {p0, v6}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 875
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->SaveProgress:Landroid/app/ProgressDialog;

    const v2, 0x7f05004d

    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 876
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->SaveProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 877
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity;->SaveProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 879
    :cond_2
    if-ne p1, v5, :cond_3

    .line 881
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 882
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 883
    const v1, 0x7f050051

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 884
    const-string v1, "\u041e\u041a"

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$4;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$4;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 891
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 892
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    .line 894
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    .line 896
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 897
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 898
    const v1, 0x7f050053

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 899
    const-string v1, "\u041e\u041a"

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$5;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$5;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 906
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 907
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    .line 909
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_4
    const/4 v1, 0x5

    if-ne p1, v1, :cond_5

    .line 911
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 912
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 913
    const v1, 0x7f050052

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 914
    const-string v1, "\u041e\u041a"

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$6;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$6;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 921
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 922
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    .line 924
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_5
    const/4 v1, 0x6

    if-ne p1, v1, :cond_6

    .line 926
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 927
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f05005f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 928
    const v1, 0x7f050063

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 929
    const-string v1, "\u041e\u041a"

    new-instance v2, Lru/andrey/notepad/FolderDataActivity$7;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderDataActivity$7;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 936
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 937
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    .line 939
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_6
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 1412
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1414
    const/16 v0, 0x6e

    const v1, 0x7f05004f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1415
    const/16 v0, 0x6f

    const v1, 0x7f05004e

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1416
    const/16 v0, 0x70

    const v1, 0x7f05005c

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1417
    const/16 v0, 0x71

    const v1, 0x7f05005d

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1418
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1424
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1425
    .local v3, "intent":Landroid/content/Intent;
    const/high16 v4, 0x80000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1426
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05002c

    invoke-virtual {p0, v5}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1428
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lru/andrey/notepad/AddActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1429
    .local v2, "in":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1430
    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    .line 1467
    .end local v2    # "in":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v7

    .line 1432
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05004f

    invoke-virtual {p0, v5}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1434
    new-instance v4, Lru/andrey/notepad/FolderDataActivity$saveTask;

    invoke-direct {v4, p0}, Lru/andrey/notepad/FolderDataActivity$saveTask;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    new-array v5, v6, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lru/andrey/notepad/FolderDataActivity$saveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1436
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05004e

    invoke-virtual {p0, v5}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1438
    new-instance v4, Lru/andrey/notepad/FolderDataActivity$importTask;

    invoke-direct {v4, p0}, Lru/andrey/notepad/FolderDataActivity$importTask;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    new-array v5, v6, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lru/andrey/notepad/FolderDataActivity$importTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1440
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05005c

    invoke-virtual {p0, v5}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1442
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lru/andrey/notepad/SyncActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1443
    .restart local v2    # "in":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1445
    .end local v2    # "in":Landroid/content/Intent;
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05005d

    invoke-virtual {p0, v5}, Lru/andrey/notepad/FolderDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1448
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "color"

    const-string v6, "#dad07f"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1449
    .local v0, "color":I
    new-instance v1, Lru/andrey/notepad/AmbilWarnaDialog;

    new-instance v4, Lru/andrey/notepad/FolderDataActivity$9;

    invoke-direct {v4, p0}, Lru/andrey/notepad/FolderDataActivity$9;-><init>(Lru/andrey/notepad/FolderDataActivity;)V

    invoke-direct {v1, p0, v0, v4}, Lru/andrey/notepad/AmbilWarnaDialog;-><init>(Landroid/content/Context;ILru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;)V

    .line 1465
    .local v1, "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    invoke-virtual {v1}, Lru/andrey/notepad/AmbilWarnaDialog;->show()V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1498
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1499
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/andrey/notepad/DataBaseHelper;->getFolderData(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    .line 1500
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1502
    sget-boolean v0, Lru/andrey/notepad/FolderDataActivity;->isSearch:Z

    if-eqz v0, :cond_0

    .line 1504
    new-instance v0, Lru/andrey/notepad/FolderObjectArrayAdapter;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    .line 1512
    :goto_0
    sget-object v0, Lru/andrey/notepad/FolderDataActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1513
    return-void

    .line 1509
    :cond_0
    new-instance v0, Lru/andrey/notepad/FolderObjectArrayAdapter;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/FolderDataActivity;->arr:Lru/andrey/notepad/FolderObjectArrayAdapter;

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 665
    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    iget-object v3, p0, Lru/andrey/notepad/FolderDataActivity;->archive:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 666
    .local v2, "zipfile":Ljava/util/zip/ZipFile;
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_0

    .line 675
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :goto_1
    return-void

    .line 668
    .restart local v0    # "e":Ljava/util/Enumeration;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    .line 670
    .local v1, "entry":Ljava/util/zip/ZipEntry;
    iget-object v3, p0, Lru/andrey/notepad/FolderDataActivity;->outputDir:Ljava/lang/String;

    invoke-direct {p0, v2, v1, v3}, Lru/andrey/notepad/FolderDataActivity;->unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 673
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "entry":Ljava/util/zip/ZipEntry;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public unzipArchive(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p1, "archive"    # Ljava/io/File;
    .param p2, "outputDir"    # Ljava/lang/String;

    .prologue
    .line 682
    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    invoke-direct {v2, p1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 683
    .local v2, "zipfile":Ljava/util/zip/ZipFile;
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_0

    .line 691
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :goto_1
    return-void

    .line 685
    .restart local v0    # "e":Ljava/util/Enumeration;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    .line 686
    .local v1, "entry":Ljava/util/zip/ZipEntry;
    invoke-direct {p0, v2, v1, p2}, Lru/andrey/notepad/FolderDataActivity;->unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 689
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "entry":Ljava/util/zip/ZipEntry;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v3

    goto :goto_1
.end method
