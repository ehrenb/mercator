.class public Lru/andrey/notepad/UnZipper;
.super Ljava/util/Observable;
.source "UnZipper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/UnZipper$UnZipTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UnZip"


# instance fields
.field private final mDestinationPath:Ljava/lang/String;

.field private final mFileName:Ljava/lang/String;

.field private final mFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "destinationPath"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 26
    iput-object p1, p0, Lru/andrey/notepad/UnZipper;->mFileName:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lru/andrey/notepad/UnZipper;->mFilePath:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lru/andrey/notepad/UnZipper;->mDestinationPath:Ljava/lang/String;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/UnZipper;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0}, Lru/andrey/notepad/UnZipper;->setChanged()V

    return-void
.end method


# virtual methods
.method public getDestinationPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lru/andrey/notepad/UnZipper;->mDestinationPath:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lru/andrey/notepad/UnZipper;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lru/andrey/notepad/UnZipper;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public unzip()V
    .locals 5

    .prologue
    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lru/andrey/notepad/UnZipper;->mFilePath:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/andrey/notepad/UnZipper;->mFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".zip"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "fullPath":Ljava/lang/String;
    const-string v1, "UnZip"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unzipping "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lru/andrey/notepad/UnZipper;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lru/andrey/notepad/UnZipper;->mDestinationPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    new-instance v1, Lru/andrey/notepad/UnZipper$UnZipTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/UnZipper$UnZipTask;-><init>(Lru/andrey/notepad/UnZipper;Lru/andrey/notepad/UnZipper$UnZipTask;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lru/andrey/notepad/UnZipper;->mDestinationPath:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lru/andrey/notepad/UnZipper$UnZipTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 51
    return-void
.end method
