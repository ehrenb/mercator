.class public Lru/andrey/notepad/ColorPickerDialog;
.super Landroid/app/Dialog;
.source "ColorPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/ColorPickerDialog$ColorPickerView;,
        Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;
    }
.end annotation


# instance fields
.field private mInitialColor:I

.field private mListener:Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;
    .param p3, "initialColor"    # I

    .prologue
    .line 222
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 224
    iput-object p2, p0, Lru/andrey/notepad/ColorPickerDialog;->mListener:Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;

    .line 225
    iput p3, p0, Lru/andrey/notepad/ColorPickerDialog;->mInitialColor:I

    .line 226
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/ColorPickerDialog;)Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lru/andrey/notepad/ColorPickerDialog;->mListener:Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 231
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 232
    new-instance v0, Lru/andrey/notepad/ColorPickerDialog$1;

    invoke-direct {v0, p0}, Lru/andrey/notepad/ColorPickerDialog$1;-><init>(Lru/andrey/notepad/ColorPickerDialog;)V

    .line 241
    .local v0, "l":Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;
    new-instance v1, Lru/andrey/notepad/ColorPickerDialog$ColorPickerView;

    invoke-virtual {p0}, Lru/andrey/notepad/ColorPickerDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lru/andrey/notepad/ColorPickerDialog;->mInitialColor:I

    invoke-direct {v1, v2, v0, v3}, Lru/andrey/notepad/ColorPickerDialog$ColorPickerView;-><init>(Landroid/content/Context;Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;I)V

    invoke-virtual {p0, v1}, Lru/andrey/notepad/ColorPickerDialog;->setContentView(Landroid/view/View;)V

    .line 242
    const-string v1, "Pick a Color"

    invoke-virtual {p0, v1}, Lru/andrey/notepad/ColorPickerDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 243
    return-void
.end method
