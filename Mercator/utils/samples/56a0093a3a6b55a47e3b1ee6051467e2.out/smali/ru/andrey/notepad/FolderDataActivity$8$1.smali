.class Lru/andrey/notepad/FolderDataActivity$8$1;
.super Ljava/lang/Object;
.source "FolderDataActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderDataActivity$8;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FolderDataActivity$8;

.field private final synthetic val$arg2:I

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$value:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity$8;Landroid/widget/EditText;ILandroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    iput-object p2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$value:Landroid/widget/EditText;

    iput p3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    iput-object p4, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$dialog:Landroid/app/Dialog;

    .line 1241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 1245
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$value:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v5, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "pass"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1247
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1249
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1250
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1251
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1252
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1253
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1254
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    .line 1324
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 1326
    return-void

    .line 1256
    :cond_0
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Camera"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1258
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1259
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1260
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1261
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1262
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1263
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1265
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PhotoText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1267
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1268
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1269
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1270
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1271
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1272
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1274
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GalleryText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1276
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1277
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1278
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1279
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1280
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1281
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1283
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Record"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1285
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1286
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1287
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1288
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1289
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1290
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1292
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Shop"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1294
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1295
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1296
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1297
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1298
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1299
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1301
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1303
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1304
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1305
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1306
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1307
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1308
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1312
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_6
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/AddActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1313
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1314
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1315
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1316
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1317
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1322
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_7
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$8$1;->this$1:Lru/andrey/notepad/FolderDataActivity$8;

    # getter for: Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$8;->access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lru/andrey/notepad/FolderDataActivity;->showDialog(I)V

    goto/16 :goto_0
.end method
