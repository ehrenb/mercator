.class Lru/andrey/notepad/ObjectArrayAdapter$2$1;
.super Ljava/lang/Object;
.source "ObjectArrayAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/ObjectArrayAdapter$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/ObjectArrayAdapter$2;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/ObjectArrayAdapter$2;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/ObjectArrayAdapter$2$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$2;

    iput p2, p0, Lru/andrey/notepad/ObjectArrayAdapter$2$1;->val$position:I

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 157
    iget-object v1, p0, Lru/andrey/notepad/ObjectArrayAdapter$2$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$2;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v1}, Lru/andrey/notepad/ObjectArrayAdapter$2;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$2;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v1

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v1}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 158
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v1, p0, Lru/andrey/notepad/ObjectArrayAdapter$2$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$2;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v1}, Lru/andrey/notepad/ObjectArrayAdapter$2;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$2;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v1

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/ObjectArrayAdapter;->access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$2$1;->val$position:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/DataBaseHelper;->removeObject(Ljava/lang/String;)V

    .line 159
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 160
    return-void
.end method
