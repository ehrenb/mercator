.class Lru/andrey/notepad/MainActivity$3;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/MainActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(I)V
    .locals 12
    .param p1, "menuId"    # I

    .prologue
    const/4 v11, 0x1

    .line 208
    packed-switch p1, :pswitch_data_0

    .line 256
    :goto_0
    :pswitch_0
    return-void

    .line 211
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v9, Lru/andrey/notepad/AddActivity;

    invoke-direct {v0, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    .local v0, "in":Landroid/content/Intent;
    const-string v8, "new"

    invoke-virtual {v0, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 213
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v8, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 216
    .end local v0    # "in":Landroid/content/Intent;
    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v9, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v1, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 217
    .local v1, "in2":Landroid/content/Intent;
    const-string v8, "new"

    invoke-virtual {v1, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 218
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v8, v1}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 221
    .end local v1    # "in2":Landroid/content/Intent;
    :pswitch_3
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Notepad/cameraimg.jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    .line 222
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    .local v5, "intent":Landroid/content/Intent;
    const-string v8, "output"

    sget-object v9, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 224
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    sget-object v9, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, v8, Lru/andrey/notepad/MainActivity;->imageUri:Landroid/net/Uri;

    .line 225
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    const/4 v9, 0x0

    invoke-virtual {v8, v5, v9}, Lru/andrey/notepad/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 228
    .end local v5    # "intent":Landroid/content/Intent;
    :pswitch_4
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Notepad/cameraimg.jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    .line 229
    new-instance v6, Landroid/content/Intent;

    const-string v8, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 230
    .local v6, "intent2":Landroid/content/Intent;
    const-string v8, "output"

    sget-object v9, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 231
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    sget-object v9, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, v8, Lru/andrey/notepad/MainActivity;->imageUri:Landroid/net/Uri;

    .line 232
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v8, v6, v11}, Lru/andrey/notepad/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 235
    .end local v6    # "intent2":Landroid/content/Intent;
    :pswitch_5
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 236
    .local v7, "intent4":Landroid/content/Intent;
    const-string v8, "image/*"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string v8, "android.intent.action.GET_CONTENT"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v9, "Select Picture"

    invoke-static {v7, v9}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lru/andrey/notepad/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 241
    .end local v7    # "intent4":Landroid/content/Intent;
    :pswitch_6
    new-instance v2, Landroid/content/Intent;

    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v9, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v2, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 242
    .local v2, "in3":Landroid/content/Intent;
    const-string v8, "new"

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 243
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v8, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 246
    .end local v2    # "in3":Landroid/content/Intent;
    :pswitch_7
    new-instance v3, Landroid/content/Intent;

    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v9, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v3, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 247
    .local v3, "in4":Landroid/content/Intent;
    const-string v8, "new"

    invoke-virtual {v3, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 248
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v8, v3}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 251
    .end local v3    # "in4":Landroid/content/Intent;
    :pswitch_8
    new-instance v4, Landroid/content/Intent;

    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v9, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v4, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 252
    .local v4, "in5":Landroid/content/Intent;
    const-string v8, "new"

    invoke-virtual {v4, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 253
    iget-object v8, p0, Lru/andrey/notepad/MainActivity$3;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v8, v4}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
