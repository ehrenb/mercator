.class public Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;
.super Ljava/lang/Object;
.source "IconContextMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/IconContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "IconContextMenuItem"
.end annotation


# instance fields
.field public final actionTag:I

.field public final image:Landroid/graphics/drawable/Drawable;

.field public final text:Ljava/lang/CharSequence;

.field final synthetic this$0:Lru/andrey/notepad/IconContextMenu;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/IconContextMenu;Landroid/content/res/Resources;III)V
    .locals 1
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "textResourceId"    # I
    .param p4, "imageResourceId"    # I
    .param p5, "actionTag"    # I

    .prologue
    .line 234
    iput-object p1, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->this$0:Lru/andrey/notepad/IconContextMenu;

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->text:Ljava/lang/CharSequence;

    .line 236
    const/4 v0, -0x1

    if-eq p4, v0, :cond_0

    .line 238
    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->image:Landroid/graphics/drawable/Drawable;

    .line 244
    :goto_0
    iput p5, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->actionTag:I

    .line 245
    return-void

    .line 242
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->image:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public constructor <init>(Lru/andrey/notepad/IconContextMenu;Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V
    .locals 1
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "title"    # Ljava/lang/CharSequence;
    .param p4, "imageResourceId"    # I
    .param p5, "actionTag"    # I

    .prologue
    .line 259
    iput-object p1, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->this$0:Lru/andrey/notepad/IconContextMenu;

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    iput-object p3, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->text:Ljava/lang/CharSequence;

    .line 261
    const/4 v0, -0x1

    if-eq p4, v0, :cond_0

    .line 263
    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->image:Landroid/graphics/drawable/Drawable;

    .line 269
    :goto_0
    iput p5, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->actionTag:I

    .line 270
    return-void

    .line 267
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->image:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
