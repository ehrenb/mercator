.class Lru/andrey/notepad/MainActivity$15$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/MainActivity$15;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/MainActivity$15;

.field private final synthetic val$arg2:I

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$value:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/MainActivity$15;Landroid/widget/EditText;ILandroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    iput-object p2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$value:Landroid/widget/EditText;

    iput p3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    iput-object p4, p0, Lru/andrey/notepad/MainActivity$15$1;->val$dialog:Landroid/app/Dialog;

    .line 1494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 1498
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->val$value:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v5, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "pass"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1500
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1502
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1503
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1504
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1505
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1506
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1507
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1577
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 1579
    return-void

    .line 1509
    :cond_0
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Camera"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1511
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1512
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1513
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1514
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1515
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1516
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1518
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PhotoText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1520
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1521
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1522
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1523
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1524
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1525
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1527
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GalleryText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1529
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1530
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1531
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1532
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1533
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1534
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1536
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Record"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1538
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1539
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1540
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1541
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1542
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1543
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1545
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Shop"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1547
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1548
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1549
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1550
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1551
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1552
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1554
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1556
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1557
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1558
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1559
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1560
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1561
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1565
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_6
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/AddActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1566
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1567
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1568
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1569
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/MainActivity$15$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1570
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1575
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_7
    iget-object v1, p0, Lru/andrey/notepad/MainActivity$15$1;->this$1:Lru/andrey/notepad/MainActivity$15;

    # getter for: Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v1}, Lru/andrey/notepad/MainActivity$15;->access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lru/andrey/notepad/MainActivity;->showDialog(I)V

    goto/16 :goto_0
.end method
