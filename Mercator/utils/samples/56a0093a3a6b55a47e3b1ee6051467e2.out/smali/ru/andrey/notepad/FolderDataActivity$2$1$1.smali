.class Lru/andrey/notepad/FolderDataActivity$2$1$1;
.super Ljava/lang/Object;
.source "FolderDataActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderDataActivity$2$1;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

.field private final synthetic val$arg2:I

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$value:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity$2$1;Landroid/widget/EditText;ILandroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    iput-object p2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$value:Landroid/widget/EditText;

    iput p3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    iput-object p4, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$dialog:Landroid/app/Dialog;

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 249
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$value:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v5, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "pass"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 251
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 254
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 255
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 258
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    .line 328
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 330
    return-void

    .line 260
    :cond_0
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Camera"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 263
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 264
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 267
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 269
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PhotoText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 272
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 273
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 276
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 278
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GalleryText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 280
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 281
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 282
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 285
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 287
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Record"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 289
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 290
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 291
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 294
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 296
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Shop"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 298
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 299
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 300
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 303
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 305
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 307
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 308
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 309
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 312
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 316
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_6
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/AddActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 317
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 318
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 319
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 321
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 326
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_7
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$2$1$1;->this$2:Lru/andrey/notepad/FolderDataActivity$2$1;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2$1;->access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lru/andrey/notepad/FolderDataActivity;->showDialog(I)V

    goto/16 :goto_0
.end method
