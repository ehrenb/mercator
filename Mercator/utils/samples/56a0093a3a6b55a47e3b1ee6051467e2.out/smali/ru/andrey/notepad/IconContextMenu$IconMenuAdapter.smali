.class public Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;
.super Landroid/widget/BaseAdapter;
.source "IconContextMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/IconContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "IconMenuAdapter"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lru/andrey/notepad/IconContextMenu;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/IconContextMenu;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    iput-object p1, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->this$0:Lru/andrey/notepad/IconContextMenu;

    .line 134
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->context:Landroid/content/Context;

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->mItems:Ljava/util/ArrayList;

    .line 136
    iput-object p2, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->context:Landroid/content/Context;

    .line 137
    return-void
.end method

.method private toPixel(Landroid/content/res/Resources;I)F
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "dip"    # I

    .prologue
    .line 207
    const/4 v1, 0x1

    int-to-float v2, p2

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 208
    .local v0, "px":F
    return v0
.end method


# virtual methods
.method public addItem(Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;)V
    .locals 1
    .param p1, "menuItem"    # Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;

    .prologue
    .line 145
    iget-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 163
    invoke-virtual {p0, p1}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;

    .line 164
    .local v0, "item":Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;
    iget v1, v0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->actionTag:I

    int-to-long v1, v1

    return-wide v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 170
    invoke-virtual {p0, p1}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;

    .line 172
    .local v0, "item":Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;
    iget-object v7, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->this$0:Lru/andrey/notepad/IconContextMenu;

    # getter for: Lru/andrey/notepad/IconContextMenu;->parentActivity:Landroid/app/Activity;
    invoke-static {v7}, Lru/andrey/notepad/IconContextMenu;->access$0(Lru/andrey/notepad/IconContextMenu;)Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 174
    .local v2, "res":Landroid/content/res/Resources;
    if-nez p2, :cond_1

    .line 176
    new-instance v3, Landroid/widget/TextView;

    iget-object v7, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->context:Landroid/content/Context;

    invoke-direct {v3, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 177
    .local v3, "temp":Landroid/widget/TextView;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v1, v7, v8}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 178
    .local v1, "param":Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    const/16 v7, 0xf

    invoke-direct {p0, v2, v7}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->toPixel(Landroid/content/res/Resources;I)F

    move-result v7

    float-to-int v7, v7

    const/4 v8, 0x0

    const/16 v9, 0xf

    invoke-direct {p0, v2, v9}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->toPixel(Landroid/content/res/Resources;I)F

    move-result v9

    float-to-int v9, v9

    const/4 v10, 0x0

    invoke-virtual {v3, v7, v8, v9, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 180
    const/16 v7, 0x10

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 182
    iget-object v7, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    .line 183
    .local v5, "th":Landroid/content/res/Resources$Theme;
    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    .line 185
    .local v6, "tv":Landroid/util/TypedValue;
    const v7, 0x1010043

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v6, v8}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 187
    iget-object v7, p0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->context:Landroid/content/Context;

    iget v8, v6, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 190
    :cond_0
    const/16 v7, 0x41

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 191
    const/16 v7, 0xe

    invoke-direct {p0, v2, v7}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->toPixel(Landroid/content/res/Resources;I)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 192
    move-object p2, v3

    .end local v1    # "param":Landroid/widget/AbsListView$LayoutParams;
    .end local v3    # "temp":Landroid/widget/TextView;
    .end local v5    # "th":Landroid/content/res/Resources$Theme;
    .end local v6    # "tv":Landroid/util/TypedValue;
    :cond_1
    move-object v4, p2

    .line 195
    check-cast v4, Landroid/widget/TextView;

    .line 196
    .local v4, "textView":Landroid/widget/TextView;
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 197
    iget-object v7, v0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->text:Ljava/lang/CharSequence;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    const/high16 v7, -0x1000000

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 199
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v4, v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 200
    iget-object v7, v0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;->image:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 202
    return-object v4
.end method
