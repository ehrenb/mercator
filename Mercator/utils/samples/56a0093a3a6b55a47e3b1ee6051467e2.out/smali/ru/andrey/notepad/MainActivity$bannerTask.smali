.class public Lru/andrey/notepad/MainActivity$bannerTask;
.super Landroid/os/AsyncTask;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "bannerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field responseBody:Ljava/lang/String;

.field text:Ljava/lang/String;

.field final synthetic this$0:Lru/andrey/notepad/MainActivity;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/MainActivity;)V
    .locals 1

    .prologue
    .line 669
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$bannerTask;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 672
    const-string v0, ""

    iput-object v0, p0, Lru/andrey/notepad/MainActivity$bannerTask;->text:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/MainActivity$bannerTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p1, "uri"    # [Ljava/lang/String;

    .prologue
    .line 683
    :try_start_0
    new-instance v15, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v15}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 684
    .local v15, "httpParameters":Lorg/apache/http/params/HttpParams;
    const-string v3, "ISO-8859-1"

    invoke-static {v15, v3}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 685
    const-string v3, "ISO-8859-1"

    invoke-static {v15, v3}, Lorg/apache/http/params/HttpProtocolParams;->setHttpElementCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 687
    new-instance v11, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v11, v15}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 689
    .local v11, "client":Lorg/apache/http/client/HttpClient;
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Lru/andrey/notepad/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/telephony/TelephonyManager;

    .line 690
    .local v18, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v18 .. v18}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v12

    .line 691
    .local v12, "countryCode":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://deviceapp.org/api_banner/api/GetData?guid=55dd-99gg-rrrr-7777-g788&iso="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&bannertype=1&type=json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 692
    .local v20, "urlStr":Ljava/lang/String;
    const-string v3, "Test"

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    new-instance v19, Ljava/net/URL;

    invoke-direct/range {v19 .. v20}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 694
    .local v19, "url":Ljava/net/URL;
    new-instance v2, Ljava/net/URI;

    invoke-virtual/range {v19 .. v19}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Ljava/net/URL;->getUserInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v19 .. v19}, Ljava/net/URL;->getPort()I

    move-result v6

    invoke-virtual/range {v19 .. v19}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v19 .. v19}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v19 .. v19}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    .local v2, "uris":Ljava/net/URI;
    invoke-virtual {v2}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v19

    .line 697
    new-instance v14, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 698
    .local v14, "get":Lorg/apache/http/client/methods/HttpGet;
    new-instance v17, Lru/andrey/notepad/MainActivity$bannerTask$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/MainActivity$bannerTask$1;-><init>(Lru/andrey/notepad/MainActivity$bannerTask;)V

    .line 713
    .local v17, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    move-object/from16 v0, v17

    invoke-interface {v11, v14, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->responseBody:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 717
    :try_start_1
    new-instance v10, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->responseBody:Ljava/lang/String;

    invoke-direct {v10, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 718
    .local v10, "body":Lorg/json/JSONObject;
    new-instance v16, Lorg/json/JSONObject;

    const-string v3, "data"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 719
    .local v16, "jobj":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v4, "image"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lru/andrey/notepad/MainActivity;->imageUrl:Ljava/lang/String;

    .line 720
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v4, "color"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lru/andrey/notepad/MainActivity;->color:Ljava/lang/String;

    .line 721
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v4, "text"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lru/andrey/notepad/MainActivity;->name:Ljava/lang/String;

    .line 722
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v4, "banner"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lru/andrey/notepad/MainActivity;->type:Ljava/lang/String;

    .line 723
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/andrey/notepad/MainActivity$bannerTask;->this$0:Lru/andrey/notepad/MainActivity;

    const-string v4, "link"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lru/andrey/notepad/MainActivity;->link:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 724
    const-string v3, "success"

    .line 737
    .end local v2    # "uris":Ljava/net/URI;
    .end local v10    # "body":Lorg/json/JSONObject;
    .end local v11    # "client":Lorg/apache/http/client/HttpClient;
    .end local v12    # "countryCode":Ljava/lang/String;
    .end local v14    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v15    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v16    # "jobj":Lorg/json/JSONObject;
    .end local v17    # "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    .end local v18    # "tm":Landroid/telephony/TelephonyManager;
    .end local v19    # "url":Ljava/net/URL;
    .end local v20    # "urlStr":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 727
    .restart local v2    # "uris":Ljava/net/URI;
    .restart local v11    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "countryCode":Ljava/lang/String;
    .restart local v14    # "get":Lorg/apache/http/client/methods/HttpGet;
    .restart local v15    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .restart local v17    # "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    .restart local v18    # "tm":Landroid/telephony/TelephonyManager;
    .restart local v19    # "url":Ljava/net/URL;
    .restart local v20    # "urlStr":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 729
    .local v13, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 730
    const-string v3, "err"

    goto :goto_0

    .line 734
    .end local v2    # "uris":Ljava/net/URI;
    .end local v11    # "client":Lorg/apache/http/client/HttpClient;
    .end local v12    # "countryCode":Ljava/lang/String;
    .end local v13    # "e":Ljava/lang/Exception;
    .end local v14    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v15    # "httpParameters":Lorg/apache/http/params/HttpParams;
    .end local v17    # "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    .end local v18    # "tm":Landroid/telephony/TelephonyManager;
    .end local v19    # "url":Ljava/net/URL;
    .end local v20    # "urlStr":Ljava/lang/String;
    :catch_1
    move-exception v13

    .line 736
    .restart local v13    # "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 737
    const-string v3, "err"

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/MainActivity$bannerTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 745
    const-string v0, "err"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 749
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 676
    return-void
.end method
