.class Lru/andrey/notepad/PictureActivity$12;
.super Ljava/lang/Object;
.source "PictureActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/PictureActivity;->setStroke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/PictureActivity;

.field private final synthetic val$seek:Landroid/widget/SeekBar;


# direct methods
.method constructor <init>(Lru/andrey/notepad/PictureActivity;Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/PictureActivity$12;->this$0:Lru/andrey/notepad/PictureActivity;

    iput-object p2, p0, Lru/andrey/notepad/PictureActivity$12;->val$seek:Landroid/widget/SeekBar;

    .line 871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 875
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$12;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$12;->val$seek:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iput v1, v0, Lru/andrey/notepad/PictureActivity;->stroke:I

    .line 876
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$12;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$12;->val$seek:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 878
    return-void
.end method
