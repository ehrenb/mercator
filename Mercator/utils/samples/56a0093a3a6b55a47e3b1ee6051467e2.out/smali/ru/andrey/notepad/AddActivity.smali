.class public Lru/andrey/notepad/AddActivity;
.super Landroid/app/Activity;
.source "AddActivity.java"


# instance fields
.field bgcolor:Ljava/lang/String;

.field color:I

.field delMode:Z

.field dh:Lru/andrey/notepad/DataBaseHelper;

.field isnew:Z

.field main:Landroid/widget/EditText;

.field name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/andrey/notepad/AddActivity;->delMode:Z

    .line 42
    const-string v0, "#dad07f"

    iput-object v0, p0, Lru/andrey/notepad/AddActivity;->bgcolor:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public static getDrawable(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 588
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 589
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 591
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 160
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050046

    invoke-virtual {p0, v10}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_2

    .line 162
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 164
    .local v3, "names":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x14

    if-le v9, v10, :cond_0

    .line 166
    const/4 v9, 0x0

    const/16 v10, 0x14

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 172
    .local v8, "value":Ljava/lang/String;
    :goto_0
    new-instance v4, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v4}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 173
    .local v4, "om":Lru/andrey/notepad/ObjectModel;
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 174
    iget v9, p0, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 175
    iget-boolean v9, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v9, :cond_1

    .line 176
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 180
    :goto_1
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v6, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 181
    .local v6, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x493e0

    add-long/2addr v9, v11

    invoke-virtual {v4, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 183
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v4}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 325
    .end local v3    # "names":Ljava/lang/String;
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "value":Ljava/lang/String;
    :goto_2
    const/4 v9, 0x1

    :goto_3
    return v9

    .line 170
    .restart local v3    # "names":Ljava/lang/String;
    :cond_0
    move-object v8, v3

    .restart local v8    # "value":Ljava/lang/String;
    goto :goto_0

    .line 178
    .restart local v4    # "om":Lru/andrey/notepad/ObjectModel;
    :cond_1
    invoke-virtual {v4, v8}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    goto :goto_1

    .line 185
    .end local v3    # "names":Ljava/lang/String;
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v8    # "value":Ljava/lang/String;
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050047

    invoke-virtual {p0, v10}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_5

    .line 187
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 189
    .restart local v3    # "names":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x14

    if-le v9, v10, :cond_3

    .line 191
    const/4 v9, 0x0

    const/16 v10, 0x14

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 197
    .restart local v8    # "value":Ljava/lang/String;
    :goto_4
    new-instance v4, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v4}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 198
    .restart local v4    # "om":Lru/andrey/notepad/ObjectModel;
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 199
    iget v9, p0, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 200
    iget-boolean v9, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v9, :cond_4

    .line 201
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 205
    :goto_5
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v6, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 206
    .restart local v6    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 207
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0xdbba0

    add-long/2addr v9, v11

    invoke-virtual {v4, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 208
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v4}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto :goto_2

    .line 195
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "value":Ljava/lang/String;
    :cond_3
    move-object v8, v3

    .restart local v8    # "value":Ljava/lang/String;
    goto :goto_4

    .line 203
    .restart local v4    # "om":Lru/andrey/notepad/ObjectModel;
    :cond_4
    invoke-virtual {v4, v8}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    goto :goto_5

    .line 210
    .end local v3    # "names":Ljava/lang/String;
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v8    # "value":Ljava/lang/String;
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050048

    invoke-virtual {p0, v10}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_8

    .line 212
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 214
    .restart local v3    # "names":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x14

    if-le v9, v10, :cond_6

    .line 216
    const/4 v9, 0x0

    const/16 v10, 0x14

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 222
    .restart local v8    # "value":Ljava/lang/String;
    :goto_6
    new-instance v4, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v4}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 223
    .restart local v4    # "om":Lru/andrey/notepad/ObjectModel;
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 224
    iget v9, p0, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 225
    iget-boolean v9, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v9, :cond_7

    .line 226
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 230
    :goto_7
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v6, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 231
    .restart local v6    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 232
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x16e360

    add-long/2addr v9, v11

    invoke-virtual {v4, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 233
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v4}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_2

    .line 220
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "value":Ljava/lang/String;
    :cond_6
    move-object v8, v3

    .restart local v8    # "value":Ljava/lang/String;
    goto :goto_6

    .line 228
    .restart local v4    # "om":Lru/andrey/notepad/ObjectModel;
    :cond_7
    invoke-virtual {v4, v8}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    goto :goto_7

    .line 235
    .end local v3    # "names":Ljava/lang/String;
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v8    # "value":Ljava/lang/String;
    :cond_8
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050049

    invoke-virtual {p0, v10}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_b

    .line 237
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 239
    .restart local v3    # "names":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x14

    if-le v9, v10, :cond_9

    .line 241
    const/4 v9, 0x0

    const/16 v10, 0x14

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 247
    .restart local v8    # "value":Ljava/lang/String;
    :goto_8
    new-instance v4, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v4}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 248
    .restart local v4    # "om":Lru/andrey/notepad/ObjectModel;
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 249
    iget v9, p0, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 250
    iget-boolean v9, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v9, :cond_a

    .line 251
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 255
    :goto_9
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v6, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 256
    .restart local v6    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x36ee80

    add-long/2addr v9, v11

    invoke-virtual {v4, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 258
    iget-object v9, p0, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v4}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_2

    .line 245
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "value":Ljava/lang/String;
    :cond_9
    move-object v8, v3

    .restart local v8    # "value":Ljava/lang/String;
    goto :goto_8

    .line 253
    .restart local v4    # "om":Lru/andrey/notepad/ObjectModel;
    :cond_a
    invoke-virtual {v4, v8}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    goto :goto_9

    .line 260
    .end local v3    # "names":Ljava/lang/String;
    .end local v4    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v8    # "value":Ljava/lang/String;
    :cond_b
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f05004a

    invoke-virtual {p0, v10}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_c

    .line 262
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 263
    .local v1, "dialog":Landroid/app/Dialog;
    const v9, 0x7f030016

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setContentView(I)V

    .line 264
    const v9, 0x7f05004a

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setTitle(I)V

    .line 265
    const/4 v9, 0x1

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 266
    const v9, 0x7f070044

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/DatePicker;

    .line 267
    .local v2, "dp":Landroid/widget/DatePicker;
    const v9, 0x7f070045

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TimePicker;

    .line 268
    .local v7, "tp":Landroid/widget/TimePicker;
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 269
    const v9, 0x7f070028

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 270
    .local v5, "save":Landroid/widget/Button;
    const v9, 0x7f070019

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 272
    .local v0, "cancel":Landroid/widget/Button;
    new-instance v9, Lru/andrey/notepad/AddActivity$1;

    invoke-direct {v9, p0, v2, v7, v1}, Lru/andrey/notepad/AddActivity$1;-><init>(Lru/andrey/notepad/AddActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V

    invoke-virtual {v5, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    new-instance v9, Lru/andrey/notepad/AddActivity$2;

    invoke-direct {v9, p0, v1}, Lru/andrey/notepad/AddActivity$2;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_2

    .line 323
    .end local v0    # "cancel":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v2    # "dp":Landroid/widget/DatePicker;
    .end local v5    # "save":Landroid/widget/Button;
    .end local v7    # "tp":Landroid/widget/TimePicker;
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v2, 0x7f030001

    invoke-virtual {p0, v2}, Lru/andrey/notepad/AddActivity;->setContentView(I)V

    .line 49
    const v2, 0x7f070015

    invoke-virtual {p0, v2}, Lru/andrey/notepad/AddActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/AdView;

    .line 50
    .local v1, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 51
    const v2, 0x7f070016

    invoke-virtual {p0, v2}, Lru/andrey/notepad/AddActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    .line 52
    const/high16 v2, -0x1000000

    iput v2, p0, Lru/andrey/notepad/AddActivity;->color:I

    .line 53
    invoke-static {p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 55
    invoke-virtual {p0}, Lru/andrey/notepad/AddActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 56
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "new"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    .line 57
    iget-boolean v2, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v2, :cond_0

    .line 59
    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    .line 60
    iget-object v2, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    const-string v3, "body"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 61
    const-string v2, "color"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lru/andrey/notepad/AddActivity;->color:I

    .line 62
    iget-object v2, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    iget v3, p0, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setTextColor(I)V

    .line 63
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "notecolor"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "#dad07f"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/AddActivity;->bgcolor:Ljava/lang/String;

    .line 64
    iget-object v2, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "notecolor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "#dad07f"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setBackgroundColor(I)V

    .line 68
    :cond_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 147
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 148
    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 149
    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 150
    const v0, 0x7f050047

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 151
    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 152
    const v0, 0x7f050049

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 153
    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 154
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v3, 0x66

    const/4 v2, 0x0

    .line 74
    const/16 v0, 0x64

    const v1, 0x7f05003f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 75
    const/16 v0, 0x65

    const v1, 0x7f050024

    invoke-virtual {p0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 76
    const v0, 0x7f050025

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 78
    iget-boolean v0, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v0, :cond_1

    .line 80
    const v0, 0x7f050026

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 81
    const/16 v0, 0x68

    const v1, 0x7f050037

    invoke-virtual {p0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 82
    const/16 v0, 0x69

    const v1, 0x7f05004b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 84
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    const/16 v0, 0x6a

    const v1, 0x7f05005f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 87
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const/16 v0, 0x6b

    const v1, 0x7f050060

    invoke-virtual {p0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 92
    :cond_1
    const/16 v0, 0x6c

    const v1, 0x7f050065

    invoke-virtual {p0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 29
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 331
    new-instance v21, Landroid/content/Intent;

    const-string v26, "android.intent.action.VIEW"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 332
    .local v21, "intent":Landroid/content/Intent;
    const/high16 v26, 0x80000

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 333
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050024

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 335
    new-instance v18, Lru/andrey/notepad/AmbilWarnaDialog;

    move-object/from16 v0, p0

    iget v0, v0, Lru/andrey/notepad/AddActivity;->color:I

    move/from16 v26, v0

    new-instance v27, Lru/andrey/notepad/AddActivity$3;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/AddActivity$3;-><init>(Lru/andrey/notepad/AddActivity;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, v26

    move-object/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lru/andrey/notepad/AmbilWarnaDialog;-><init>(Landroid/content/Context;ILru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;)V

    .line 350
    .local v18, "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    invoke-virtual/range {v18 .. v18}, Lru/andrey/notepad/AmbilWarnaDialog;->show()V

    .line 583
    .end local v18    # "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    :cond_0
    :goto_0
    const/16 v26, 0x1

    return v26

    .line 352
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f05004b

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2

    .line 354
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/AddActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 355
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/AddActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 357
    :cond_2
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050025

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 359
    new-instance v22, Landroid/content/Intent;

    const-string v26, "android.intent.action.SEND"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 360
    .local v22, "sharingIntent":Landroid/content/Intent;
    const-string v26, "text/plain"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    const-string v26, "android.intent.extra.TEXT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    const v26, 0x7f050028

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 364
    .end local v22    # "sharingIntent":Landroid/content/Intent;
    :cond_3
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050029

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 366
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 367
    .local v6, "alert":Landroid/app/AlertDialog$Builder;
    new-instance v20, Landroid/widget/EditText;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 368
    .local v20, "input":Landroid/widget/EditText;
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 369
    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 370
    const v26, 0x7f05002b

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 371
    const v26, 0x7f050029

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v26

    new-instance v27, Lru/andrey/notepad/AddActivity$4;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$4;-><init>(Lru/andrey/notepad/AddActivity;Landroid/widget/EditText;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 390
    const v26, 0x7f05002a

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v26

    new-instance v27, Lru/andrey/notepad/AddActivity$5;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/AddActivity$5;-><init>(Lru/andrey/notepad/AddActivity;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 397
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 399
    .end local v6    # "alert":Landroid/app/AlertDialog$Builder;
    .end local v20    # "input":Landroid/widget/EditText;
    :cond_4
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050026

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 401
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lru/andrey/notepad/DataBaseHelper;->removeObject(Ljava/lang/String;)V

    .line 402
    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lru/andrey/notepad/AddActivity;->delMode:Z

    .line 403
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/AddActivity;->finish()V

    goto/16 :goto_0

    .line 405
    :cond_5
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050027

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 407
    new-instance v7, Landroid/content/Intent;

    const-string v26, "android.intent.action.VIEW"

    const-string v27, "market://search?q=pub:Power"

    invoke-static/range {v27 .. v27}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v7, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 408
    .local v7, "browserIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lru/andrey/notepad/AddActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 410
    .end local v7    # "browserIntent":Landroid/content/Intent;
    :cond_6
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f05003f

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 412
    new-instance v19, Landroid/content/Intent;

    const-class v26, Lru/andrey/notepad/MainActivity;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 413
    .local v19, "in":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 415
    .end local v19    # "in":Landroid/content/Intent;
    :cond_7
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050037

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 417
    new-instance v23, Landroid/content/Intent;

    const-class v26, Lru/andrey/notepad/AddActivity;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 418
    .local v23, "shortcutIntent":Landroid/content/Intent;
    const-string v26, "new"

    const/16 v27, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 419
    const-string v26, "name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    const-string v26, "body"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    const-string v26, "color"

    move-object/from16 v0, p0

    iget v0, v0, Lru/andrey/notepad/AddActivity;->color:I

    move/from16 v27, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 422
    const/high16 v26, 0x10000000

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 423
    const/high16 v26, 0x4000000

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 424
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 425
    .local v5, "addIntent":Landroid/content/Intent;
    const-string v26, "android.intent.extra.shortcut.INTENT"

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 426
    const-string v26, "android.intent.extra.shortcut.NAME"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    const-string v26, "android.intent.extra.shortcut.ICON_RESOURCE"

    const-string v27, "icon64text"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lru/andrey/notepad/AddActivity;->getDrawable(Landroid/content/Context;Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 428
    const-string v26, "com.android.launcher.action.INSTALL_SHORTCUT"

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 429
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lru/andrey/notepad/AddActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 431
    .end local v5    # "addIntent":Landroid/content/Intent;
    .end local v23    # "shortcutIntent":Landroid/content/Intent;
    :cond_8
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f05005f

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 433
    new-instance v18, Landroid/app/Dialog;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 434
    .local v18, "dialog":Landroid/app/Dialog;
    const v26, 0x7f030007

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 435
    const v26, 0x7f05005f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 436
    const/16 v26, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 437
    const v26, 0x7f070016

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/EditText;

    .line 438
    .local v24, "value":Landroid/widget/EditText;
    const v26, 0x7f07002d

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/EditText;

    .line 440
    .local v25, "value2":Landroid/widget/EditText;
    const v26, 0x7f070028

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/Button;

    .line 441
    .local v17, "button":Landroid/widget/Button;
    new-instance v26, Lru/andrey/notepad/AddActivity$6;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    move-object/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lru/andrey/notepad/AddActivity$6;-><init>(Lru/andrey/notepad/AddActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/app/Dialog;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 461
    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 463
    .end local v17    # "button":Landroid/widget/Button;
    .end local v18    # "dialog":Landroid/app/Dialog;
    .end local v24    # "value":Landroid/widget/EditText;
    .end local v25    # "value2":Landroid/widget/EditText;
    :cond_9
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050060

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 465
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-interface/range {v26 .. v28}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 467
    :cond_a
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    const v27, 0x7f050065

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 469
    new-instance v18, Landroid/app/Dialog;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 470
    .restart local v18    # "dialog":Landroid/app/Dialog;
    const v26, 0x7f030015

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 471
    const v26, 0x7f050065

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 472
    const/16 v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 473
    const v26, 0x7f070028

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 474
    .local v8, "btn1":Landroid/widget/Button;
    const v26, 0x7f070019

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    .line 475
    .local v9, "btn2":Landroid/widget/Button;
    const v26, 0x7f07001a

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 476
    .local v10, "btn3":Landroid/widget/Button;
    const v26, 0x7f07001b

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    .line 477
    .local v11, "btn4":Landroid/widget/Button;
    const v26, 0x7f07001c

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    .line 478
    .local v12, "btn5":Landroid/widget/Button;
    const v26, 0x7f07003f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    .line 479
    .local v13, "btn6":Landroid/widget/Button;
    const v26, 0x7f070041

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    .line 480
    .local v14, "btn7":Landroid/widget/Button;
    const v26, 0x7f070042

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/Button;

    .line 481
    .local v15, "btn8":Landroid/widget/Button;
    const v26, 0x7f070043

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    .line 483
    .local v16, "btn9":Landroid/widget/Button;
    new-instance v26, Lru/andrey/notepad/AddActivity$7;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$7;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 494
    new-instance v26, Lru/andrey/notepad/AddActivity$8;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$8;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 505
    new-instance v26, Lru/andrey/notepad/AddActivity$9;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$9;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 516
    new-instance v26, Lru/andrey/notepad/AddActivity$10;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$10;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 527
    new-instance v26, Lru/andrey/notepad/AddActivity$11;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$11;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 538
    new-instance v26, Lru/andrey/notepad/AddActivity$12;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$12;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 549
    new-instance v26, Lru/andrey/notepad/AddActivity$13;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$13;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560
    new-instance v26, Lru/andrey/notepad/AddActivity$14;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$14;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 571
    new-instance v26, Lru/andrey/notepad/AddActivity$15;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/AddActivity$15;-><init>(Lru/andrey/notepad/AddActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 581
    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v4, 0x6b

    const/16 v3, 0x6a

    const/4 v2, 0x0

    .line 100
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 101
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 102
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 103
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v0, :cond_0

    .line 104
    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 106
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v0, :cond_1

    .line 107
    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Lru/andrey/notepad/AddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 108
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 7

    .prologue
    const/16 v5, 0x14

    .line 114
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 115
    iget-boolean v4, p0, Lru/andrey/notepad/AddActivity;->delMode:Z

    if-nez v4, :cond_0

    .line 117
    iget-object v4, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "names":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v5, :cond_1

    .line 121
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 127
    .local v3, "value":Ljava/lang/String;
    :goto_0
    new-instance v1, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v1}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 128
    .local v1, "om":Lru/andrey/notepad/ObjectModel;
    iget-object v4, p0, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 129
    iget v4, p0, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 130
    iget-boolean v4, p0, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v4, :cond_2

    .line 131
    iget-object v4, p0, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 135
    :goto_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 136
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 137
    iget-object v4, p0, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v1}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 138
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "notecolor"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lru/andrey/notepad/AddActivity;->bgcolor:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 140
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 142
    .end local v0    # "names":Ljava/lang/String;
    .end local v1    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v3    # "value":Ljava/lang/String;
    :cond_0
    return-void

    .line 125
    .restart local v0    # "names":Ljava/lang/String;
    :cond_1
    move-object v3, v0

    .restart local v3    # "value":Ljava/lang/String;
    goto :goto_0

    .line 133
    .restart local v1    # "om":Lru/andrey/notepad/ObjectModel;
    :cond_2
    invoke-virtual {v1, v3}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    goto :goto_1
.end method
