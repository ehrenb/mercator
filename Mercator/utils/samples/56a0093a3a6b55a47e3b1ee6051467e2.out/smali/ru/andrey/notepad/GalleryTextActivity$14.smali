.class Lru/andrey/notepad/GalleryTextActivity$14;
.super Ljava/lang/Object;
.source "GalleryTextActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/GalleryTextActivity;->showSave()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/GalleryTextActivity;

.field private final synthetic val$count:I

.field private final synthetic val$edit:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/GalleryTextActivity;ILandroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/GalleryTextActivity$14;->this$0:Lru/andrey/notepad/GalleryTextActivity;

    iput p2, p0, Lru/andrey/notepad/GalleryTextActivity$14;->val$count:I

    iput-object p3, p0, Lru/andrey/notepad/GalleryTextActivity$14;->val$edit:Landroid/widget/EditText;

    .line 573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 577
    iget-object v3, p0, Lru/andrey/notepad/GalleryTextActivity$14;->this$0:Lru/andrey/notepad/GalleryTextActivity;

    iget-object v3, v3, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 578
    .local v2, "t":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 579
    const-string v2, " "

    .line 581
    :cond_0
    new-instance v0, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v0}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 582
    .local v0, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GalleryText/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 583
    iget v3, p0, Lru/andrey/notepad/GalleryTextActivity$14;->val$count:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 584
    iget-object v3, p0, Lru/andrey/notepad/GalleryTextActivity$14;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 585
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm MM-dd-yyyy"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 586
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 587
    iget-object v3, p0, Lru/andrey/notepad/GalleryTextActivity$14;->this$0:Lru/andrey/notepad/GalleryTextActivity;

    iget-object v3, v3, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v3, v0}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 588
    iget-object v3, p0, Lru/andrey/notepad/GalleryTextActivity$14;->this$0:Lru/andrey/notepad/GalleryTextActivity;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "notecolor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lru/andrey/notepad/GalleryTextActivity$14;->this$0:Lru/andrey/notepad/GalleryTextActivity;

    iget-object v5, v5, Lru/andrey/notepad/GalleryTextActivity;->bgcolor:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 590
    new-instance v3, Lru/andrey/notepad/GalleryTextActivity$saveTask;

    iget-object v4, p0, Lru/andrey/notepad/GalleryTextActivity$14;->this$0:Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v3, v4}, Lru/andrey/notepad/GalleryTextActivity$saveTask;-><init>(Lru/andrey/notepad/GalleryTextActivity;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lru/andrey/notepad/GalleryTextActivity$14;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lru/andrey/notepad/GalleryTextActivity$saveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 591
    return-void
.end method
