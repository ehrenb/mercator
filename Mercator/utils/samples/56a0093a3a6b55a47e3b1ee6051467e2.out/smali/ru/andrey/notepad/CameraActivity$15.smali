.class Lru/andrey/notepad/CameraActivity$15;
.super Ljava/lang/Object;
.source "CameraActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/CameraActivity;->showSave()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/CameraActivity;

.field private final synthetic val$count:I

.field private final synthetic val$edit:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/CameraActivity;ILandroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    iput p2, p0, Lru/andrey/notepad/CameraActivity$15;->val$count:I

    iput-object p3, p0, Lru/andrey/notepad/CameraActivity$15;->val$edit:Landroid/widget/EditText;

    .line 991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 15
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 996
    const-string v11, "Camera"

    .line 997
    .local v11, "value":Ljava/lang/String;
    new-instance v6, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v6}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 998
    .local v6, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v6, v11}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 999
    iget v12, p0, Lru/andrey/notepad/CameraActivity$15;->val$count:I

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {v6, v12}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 1000
    iget-object v12, p0, Lru/andrey/notepad/CameraActivity$15;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-interface {v12}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 1001
    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string v12, "HH:mm MM-dd-yyyy"

    invoke-direct {v9, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1002
    .local v9, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v12, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-direct {v12, v13, v14}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 1003
    iget-object v12, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v12, v12, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v12, v6}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 1005
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Notepad"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1006
    .local v8, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1007
    .local v1, "fOut":Ljava/io/OutputStream;
    new-instance v3, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Camera"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v13, p0, Lru/andrey/notepad/CameraActivity$15;->val$count:I

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".png"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v3, v8, v12}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    .local v3, "file":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1012
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .local v2, "fOut":Ljava/io/OutputStream;
    :try_start_1
    iget-object v12, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v12, v12, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    # getter for: Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v12}, Lru/andrey/notepad/CameraActivity$MyView;->access$0(Lru/andrey/notepad/CameraActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    iget-object v13, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v13, v13, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    # getter for: Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lru/andrey/notepad/CameraActivity$MyView;->access$0(Lru/andrey/notepad/CameraActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1013
    .local v10, "toSave":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1015
    .local v5, "mCanvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    const/4 v12, 0x2

    invoke-direct {v7, v12}, Landroid/graphics/Paint;-><init>(I)V

    .line 1016
    .local v7, "paint":Landroid/graphics/Paint;
    iget-object v12, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v12, v12, Lru/andrey/notepad/CameraActivity;->photoBitmap:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v5, v12, v13, v14, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1018
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v12, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v12}, Lru/andrey/notepad/CameraActivity;->access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lt v4, v12, :cond_0

    .line 1023
    sget-object v12, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v13, 0x55

    invoke-virtual {v10, v12, v13, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1025
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 1026
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 1035
    .end local v2    # "fOut":Ljava/io/OutputStream;
    .end local v4    # "i":I
    .end local v5    # "mCanvas":Landroid/graphics/Canvas;
    .end local v7    # "paint":Landroid/graphics/Paint;
    .end local v10    # "toSave":Landroid/graphics/Bitmap;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :goto_1
    iget-object v12, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    invoke-virtual {v12}, Lru/andrey/notepad/CameraActivity;->finish()V

    .line 1036
    return-void

    .line 1020
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "fOut":Ljava/io/OutputStream;
    .restart local v4    # "i":I
    .restart local v5    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v7    # "paint":Landroid/graphics/Paint;
    .restart local v10    # "toSave":Landroid/graphics/Bitmap;
    :cond_0
    :try_start_2
    iget-object v12, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v12}, Lru/andrey/notepad/CameraActivity;->access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/Path;

    iget-object v13, p0, Lru/andrey/notepad/CameraActivity$15;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v13}, Lru/andrey/notepad/CameraActivity;->access$3(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/Paint;

    invoke-virtual {v5, v12, v13}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1018
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1029
    .end local v2    # "fOut":Ljava/io/OutputStream;
    .end local v4    # "i":I
    .end local v5    # "mCanvas":Landroid/graphics/Canvas;
    .end local v7    # "paint":Landroid/graphics/Paint;
    .end local v10    # "toSave":Landroid/graphics/Bitmap;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 1032
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1029
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "fOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    goto :goto_2
.end method
