.class public Lru/andrey/notepad/GalleryTextActivity;
.super Landroid/app/Activity;
.source "GalleryTextActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/GalleryTextActivity$saveTask;
    }
.end annotation


# static fields
.field private static final BGCOLOR_MENU_ID:I = 0xf

.field private static final DELETE_MENU_ID:I = 0x5

.field private static final HOME_MENU_ID:I = 0xb

.field private static final IDD_LOAD_PROGRESS:I = 0x1

.field private static final MORE_MENU_ID:I = 0x7

.field private static final PASS_MENU_ID:I = 0xd

.field private static final REMIND_MENU_ID:I = 0xc

.field private static final SHARE_MENU_ID:I = 0x9

.field private static final SHORTCUT_MENU_ID:I = 0xa

.field private static final UNPASS_MENU_ID:I = 0xe


# instance fields
.field LoadProgress:Landroid/app/ProgressDialog;

.field bg:Landroid/graphics/Bitmap;

.field bgcolor:Ljava/lang/String;

.field bmp:Landroid/graphics/Bitmap;

.field count:I

.field delMode:Z

.field dh:Lru/andrey/notepad/DataBaseHelper;

.field erase:Z

.field isnew:Z

.field name:Ljava/lang/String;

.field num:I

.field photo:Landroid/widget/ImageView;

.field photoBitmap:Landroid/graphics/Bitmap;

.field text:Landroid/widget/EditText;

.field turn:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    iput-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->erase:Z

    .line 50
    iput-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    .line 55
    iput-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->delMode:Z

    .line 57
    iput v0, p0, Lru/andrey/notepad/GalleryTextActivity;->count:I

    .line 64
    const-string v0, "#dad07f"

    iput-object v0, p0, Lru/andrey/notepad/GalleryTextActivity;->bgcolor:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static getDrawable(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 508
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 509
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 511
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static rotate(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "src"    # Landroid/graphics/Bitmap;
    .param p1, "degree"    # F

    .prologue
    const/4 v1, 0x0

    .line 126
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 128
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 131
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public checkAndCreateDirectory(Ljava/lang/String;)V
    .locals 3
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 116
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 117
    .local v0, "new_dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 121
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 544
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 545
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v11, 0x7f05004a

    const/4 v8, 0x1

    .line 393
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050046

    invoke-virtual {p0, v10}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_1

    .line 395
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 396
    .local v6, "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_0

    .line 397
    const-string v6, " "

    .line 398
    :cond_0
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 399
    .local v3, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "GalleryText/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 400
    iget v9, p0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 401
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 402
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 403
    .local v5, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 404
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x493e0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 405
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 503
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :goto_0
    return v8

    .line 407
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050047

    invoke-virtual {p0, v10}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_3

    .line 409
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 410
    .restart local v6    # "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_2

    .line 411
    const-string v6, " "

    .line 412
    :cond_2
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 413
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "GalleryText/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 414
    iget v9, p0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 415
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 416
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 417
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 418
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0xdbba0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 419
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto :goto_0

    .line 421
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050048

    invoke-virtual {p0, v10}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_5

    .line 423
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 424
    .restart local v6    # "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_4

    .line 425
    const-string v6, " "

    .line 426
    :cond_4
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 427
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "GalleryText/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 428
    iget v9, p0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 429
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 430
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 431
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 432
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x16e360

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 433
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 435
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050049

    invoke-virtual {p0, v10}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_7

    .line 437
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 438
    .restart local v6    # "t":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_6

    .line 439
    const-string v6, " "

    .line 440
    :cond_6
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 441
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "GalleryText/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 442
    iget v9, p0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 443
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 444
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 445
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 446
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x36ee80

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 447
    iget-object v9, p0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 449
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v6    # "t":Ljava/lang/String;
    :cond_7
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {p0, v11}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_8

    .line 451
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 452
    .local v1, "dialog":Landroid/app/Dialog;
    const v9, 0x7f030016

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setContentView(I)V

    .line 453
    invoke-virtual {v1, v11}, Landroid/app/Dialog;->setTitle(I)V

    .line 454
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 455
    const v9, 0x7f070044

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/DatePicker;

    .line 456
    .local v2, "dp":Landroid/widget/DatePicker;
    const v9, 0x7f070045

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TimePicker;

    .line 457
    .local v7, "tp":Landroid/widget/TimePicker;
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 458
    const v9, 0x7f070028

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 459
    .local v4, "save":Landroid/widget/Button;
    const v9, 0x7f070019

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 461
    .local v0, "cancel":Landroid/widget/Button;
    new-instance v9, Lru/andrey/notepad/GalleryTextActivity$12;

    invoke-direct {v9, p0, v2, v7, v1}, Lru/andrey/notepad/GalleryTextActivity$12;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 488
    new-instance v9, Lru/andrey/notepad/GalleryTextActivity$13;

    invoke-direct {v9, p0, v1}, Lru/andrey/notepad/GalleryTextActivity$13;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 497
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 501
    .end local v0    # "cancel":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v2    # "dp":Landroid/widget/DatePicker;
    .end local v4    # "save":Landroid/widget/Button;
    .end local v7    # "tp":Landroid/widget/TimePicker;
    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v2, 0x7f03000e

    invoke-virtual {p0, v2}, Lru/andrey/notepad/GalleryTextActivity;->setContentView(I)V

    .line 71
    invoke-virtual {p0}, Lru/andrey/notepad/GalleryTextActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 73
    const v2, 0x7f070015

    invoke-virtual {p0, v2}, Lru/andrey/notepad/GalleryTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/AdView;

    .line 74
    .local v1, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 75
    const v2, 0x7f070030

    invoke-virtual {p0, v2}, Lru/andrey/notepad/GalleryTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->photo:Landroid/widget/ImageView;

    .line 76
    const v2, 0x7f070016

    invoke-virtual {p0, v2}, Lru/andrey/notepad/GalleryTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    .line 77
    const v2, 0x7f070028

    invoke-virtual {p0, v2}, Lru/andrey/notepad/GalleryTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->turn:Landroid/widget/Button;

    .line 79
    invoke-virtual {p0}, Lru/andrey/notepad/GalleryTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 80
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "new"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    .line 81
    iget-boolean v2, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v2, :cond_0

    .line 83
    const-string v2, "color"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    .line 84
    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Notepad/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->photoBitmap:Landroid/graphics/Bitmap;

    .line 86
    iget-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    const-string v3, "body"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 87
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "notecolor"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "#dad07f"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->bgcolor:Ljava/lang/String;

    .line 88
    iget-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "notecolor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "#dad07f"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setBackgroundColor(I)V

    .line 95
    :goto_0
    iget-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->photo:Landroid/widget/ImageView;

    iget-object v3, p0, Lru/andrey/notepad/GalleryTextActivity;->photoBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 97
    invoke-static {p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 99
    const-string v2, "/Notepad"

    invoke-virtual {p0, v2}, Lru/andrey/notepad/GalleryTextActivity;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->turn:Landroid/widget/Button;

    new-instance v3, Lru/andrey/notepad/GalleryTextActivity$1;

    invoke-direct {v3, p0}, Lru/andrey/notepad/GalleryTextActivity$1;-><init>(Lru/andrey/notepad/GalleryTextActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    return-void

    .line 92
    :cond_0
    sget-object v2, Lru/andrey/notepad/MainActivity;->cambmp:Landroid/graphics/Bitmap;

    sget-object v3, Lru/andrey/notepad/MainActivity;->cambmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lru/andrey/notepad/GalleryTextActivity;->photoBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 380
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 381
    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 382
    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 383
    const v0, 0x7f050047

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 384
    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 385
    const v0, 0x7f050049

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 386
    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 387
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 689
    packed-switch p1, :pswitch_data_0

    .line 699
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 692
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/andrey/notepad/GalleryTextActivity;->LoadProgress:Landroid/app/ProgressDialog;

    .line 693
    iget-object v0, p0, Lru/andrey/notepad/GalleryTextActivity;->LoadProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 694
    iget-object v0, p0, Lru/andrey/notepad/GalleryTextActivity;->LoadProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f05003b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 695
    iget-object v0, p0, Lru/andrey/notepad/GalleryTextActivity;->LoadProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f05003c

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 696
    iget-object v0, p0, Lru/andrey/notepad/GalleryTextActivity;->LoadProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 697
    iget-object v0, p0, Lru/andrey/notepad/GalleryTextActivity;->LoadProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 689
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v7, 0x37

    const/16 v6, 0x36

    const/16 v5, 0x34

    const/16 v4, 0x64

    const/4 v3, 0x0

    .line 147
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 149
    const/16 v0, 0xb

    const v1, 0x7f05003f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x31

    const/16 v2, 0x68

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 151
    iget-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x5

    const v1, 0x7f050026

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 153
    :cond_0
    iget-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    const/16 v0, 0xd

    const v1, 0x7f05005f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 155
    :cond_1
    iget-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v0, :cond_2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    const/16 v0, 0xe

    const v1, 0x7f050060

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 158
    :cond_2
    const/16 v0, 0x9

    const v1, 0x7f050025

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x6d

    invoke-interface {v0, v6, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 159
    iget-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v0, :cond_3

    .line 160
    const/16 v0, 0xa

    const v1, 0x7f050037

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v7, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 161
    :cond_3
    iget-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v0, :cond_4

    .line 162
    const/16 v0, 0xc

    const v1, 0x7f05004b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v7, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 164
    :cond_4
    const/16 v0, 0xf

    const v1, 0x7f050065

    invoke-virtual {p0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x6d

    invoke-interface {v0, v6, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 166
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 608
    const/4 v4, 0x4

    if-ne p1, v4, :cond_2

    .line 611
    iget-boolean v4, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v4, :cond_1

    .line 613
    iget-object v4, p0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 614
    .local v2, "t":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    .line 615
    const-string v2, " "

    .line 617
    :cond_0
    new-instance v0, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v0}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 618
    .local v0, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GalleryText/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 619
    iget v4, p0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    invoke-virtual {v0, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 620
    iget-object v4, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 621
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 622
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 623
    iget-object v4, p0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v0}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 624
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "notecolor"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lru/andrey/notepad/GalleryTextActivity;->bgcolor:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 626
    new-instance v4, Lru/andrey/notepad/GalleryTextActivity$saveTask;

    invoke-direct {v4, p0}, Lru/andrey/notepad/GalleryTextActivity$saveTask;-><init>(Lru/andrey/notepad/GalleryTextActivity;)V

    new-array v5, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lru/andrey/notepad/GalleryTextActivity$saveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 635
    .end local v0    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v1    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v2    # "t":Ljava/lang/String;
    :goto_0
    return v3

    .line 631
    :cond_1
    invoke-virtual {p0}, Lru/andrey/notepad/GalleryTextActivity;->showSave()V

    goto :goto_0

    .line 635
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 26
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 186
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v23

    packed-switch v23, :pswitch_data_0

    .line 374
    :pswitch_0
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v23

    :goto_0
    return v23

    .line 190
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lru/andrey/notepad/DataBaseHelper;->removeObject(Ljava/lang/String;)V

    .line 191
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lru/andrey/notepad/GalleryTextActivity;->delMode:Z

    .line 192
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/GalleryTextActivity;->finish()V

    .line 193
    const/16 v23, 0x1

    goto :goto_0

    .line 195
    :pswitch_2
    new-instance v6, Landroid/content/Intent;

    const-string v23, "android.intent.action.VIEW"

    const-string v24, "market://search?q=pub:Power"

    invoke-static/range {v24 .. v24}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 196
    .local v6, "browserIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lru/andrey/notepad/GalleryTextActivity;->startActivity(Landroid/content/Intent;)V

    .line 197
    const/16 v23, 0x1

    goto :goto_0

    .line 199
    .end local v6    # "browserIntent":Landroid/content/Intent;
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/GalleryTextActivity;->share()V

    .line 200
    const/16 v23, 0x1

    goto :goto_0

    .line 202
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/GalleryTextActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lru/andrey/notepad/GalleryTextActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 203
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/GalleryTextActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lru/andrey/notepad/GalleryTextActivity;->openContextMenu(Landroid/view/View;)V

    .line 204
    const/16 v23, 0x1

    goto :goto_0

    .line 206
    :pswitch_5
    new-instance v20, Landroid/content/Intent;

    const-class v23, Lru/andrey/notepad/GalleryTextActivity;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 207
    .local v20, "shortcutIntent":Landroid/content/Intent;
    const-string v23, "new"

    const/16 v24, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208
    const-string v23, "name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const-string v23, "body"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "GalleryText/"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/GalleryTextActivity;->text:Landroid/widget/EditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const-string v23, "color"

    move-object/from16 v0, p0

    iget v0, v0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    move/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 211
    const/high16 v23, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 212
    const/high16 v23, 0x4000000

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 213
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 214
    .local v5, "addIntent":Landroid/content/Intent;
    const-string v23, "android.intent.extra.shortcut.INTENT"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 215
    const-string v23, "android.intent.extra.shortcut.NAME"

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const-string v23, "android.intent.extra.shortcut.ICON_RESOURCE"

    const-string v24, "icon64gallery"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lru/andrey/notepad/GalleryTextActivity;->getDrawable(Landroid/content/Context;Ljava/lang/String;)I

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 217
    const-string v23, "com.android.launcher.action.INSTALL_SHORTCUT"

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lru/andrey/notepad/GalleryTextActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 219
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 221
    .end local v5    # "addIntent":Landroid/content/Intent;
    .end local v20    # "shortcutIntent":Landroid/content/Intent;
    :pswitch_6
    new-instance v17, Landroid/app/Dialog;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 222
    .local v17, "dialog":Landroid/app/Dialog;
    const v23, 0x7f030015

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 223
    const v23, 0x7f050065

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 224
    const/16 v23, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 225
    const v23, 0x7f070028

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 226
    .local v7, "btn1":Landroid/widget/Button;
    const v23, 0x7f070019

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 227
    .local v8, "btn2":Landroid/widget/Button;
    const v23, 0x7f07001a

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    .line 228
    .local v9, "btn3":Landroid/widget/Button;
    const v23, 0x7f07001b

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 229
    .local v10, "btn4":Landroid/widget/Button;
    const v23, 0x7f07001c

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    .line 230
    .local v11, "btn5":Landroid/widget/Button;
    const v23, 0x7f07003f

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    .line 231
    .local v12, "btn6":Landroid/widget/Button;
    const v23, 0x7f070041

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    .line 232
    .local v13, "btn7":Landroid/widget/Button;
    const v23, 0x7f070042

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    .line 233
    .local v14, "btn8":Landroid/widget/Button;
    const v23, 0x7f070043

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/Button;

    .line 235
    .local v15, "btn9":Landroid/widget/Button;
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$2;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$3;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$3;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$4;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$4;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$5;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$5;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$6;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$6;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$7;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$7;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$8;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$8;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 312
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$9;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$9;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$10;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$10;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    invoke-virtual/range {v17 .. v17}, Landroid/app/Dialog;->show()V

    .line 334
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 336
    .end local v7    # "btn1":Landroid/widget/Button;
    .end local v8    # "btn2":Landroid/widget/Button;
    .end local v9    # "btn3":Landroid/widget/Button;
    .end local v10    # "btn4":Landroid/widget/Button;
    .end local v11    # "btn5":Landroid/widget/Button;
    .end local v12    # "btn6":Landroid/widget/Button;
    .end local v13    # "btn7":Landroid/widget/Button;
    .end local v14    # "btn8":Landroid/widget/Button;
    .end local v15    # "btn9":Landroid/widget/Button;
    .end local v17    # "dialog":Landroid/app/Dialog;
    :pswitch_7
    new-instance v18, Landroid/app/Dialog;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 337
    .local v18, "dialog1":Landroid/app/Dialog;
    const v23, 0x7f030007

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 338
    const v23, 0x7f05005f

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 339
    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 340
    const v23, 0x7f070016

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/EditText;

    .line 341
    .local v21, "value":Landroid/widget/EditText;
    const v23, 0x7f07002d

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/EditText;

    .line 343
    .local v22, "value2":Landroid/widget/EditText;
    const v23, 0x7f070028

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    .line 344
    .local v16, "button":Landroid/widget/Button;
    new-instance v23, Lru/andrey/notepad/GalleryTextActivity$11;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move-object/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lru/andrey/notepad/GalleryTextActivity$11;-><init>(Lru/andrey/notepad/GalleryTextActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/app/Dialog;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364
    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->show()V

    .line 365
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 367
    .end local v16    # "button":Landroid/widget/Button;
    .end local v18    # "dialog1":Landroid/app/Dialog;
    .end local v21    # "value":Landroid/widget/EditText;
    .end local v22    # "value2":Landroid/widget/EditText;
    :pswitch_8
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-interface/range {v23 .. v25}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 368
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 370
    :pswitch_9
    new-instance v19, Landroid/content/Intent;

    const-class v23, Lru/andrey/notepad/MainActivity;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 371
    .local v19, "in":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lru/andrey/notepad/GalleryTextActivity;->startActivity(Landroid/content/Intent;)V

    .line 372
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_9
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_6
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v6, 0x64

    const/16 v5, 0x34

    const/16 v4, 0xe

    const/16 v3, 0xd

    const/4 v2, 0x0

    .line 172
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 173
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 174
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 175
    iget-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 177
    :cond_0
    iget-boolean v0, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/GalleryTextActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 179
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 706
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 707
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 709
    return-void
.end method

.method public share()V
    .locals 10

    .prologue
    .line 516
    const-string v6, "NotepadSharing"

    .line 517
    .local v6, "value":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/Notepad"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 518
    .local v3, "path":Ljava/lang/String;
    const/4 v0, 0x0

    .line 519
    .local v0, "fOut":Ljava/io/OutputStream;
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ".png"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .local v1, "fOut":Ljava/io/OutputStream;
    :try_start_1
    iget-object v7, p0, Lru/andrey/notepad/GalleryTextActivity;->photoBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    iget-object v8, p0, Lru/andrey/notepad/GalleryTextActivity;->photoBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 524
    .local v5, "toSave":Landroid/graphics/Bitmap;
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x55

    invoke-virtual {v5, v7, v8, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 526
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 527
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 534
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v5    # "toSave":Landroid/graphics/Bitmap;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    :goto_0
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.SEND"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 535
    .local v4, "share":Landroid/content/Intent;
    const-string v7, "image/png"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 536
    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 537
    const-string v7, "Share Image"

    invoke-static {v4, v7}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lru/andrey/notepad/GalleryTextActivity;->startActivity(Landroid/content/Intent;)V

    .line 539
    return-void

    .line 529
    .end local v4    # "share":Landroid/content/Intent;
    :catch_0
    move-exception v7

    goto :goto_0

    .end local v0    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    goto :goto_0
.end method

.method public showSave()V
    .locals 7

    .prologue
    .line 549
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 551
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f050033

    invoke-virtual {p0, v5}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 553
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 555
    .local v3, "linear":Landroid/widget/LinearLayout;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 557
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 559
    .local v2, "edit":Landroid/widget/EditText;
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 562
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 565
    iget-boolean v5, p0, Lru/andrey/notepad/GalleryTextActivity;->isnew:Z

    if-eqz v5, :cond_0

    .line 566
    iget-object v5, p0, Lru/andrey/notepad/GalleryTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v5}, Lru/andrey/notepad/DataBaseHelper;->getGalleryTextCount()I

    move-result v1

    .line 570
    .local v1, "count":I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GalleryText "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 571
    .local v4, "names":Ljava/lang/String;
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 573
    const v5, 0x7f050029

    invoke-virtual {p0, v5}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/GalleryTextActivity$14;

    invoke-direct {v6, p0, v1, v2}, Lru/andrey/notepad/GalleryTextActivity$14;-><init>(Lru/andrey/notepad/GalleryTextActivity;ILandroid/widget/EditText;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 594
    const v5, 0x7f05002a

    invoke-virtual {p0, v5}, Lru/andrey/notepad/GalleryTextActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/GalleryTextActivity$15;

    invoke-direct {v6, p0}, Lru/andrey/notepad/GalleryTextActivity$15;-><init>(Lru/andrey/notepad/GalleryTextActivity;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 602
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 603
    return-void

    .line 568
    .end local v1    # "count":I
    .end local v4    # "names":Ljava/lang/String;
    :cond_0
    iget v5, p0, Lru/andrey/notepad/GalleryTextActivity;->num:I

    add-int/lit8 v1, v5, -0x1

    .restart local v1    # "count":I
    goto :goto_0
.end method
