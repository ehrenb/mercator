.class Lru/andrey/notepad/BuyActivity$5;
.super Ljava/lang/Object;
.source "BuyActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/BuyActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/BuyActivity;

.field private final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/BuyActivity;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/BuyActivity$5;->this$0:Lru/andrey/notepad/BuyActivity;

    iput-object p2, p0, Lru/andrey/notepad/BuyActivity$5;->val$input:Landroid/widget/EditText;

    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 299
    iget-object v3, p0, Lru/andrey/notepad/BuyActivity$5;->val$input:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 301
    .local v2, "value":Ljava/lang/String;
    new-instance v0, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v0}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 303
    .local v0, "om":Lru/andrey/notepad/ObjectModel;
    iget-object v3, p0, Lru/andrey/notepad/BuyActivity$5;->this$0:Lru/andrey/notepad/BuyActivity;

    iget v3, v3, Lru/andrey/notepad/BuyActivity;->color:I

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 304
    invoke-virtual {v0, v2}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 305
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm MM-dd-yyyy"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 306
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 307
    iget-object v3, p0, Lru/andrey/notepad/BuyActivity$5;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v3, v3, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v3, v0}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 308
    iget-object v3, p0, Lru/andrey/notepad/BuyActivity$5;->this$0:Lru/andrey/notepad/BuyActivity;

    invoke-virtual {v3}, Lru/andrey/notepad/BuyActivity;->finish()V

    .line 309
    return-void
.end method
