.class Lru/andrey/notepad/FolderDataActivity$saveTask;
.super Landroid/os/AsyncTask;
.source "FolderDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/FolderDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "saveTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/FolderDataActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity;)V
    .locals 0

    .prologue
    .line 1079
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/FolderDataActivity$saveTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 22
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 1090
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    move-object/from16 v18, v0

    const-string v19, "/NotepadBackup"

    invoke-virtual/range {v18 .. v19}, Lru/andrey/notepad/FolderDataActivity;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 1091
    new-instance v12, Ljava/io/File;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/NotepadBackup/database.sql"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1092
    .local v12, "logFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 1093
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1094
    .local v3, "buy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/BuyModel;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1095
    .local v11, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1096
    .local v16, "remind":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1097
    .local v8, "folders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1098
    .local v9, "foldersData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    sget-object v18, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual/range {v18 .. v18}, Lru/andrey/notepad/DataBaseHelper;->getBuy()Ljava/util/ArrayList;

    move-result-object v3

    .line 1099
    sget-object v18, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual/range {v18 .. v18}, Lru/andrey/notepad/DataBaseHelper;->getNotes()Ljava/util/ArrayList;

    move-result-object v11

    .line 1100
    sget-object v18, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual/range {v18 .. v18}, Lru/andrey/notepad/DataBaseHelper;->getReminds()Ljava/util/ArrayList;

    move-result-object v16

    .line 1101
    sget-object v18, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual/range {v18 .. v18}, Lru/andrey/notepad/DataBaseHelper;->getFolders()Ljava/util/ArrayList;

    move-result-object v8

    .line 1102
    sget-object v18, Lru/andrey/notepad/FolderDataActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual/range {v18 .. v18}, Lru/andrey/notepad/DataBaseHelper;->getFoldersData()Ljava/util/ArrayList;

    move-result-object v9

    .line 1104
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v10, v0, :cond_1

    .line 1110
    const/4 v10, 0x0

    :goto_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v10, v0, :cond_2

    .line 1120
    const/4 v10, 0x0

    :goto_2
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v10, v0, :cond_3

    .line 1129
    const/4 v10, 0x0

    :goto_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v10, v0, :cond_4

    .line 1139
    const/4 v10, 0x0

    :goto_4
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v10, v0, :cond_5

    .line 1149
    new-instance v14, Ljava/io/File;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/Notepad"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1150
    .local v14, "mdir":Ljava/io/File;
    new-instance v17, Ljava/io/File;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/NotepadBackup/data.zip"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1152
    .local v17, "zipto":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_0

    .line 1156
    :try_start_0
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1165
    :cond_0
    :goto_5
    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 1166
    .local v7, "files1":[Ljava/io/File;
    array-length v0, v7

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v6, v0, [Ljava/lang/String;

    .line 1167
    .local v6, "files":[Ljava/lang/String;
    const/4 v10, 0x0

    :goto_6
    array-length v0, v7

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v10, v0, :cond_6

    .line 1171
    new-instance v4, Lru/andrey/notepad/Compress;

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v4, v6, v0}, Lru/andrey/notepad/Compress;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    .local v4, "c":Lru/andrey/notepad/Compress;
    invoke-virtual {v4}, Lru/andrey/notepad/Compress;->zip()V

    .line 1173
    const/16 v18, 0x0

    return-object v18

    .line 1106
    .end local v4    # "c":Lru/andrey/notepad/Compress;
    .end local v6    # "files":[Ljava/lang/String;
    .end local v7    # "files1":[Ljava/io/File;
    .end local v14    # "mdir":Ljava/io/File;
    .end local v17    # "zipto":Ljava/io/File;
    :cond_1
    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/andrey/notepad/BuyModel;

    .line 1107
    .local v13, "m":Lru/andrey/notepad/BuyModel;
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "REPLACE INTO Buy(`listId`, `name`, `price`, `quantity`,`pos`,`isDone`) VALUES (\'"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lru/andrey/notepad/BuyModel;->getListId()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/BuyModel;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/BuyModel;->getPos()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/BuyModel;->getisDone()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\');"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lru/andrey/notepad/FolderDataActivity;->appendSQLLog(Ljava/lang/String;)V

    .line 1104
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 1112
    .end local v13    # "m":Lru/andrey/notepad/BuyModel;
    :cond_2
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/andrey/notepad/ObjectModel;

    .line 1113
    .local v13, "m":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 1114
    .local v2, "body":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1115
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v15

    .line 1116
    .local v15, "name":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1117
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "REPLACE INTO Notes(`name`, `date`, `body`, `color`) VALUES (\'"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\');"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lru/andrey/notepad/FolderDataActivity;->appendSQLLog(Ljava/lang/String;)V

    .line 1110
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1122
    .end local v2    # "body":Ljava/lang/String;
    .end local v13    # "m":Lru/andrey/notepad/ObjectModel;
    .end local v15    # "name":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/andrey/notepad/ObjectModel;

    .line 1123
    .restart local v13    # "m":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 1124
    .restart local v2    # "body":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1125
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v15

    .line 1126
    .restart local v15    # "name":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1127
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "REPLACE INTO Remind(`whentime`, `name`, `date`, `body`, `color`) VALUES (\'"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getWhen()J

    move-result-wide v20

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\');"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lru/andrey/notepad/FolderDataActivity;->appendSQLLog(Ljava/lang/String;)V

    .line 1120
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    .line 1131
    .end local v2    # "body":Ljava/lang/String;
    .end local v13    # "m":Lru/andrey/notepad/ObjectModel;
    .end local v15    # "name":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/andrey/notepad/ObjectModel;

    .line 1132
    .restart local v13    # "m":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 1133
    .restart local v2    # "body":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1134
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v15

    .line 1135
    .restart local v15    # "name":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1136
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "REPLACE INTO Folders(`foldername`, `date`) VALUES (\'"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\');"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lru/andrey/notepad/FolderDataActivity;->appendSQLLog(Ljava/lang/String;)V

    .line 1129
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 1141
    .end local v2    # "body":Ljava/lang/String;
    .end local v13    # "m":Lru/andrey/notepad/ObjectModel;
    .end local v15    # "name":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/andrey/notepad/ObjectModel;

    .line 1142
    .restart local v13    # "m":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 1143
    .restart local v2    # "body":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1144
    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v15

    .line 1145
    .restart local v15    # "name":Ljava/lang/String;
    const-string v18, "\'"

    const-string v19, "\'\'"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1146
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "REPLACE INTO FoldersData(`foldername`, `name`, `date`, `body`, `color`) VALUES (\'"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getFname()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\',\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\');"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lru/andrey/notepad/FolderDataActivity;->appendSQLLog(Ljava/lang/String;)V

    .line 1139
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_4

    .line 1158
    .end local v2    # "body":Ljava/lang/String;
    .end local v13    # "m":Lru/andrey/notepad/ObjectModel;
    .end local v15    # "name":Ljava/lang/String;
    .restart local v14    # "mdir":Ljava/io/File;
    .restart local v17    # "zipto":Ljava/io/File;
    :catch_0
    move-exception v5

    .line 1161
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 1169
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v6    # "files":[Ljava/lang/String;
    .restart local v7    # "files1":[Ljava/io/File;
    :cond_6
    aget-object v18, v7, v10

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v6, v10

    .line 1167
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_6
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/FolderDataActivity$saveTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 1179
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity;->SaveProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1180
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 1084
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$saveTask;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lru/andrey/notepad/FolderDataActivity;->showDialog(I)V

    .line 1085
    return-void
.end method
