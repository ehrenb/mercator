.class public Lru/andrey/notepad/PictureActivity;
.super Lru/andrey/notepad/GraphicsActivity;
.source "PictureActivity.java"

# interfaces
.implements Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/PictureActivity$MyView;
    }
.end annotation


# static fields
.field private static final BACK_MENU_ID:I = 0x8

.field private static final BLUR_MENU_ID:I = 0x3

.field private static final COLOR_MENU_ID:I = 0x1

.field private static final DELETE_MENU_ID:I = 0x5

.field private static final EMBOSS_MENU_ID:I = 0x2

.field private static final ERASE_MENU_ID:I = 0x4

.field private static final HOME_MENU_ID:I = 0xb

.field private static final MORE_MENU_ID:I = 0x7

.field private static final PASS_MENU_ID:I = 0xd

.field private static final REMIND_MENU_ID:I = 0xc

.field private static final SHARE_MENU_ID:I = 0x9

.field private static final SHORTCUT_MENU_ID:I = 0xa

.field private static final STROKE_MENU_ID:I = 0x6

.field private static final UNPASS_MENU_ID:I = 0xe


# instance fields
.field bg:Landroid/graphics/Bitmap;

.field bmp:Landroid/graphics/Bitmap;

.field clear:Landroid/widget/Button;

.field color:I

.field count:I

.field delMode:Z

.field dh:Lru/andrey/notepad/DataBaseHelper;

.field erase:Z

.field intens:I

.field isnew:Z

.field private mBlur:Landroid/graphics/MaskFilter;

.field private mEmboss:Landroid/graphics/MaskFilter;

.field private mPaint:Landroid/graphics/Paint;

.field mv:Lru/andrey/notepad/PictureActivity$MyView;

.field name:Ljava/lang/String;

.field num:I

.field private final paints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field private final paths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field stroke:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lru/andrey/notepad/GraphicsActivity;-><init>()V

    .line 55
    const/high16 v0, -0x10000

    iput v0, p0, Lru/andrey/notepad/PictureActivity;->color:I

    .line 56
    iput-boolean v1, p0, Lru/andrey/notepad/PictureActivity;->erase:Z

    .line 58
    iput-boolean v1, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    .line 63
    iput-boolean v1, p0, Lru/andrey/notepad/PictureActivity;->delMode:Z

    .line 64
    const/16 v0, 0xc

    iput v0, p0, Lru/andrey/notepad/PictureActivity;->stroke:I

    .line 66
    const/16 v0, 0x64

    iput v0, p0, Lru/andrey/notepad/PictureActivity;->intens:I

    .line 67
    iput v1, p0, Lru/andrey/notepad/PictureActivity;->count:I

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;

    .line 52
    return-void
.end method

.method static synthetic access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lru/andrey/notepad/PictureActivity;Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public static getDrawable(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 636
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 637
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 639
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public checkAndCreateDirectory(Ljava/lang/String;)V
    .locals 3
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 247
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 248
    .local v0, "new_dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 250
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 252
    :cond_0
    return-void
.end method

.method public colorChanged(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 242
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 243
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 847
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 848
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v11, 0x7f05004a

    const/4 v8, 0x1

    .line 658
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050046

    invoke-virtual {p0, v10}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_0

    .line 660
    const-string v7, "drawing"

    .line 661
    .local v7, "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 662
    .local v3, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 663
    iget v9, p0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 664
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 665
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 666
    .local v5, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 667
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x493e0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 668
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 758
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :goto_0
    return v8

    .line 670
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050047

    invoke-virtual {p0, v10}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_1

    .line 672
    const-string v7, "drawing"

    .line 673
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 674
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 675
    iget v9, p0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 676
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 677
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 678
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 679
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0xdbba0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 680
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto :goto_0

    .line 682
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050048

    invoke-virtual {p0, v10}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_2

    .line 684
    const-string v7, "drawing"

    .line 685
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 686
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 687
    iget v9, p0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 688
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 689
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 690
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 691
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x16e360

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 692
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 694
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050049

    invoke-virtual {p0, v10}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_3

    .line 696
    const-string v7, "drawing"

    .line 697
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 698
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 699
    iget v9, p0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 700
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 701
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 702
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 703
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x36ee80

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 704
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 706
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {p0, v11}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_4

    .line 708
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 709
    .local v1, "dialog":Landroid/app/Dialog;
    const v9, 0x7f030016

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setContentView(I)V

    .line 710
    invoke-virtual {v1, v11}, Landroid/app/Dialog;->setTitle(I)V

    .line 711
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 712
    const v9, 0x7f070044

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/DatePicker;

    .line 713
    .local v2, "dp":Landroid/widget/DatePicker;
    const v9, 0x7f070045

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TimePicker;

    .line 714
    .local v6, "tp":Landroid/widget/TimePicker;
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 715
    const v9, 0x7f070028

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 716
    .local v4, "save":Landroid/widget/Button;
    const v9, 0x7f070019

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 718
    .local v0, "cancel":Landroid/widget/Button;
    new-instance v9, Lru/andrey/notepad/PictureActivity$8;

    invoke-direct {v9, p0, v2, v6, v1}, Lru/andrey/notepad/PictureActivity$8;-><init>(Lru/andrey/notepad/PictureActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 743
    new-instance v9, Lru/andrey/notepad/PictureActivity$9;

    invoke-direct {v9, p0, v1}, Lru/andrey/notepad/PictureActivity$9;-><init>(Lru/andrey/notepad/PictureActivity;Landroid/app/Dialog;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 752
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 756
    .end local v0    # "cancel":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v2    # "dp":Landroid/widget/DatePicker;
    .end local v4    # "save":Landroid/widget/Button;
    .end local v6    # "tp":Landroid/widget/TimePicker;
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super/range {p0 .. p1}, Lru/andrey/notepad/GraphicsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/PictureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 78
    .local v4, "extras":Landroid/os/Bundle;
    const-string v13, "new"

    invoke-virtual {v4, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    .line 79
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v13, :cond_0

    .line 81
    const-string v13, "color"

    invoke-virtual {v4, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lru/andrey/notepad/PictureActivity;->num:I

    .line 82
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/Notepad/Drawing"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".png"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v13

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->bmp:Landroid/graphics/Bitmap;

    .line 83
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->bmp:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lru/andrey/notepad/PictureActivity;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->bg:Landroid/graphics/Bitmap;

    .line 84
    new-instance v13, Lru/andrey/notepad/PictureActivity$MyView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lru/andrey/notepad/PictureActivity;->bmp:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v13, v0, v1, v14}, Lru/andrey/notepad/PictureActivity$MyView;-><init>(Lru/andrey/notepad/PictureActivity;Landroid/content/Context;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    .line 85
    const-string v13, "name"

    invoke-virtual {v4, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    .line 92
    :goto_0
    new-instance v12, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 93
    .local v12, "vg":Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 94
    const/4 v13, -0x1

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 95
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/PictureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 97
    .local v8, "r":Landroid/content/res/Resources;
    new-instance v3, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 98
    .local v3, "color":Landroid/widget/Button;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v13, 0x1

    const/high16 v14, 0x42480000    # 50.0f

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    float-to-int v13, v13

    const/4 v14, 0x1

    const/high16 v15, 0x42480000    # 50.0f

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v14

    float-to-int v14, v14

    invoke-direct {v9, v13, v14}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 99
    .local v9, "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v13, 0xc

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 100
    const/16 v13, 0xe

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 101
    invoke-virtual {v3, v9}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    const v13, 0x7f02000a

    invoke-virtual {v3, v13}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 104
    new-instance v13, Lru/andrey/notepad/PictureActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lru/andrey/notepad/PictureActivity$1;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v3, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    new-instance v7, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 136
    .local v7, "op":Landroid/widget/Button;
    invoke-virtual {v7, v9}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    const v13, 0x7f020040

    invoke-virtual {v7, v13}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 139
    new-instance v13, Lru/andrey/notepad/PictureActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lru/andrey/notepad/PictureActivity$2;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v7, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    new-instance v11, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 149
    .local v11, "st":Landroid/widget/Button;
    invoke-virtual {v11, v9}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    const v13, 0x7f02004f

    invoke-virtual {v11, v13}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 152
    new-instance v13, Lru/andrey/notepad/PictureActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lru/andrey/notepad/PictureActivity$3;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v11, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    new-instance v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 162
    .local v2, "back":Landroid/widget/Button;
    invoke-virtual {v2, v9}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    const v13, 0x7f020054

    invoke-virtual {v2, v13}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 165
    new-instance v13, Lru/andrey/notepad/PictureActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lru/andrey/notepad/PictureActivity$4;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v2, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v13, 0x1

    const/high16 v14, 0x42480000    # 50.0f

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    float-to-int v13, v13

    const/4 v14, 0x1

    const/high16 v15, 0x42480000    # 50.0f

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v14

    float-to-int v14, v14

    invoke-direct {v10, v13, v14}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 179
    .local v10, "relativeParams2":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v13, 0xc

    invoke-virtual {v10, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 180
    const/16 v13, 0xe

    invoke-virtual {v10, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 181
    const/4 v13, 0x1

    const/high16 v14, 0x41a00000    # 20.0f

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    float-to-int v13, v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v10, v13, v14, v15, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 183
    new-instance v13, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    .line 184
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    invoke-virtual {v13, v9}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 185
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    const v14, 0x7f020043

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 187
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    new-instance v14, Lru/andrey/notepad/PictureActivity$5;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lru/andrey/notepad/PictureActivity$5;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    new-instance v5, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 197
    .local v5, "ll":Landroid/widget/LinearLayout;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v13, -0x1

    const/4 v14, -0x2

    invoke-direct {v6, v13, v14}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 198
    .local v6, "llp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v13, 0xc

    invoke-virtual {v6, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 199
    const/16 v13, 0xe

    invoke-virtual {v6, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 200
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/high16 v17, 0x41200000    # 10.0f

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v16

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v6, v13, v14, v15, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 201
    const/16 v13, 0x11

    invoke-virtual {v5, v13}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 202
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    invoke-virtual {v5, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 204
    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 205
    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 206
    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 207
    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 208
    invoke-virtual {v12, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 210
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lru/andrey/notepad/PictureActivity;->setContentView(Landroid/view/View;)V

    .line 216
    invoke-static/range {p0 .. p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 218
    const-string v13, "/Notepad"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/andrey/notepad/PictureActivity;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 220
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    .line 221
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 222
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setDither(Z)V

    .line 223
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    const/high16 v14, -0x10000

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setColor(I)V

    .line 224
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    sget-object v14, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 225
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    sget-object v14, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    sget-object v14, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v14, v0, Lru/andrey/notepad/PictureActivity;->stroke:I

    int-to-float v14, v14

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 229
    new-instance v13, Landroid/graphics/EmbossMaskFilter;

    const/4 v14, 0x3

    new-array v14, v14, [F

    fill-array-data v14, :array_0

    const v15, 0x3ecccccd    # 0.4f

    const/high16 v16, 0x40c00000    # 6.0f

    const/high16 v17, 0x40600000    # 3.5f

    invoke-direct/range {v13 .. v17}, Landroid/graphics/EmbossMaskFilter;-><init>([FFFF)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->mEmboss:Landroid/graphics/MaskFilter;

    .line 231
    new-instance v13, Landroid/graphics/BlurMaskFilter;

    const/high16 v14, 0x41000000    # 8.0f

    sget-object v15, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v13, v14, v15}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->mBlur:Landroid/graphics/MaskFilter;

    .line 233
    return-void

    .line 89
    .end local v2    # "back":Landroid/widget/Button;
    .end local v3    # "color":Landroid/widget/Button;
    .end local v5    # "ll":Landroid/widget/LinearLayout;
    .end local v6    # "llp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "op":Landroid/widget/Button;
    .end local v8    # "r":Landroid/content/res/Resources;
    .end local v9    # "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v10    # "relativeParams2":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v11    # "st":Landroid/widget/Button;
    .end local v12    # "vg":Landroid/view/ViewGroup;
    :cond_0
    new-instance v13, Lru/andrey/notepad/PictureActivity$MyView;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v13, v0, v1}, Lru/andrey/notepad/PictureActivity$MyView;-><init>(Lru/andrey/notepad/PictureActivity;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    goto/16 :goto_0

    .line 229
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 645
    invoke-super {p0, p1, p2, p3}, Lru/andrey/notepad/GraphicsActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 646
    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 647
    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 648
    const v0, 0x7f050047

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 649
    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 650
    const v0, 0x7f050049

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 651
    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 652
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v2, 0x31

    const/4 v6, 0x1

    const/16 v5, 0x34

    const/16 v4, 0x64

    const/4 v3, 0x0

    .line 448
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 450
    const/16 v0, 0xb

    const v1, 0x7f05003f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x68

    invoke-interface {v0, v2, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 452
    const v0, 0x7f050024

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v6, v3, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x63

    invoke-interface {v0, v2, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 453
    const/4 v0, 0x2

    const v1, 0x7f050035

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x73

    invoke-interface {v0, v5, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 455
    const/4 v0, 0x4

    const v1, 0x7f050030

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x32

    const/16 v2, 0x7a

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 457
    const/4 v0, 0x6

    const v1, 0x7f050031

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x33

    const/16 v2, 0x7a

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 458
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 459
    const/16 v0, 0xd

    const v1, 0x7f05005f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 460
    :cond_0
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461
    const/16 v0, 0xe

    const v1, 0x7f050060

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 462
    :cond_1
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v0, :cond_2

    .line 463
    const/4 v0, 0x5

    const v1, 0x7f050026

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 466
    :cond_2
    const/16 v0, 0x9

    const v1, 0x7f050025

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x36

    const/16 v2, 0x6d

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 467
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v0, :cond_3

    .line 468
    const/16 v0, 0xa

    const v1, 0x7f050037

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x37

    invoke-interface {v0, v1, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 470
    :cond_3
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v0, :cond_4

    .line 471
    const/16 v0, 0xc

    const v1, 0x7f05004b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x37

    invoke-interface {v0, v1, v4}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 482
    :cond_4
    return v6
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 14
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 980
    const/4 v10, 0x4

    if-ne p1, v10, :cond_3

    .line 983
    iget-boolean v10, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v10, :cond_2

    .line 985
    const-string v9, "drawing"

    .line 986
    .local v9, "value":Ljava/lang/String;
    new-instance v5, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v5}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 987
    .local v5, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v5, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 988
    iget v10, p0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v5, v10}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 989
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v5, v10}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 990
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v10, "HH:mm MM-dd-yyyy"

    invoke-direct {v7, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 991
    .local v7, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v10, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-direct {v10, v11, v12}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 992
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v10, v5}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 994
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Notepad"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 995
    .local v6, "path":Ljava/lang/String;
    const/4 v0, 0x0

    .line 996
    .local v0, "fOut":Ljava/io/OutputStream;
    new-instance v2, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Drawing"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, p0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".png"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1000
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .local v1, "fOut":Ljava/io/OutputStream;
    :try_start_1
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    # getter for: Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v10}, Lru/andrey/notepad/PictureActivity$MyView;->access$0(Lru/andrey/notepad/PictureActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    # getter for: Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v11}, Lru/andrey/notepad/PictureActivity$MyView;->access$0(Lru/andrey/notepad/PictureActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1001
    .local v8, "toSave":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1003
    .local v4, "mCanvas":Landroid/graphics/Canvas;
    iget-boolean v10, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v10, :cond_0

    .line 1005
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->bg:Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v4, v10, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1008
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v3, v10, :cond_1

    .line 1013
    sget-object v10, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v11, 0x55

    invoke-virtual {v8, v10, v11, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1015
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 1016
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 1023
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v8    # "toSave":Landroid/graphics/Bitmap;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    :goto_1
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity;->finish()V

    .line 1030
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v9    # "value":Ljava/lang/String;
    :goto_2
    const/4 v10, 0x1

    .line 1032
    :goto_3
    return v10

    .line 1010
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "i":I
    .restart local v4    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v8    # "toSave":Landroid/graphics/Bitmap;
    .restart local v9    # "value":Ljava/lang/String;
    :cond_1
    :try_start_2
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Path;

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Paint;

    invoke-virtual {v4, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1008
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1028
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "toSave":Landroid/graphics/Bitmap;
    .end local v9    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity;->showSave()V

    goto :goto_2

    .line 1032
    :cond_3
    invoke-super/range {p0 .. p2}, Lru/andrey/notepad/GraphicsActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v10

    goto :goto_3

    .line 1018
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v9    # "value":Ljava/lang/String;
    :catch_0
    move-exception v10

    goto :goto_1

    .end local v0    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v10

    move-object v0, v1

    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v9, 0x1

    .line 501
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v13}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 502
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    const/16 v11, 0xff

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 504
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 631
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v9

    :goto_0
    :pswitch_0
    return v9

    .line 507
    :pswitch_1
    new-instance v3, Lru/andrey/notepad/AmbilWarnaDialog;

    iget v10, p0, Lru/andrey/notepad/PictureActivity;->color:I

    new-instance v11, Lru/andrey/notepad/PictureActivity$6;

    invoke-direct {v11, p0}, Lru/andrey/notepad/PictureActivity$6;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-direct {v3, p0, v10, v11}, Lru/andrey/notepad/AmbilWarnaDialog;-><init>(Landroid/content/Context;ILru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;)V

    .line 530
    .local v3, "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    invoke-virtual {v3}, Lru/andrey/notepad/AmbilWarnaDialog;->show()V

    goto :goto_0

    .line 533
    .end local v3    # "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    :pswitch_2
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 542
    :pswitch_3
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity;->setIntens()V

    goto :goto_0

    .line 545
    :pswitch_4
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    iget v11, p0, Lru/andrey/notepad/PictureActivity;->color:I

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 546
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v10}, Landroid/graphics/Paint;->getMaskFilter()Landroid/graphics/MaskFilter;

    move-result-object v10

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->mBlur:Landroid/graphics/MaskFilter;

    if-eq v10, v11, :cond_0

    .line 548
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->mBlur:Landroid/graphics/MaskFilter;

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    goto :goto_0

    .line 552
    :cond_0
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v13}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    goto :goto_0

    .line 556
    :pswitch_5
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    invoke-virtual {v10}, Lru/andrey/notepad/PictureActivity$MyView;->eraseMode()V

    goto :goto_0

    .line 559
    :pswitch_6
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    invoke-virtual {p0, v10}, Lru/andrey/notepad/PictureActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 560
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    invoke-virtual {p0, v10}, Lru/andrey/notepad/PictureActivity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 563
    :pswitch_7
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity;->setStroke()V

    goto :goto_0

    .line 566
    :pswitch_8
    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lru/andrey/notepad/DataBaseHelper;->removeObject(Ljava/lang/String;)V

    .line 567
    iput-boolean v9, p0, Lru/andrey/notepad/PictureActivity;->delMode:Z

    .line 568
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity;->finish()V

    goto :goto_0

    .line 571
    :pswitch_9
    new-instance v4, Landroid/app/Dialog;

    invoke-direct {v4, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 572
    .local v4, "dialog1":Landroid/app/Dialog;
    const v10, 0x7f030007

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->setContentView(I)V

    .line 573
    const v10, 0x7f05005f

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->setTitle(I)V

    .line 574
    invoke-virtual {v4, v12}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 575
    const v10, 0x7f070016

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    .line 576
    .local v7, "value":Landroid/widget/EditText;
    const v10, 0x7f07002d

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    .line 578
    .local v8, "value2":Landroid/widget/EditText;
    const v10, 0x7f070028

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 579
    .local v2, "button":Landroid/widget/Button;
    new-instance v10, Lru/andrey/notepad/PictureActivity$7;

    invoke-direct {v10, p0, v7, v8, v4}, Lru/andrey/notepad/PictureActivity$7;-><init>(Lru/andrey/notepad/PictureActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599
    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 602
    .end local v2    # "button":Landroid/widget/Button;
    .end local v4    # "dialog1":Landroid/app/Dialog;
    .end local v7    # "value":Landroid/widget/EditText;
    .end local v8    # "value2":Landroid/widget/EditText;
    :pswitch_a
    new-instance v1, Landroid/content/Intent;

    const-string v10, "android.intent.action.VIEW"

    const-string v11, "market://search?q=pub:Power"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v1, v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 603
    .local v1, "browserIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lru/andrey/notepad/PictureActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 609
    .end local v1    # "browserIntent":Landroid/content/Intent;
    :pswitch_b
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity;->share()V

    goto/16 :goto_0

    .line 612
    :pswitch_c
    new-instance v6, Landroid/content/Intent;

    const-class v10, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v6, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 613
    .local v6, "shortcutIntent":Landroid/content/Intent;
    const-string v10, "new"

    invoke-virtual {v6, v10, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 614
    const-string v10, "name"

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 615
    const-string v10, "body"

    const-string v11, "drawing"

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 616
    const-string v10, "color"

    iget v11, p0, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 617
    const/high16 v10, 0x10000000

    invoke-virtual {v6, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 618
    const/high16 v10, 0x4000000

    invoke-virtual {v6, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 619
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 620
    .local v0, "addIntent":Landroid/content/Intent;
    const-string v10, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 621
    const-string v10, "android.intent.extra.shortcut.NAME"

    iget-object v11, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    const-string v10, "android.intent.extra.shortcut.ICON_RESOURCE"

    const-string v11, "icon64draw"

    invoke-static {p0, v11}, Lru/andrey/notepad/PictureActivity;->getDrawable(Landroid/content/Context;Ljava/lang/String;)I

    move-result v11

    invoke-static {p0, v11}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 623
    const-string v10, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-virtual {v0, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 624
    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 627
    .end local v0    # "addIntent":Landroid/content/Intent;
    .end local v6    # "shortcutIntent":Landroid/content/Intent;
    :pswitch_d
    new-instance v5, Landroid/content/Intent;

    const-class v10, Lru/andrey/notepad/MainActivity;

    invoke-direct {v5, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 628
    .local v5, "in":Landroid/content/Intent;
    invoke-virtual {p0, v5}, Lru/andrey/notepad/PictureActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 504
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_7
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_6
        :pswitch_9
        :pswitch_2
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v6, 0x64

    const/16 v5, 0x34

    const/16 v4, 0xe

    const/16 v3, 0xd

    const/4 v2, 0x0

    .line 488
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 489
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 490
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 491
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 493
    :cond_0
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 495
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 1038
    invoke-super {p0}, Lru/andrey/notepad/GraphicsActivity;->onStop()V

    .line 1039
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 1041
    return-void
.end method

.method public bridge synthetic setContentView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setIntens()V
    .locals 5

    .prologue
    .line 802
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 804
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f050034

    invoke-virtual {p0, v3}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 807
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 809
    .local v1, "linear":Landroid/widget/LinearLayout;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 814
    new-instance v2, Landroid/widget/SeekBar;

    invoke-direct {v2, p0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 815
    .local v2, "seek":Landroid/widget/SeekBar;
    iget v3, p0, Lru/andrey/notepad/PictureActivity;->intens:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 816
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 819
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 821
    const v3, 0x7f050029

    invoke-virtual {p0, v3}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/PictureActivity$10;

    invoke-direct {v4, p0, v2}, Lru/andrey/notepad/PictureActivity$10;-><init>(Lru/andrey/notepad/PictureActivity;Landroid/widget/SeekBar;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 835
    const v3, 0x7f05002a

    invoke-virtual {p0, v3}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/PictureActivity$11;

    invoke-direct {v4, p0}, Lru/andrey/notepad/PictureActivity$11;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 841
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 842
    return-void
.end method

.method public setStroke()V
    .locals 5

    .prologue
    .line 852
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 854
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f050032

    invoke-virtual {p0, v3}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 857
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 859
    .local v1, "linear":Landroid/widget/LinearLayout;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 864
    new-instance v2, Landroid/widget/SeekBar;

    invoke-direct {v2, p0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 865
    .local v2, "seek":Landroid/widget/SeekBar;
    iget v3, p0, Lru/andrey/notepad/PictureActivity;->stroke:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 866
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 869
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 871
    const v3, 0x7f050029

    invoke-virtual {p0, v3}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/PictureActivity$12;

    invoke-direct {v4, p0, v2}, Lru/andrey/notepad/PictureActivity$12;-><init>(Lru/andrey/notepad/PictureActivity;Landroid/widget/SeekBar;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 881
    const v3, 0x7f05002a

    invoke-virtual {p0, v3}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/PictureActivity$13;

    invoke-direct {v4, p0}, Lru/andrey/notepad/PictureActivity$13;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 887
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 888
    return-void
.end method

.method public share()V
    .locals 13

    .prologue
    .line 763
    const-string v8, "NotepadSharing"

    .line 764
    .local v8, "value":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Notepad"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 765
    .local v5, "path":Ljava/lang/String;
    const/4 v0, 0x0

    .line 766
    .local v0, "fOut":Ljava/io/OutputStream;
    new-instance v2, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ".png"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 770
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .local v1, "fOut":Ljava/io/OutputStream;
    :try_start_1
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    # getter for: Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v9}, Lru/andrey/notepad/PictureActivity$MyView;->access$0(Lru/andrey/notepad/PictureActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    # getter for: Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v10}, Lru/andrey/notepad/PictureActivity$MyView;->access$0(Lru/andrey/notepad/PictureActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v10, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 771
    .local v7, "toSave":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 772
    .local v4, "mCanvas":Landroid/graphics/Canvas;
    const/4 v9, -0x1

    invoke-virtual {v4, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 773
    iget-boolean v9, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-nez v9, :cond_0

    .line 775
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->bg:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v4, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 778
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v3, v9, :cond_1

    .line 783
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v10, 0x55

    invoke-virtual {v7, v9, v10, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 785
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 786
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 793
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v7    # "toSave":Landroid/graphics/Bitmap;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    :goto_1
    new-instance v6, Landroid/content/Intent;

    const-string v9, "android.intent.action.SEND"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 794
    .local v6, "share":Landroid/content/Intent;
    const-string v9, "image/png"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 795
    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 796
    const-string v9, "Share Image"

    invoke-static {v6, v9}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {p0, v9}, Lru/andrey/notepad/PictureActivity;->startActivity(Landroid/content/Intent;)V

    .line 798
    return-void

    .line 780
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .end local v6    # "share":Landroid/content/Intent;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v3    # "i":I
    .restart local v4    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v7    # "toSave":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_2
    iget-object v9, p0, Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/Path;

    iget-object v10, p0, Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Paint;

    invoke-virtual {v4, v9, v10}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 778
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 788
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v7    # "toSave":Landroid/graphics/Bitmap;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    :catch_0
    move-exception v9

    goto :goto_1

    .end local v0    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v9

    move-object v0, v1

    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public showSave()V
    .locals 7

    .prologue
    .line 892
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 894
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f050033

    invoke-virtual {p0, v5}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 896
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 898
    .local v3, "linear":Landroid/widget/LinearLayout;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 900
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 902
    .local v2, "edit":Landroid/widget/EditText;
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 905
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 908
    iget-boolean v5, p0, Lru/andrey/notepad/PictureActivity;->isnew:Z

    if-eqz v5, :cond_0

    .line 909
    iget-object v5, p0, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v5}, Lru/andrey/notepad/DataBaseHelper;->getDrawCount()I

    move-result v1

    .line 913
    .local v1, "count":I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Drawing "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 914
    .local v4, "names":Ljava/lang/String;
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 916
    const v5, 0x7f050029

    invoke-virtual {p0, v5}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/PictureActivity$14;

    invoke-direct {v6, p0, v1, v2}, Lru/andrey/notepad/PictureActivity$14;-><init>(Lru/andrey/notepad/PictureActivity;ILandroid/widget/EditText;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 966
    const v5, 0x7f05002a

    invoke-virtual {p0, v5}, Lru/andrey/notepad/PictureActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/PictureActivity$15;

    invoke-direct {v6, p0}, Lru/andrey/notepad/PictureActivity$15;-><init>(Lru/andrey/notepad/PictureActivity;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 974
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 975
    return-void

    .line 911
    .end local v1    # "count":I
    .end local v4    # "names":Ljava/lang/String;
    :cond_0
    iget v5, p0, Lru/andrey/notepad/PictureActivity;->num:I

    add-int/lit8 v1, v5, -0x1

    .restart local v1    # "count":I
    goto :goto_0
.end method
