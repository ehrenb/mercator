.class Lru/andrey/notepad/MainActivity$importTask;
.super Landroid/os/AsyncTask;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "importTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/MainActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/MainActivity;)V
    .locals 0

    .prologue
    .line 1244
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/MainActivity$importTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 1255
    new-instance v1, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/NotepadBackup"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1256
    .local v1, "backFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Notepad"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1257
    .local v0, "appFile":Ljava/io/File;
    new-instance v8, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/NotepadBackup/database.sql"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1259
    .local v8, "logFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1261
    const-string v10, "bad"

    .line 1303
    :goto_0
    return-object v10

    .line 1265
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1268
    .local v9, "sb":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v8}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1271
    .local v2, "br":Ljava/io/BufferedReader;
    :goto_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .local v7, "line":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 1283
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "line":Ljava/lang/String;
    :goto_2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1284
    .local v3, "db":Ljava/lang/String;
    const-string v10, "#SQLITE#"

    invoke-virtual {v3, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1285
    .local v4, "dblist":[Ljava/lang/String;
    const-string v10, "Test"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "dbsize:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v12, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    array-length v10, v4

    if-lt v6, v10, :cond_2

    .line 1291
    const/4 v6, 0x0

    :goto_4
    array-length v10, v4

    if-lt v6, v10, :cond_3

    .line 1300
    iget-object v10, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/data.zip"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lru/andrey/notepad/MainActivity;->UnZip(Ljava/io/File;Ljava/lang/String;)V

    .line 1303
    const-string v10, "suc"

    goto :goto_0

    .line 1274
    .end local v3    # "db":Ljava/lang/String;
    .end local v4    # "dblist":[Ljava/lang/String;
    .end local v6    # "i":I
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v7    # "line":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1278
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "line":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 1280
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1288
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v3    # "db":Ljava/lang/String;
    .restart local v4    # "dblist":[Ljava/lang/String;
    .restart local v6    # "i":I
    :cond_2
    const-string v10, "Test"

    aget-object v11, v4, v6

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1293
    :cond_3
    sget-object v10, Lru/andrey/notepad/MainActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    aget-object v11, v4, v6

    invoke-virtual {v10, v11}, Lru/andrey/notepad/DataBaseHelper;->makeQuery(Ljava/lang/String;)V

    .line 1291
    add-int/lit8 v6, v6, 0x1

    goto :goto_4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/MainActivity$importTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 1309
    iget-object v0, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    iget-object v0, v0, Lru/andrey/notepad/MainActivity;->ImportProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1310
    const-string v0, "bad"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1312
    iget-object v0, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lru/andrey/notepad/MainActivity;->showDialog(I)V

    .line 1331
    :goto_0
    return-void

    .line 1316
    :cond_0
    iget-object v0, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lru/andrey/notepad/MainActivity;->showDialog(I)V

    .line 1317
    sget-object v0, Lru/andrey/notepad/MainActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v0}, Lru/andrey/notepad/DataBaseHelper;->getNotes()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    .line 1319
    sget-boolean v0, Lru/andrey/notepad/MainActivity;->isSearch:Z

    if-eqz v0, :cond_1

    .line 1321
    new-instance v0, Lru/andrey/notepad/ObjectArrayAdapter;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    sget-object v2, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    .line 1329
    :goto_1
    sget-object v0, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 1326
    :cond_1
    new-instance v0, Lru/andrey/notepad/ObjectArrayAdapter;

    iget-object v1, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    sget-object v2, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 1249
    iget-object v0, p0, Lru/andrey/notepad/MainActivity$importTask;->this$0:Lru/andrey/notepad/MainActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lru/andrey/notepad/MainActivity;->showDialog(I)V

    .line 1250
    return-void
.end method
