.class Lru/andrey/notepad/FoldersArrayAdapter$2;
.super Ljava/lang/Object;
.source "FoldersArrayAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FoldersArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/FoldersArrayAdapter;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/FoldersArrayAdapter;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    iput p2, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->val$position:I

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/FoldersArrayAdapter$2;)Lru/andrey/notepad/FoldersArrayAdapter;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 117
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v4}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 118
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v4}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f050033

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 119
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v4}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 120
    .local v2, "linear":Landroid/widget/LinearLayout;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 122
    new-instance v1, Landroid/widget/EditText;

    iget-object v4, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v4}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 124
    .local v1, "edit":Landroid/widget/EditText;
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 127
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 129
    iget-object v4, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v4}, Lru/andrey/notepad/FoldersArrayAdapter;->access$1(Lru/andrey/notepad/FoldersArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    iget v5, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->val$position:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v3

    .line 130
    .local v3, "names":Ljava/lang/String;
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v4, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v4}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f050029

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lru/andrey/notepad/FoldersArrayAdapter$2$1;

    iget v6, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->val$position:I

    invoke-direct {v5, p0, v1, v6}, Lru/andrey/notepad/FoldersArrayAdapter$2$1;-><init>(Lru/andrey/notepad/FoldersArrayAdapter$2;Landroid/widget/EditText;I)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 145
    iget-object v4, p0, Lru/andrey/notepad/FoldersArrayAdapter$2;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v4}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f05002a

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lru/andrey/notepad/FoldersArrayAdapter$2$2;

    invoke-direct {v5, p0}, Lru/andrey/notepad/FoldersArrayAdapter$2$2;-><init>(Lru/andrey/notepad/FoldersArrayAdapter$2;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 151
    return-void
.end method
