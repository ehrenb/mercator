.class Lru/andrey/notepad/BuyActivity$8;
.super Ljava/lang/Object;
.source "BuyActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/BuyActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/BuyActivity;

.field private final synthetic val$bm:Lru/andrey/notepad/BuyModel;

.field private final synthetic val$count:Landroid/widget/EditText;

.field private final synthetic val$name:Landroid/widget/EditText;

.field private final synthetic val$price:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/BuyActivity;Landroid/widget/EditText;Lru/andrey/notepad/BuyModel;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/BuyActivity$8;->this$0:Lru/andrey/notepad/BuyActivity;

    iput-object p2, p0, Lru/andrey/notepad/BuyActivity$8;->val$name:Landroid/widget/EditText;

    iput-object p3, p0, Lru/andrey/notepad/BuyActivity$8;->val$bm:Lru/andrey/notepad/BuyModel;

    iput-object p4, p0, Lru/andrey/notepad/BuyActivity$8;->val$price:Landroid/widget/EditText;

    iput-object p5, p0, Lru/andrey/notepad/BuyActivity$8;->val$count:Landroid/widget/EditText;

    .line 435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 439
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$name:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_0

    .line 441
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$bm:Lru/andrey/notepad/BuyModel;

    iget-object v1, p0, Lru/andrey/notepad/BuyActivity$8;->val$name:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/BuyModel;->setName(Ljava/lang/String;)V

    .line 442
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$price:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_1

    .line 443
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$bm:Lru/andrey/notepad/BuyModel;

    iget-object v1, p0, Lru/andrey/notepad/BuyActivity$8;->val$price:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/BuyModel;->setPrice(I)V

    .line 446
    :goto_0
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$count:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_2

    .line 447
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$bm:Lru/andrey/notepad/BuyModel;

    iget-object v1, p0, Lru/andrey/notepad/BuyActivity$8;->val$count:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/BuyModel;->setQuantity(I)V

    .line 450
    :goto_1
    sget-object v0, Lru/andrey/notepad/BuyActivity;->arr:Lru/andrey/notepad/BoughtArrayAdapter;

    invoke-virtual {v0}, Lru/andrey/notepad/BoughtArrayAdapter;->notifyDataSetChanged()V

    .line 451
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->this$0:Lru/andrey/notepad/BuyActivity;

    invoke-virtual {v0, v2}, Lru/andrey/notepad/BuyActivity;->removeDialog(I)V

    .line 453
    :cond_0
    return-void

    .line 445
    :cond_1
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$bm:Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0, v3}, Lru/andrey/notepad/BuyModel;->setPrice(I)V

    goto :goto_0

    .line 449
    :cond_2
    iget-object v0, p0, Lru/andrey/notepad/BuyActivity$8;->val$bm:Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0, v3}, Lru/andrey/notepad/BuyModel;->setQuantity(I)V

    goto :goto_1
.end method
