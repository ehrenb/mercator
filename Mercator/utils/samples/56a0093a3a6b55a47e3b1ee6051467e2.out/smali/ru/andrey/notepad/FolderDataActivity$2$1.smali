.class Lru/andrey/notepad/FolderDataActivity$2$1;
.super Ljava/lang/Object;
.source "FolderDataActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderDataActivity$2;->onTextChanged(Ljava/lang/CharSequence;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FolderDataActivity$2;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity$2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/FolderDataActivity$2$1;)Lru/andrey/notepad/FolderDataActivity$2;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 237
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 239
    new-instance v1, Landroid/app/Dialog;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 240
    .local v1, "dialog":Landroid/app/Dialog;
    const v4, 0x7f030006

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 241
    const v4, 0x7f050066

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setTitle(I)V

    .line 242
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 243
    const v4, 0x7f070016

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 244
    .local v3, "value":Landroid/widget/EditText;
    const v4, 0x7f070028

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 245
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lru/andrey/notepad/FolderDataActivity$2$1$1;

    invoke-direct {v4, p0, v3, p3, v1}, Lru/andrey/notepad/FolderDataActivity$2$1$1;-><init>(Lru/andrey/notepad/FolderDataActivity$2$1;Landroid/widget/EditText;ILandroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 409
    .end local v0    # "button":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v3    # "value":Landroid/widget/EditText;
    :goto_0
    return-void

    .line 336
    :cond_0
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawing"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 338
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 339
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 340
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 343
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 345
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Camera"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 347
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 348
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 349
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 352
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 354
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PhotoText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 356
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 357
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 358
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 360
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 361
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 363
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GalleryText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 365
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 366
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 367
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 368
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 370
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 372
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Record"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 374
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 375
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 376
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 379
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 381
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Shop"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 383
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 384
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 385
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 386
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 388
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 390
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Video"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 392
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 393
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 394
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 397
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 401
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/AddActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 402
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 403
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 406
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$2$1;->this$1:Lru/andrey/notepad/FolderDataActivity$2;

    # getter for: Lru/andrey/notepad/FolderDataActivity$2;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v4}, Lru/andrey/notepad/FolderDataActivity$2;->access$0(Lru/andrey/notepad/FolderDataActivity$2;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
