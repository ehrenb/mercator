.class Lru/andrey/notepad/PictureActivity$14;
.super Ljava/lang/Object;
.source "PictureActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/PictureActivity;->showSave()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/PictureActivity;

.field private final synthetic val$count:I

.field private final synthetic val$edit:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/PictureActivity;ILandroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    iput p2, p0, Lru/andrey/notepad/PictureActivity$14;->val$count:I

    iput-object p3, p0, Lru/andrey/notepad/PictureActivity$14;->val$edit:Landroid/widget/EditText;

    .line 916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 14
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 921
    const-string v10, "drawing"

    .line 922
    .local v10, "value":Ljava/lang/String;
    new-instance v6, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v6}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 923
    .local v6, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v6, v10}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 924
    iget v11, p0, Lru/andrey/notepad/PictureActivity$14;->val$count:I

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v6, v11}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 925
    iget-object v11, p0, Lru/andrey/notepad/PictureActivity$14;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-interface {v11}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 926
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v11, "HH:mm MM-dd-yyyy"

    invoke-direct {v8, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 927
    .local v8, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v11, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 928
    iget-object v11, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v11, v11, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v11, v6}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 930
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Notepad"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 931
    .local v7, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 932
    .local v1, "fOut":Ljava/io/OutputStream;
    new-instance v3, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Drawing"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lru/andrey/notepad/PictureActivity$14;->val$count:I

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".png"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v3, v7, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    .local v3, "file":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 937
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .local v2, "fOut":Ljava/io/OutputStream;
    :try_start_1
    iget-object v11, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v11, v11, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    # getter for: Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v11}, Lru/andrey/notepad/PictureActivity$MyView;->access$0(Lru/andrey/notepad/PictureActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    iget-object v12, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v12, v12, Lru/andrey/notepad/PictureActivity;->mv:Lru/andrey/notepad/PictureActivity$MyView;

    # getter for: Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v12}, Lru/andrey/notepad/PictureActivity$MyView;->access$0(Lru/andrey/notepad/PictureActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v12, v13}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 938
    .local v9, "toSave":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 945
    .local v5, "mCanvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v11, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v11}, Lru/andrey/notepad/PictureActivity;->access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lt v4, v11, :cond_0

    .line 950
    sget-object v11, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v12, 0x55

    invoke-virtual {v9, v11, v12, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 952
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 953
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 962
    .end local v2    # "fOut":Ljava/io/OutputStream;
    .end local v4    # "i":I
    .end local v5    # "mCanvas":Landroid/graphics/Canvas;
    .end local v9    # "toSave":Landroid/graphics/Bitmap;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :goto_1
    iget-object v11, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    invoke-virtual {v11}, Lru/andrey/notepad/PictureActivity;->finish()V

    .line 963
    return-void

    .line 947
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "fOut":Ljava/io/OutputStream;
    .restart local v4    # "i":I
    .restart local v5    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v9    # "toSave":Landroid/graphics/Bitmap;
    :cond_0
    :try_start_2
    iget-object v11, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v11}, Lru/andrey/notepad/PictureActivity;->access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Path;

    iget-object v12, p0, Lru/andrey/notepad/PictureActivity$14;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v12}, Lru/andrey/notepad/PictureActivity;->access$3(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/Paint;

    invoke-virtual {v5, v11, v12}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 945
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 956
    .end local v2    # "fOut":Ljava/io/OutputStream;
    .end local v4    # "i":I
    .end local v5    # "mCanvas":Landroid/graphics/Canvas;
    .end local v9    # "toSave":Landroid/graphics/Bitmap;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 959
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 956
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "fOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    goto :goto_2
.end method
