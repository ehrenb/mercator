.class public Lru/andrey/notepad/VideoActivity$saveTask;
.super Landroid/os/AsyncTask;
.source "VideoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/VideoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "saveTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/VideoActivity;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/VideoActivity;)V
    .locals 0

    .prologue
    .line 746
    iput-object p1, p0, Lru/andrey/notepad/VideoActivity$saveTask;->this$0:Lru/andrey/notepad/VideoActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/VideoActivity$saveTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # [Ljava/lang/String;

    .prologue
    .line 760
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lru/andrey/notepad/VideoActivity$saveTask;->this$0:Lru/andrey/notepad/VideoActivity;

    iget-object v5, v5, Lru/andrey/notepad/VideoActivity;->name:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".mp4"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 761
    .local v3, "value":Ljava/lang/String;
    iget-object v4, p0, Lru/andrey/notepad/VideoActivity$saveTask;->this$0:Lru/andrey/notepad/VideoActivity;

    iget-object v1, v4, Lru/andrey/notepad/VideoActivity;->audioTmp:Ljava/io/File;

    .line 762
    .local v1, "from":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Notepad/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 763
    .local v2, "to":Ljava/io/File;
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 770
    const/4 v4, 0x0

    .end local v1    # "from":Ljava/io/File;
    .end local v2    # "to":Ljava/io/File;
    .end local v3    # "value":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 765
    :catch_0
    move-exception v0

    .line 767
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 768
    const-string v4, "err"

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/VideoActivity$saveTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 777
    iget-object v0, p0, Lru/andrey/notepad/VideoActivity$saveTask;->this$0:Lru/andrey/notepad/VideoActivity;

    invoke-virtual {v0}, Lru/andrey/notepad/VideoActivity;->finish()V

    .line 778
    iget-object v0, p0, Lru/andrey/notepad/VideoActivity$saveTask;->this$0:Lru/andrey/notepad/VideoActivity;

    iget-object v0, v0, Lru/andrey/notepad/VideoActivity;->LoadProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 780
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 752
    iget-object v0, p0, Lru/andrey/notepad/VideoActivity$saveTask;->this$0:Lru/andrey/notepad/VideoActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/VideoActivity;->showDialog(I)V

    .line 753
    return-void
.end method
