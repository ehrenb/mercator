.class public Lru/andrey/notepad/FolderActivity;
.super Landroid/app/Activity;
.source "FolderActivity.java"


# static fields
.field static arr:Lru/andrey/notepad/FoldersArrayAdapter;

.field static ctx:Landroid/content/Context;

.field static dh:Lru/andrey/notepad/DataBaseHelper;

.field static folders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field

.field static lv:Landroid/widget/ListView;


# instance fields
.field add:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static update()V
    .locals 3

    .prologue
    .line 91
    sget-object v0, Lru/andrey/notepad/FolderActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v0}, Lru/andrey/notepad/DataBaseHelper;->getFolders()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    .line 92
    sget-object v0, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 93
    new-instance v1, Lru/andrey/notepad/FoldersArrayAdapter;

    sget-object v0, Lru/andrey/notepad/FolderActivity;->ctx:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    sget-object v2, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    invoke-direct {v1, v0, v2}, Lru/andrey/notepad/FoldersArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/FolderActivity;->arr:Lru/andrey/notepad/FoldersArrayAdapter;

    .line 94
    sget-object v0, Lru/andrey/notepad/FolderActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/FolderActivity;->arr:Lru/andrey/notepad/FoldersArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v1, 0x7f03000c

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderActivity;->setContentView(I)V

    .line 39
    const v1, 0x7f070015

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/AdView;

    .line 40
    .local v0, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 41
    sput-object p0, Lru/andrey/notepad/FolderActivity;->ctx:Landroid/content/Context;

    .line 42
    const v1, 0x7f070017

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    sput-object v1, Lru/andrey/notepad/FolderActivity;->lv:Landroid/widget/ListView;

    .line 43
    const v1, 0x7f070028

    invoke-virtual {p0, v1}, Lru/andrey/notepad/FolderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lru/andrey/notepad/FolderActivity;->add:Landroid/widget/Button;

    .line 44
    invoke-static {p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v1

    sput-object v1, Lru/andrey/notepad/FolderActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 46
    iget-object v1, p0, Lru/andrey/notepad/FolderActivity;->add:Landroid/widget/Button;

    new-instance v2, Lru/andrey/notepad/FolderActivity$1;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderActivity$1;-><init>(Lru/andrey/notepad/FolderActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    sget-object v1, Lru/andrey/notepad/FolderActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v1}, Lru/andrey/notepad/DataBaseHelper;->getFolders()Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    .line 74
    new-instance v1, Lru/andrey/notepad/FoldersArrayAdapter;

    sget-object v2, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/FoldersArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/FolderActivity;->arr:Lru/andrey/notepad/FoldersArrayAdapter;

    .line 75
    sget-object v1, Lru/andrey/notepad/FolderActivity;->lv:Landroid/widget/ListView;

    sget-object v2, Lru/andrey/notepad/FolderActivity;->arr:Lru/andrey/notepad/FoldersArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 77
    sget-object v1, Lru/andrey/notepad/FolderActivity;->lv:Landroid/widget/ListView;

    new-instance v2, Lru/andrey/notepad/FolderActivity$2;

    invoke-direct {v2, p0}, Lru/andrey/notepad/FolderActivity$2;-><init>(Lru/andrey/notepad/FolderActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 87
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 101
    sget-object v0, Lru/andrey/notepad/FolderActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v0}, Lru/andrey/notepad/DataBaseHelper;->getFolders()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    .line 102
    sget-object v0, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 103
    new-instance v0, Lru/andrey/notepad/FoldersArrayAdapter;

    sget-object v1, Lru/andrey/notepad/FolderActivity;->folders:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lru/andrey/notepad/FoldersArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/FolderActivity;->arr:Lru/andrey/notepad/FoldersArrayAdapter;

    .line 104
    sget-object v0, Lru/andrey/notepad/FolderActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/FolderActivity;->arr:Lru/andrey/notepad/FoldersArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 105
    return-void
.end method
