.class Lru/andrey/notepad/BoughtArrayAdapter$3;
.super Ljava/lang/Object;
.source "BoughtArrayAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/BoughtArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/BoughtArrayAdapter;

.field private final synthetic val$dh:Lru/andrey/notepad/DataBaseHelper;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/BoughtArrayAdapter;Lru/andrey/notepad/DataBaseHelper;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->this$0:Lru/andrey/notepad/BoughtArrayAdapter;

    iput-object p2, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    iput p3, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->val$position:I

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 114
    iget-object v1, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->this$0:Lru/andrey/notepad/BoughtArrayAdapter;

    # getter for: Lru/andrey/notepad/BoughtArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/BoughtArrayAdapter;->access$0(Lru/andrey/notepad/BoughtArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->val$position:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0}, Lru/andrey/notepad/BuyModel;->getListId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->this$0:Lru/andrey/notepad/BoughtArrayAdapter;

    # getter for: Lru/andrey/notepad/BoughtArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/BoughtArrayAdapter;->access$0(Lru/andrey/notepad/BoughtArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    iget v3, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->val$position:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0}, Lru/andrey/notepad/BuyModel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lru/andrey/notepad/DataBaseHelper;->removeBuy(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->this$0:Lru/andrey/notepad/BoughtArrayAdapter;

    # getter for: Lru/andrey/notepad/BoughtArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/BoughtArrayAdapter;->access$0(Lru/andrey/notepad/BoughtArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lru/andrey/notepad/BoughtArrayAdapter$3;->val$position:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 116
    invoke-static {}, Lru/andrey/notepad/BuyActivity;->refresh()V

    .line 117
    return-void
.end method
