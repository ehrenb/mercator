.class Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;
.super Ljava/lang/Object;
.source "FolderObjectArrayAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderObjectArrayAdapter$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$2;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderObjectArrayAdapter$2;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;->this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$2;

    iput p2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;->val$position:I

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 160
    iget-object v1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;->this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$2;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter$2;)Lru/andrey/notepad/FolderObjectArrayAdapter;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 161
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;->this$1:Lru/andrey/notepad/FolderObjectArrayAdapter$2;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter$2;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter$2;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter$2;)Lru/andrey/notepad/FolderObjectArrayAdapter;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$1(Lru/andrey/notepad/FolderObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$2$1;->val$position:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lru/andrey/notepad/FolderDataActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lru/andrey/notepad/DataBaseHelper;->removeFolderData(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lru/andrey/notepad/FolderDataActivity;->update()V

    .line 163
    return-void
.end method
