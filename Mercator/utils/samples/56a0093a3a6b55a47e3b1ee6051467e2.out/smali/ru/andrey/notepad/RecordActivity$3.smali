.class Lru/andrey/notepad/RecordActivity$3;
.super Ljava/lang/Object;
.source "RecordActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/RecordActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/RecordActivity;

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$dp:Landroid/widget/DatePicker;

.field private final synthetic val$tp:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Lru/andrey/notepad/RecordActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/RecordActivity$3;->this$0:Lru/andrey/notepad/RecordActivity;

    iput-object p2, p0, Lru/andrey/notepad/RecordActivity$3;->val$dp:Landroid/widget/DatePicker;

    iput-object p3, p0, Lru/andrey/notepad/RecordActivity$3;->val$tp:Landroid/widget/TimePicker;

    iput-object p4, p0, Lru/andrey/notepad/RecordActivity$3;->val$dialog:Landroid/app/Dialog;

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 228
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 229
    .local v0, "c":Ljava/util/Calendar;
    const/4 v4, 0x5

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity$3;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 230
    const/4 v4, 0x2

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity$3;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 231
    const/4 v4, 0x1

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity$3;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getYear()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 232
    const/16 v4, 0xb

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity$3;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 233
    const/16 v4, 0xc

    iget-object v5, p0, Lru/andrey/notepad/RecordActivity$3;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 235
    iget-object v4, p0, Lru/andrey/notepad/RecordActivity$3;->this$0:Lru/andrey/notepad/RecordActivity;

    iget-object v4, v4, Lru/andrey/notepad/RecordActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 236
    .local v3, "t":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    .line 237
    const-string v3, " "

    .line 239
    :cond_0
    new-instance v1, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v1}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 240
    .local v1, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Record/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 241
    iget-object v4, p0, Lru/andrey/notepad/RecordActivity$3;->this$0:Lru/andrey/notepad/RecordActivity;

    iget v4, v4, Lru/andrey/notepad/RecordActivity;->num:I

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 242
    iget-object v4, p0, Lru/andrey/notepad/RecordActivity$3;->this$0:Lru/andrey/notepad/RecordActivity;

    iget-object v4, v4, Lru/andrey/notepad/RecordActivity;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 243
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 244
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 245
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 246
    iget-object v4, p0, Lru/andrey/notepad/RecordActivity$3;->this$0:Lru/andrey/notepad/RecordActivity;

    iget-object v4, v4, Lru/andrey/notepad/RecordActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v1}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 248
    iget-object v4, p0, Lru/andrey/notepad/RecordActivity$3;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->dismiss()V

    .line 250
    return-void
.end method
