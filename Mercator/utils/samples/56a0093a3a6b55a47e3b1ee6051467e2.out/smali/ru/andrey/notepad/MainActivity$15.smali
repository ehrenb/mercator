.class Lru/andrey/notepad/MainActivity$15;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/MainActivity;->find(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/MainActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    .line 1481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/MainActivity$15;)Lru/andrey/notepad/MainActivity;
    .locals 1

    .prologue
    .line 1481
    iget-object v0, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 1486
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1488
    new-instance v1, Landroid/app/Dialog;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-direct {v1, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1489
    .local v1, "dialog":Landroid/app/Dialog;
    const v4, 0x7f030006

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 1490
    const v4, 0x7f050066

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setTitle(I)V

    .line 1491
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1492
    const v4, 0x7f070016

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 1493
    .local v3, "value":Landroid/widget/EditText;
    const v4, 0x7f070028

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1494
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lru/andrey/notepad/MainActivity$15$1;

    invoke-direct {v4, p0, v3, p3, v1}, Lru/andrey/notepad/MainActivity$15$1;-><init>(Lru/andrey/notepad/MainActivity$15;Landroid/widget/EditText;ILandroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1581
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 1658
    .end local v0    # "button":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v3    # "value":Landroid/widget/EditText;
    :goto_0
    return-void

    .line 1585
    :cond_0
    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawing"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1587
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1588
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1589
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1590
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1591
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1592
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1594
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Camera"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1596
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1597
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1598
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1599
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1600
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1601
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1603
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PhotoText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1605
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1606
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1607
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1608
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1609
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1610
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1612
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GalleryText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1614
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1615
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1616
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1617
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1618
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1619
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1621
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Record"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1623
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1624
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1625
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1626
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1627
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1628
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1630
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Shop"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1632
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1633
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1634
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1635
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1636
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1637
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1639
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Video"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1641
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1642
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1643
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1644
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1645
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1646
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1650
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    const-class v5, Lru/andrey/notepad/AddActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1651
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1652
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1653
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1654
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1655
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$15;->this$0:Lru/andrey/notepad/MainActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
