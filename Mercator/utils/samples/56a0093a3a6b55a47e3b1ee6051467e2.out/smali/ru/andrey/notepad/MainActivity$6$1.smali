.class Lru/andrey/notepad/MainActivity$6$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/MainActivity$6;->onTextChanged(Ljava/lang/CharSequence;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/MainActivity$6;


# direct methods
.method constructor <init>(Lru/andrey/notepad/MainActivity$6;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/MainActivity$6$1;)Lru/andrey/notepad/MainActivity$6;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 308
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 310
    new-instance v1, Landroid/app/Dialog;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 311
    .local v1, "dialog":Landroid/app/Dialog;
    const v4, 0x7f030006

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 312
    const v4, 0x7f050066

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setTitle(I)V

    .line 313
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 314
    const v4, 0x7f070016

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 315
    .local v3, "value":Landroid/widget/EditText;
    const v4, 0x7f070028

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 316
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lru/andrey/notepad/MainActivity$6$1$1;

    invoke-direct {v4, p0, v3, p3, v1}, Lru/andrey/notepad/MainActivity$6$1$1;-><init>(Lru/andrey/notepad/MainActivity$6$1;Landroid/widget/EditText;ILandroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 480
    .end local v0    # "button":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v3    # "value":Landroid/widget/EditText;
    :goto_0
    return-void

    .line 407
    :cond_0
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawing"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 409
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 410
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 411
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 413
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 414
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 416
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Camera"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 418
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 419
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 420
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 422
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 423
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 425
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PhotoText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 427
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 428
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 429
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 430
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 431
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 432
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 434
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GalleryText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 436
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 437
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 438
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 441
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 443
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Record"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 445
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 446
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 447
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 450
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 452
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Shop"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 454
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 455
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 456
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 458
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 459
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 461
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Video"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 463
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 464
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 465
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 468
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 472
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    const-class v5, Lru/andrey/notepad/AddActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 473
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 474
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 476
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 477
    iget-object v4, p0, Lru/andrey/notepad/MainActivity$6$1;->this$1:Lru/andrey/notepad/MainActivity$6;

    # getter for: Lru/andrey/notepad/MainActivity$6;->this$0:Lru/andrey/notepad/MainActivity;
    invoke-static {v4}, Lru/andrey/notepad/MainActivity$6;->access$0(Lru/andrey/notepad/MainActivity$6;)Lru/andrey/notepad/MainActivity;

    move-result-object v4

    invoke-virtual {v4, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
