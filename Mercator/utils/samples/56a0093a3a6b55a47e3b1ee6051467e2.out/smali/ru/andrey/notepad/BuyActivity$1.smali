.class Lru/andrey/notepad/BuyActivity$1;
.super Ljava/lang/Object;
.source "BuyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/BuyActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/BuyActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/BuyActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/BuyActivity$1;->this$0:Lru/andrey/notepad/BuyActivity;

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 74
    iget-object v1, p0, Lru/andrey/notepad/BuyActivity$1;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v1, v1, Lru/andrey/notepad/BuyActivity;->itemName:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    .line 76
    new-instance v0, Lru/andrey/notepad/BuyModel;

    invoke-direct {v0}, Lru/andrey/notepad/BuyModel;-><init>()V

    .line 77
    .local v0, "bm":Lru/andrey/notepad/BuyModel;
    iget-object v1, p0, Lru/andrey/notepad/BuyActivity$1;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v1, v1, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/andrey/notepad/BuyModel;->setListId(Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lru/andrey/notepad/BuyActivity$1;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v1, v1, Lru/andrey/notepad/BuyActivity;->itemName:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/BuyModel;->setName(Ljava/lang/String;)V

    .line 79
    sget-object v1, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v1, Lru/andrey/notepad/BuyActivity;->arr:Lru/andrey/notepad/BoughtArrayAdapter;

    invoke-virtual {v1}, Lru/andrey/notepad/BoughtArrayAdapter;->notifyDataSetChanged()V

    .line 81
    iget-object v1, p0, Lru/andrey/notepad/BuyActivity$1;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v1, v1, Lru/andrey/notepad/BuyActivity;->itemName:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 83
    .end local v0    # "bm":Lru/andrey/notepad/BuyModel;
    :cond_0
    return-void
.end method
