.class public Lru/andrey/notepad/CameraActivity$MyView;
.super Landroid/view/View;
.source "CameraActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/CameraActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# static fields
.field private static final MAXP:F = 0.75f

.field private static final MINP:F = 0.25f

.field private static final TOUCH_TOLERANCE:F = 4.0f


# instance fields
.field private background:Landroid/graphics/Bitmap;

.field drawed:Z

.field isnew:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBitmapPaint:Landroid/graphics/Paint;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mPath:Landroid/graphics/Path;

.field private mX:F

.field private mY:F

.field final synthetic this$0:Lru/andrey/notepad/CameraActivity;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/CameraActivity;Landroid/content/Context;)V
    .locals 2
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 315
    iput-object p1, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    .line 316
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->isnew:Z

    .line 312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->drawed:Z

    .line 317
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    .line 318
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 319
    return-void
.end method

.method public constructor <init>(Lru/andrey/notepad/CameraActivity;Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "b"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 322
    iput-object p1, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    .line 323
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 311
    iput-boolean v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->isnew:Z

    .line 312
    iput-boolean v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->drawed:Z

    .line 324
    iput-object p3, p0, Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 325
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->background:Landroid/graphics/Bitmap;

    .line 326
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    .line 327
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 328
    iput-boolean v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->isnew:Z

    .line 329
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/CameraActivity$MyView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private overlay(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "bmp1"    # Landroid/graphics/Bitmap;
    .param p2, "bmp2"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x0

    .line 413
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 414
    .local v0, "bmOverlay":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 415
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v1, p1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 416
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v1, p2, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 417
    return-object v0
.end method

.method private touch_move(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v3, 0x40800000    # 4.0f

    const/high16 v7, 0x40000000    # 2.0f

    .line 439
    iget v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->mX:F

    sub-float v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 440
    .local v0, "dx":F
    iget v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->mY:F

    sub-float v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 441
    .local v1, "dy":F
    cmpl-float v2, v0, v3

    if-gez v2, :cond_0

    cmpl-float v2, v1, v3

    if-ltz v2, :cond_1

    .line 443
    :cond_0
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    iget v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->mX:F

    iget v4, p0, Lru/andrey/notepad/CameraActivity$MyView;->mY:F

    iget v5, p0, Lru/andrey/notepad/CameraActivity$MyView;->mX:F

    add-float/2addr v5, p1

    div-float/2addr v5, v7

    iget v6, p0, Lru/andrey/notepad/CameraActivity$MyView;->mY:F

    add-float/2addr v6, p2

    div-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 444
    iput p1, p0, Lru/andrey/notepad/CameraActivity$MyView;->mX:F

    .line 445
    iput p2, p0, Lru/andrey/notepad/CameraActivity$MyView;->mY:F

    .line 447
    :cond_1
    return-void
.end method

.method private touch_start(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 431
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 432
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 433
    iput p1, p0, Lru/andrey/notepad/CameraActivity$MyView;->mX:F

    .line 434
    iput p2, p0, Lru/andrey/notepad/CameraActivity$MyView;->mY:F

    .line 435
    return-void
.end method

.method private touch_up()V
    .locals 3

    .prologue
    .line 451
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    iget v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->mX:F

    iget v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->mY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 452
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 453
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/CameraActivity;->access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/CameraActivity;->access$3(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity$MyView;->generatePaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-static {v0, v1}, Lru/andrey/notepad/CameraActivity;->access$4(Lru/andrey/notepad/CameraActivity;Landroid/graphics/Paint;)V

    .line 456
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    .line 457
    return-void
.end method


# virtual methods
.method public backAction()V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/CameraActivity;->access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/CameraActivity;->access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 423
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/CameraActivity;->access$3(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/CameraActivity;->access$3(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 424
    return-void
.end method

.method public eraseMode()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-boolean v0, v0, Lru/andrey/notepad/CameraActivity;->erase:Z

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    const v1, 0x7f020038

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 336
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    iput v1, v0, Lru/andrey/notepad/CameraActivity;->color:I

    .line 337
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 338
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lru/andrey/notepad/CameraActivity;->erase:Z

    .line 347
    :goto_0
    return-void

    .line 342
    :cond_0
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    const v1, 0x7f020043

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 343
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lru/andrey/notepad/CameraActivity;->erase:Z

    .line 344
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget v1, v1, Lru/andrey/notepad/CameraActivity;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method public generatePaint()Landroid/graphics/Paint;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 351
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 352
    .local v2, "paint":Landroid/graphics/Paint;
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 353
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 354
    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-boolean v3, v3, Lru/andrey/notepad/CameraActivity;->erase:Z

    if-eqz v3, :cond_0

    .line 356
    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v3, v3, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    const v4, 0x7f020038

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 357
    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    iput v4, v3, Lru/andrey/notepad/CameraActivity;->color:I

    .line 358
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 365
    :goto_0
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 366
    sget-object v3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 367
    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 368
    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget v3, v3, Lru/andrey/notepad/CameraActivity;->stroke:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "calc1":I
    const/4 v1, 0x0

    .line 371
    .local v1, "calc2":I
    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget v0, v3, Lru/andrey/notepad/CameraActivity;->intens:I

    .line 372
    mul-int/lit8 v1, v0, 0x2

    .line 373
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 375
    return-object v2

    .line 362
    .end local v0    # "calc1":I
    .end local v1    # "calc2":I
    :cond_0
    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v3, v3, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    const v4, 0x7f020043

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 363
    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget v3, v3, Lru/andrey/notepad/CameraActivity;->color:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method public getBmp()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 394
    iget-boolean v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->isnew:Z

    if-nez v2, :cond_0

    .line 396
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->background:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v4, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 404
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity;->access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 408
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->mPath:Landroid/graphics/Path;

    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v3}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 409
    return-void

    .line 400
    .end local v0    # "i":I
    :cond_0
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    .line 401
    .local v1, "paint":Landroid/graphics/Paint;
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v2, v2, Lru/andrey/notepad/CameraActivity;->photoBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v2, v4, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 406
    .end local v1    # "paint":Landroid/graphics/Paint;
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity;->access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Path;

    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$MyView;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v3}, Lru/andrey/notepad/CameraActivity;->access$3(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 404
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 381
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 382
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->isnew:Z

    if-eqz v0, :cond_0

    .line 383
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 384
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity$MyView;->mCanvas:Landroid/graphics/Canvas;

    .line 386
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 462
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 463
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 465
    .local v1, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 480
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 468
    :pswitch_0
    invoke-direct {p0, v0, v1}, Lru/andrey/notepad/CameraActivity$MyView;->touch_start(FF)V

    .line 469
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity$MyView;->invalidate()V

    goto :goto_0

    .line 472
    :pswitch_1
    invoke-direct {p0, v0, v1}, Lru/andrey/notepad/CameraActivity$MyView;->touch_move(FF)V

    .line 473
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity$MyView;->invalidate()V

    goto :goto_0

    .line 476
    :pswitch_2
    invoke-direct {p0}, Lru/andrey/notepad/CameraActivity$MyView;->touch_up()V

    .line 477
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity$MyView;->invalidate()V

    goto :goto_0

    .line 465
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
