.class public Lru/andrey/notepad/PictureActivity$MyView;
.super Landroid/view/View;
.source "PictureActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/PictureActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# static fields
.field private static final MAXP:F = 0.75f

.field private static final MINP:F = 0.25f

.field private static final TOUCH_TOLERANCE:F = 4.0f


# instance fields
.field private background:Landroid/graphics/Bitmap;

.field drawed:Z

.field isnew:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBitmapPaint:Landroid/graphics/Paint;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mPath:Landroid/graphics/Path;

.field private mX:F

.field private mY:F

.field final synthetic this$0:Lru/andrey/notepad/PictureActivity;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/PictureActivity;Landroid/content/Context;)V
    .locals 2
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 269
    iput-object p1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    .line 270
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 265
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->isnew:Z

    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->drawed:Z

    .line 271
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    .line 272
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 273
    return-void
.end method

.method public constructor <init>(Lru/andrey/notepad/PictureActivity;Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "b"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 276
    iput-object p1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    .line 277
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 265
    iput-boolean v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->isnew:Z

    .line 266
    iput-boolean v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->drawed:Z

    .line 278
    iput-object p3, p0, Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 279
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->background:Landroid/graphics/Bitmap;

    .line 280
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    .line 281
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 282
    iput-boolean v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->isnew:Z

    .line 283
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/PictureActivity$MyView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private touch_move(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v3, 0x40800000    # 4.0f

    const/high16 v7, 0x40000000    # 2.0f

    .line 378
    iget v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->mX:F

    sub-float v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 379
    .local v0, "dx":F
    iget v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->mY:F

    sub-float v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 380
    .local v1, "dy":F
    cmpl-float v2, v0, v3

    if-gez v2, :cond_0

    cmpl-float v2, v1, v3

    if-ltz v2, :cond_1

    .line 382
    :cond_0
    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    iget v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->mX:F

    iget v4, p0, Lru/andrey/notepad/PictureActivity$MyView;->mY:F

    iget v5, p0, Lru/andrey/notepad/PictureActivity$MyView;->mX:F

    add-float/2addr v5, p1

    div-float/2addr v5, v7

    iget v6, p0, Lru/andrey/notepad/PictureActivity$MyView;->mY:F

    add-float/2addr v6, p2

    div-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 383
    iput p1, p0, Lru/andrey/notepad/PictureActivity$MyView;->mX:F

    .line 384
    iput p2, p0, Lru/andrey/notepad/PictureActivity$MyView;->mY:F

    .line 386
    :cond_1
    return-void
.end method

.method private touch_start(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 370
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 371
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 372
    iput p1, p0, Lru/andrey/notepad/PictureActivity$MyView;->mX:F

    .line 373
    iput p2, p0, Lru/andrey/notepad/PictureActivity$MyView;->mY:F

    .line 374
    return-void
.end method

.method private touch_up()V
    .locals 3

    .prologue
    .line 390
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    iget v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->mX:F

    iget v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->mY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 391
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 392
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/PictureActivity;->access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/PictureActivity;->access$3(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity$MyView;->generatePaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-static {v0, v1}, Lru/andrey/notepad/PictureActivity;->access$4(Lru/andrey/notepad/PictureActivity;Landroid/graphics/Paint;)V

    .line 395
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    .line 396
    return-void
.end method


# virtual methods
.method public backAction()V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/PictureActivity;->access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/PictureActivity;->access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 362
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v0}, Lru/andrey/notepad/PictureActivity;->access$3(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/PictureActivity;->access$3(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 363
    return-void
.end method

.method public eraseMode()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-boolean v0, v0, Lru/andrey/notepad/PictureActivity;->erase:Z

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v0, v0, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    const v1, 0x7f020038

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 290
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    iput v1, v0, Lru/andrey/notepad/PictureActivity;->color:I

    .line 291
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 292
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lru/andrey/notepad/PictureActivity;->erase:Z

    .line 301
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v0, v0, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    const v1, 0x7f020043

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 297
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lru/andrey/notepad/PictureActivity;->erase:Z

    .line 298
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget v1, v1, Lru/andrey/notepad/PictureActivity;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method public generatePaint()Landroid/graphics/Paint;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 305
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 306
    .local v2, "paint":Landroid/graphics/Paint;
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 307
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 308
    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-boolean v3, v3, Lru/andrey/notepad/PictureActivity;->erase:Z

    if-eqz v3, :cond_0

    .line 310
    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v3, v3, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    const v4, 0x7f020038

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 311
    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    iput v4, v3, Lru/andrey/notepad/PictureActivity;->color:I

    .line 312
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 319
    :goto_0
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 320
    sget-object v3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 321
    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 322
    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget v3, v3, Lru/andrey/notepad/PictureActivity;->stroke:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 323
    const/4 v0, 0x0

    .line 324
    .local v0, "calc1":I
    const/4 v1, 0x0

    .line 325
    .local v1, "calc2":I
    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget v0, v3, Lru/andrey/notepad/PictureActivity;->intens:I

    .line 326
    mul-int/lit8 v1, v0, 0x2

    .line 327
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 329
    return-object v2

    .line 316
    .end local v0    # "calc1":I
    .end local v1    # "calc2":I
    :cond_0
    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v3, v3, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    const v4, 0x7f020043

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 317
    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    iget v3, v3, Lru/andrey/notepad/PictureActivity;->color:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method public getBmp()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 347
    iget-boolean v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->isnew:Z

    if-nez v1, :cond_0

    .line 349
    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->background:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 352
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/PictureActivity;->access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 356
    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 357
    return-void

    .line 354
    :cond_1
    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paths:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/PictureActivity;->access$2(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Path;

    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$MyView;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->paints:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/PictureActivity;->access$3(Lru/andrey/notepad/PictureActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 335
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 336
    iget-boolean v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->isnew:Z

    if-eqz v0, :cond_0

    .line 337
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 338
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lru/andrey/notepad/PictureActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lru/andrey/notepad/PictureActivity$MyView;->mCanvas:Landroid/graphics/Canvas;

    .line 339
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 401
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 402
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 404
    .local v1, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 419
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 407
    :pswitch_0
    invoke-direct {p0, v0, v1}, Lru/andrey/notepad/PictureActivity$MyView;->touch_start(FF)V

    .line 408
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity$MyView;->invalidate()V

    goto :goto_0

    .line 411
    :pswitch_1
    invoke-direct {p0, v0, v1}, Lru/andrey/notepad/PictureActivity$MyView;->touch_move(FF)V

    .line 412
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity$MyView;->invalidate()V

    goto :goto_0

    .line 415
    :pswitch_2
    invoke-direct {p0}, Lru/andrey/notepad/PictureActivity$MyView;->touch_up()V

    .line 416
    invoke-virtual {p0}, Lru/andrey/notepad/PictureActivity$MyView;->invalidate()V

    goto :goto_0

    .line 404
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
