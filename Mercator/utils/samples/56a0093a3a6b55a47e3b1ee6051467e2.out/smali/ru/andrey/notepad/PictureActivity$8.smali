.class Lru/andrey/notepad/PictureActivity$8;
.super Ljava/lang/Object;
.source "PictureActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/PictureActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/PictureActivity;

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$dp:Landroid/widget/DatePicker;

.field private final synthetic val$tp:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Lru/andrey/notepad/PictureActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/PictureActivity$8;->this$0:Lru/andrey/notepad/PictureActivity;

    iput-object p2, p0, Lru/andrey/notepad/PictureActivity$8;->val$dp:Landroid/widget/DatePicker;

    iput-object p3, p0, Lru/andrey/notepad/PictureActivity$8;->val$tp:Landroid/widget/TimePicker;

    iput-object p4, p0, Lru/andrey/notepad/PictureActivity$8;->val$dialog:Landroid/app/Dialog;

    .line 718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 722
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 723
    .local v0, "c":Ljava/util/Calendar;
    const/4 v4, 0x5

    iget-object v5, p0, Lru/andrey/notepad/PictureActivity$8;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 724
    const/4 v4, 0x2

    iget-object v5, p0, Lru/andrey/notepad/PictureActivity$8;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 725
    const/4 v4, 0x1

    iget-object v5, p0, Lru/andrey/notepad/PictureActivity$8;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getYear()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 726
    const/16 v4, 0xb

    iget-object v5, p0, Lru/andrey/notepad/PictureActivity$8;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 727
    const/16 v4, 0xc

    iget-object v5, p0, Lru/andrey/notepad/PictureActivity$8;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 729
    const-string v3, "drawing"

    .line 730
    .local v3, "value":Ljava/lang/String;
    new-instance v1, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v1}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 731
    .local v1, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v1, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 732
    iget-object v4, p0, Lru/andrey/notepad/PictureActivity$8;->this$0:Lru/andrey/notepad/PictureActivity;

    iget v4, v4, Lru/andrey/notepad/PictureActivity;->num:I

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 733
    iget-object v4, p0, Lru/andrey/notepad/PictureActivity$8;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v4, v4, Lru/andrey/notepad/PictureActivity;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 734
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 735
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 736
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 737
    iget-object v4, p0, Lru/andrey/notepad/PictureActivity$8;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v4, v4, Lru/andrey/notepad/PictureActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v1}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 739
    iget-object v4, p0, Lru/andrey/notepad/PictureActivity$8;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->dismiss()V

    .line 741
    return-void
.end method
