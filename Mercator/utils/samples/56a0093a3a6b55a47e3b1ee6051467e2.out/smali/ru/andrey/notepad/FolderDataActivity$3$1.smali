.class Lru/andrey/notepad/FolderDataActivity$3$1;
.super Ljava/lang/Object;
.source "FolderDataActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderDataActivity$3;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FolderDataActivity$3;

.field private final synthetic val$arg2:I

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$value:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity$3;Landroid/widget/EditText;ILandroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    iput-object p2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$value:Landroid/widget/EditText;

    iput p3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    iput-object p4, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$dialog:Landroid/app/Dialog;

    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 432
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$value:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v5, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "pass"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 434
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 436
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 437
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 438
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 441
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    .line 511
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 513
    return-void

    .line 443
    :cond_0
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Camera"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 445
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 446
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 447
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 450
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 452
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PhotoText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 454
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 455
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 456
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 458
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 459
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 461
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GalleryText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 463
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 464
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 465
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 468
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 470
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Record"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 472
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 473
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 474
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 476
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 477
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 479
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Shop"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 481
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 482
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 483
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 484
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 485
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 486
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 488
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v2, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 490
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 491
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 492
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 493
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 495
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 499
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_6
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const-class v2, Lru/andrey/notepad/AddActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 500
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 501
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 502
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/FolderDataActivity;->object:Ljava/util/ArrayList;

    iget v3, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->val$arg2:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 504
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 509
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_7
    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$3$1;->this$1:Lru/andrey/notepad/FolderDataActivity$3;

    # getter for: Lru/andrey/notepad/FolderDataActivity$3;->this$0:Lru/andrey/notepad/FolderDataActivity;
    invoke-static {v1}, Lru/andrey/notepad/FolderDataActivity$3;->access$0(Lru/andrey/notepad/FolderDataActivity$3;)Lru/andrey/notepad/FolderDataActivity;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lru/andrey/notepad/FolderDataActivity;->showDialog(I)V

    goto/16 :goto_0
.end method
