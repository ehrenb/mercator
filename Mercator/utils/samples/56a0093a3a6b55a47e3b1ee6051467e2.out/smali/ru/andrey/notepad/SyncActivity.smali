.class public Lru/andrey/notepad/SyncActivity;
.super Landroid/app/Activity;
.source "SyncActivity.java"


# instance fields
.field bg:Landroid/widget/RelativeLayout;

.field dow:Landroid/widget/Spinner;

.field rb1:Landroid/widget/RadioButton;

.field rb2:Landroid/widget/RadioButton;

.field save:Landroid/widget/Button;

.field tp:Landroid/widget/TimePicker;

.field use:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    const v3, 0x7f030019

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->setContentView(I)V

    .line 37
    const v3, 0x7f070015

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/ads/AdView;

    .line 38
    .local v2, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v3, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 39
    const v3, 0x7f070049

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lru/andrey/notepad/SyncActivity;->use:Landroid/widget/CheckBox;

    .line 40
    const v3, 0x7f07004b

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lru/andrey/notepad/SyncActivity;->rb1:Landroid/widget/RadioButton;

    .line 41
    const v3, 0x7f07004c

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lru/andrey/notepad/SyncActivity;->rb2:Landroid/widget/RadioButton;

    .line 42
    const v3, 0x7f07004d

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lru/andrey/notepad/SyncActivity;->dow:Landroid/widget/Spinner;

    .line 43
    const v3, 0x7f070045

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TimePicker;

    iput-object v3, p0, Lru/andrey/notepad/SyncActivity;->tp:Landroid/widget/TimePicker;

    .line 44
    const v3, 0x7f070028

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lru/andrey/notepad/SyncActivity;->save:Landroid/widget/Button;

    .line 46
    const v3, 0x7f050021

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "en"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    const/4 v3, 0x7

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, "Sunday"

    aput-object v3, v0, v6

    const-string v3, "Monday"

    aput-object v3, v0, v7

    const-string v3, "Tuesday"

    aput-object v3, v0, v5

    const-string v3, "Wednesday"

    aput-object v3, v0, v8

    const-string v3, "Thursday"

    aput-object v3, v0, v9

    const/4 v3, 0x5

    const-string v4, "Friday"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "Saturday"

    aput-object v4, v0, v3

    .line 49
    .local v0, "days":[Ljava/lang/String;
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x1090008

    invoke-direct {v1, p0, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 50
    .local v1, "days_arr":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 51
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->dow:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 60
    :goto_0
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->use:Landroid/widget/CheckBox;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "use"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 61
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->rb1:Landroid/widget/RadioButton;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "ed"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 62
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->rb2:Landroid/widget/RadioButton;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "ew"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 63
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->dow:Landroid/widget/Spinner;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "dow"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 64
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->tp:Landroid/widget/TimePicker;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 66
    const v3, 0x7f070014

    invoke-virtual {p0, v3}, Lru/andrey/notepad/SyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lru/andrey/notepad/SyncActivity;->bg:Landroid/widget/RelativeLayout;

    .line 67
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->bg:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "color"

    const-string v6, "#dad07f"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 69
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->save:Landroid/widget/Button;

    new-instance v4, Lru/andrey/notepad/SyncActivity$1;

    invoke-direct {v4, p0}, Lru/andrey/notepad/SyncActivity$1;-><init>(Lru/andrey/notepad/SyncActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    return-void

    .line 55
    .end local v0    # "days":[Ljava/lang/String;
    .end local v1    # "days_arr":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    :cond_0
    const/4 v3, 0x7

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, "\u0412\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435"

    aput-object v3, v0, v6

    const-string v3, "\u041f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a"

    aput-object v3, v0, v7

    const-string v3, "\u0412\u0442\u043e\u0440\u043d\u0438\u043a"

    aput-object v3, v0, v5

    const-string v3, "\u0421\u0440\u0435\u0434\u0430"

    aput-object v3, v0, v8

    const-string v3, "\u0427\u0435\u0442\u0432\u0435\u0440\u0433"

    aput-object v3, v0, v9

    const/4 v3, 0x5

    const-string v4, "\u041f\u044f\u0442\u043d\u0438\u0446\u0430"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "\u0421\u0443\u0431\u0431\u043e\u0442\u0430"

    aput-object v4, v0, v3

    .line 56
    .restart local v0    # "days":[Ljava/lang/String;
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x1090008

    invoke-direct {v1, p0, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 57
    .restart local v1    # "days_arr":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 58
    iget-object v3, p0, Lru/andrey/notepad/SyncActivity;->dow:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto/16 :goto_0
.end method
