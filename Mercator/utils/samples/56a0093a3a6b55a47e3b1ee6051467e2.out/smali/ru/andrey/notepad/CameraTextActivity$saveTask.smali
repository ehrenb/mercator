.class public Lru/andrey/notepad/CameraTextActivity$saveTask;
.super Landroid/os/AsyncTask;
.source "CameraTextActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/CameraTextActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "saveTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/CameraTextActivity;


# direct methods
.method public constructor <init>(Lru/andrey/notepad/CameraTextActivity;)V
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lru/andrey/notepad/CameraTextActivity$saveTask;->this$0:Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/CameraTextActivity$saveTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "uri"    # [Ljava/lang/String;

    .prologue
    .line 653
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Notepad"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 654
    .local v4, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 655
    .local v1, "fOut":Ljava/io/OutputStream;
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    const/4 v6, 0x0

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 658
    .local v3, "file":Ljava/io/File;
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 659
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .local v2, "fOut":Ljava/io/OutputStream;
    :try_start_2
    iget-object v5, p0, Lru/andrey/notepad/CameraTextActivity$saveTask;->this$0:Lru/andrey/notepad/CameraTextActivity;

    iget-object v5, v5, Lru/andrey/notepad/CameraTextActivity;->photoBitmap:Landroid/graphics/Bitmap;

    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x55

    invoke-virtual {v5, v6, v7, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 661
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 662
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-object v1, v2

    .line 674
    .end local v2    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :goto_0
    const/4 v5, 0x0

    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "path":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 669
    :catch_0
    move-exception v0

    .line 671
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 672
    const-string v5, "err"

    goto :goto_1

    .line 664
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "path":Ljava/lang/String;
    :catch_1
    move-exception v5

    goto :goto_0

    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "fOut":Ljava/io/OutputStream;
    :catch_2
    move-exception v5

    move-object v1, v2

    .end local v2    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/CameraTextActivity$saveTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 681
    iget-object v0, p0, Lru/andrey/notepad/CameraTextActivity$saveTask;->this$0:Lru/andrey/notepad/CameraTextActivity;

    invoke-virtual {v0}, Lru/andrey/notepad/CameraTextActivity;->finish()V

    .line 682
    iget-object v0, p0, Lru/andrey/notepad/CameraTextActivity$saveTask;->this$0:Lru/andrey/notepad/CameraTextActivity;

    iget-object v0, v0, Lru/andrey/notepad/CameraTextActivity;->LoadProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 684
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 645
    iget-object v0, p0, Lru/andrey/notepad/CameraTextActivity$saveTask;->this$0:Lru/andrey/notepad/CameraTextActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/CameraTextActivity;->showDialog(I)V

    .line 646
    return-void
.end method
