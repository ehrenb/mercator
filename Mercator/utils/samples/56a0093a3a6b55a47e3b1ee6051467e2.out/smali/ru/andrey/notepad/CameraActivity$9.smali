.class Lru/andrey/notepad/CameraActivity$9;
.super Ljava/lang/Object;
.source "CameraActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/CameraActivity;->setIntens()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/CameraActivity;

.field private final synthetic val$seek:Landroid/widget/SeekBar;


# direct methods
.method constructor <init>(Lru/andrey/notepad/CameraActivity;Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/CameraActivity$9;->this$0:Lru/andrey/notepad/CameraActivity;

    iput-object p2, p0, Lru/andrey/notepad/CameraActivity$9;->val$seek:Landroid/widget/SeekBar;

    .line 777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 781
    const/4 v0, 0x0

    .line 782
    .local v0, "calc1":I
    const/4 v1, 0x0

    .line 783
    .local v1, "calc2":I
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$9;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$9;->val$seek:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    iput v3, v2, Lru/andrey/notepad/CameraActivity;->intens:I

    .line 784
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$9;->this$0:Lru/andrey/notepad/CameraActivity;

    iget v0, v2, Lru/andrey/notepad/CameraActivity;->intens:I

    .line 785
    mul-int/lit8 v1, v0, 0x2

    .line 786
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$9;->this$0:Lru/andrey/notepad/CameraActivity;

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 788
    return-void
.end method
