.class public Lru/andrey/notepad/IconContextMenu;
.super Ljava/lang/Object;
.source "IconContextMenu.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;,
        Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;,
        Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;
    }
.end annotation


# static fields
.field private static final LIST_PREFERED_HEIGHT:I = 0x41


# instance fields
.field private clickHandler:Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;

.field private dialogId:I

.field private menuAdapter:Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

.field private parentActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 2
    .param p1, "parent"    # Landroid/app/Activity;
    .param p2, "id"    # I

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v1, p0, Lru/andrey/notepad/IconContextMenu;->menuAdapter:Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

    .line 32
    iput-object v1, p0, Lru/andrey/notepad/IconContextMenu;->parentActivity:Landroid/app/Activity;

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lru/andrey/notepad/IconContextMenu;->dialogId:I

    .line 35
    iput-object v1, p0, Lru/andrey/notepad/IconContextMenu;->clickHandler:Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;

    .line 44
    iput-object p1, p0, Lru/andrey/notepad/IconContextMenu;->parentActivity:Landroid/app/Activity;

    .line 45
    iput p2, p0, Lru/andrey/notepad/IconContextMenu;->dialogId:I

    .line 47
    new-instance v0, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

    iget-object v1, p0, Lru/andrey/notepad/IconContextMenu;->parentActivity:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;-><init>(Lru/andrey/notepad/IconContextMenu;Landroid/content/Context;)V

    iput-object v0, p0, Lru/andrey/notepad/IconContextMenu;->menuAdapter:Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

    .line 48
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/IconContextMenu;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lru/andrey/notepad/IconContextMenu;->parentActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1(Lru/andrey/notepad/IconContextMenu;)Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lru/andrey/notepad/IconContextMenu;->menuAdapter:Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

    return-object v0
.end method

.method static synthetic access$2(Lru/andrey/notepad/IconContextMenu;)Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lru/andrey/notepad/IconContextMenu;->clickHandler:Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;

    return-object v0
.end method

.method private cleanup()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lru/andrey/notepad/IconContextMenu;->parentActivity:Landroid/app/Activity;

    iget v1, p0, Lru/andrey/notepad/IconContextMenu;->dialogId:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V

    .line 115
    return-void
.end method


# virtual methods
.method public addItem(Landroid/content/res/Resources;III)V
    .locals 7
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "textResourceId"    # I
    .param p3, "imageResourceId"    # I
    .param p4, "actionTag"    # I

    .prologue
    .line 61
    iget-object v6, p0, Lru/andrey/notepad/IconContextMenu;->menuAdapter:Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

    new-instance v0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;-><init>(Lru/andrey/notepad/IconContextMenu;Landroid/content/res/Resources;III)V

    invoke-virtual {v6, v0}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->addItem(Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;)V

    .line 62
    return-void
.end method

.method public addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V
    .locals 7
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "imageResourceId"    # I
    .param p4, "actionTag"    # I

    .prologue
    .line 56
    iget-object v6, p0, Lru/andrey/notepad/IconContextMenu;->menuAdapter:Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

    new-instance v0, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;-><init>(Lru/andrey/notepad/IconContextMenu;Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    invoke-virtual {v6, v0}, Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;->addItem(Lru/andrey/notepad/IconContextMenu$IconContextMenuItem;)V

    .line 57
    return-void
.end method

.method public createMenu(Ljava/lang/String;)Landroid/app/Dialog;
    .locals 4
    .param p1, "menuItitle"    # Ljava/lang/String;

    .prologue
    .line 79
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lru/andrey/notepad/IconContextMenu;->parentActivity:Landroid/app/Activity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 81
    iget-object v2, p0, Lru/andrey/notepad/IconContextMenu;->menuAdapter:Lru/andrey/notepad/IconContextMenu$IconMenuAdapter;

    new-instance v3, Lru/andrey/notepad/IconContextMenu$1;

    invoke-direct {v3, p0}, Lru/andrey/notepad/IconContextMenu$1;-><init>(Lru/andrey/notepad/IconContextMenu;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 96
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 97
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1, p0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 98
    invoke-virtual {v1, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 99
    return-object v1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 105
    invoke-direct {p0}, Lru/andrey/notepad/IconContextMenu;->cleanup()V

    .line 106
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 110
    return-void
.end method

.method public setOnClickListener(Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;)V
    .locals 0
    .param p1, "listener"    # Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;

    .prologue
    .line 70
    iput-object p1, p0, Lru/andrey/notepad/IconContextMenu;->clickHandler:Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;

    .line 71
    return-void
.end method
