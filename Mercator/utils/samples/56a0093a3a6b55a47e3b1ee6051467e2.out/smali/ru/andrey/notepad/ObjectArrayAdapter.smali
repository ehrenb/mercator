.class public Lru/andrey/notepad/ObjectArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ObjectArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lru/andrey/notepad/ObjectModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Activity;

.field private final obj:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "obj":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    const v0, 0x7f030012

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 34
    iput-object p1, p0, Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;

    .line 35
    iput-object p2, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 54
    move-object v6, p2

    .line 55
    .local v6, "rowView":Landroid/view/View;
    const-string v7, "Test"

    const-string v8, "view"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    if-nez v6, :cond_0

    .line 58
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 59
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f030012

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 60
    new-instance v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;

    invoke-direct {v2}, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;-><init>()V

    .line 61
    .local v2, "holder":Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;
    const v7, 0x7f070037

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 62
    const v7, 0x7f07003e

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    .line 63
    const v7, 0x7f070030

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    .line 64
    const v7, 0x7f070038

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv2:Landroid/widget/ImageView;

    .line 65
    const v7, 0x7f070039

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv3:Landroid/widget/ImageView;

    .line 66
    const v7, 0x7f07003c

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv4:Landroid/widget/ImageView;

    .line 67
    const v7, 0x7f07003d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv5:Landroid/widget/ImageView;

    .line 68
    const v7, 0x7f07003a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv6:Landroid/widget/ImageView;

    .line 69
    invoke-virtual {v6, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 76
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    .line 77
    .local v1, "ff":Lru/andrey/notepad/ObjectModel;
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v7

    const-string v8, "drawing"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 80
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f020045

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 96
    :goto_1
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;

    invoke-static {v7}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 97
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lru/andrey/notepad/DataBaseHelper;->checkName(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_8

    .line 99
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv4:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    :goto_2
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv6:Landroid/widget/ImageView;

    new-instance v8, Lru/andrey/notepad/ObjectArrayAdapter$1;

    invoke-direct {v8, p0, v0, p1}, Lru/andrey/notepad/ObjectArrayAdapter$1;-><init>(Lru/andrey/notepad/ObjectArrayAdapter;Lru/andrey/notepad/DataBaseHelper;I)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv2:Landroid/widget/ImageView;

    new-instance v8, Lru/andrey/notepad/ObjectArrayAdapter$2;

    invoke-direct {v8, p0, p1}, Lru/andrey/notepad/ObjectArrayAdapter$2;-><init>(Lru/andrey/notepad/ObjectArrayAdapter;I)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v8, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 176
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv5:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 182
    :goto_3
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv3:Landroid/widget/ImageView;

    new-instance v8, Lru/andrey/notepad/ObjectArrayAdapter$3;

    invoke-direct {v8, p0, p1}, Lru/andrey/notepad/ObjectArrayAdapter$3;-><init>(Lru/andrey/notepad/ObjectArrayAdapter;I)V

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    return-object v6

    .line 73
    .end local v0    # "dh":Lru/andrey/notepad/DataBaseHelper;
    .end local v1    # "ff":Lru/andrey/notepad/ObjectModel;
    .end local v2    # "holder":Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;
    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;

    .restart local v2    # "holder":Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;
    goto/16 :goto_0

    .line 81
    .restart local v1    # "ff":Lru/andrey/notepad/ObjectModel;
    :cond_1
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Camera"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 82
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f020009

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 83
    :cond_2
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v7

    const-string v8, "PhotoText"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 84
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f020050

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 85
    :cond_3
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v7

    const-string v8, "GalleryText"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 86
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f020036

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 87
    :cond_4
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Record"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 88
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f02004d

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 89
    :cond_5
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Shop"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 90
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f020035

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 91
    :cond_6
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Video"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 92
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f020037

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 94
    :cond_7
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v8, 0x7f02003e

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 103
    .restart local v0    # "dh":Lru/andrey/notepad/DataBaseHelper;
    :cond_8
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v7}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v8, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 105
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv5:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 106
    .local v4, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 107
    .local v5, "r":Landroid/content/res/Resources;
    const/high16 v7, 0x42200000    # 40.0f

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-static {v10, v7, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v4, v7, v9, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 108
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv5:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    .end local v4    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v5    # "r":Landroid/content/res/Resources;
    :cond_9
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv4:Landroid/widget/ImageView;

    invoke-virtual {v7, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 180
    :cond_a
    iget-object v7, v2, Lru/andrey/notepad/ObjectArrayAdapter$ViewHolder;->iv5:Landroid/widget/ImageView;

    invoke-virtual {v7, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3
.end method
