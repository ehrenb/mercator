.class Lru/andrey/notepad/BuyActivity$10;
.super Ljava/lang/Object;
.source "BuyActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/BuyActivity;->showSave()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/BuyActivity;

.field private final synthetic val$count:I

.field private final synthetic val$edit:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/BuyActivity;ILandroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/BuyActivity$10;->this$0:Lru/andrey/notepad/BuyActivity;

    iput p2, p0, Lru/andrey/notepad/BuyActivity$10;->val$count:I

    iput-object p3, p0, Lru/andrey/notepad/BuyActivity$10;->val$edit:Landroid/widget/EditText;

    .line 495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 500
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ShopList/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lru/andrey/notepad/BuyActivity$10;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v5, v5, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 501
    .local v3, "value":Ljava/lang/String;
    new-instance v1, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v1}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 502
    .local v1, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v1, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 503
    iget v4, p0, Lru/andrey/notepad/BuyActivity$10;->val$count:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 504
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity$10;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 505
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 506
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 507
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity$10;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v4, v4, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v5, p0, Lru/andrey/notepad/BuyActivity$10;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v5, v5, Lru/andrey/notepad/BuyActivity;->uniID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lru/andrey/notepad/DataBaseHelper;->remove(Ljava/lang/String;)V

    .line 508
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v4, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_0

    .line 512
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity$10;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v4, v4, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v1}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 513
    sget-object v4, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 515
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity$10;->this$0:Lru/andrey/notepad/BuyActivity;

    invoke-virtual {v4}, Lru/andrey/notepad/BuyActivity;->finish()V

    .line 516
    return-void

    .line 510
    :cond_0
    iget-object v4, p0, Lru/andrey/notepad/BuyActivity$10;->this$0:Lru/andrey/notepad/BuyActivity;

    iget-object v5, v4, Lru/andrey/notepad/BuyActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    sget-object v4, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/BuyModel;

    invoke-virtual {v5, v4}, Lru/andrey/notepad/DataBaseHelper;->addBuy(Lru/andrey/notepad/BuyModel;)V

    .line 508
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
