.class public Lru/andrey/notepad/CameraActivity;
.super Lru/andrey/notepad/GraphicsActivity;
.source "CameraActivity.java"

# interfaces
.implements Lru/andrey/notepad/ColorPickerDialog$OnColorChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/CameraActivity$MyView;
    }
.end annotation


# static fields
.field private static final BACK_MENU_ID:I = 0x8

.field private static final BLUR_MENU_ID:I = 0x3

.field private static final COLOR_MENU_ID:I = 0x1

.field private static final DELETE_MENU_ID:I = 0x5

.field private static final EMBOSS_MENU_ID:I = 0x2

.field private static final ERASE_MENU_ID:I = 0x4

.field private static final HOME_MENU_ID:I = 0xb

.field private static final MORE_MENU_ID:I = 0x7

.field private static final PASS_MENU_ID:I = 0xd

.field private static final REMIND_MENU_ID:I = 0xc

.field private static final SHARE_MENU_ID:I = 0x9

.field private static final SHORTCUT_MENU_ID:I = 0xa

.field private static final STROKE_MENU_ID:I = 0x6

.field private static final UNPASS_MENU_ID:I = 0xe


# instance fields
.field bg:Landroid/graphics/Bitmap;

.field bmp:Landroid/graphics/Bitmap;

.field clear:Landroid/widget/Button;

.field color:I

.field count:I

.field delMode:Z

.field dh:Lru/andrey/notepad/DataBaseHelper;

.field erase:Z

.field intens:I

.field isnew:Z

.field private mBlur:Landroid/graphics/MaskFilter;

.field private mEmboss:Landroid/graphics/MaskFilter;

.field private mPaint:Landroid/graphics/Paint;

.field mv:Lru/andrey/notepad/CameraActivity$MyView;

.field name:Ljava/lang/String;

.field num:I

.field private final paints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field private final paths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field photoBitmap:Landroid/graphics/Bitmap;

.field stroke:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lru/andrey/notepad/GraphicsActivity;-><init>()V

    .line 57
    const/high16 v0, -0x10000

    iput v0, p0, Lru/andrey/notepad/CameraActivity;->color:I

    .line 58
    iput-boolean v1, p0, Lru/andrey/notepad/CameraActivity;->erase:Z

    .line 60
    iput-boolean v1, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    .line 65
    iput-boolean v1, p0, Lru/andrey/notepad/CameraActivity;->delMode:Z

    .line 66
    const/16 v0, 0xc

    iput v0, p0, Lru/andrey/notepad/CameraActivity;->stroke:I

    .line 68
    const/16 v0, 0x64

    iput v0, p0, Lru/andrey/notepad/CameraActivity;->intens:I

    .line 69
    iput v1, p0, Lru/andrey/notepad/CameraActivity;->count:I

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;

    .line 54
    return-void
.end method

.method static synthetic access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$2(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lru/andrey/notepad/CameraActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lru/andrey/notepad/CameraActivity;Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public static getDrawable(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 700
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 701
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 703
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static rotate(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "src"    # Landroid/graphics/Bitmap;
    .param p1, "degree"    # F

    .prologue
    const/4 v1, 0x0

    .line 748
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 750
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 753
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public checkAndCreateDirectory(Ljava/lang/String;)V
    .locals 3
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 293
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 294
    .local v0, "new_dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 296
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 298
    :cond_0
    return-void
.end method

.method public colorChanged(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 288
    iget-object v0, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 289
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 922
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 923
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v11, 0x7f05004a

    const/4 v8, 0x1

    .line 816
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050046

    invoke-virtual {p0, v10}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_0

    .line 818
    const-string v7, "Camera"

    .line 819
    .local v7, "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 820
    .local v3, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 821
    iget v9, p0, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 822
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 823
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 824
    .local v5, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 825
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x493e0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 826
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 916
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :goto_0
    return v8

    .line 828
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050047

    invoke-virtual {p0, v10}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_1

    .line 830
    const-string v7, "Camera"

    .line 831
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 832
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 833
    iget v9, p0, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 834
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 835
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 836
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 837
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0xdbba0

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 838
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto :goto_0

    .line 840
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050048

    invoke-virtual {p0, v10}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_2

    .line 842
    const-string v7, "Camera"

    .line 843
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 844
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 845
    iget v9, p0, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 846
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 847
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 848
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 849
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x16e360

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 850
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 852
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    const v10, 0x7f050049

    invoke-virtual {p0, v10}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_3

    .line 854
    const-string v7, "Camera"

    .line 855
    .restart local v7    # "value":Ljava/lang/String;
    new-instance v3, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v3}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 856
    .restart local v3    # "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v3, v7}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 857
    iget v9, p0, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 858
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 859
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v9, "HH:mm MM-dd-yyyy"

    invoke-direct {v5, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 860
    .restart local v5    # "sdf":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 861
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/32 v11, 0x36ee80

    add-long/2addr v9, v11

    invoke-virtual {v3, v9, v10}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 862
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v9, v3}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    goto/16 :goto_0

    .line 864
    .end local v3    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v5    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v7    # "value":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {p0, v11}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-ne v9, v10, :cond_4

    .line 866
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 867
    .local v1, "dialog":Landroid/app/Dialog;
    const v9, 0x7f030016

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->setContentView(I)V

    .line 868
    invoke-virtual {v1, v11}, Landroid/app/Dialog;->setTitle(I)V

    .line 869
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 870
    const v9, 0x7f070044

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/DatePicker;

    .line 871
    .local v2, "dp":Landroid/widget/DatePicker;
    const v9, 0x7f070045

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TimePicker;

    .line 872
    .local v6, "tp":Landroid/widget/TimePicker;
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 873
    const v9, 0x7f070028

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 874
    .local v4, "save":Landroid/widget/Button;
    const v9, 0x7f070019

    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 876
    .local v0, "cancel":Landroid/widget/Button;
    new-instance v9, Lru/andrey/notepad/CameraActivity$11;

    invoke-direct {v9, p0, v2, v6, v1}, Lru/andrey/notepad/CameraActivity$11;-><init>(Lru/andrey/notepad/CameraActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 901
    new-instance v9, Lru/andrey/notepad/CameraActivity$12;

    invoke-direct {v9, p0, v1}, Lru/andrey/notepad/CameraActivity$12;-><init>(Lru/andrey/notepad/CameraActivity;Landroid/app/Dialog;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 910
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 914
    .end local v0    # "cancel":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v2    # "dp":Landroid/widget/DatePicker;
    .end local v4    # "save":Landroid/widget/Button;
    .end local v6    # "tp":Landroid/widget/TimePicker;
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 30
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super/range {p0 .. p1}, Lru/andrey/notepad/GraphicsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/CameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 81
    .local v8, "extras":Landroid/os/Bundle;
    const-string v24, "new"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lru/andrey/notepad/CameraActivity;->isnew:Z

    .line 82
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    move/from16 v24, v0

    if-nez v24, :cond_3

    .line 84
    const-string v24, "color"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lru/andrey/notepad/CameraActivity;->num:I

    .line 85
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/Notepad/Camera"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget v0, v0, Lru/andrey/notepad/CameraActivity;->num:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ".png"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v24

    sget-object v25, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/16 v26, 0x1

    invoke-virtual/range {v24 .. v26}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->bmp:Landroid/graphics/Bitmap;

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->bmp:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->bmp:Landroid/graphics/Bitmap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v25

    const/16 v26, 0x1

    invoke-virtual/range {v24 .. v26}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->bg:Landroid/graphics/Bitmap;

    .line 87
    new-instance v24, Lru/andrey/notepad/CameraActivity$MyView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->bmp:Landroid/graphics/Bitmap;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move-object/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lru/andrey/notepad/CameraActivity$MyView;-><init>(Lru/andrey/notepad/CameraActivity;Landroid/content/Context;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    .line 88
    const-string v24, "name"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    .line 95
    :goto_0
    new-instance v21, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 96
    .local v21, "vg":Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    move-object/from16 v24, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 97
    const/16 v24, -0x1

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 98
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/CameraActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 100
    .local v15, "r":Landroid/content/res/Resources;
    new-instance v19, Landroid/widget/Button;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 101
    .local v19, "rotate":Landroid/widget/Button;
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, 0x1

    const/high16 v25, 0x42480000    # 50.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    const/16 v25, 0x1

    const/high16 v26, 0x42480000    # 50.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v27

    invoke-static/range {v25 .. v27}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 102
    .local v18, "relativeParams3":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v24, 0xa

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 103
    const/16 v24, 0xb

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 104
    const/16 v24, 0x0

    const/16 v25, 0x1

    const/high16 v26, 0x41200000    # 10.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v27

    invoke-static/range {v25 .. v27}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    const/16 v26, 0x1

    const/high16 v27, 0x41200000    # 10.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 106
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    const v24, 0x7f020053

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 108
    new-instance v24, Lru/andrey/notepad/CameraActivity$1;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/CameraActivity$1;-><init>(Lru/andrey/notepad/CameraActivity;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    move/from16 v24, v0

    if-nez v24, :cond_0

    .line 120
    const/16 v24, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 123
    :cond_0
    new-instance v6, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 124
    .local v6, "color":Landroid/widget/Button;
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, 0x1

    const/high16 v25, 0x42480000    # 50.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    const/16 v25, 0x1

    const/high16 v26, 0x42480000    # 50.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v27

    invoke-static/range {v25 .. v27}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 125
    .local v16, "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v24, 0xc

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 126
    const/16 v24, 0xe

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 127
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    const v24, 0x7f02000a

    move/from16 v0, v24

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 130
    new-instance v24, Lru/andrey/notepad/CameraActivity$2;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/CameraActivity$2;-><init>(Lru/andrey/notepad/CameraActivity;)V

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    new-instance v14, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 162
    .local v14, "op":Landroid/widget/Button;
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    const v24, 0x7f020040

    move/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 165
    new-instance v24, Lru/andrey/notepad/CameraActivity$3;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/CameraActivity$3;-><init>(Lru/andrey/notepad/CameraActivity;)V

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    new-instance v20, Landroid/widget/Button;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 175
    .local v20, "st":Landroid/widget/Button;
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    const v24, 0x7f02004f

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 178
    new-instance v24, Lru/andrey/notepad/CameraActivity$4;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/CameraActivity$4;-><init>(Lru/andrey/notepad/CameraActivity;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    new-instance v5, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 188
    .local v5, "back":Landroid/widget/Button;
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    const v24, 0x7f020054

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 191
    new-instance v24, Lru/andrey/notepad/CameraActivity$5;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/CameraActivity$5;-><init>(Lru/andrey/notepad/CameraActivity;)V

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    new-instance v17, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, 0x1

    const/high16 v25, 0x42480000    # 50.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    const/16 v25, 0x1

    const/high16 v26, 0x42480000    # 50.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v27

    invoke-static/range {v25 .. v27}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 205
    .local v17, "relativeParams2":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v24, 0xc

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 206
    const/16 v24, 0xe

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 207
    const/16 v24, 0x1

    const/high16 v25, 0x41a00000    # 20.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 209
    new-instance v24, Landroid/widget/Button;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    move-object/from16 v24, v0

    const v25, 0x7f020043

    invoke-virtual/range {v24 .. v25}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    move-object/from16 v24, v0

    new-instance v25, Lru/andrey/notepad/CameraActivity$6;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lru/andrey/notepad/CameraActivity$6;-><init>(Lru/andrey/notepad/CameraActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    new-instance v9, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 223
    .local v9, "ll":Landroid/widget/LinearLayout;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v24, -0x1

    const/16 v25, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 224
    .local v10, "llp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v24, 0xc

    move/from16 v0, v24

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 225
    const/16 v24, 0xe

    move/from16 v0, v24

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 226
    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x1

    const/high16 v28, 0x41200000    # 10.0f

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 227
    const/16 v24, 0x11

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 228
    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 230
    invoke-virtual {v9, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 231
    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 232
    invoke-virtual {v9, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 233
    invoke-virtual {v9, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 234
    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 235
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 236
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lru/andrey/notepad/CameraActivity;->setContentView(Landroid/view/View;)V

    .line 238
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    move/from16 v24, v0

    if-eqz v24, :cond_2

    .line 241
    invoke-virtual/range {p0 .. p0}, Lru/andrey/notepad/CameraActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    .line 242
    .local v7, "display":Landroid/view/Display;
    sget-object v24, Lru/andrey/notepad/MainActivity;->cambmp:Landroid/graphics/Bitmap;

    sget-object v25, Lru/andrey/notepad/MainActivity;->cambmp:Landroid/graphics/Bitmap;

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v25

    const/16 v26, 0x1

    invoke-virtual/range {v24 .. v26}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 243
    .local v11, "newbg":Landroid/graphics/Bitmap;
    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    div-float v22, v24, v25

    .line 244
    .local v22, "xscale":F
    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    div-float v23, v24, v25

    .line 245
    .local v23, "yscale":F
    cmpl-float v24, v22, v23

    if-lez v24, :cond_1

    .line 246
    move/from16 v22, v23

    .line 247
    :cond_1
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v12, v24, v22

    .line 248
    .local v12, "newx":F
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v13, v24, v22

    .line 252
    .local v13, "newy":F
    float-to-int v0, v12

    move/from16 v24, v0

    float-to-int v0, v13

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v11, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->photoBitmap:Landroid/graphics/Bitmap;

    .line 262
    .end local v7    # "display":Landroid/view/Display;
    .end local v11    # "newbg":Landroid/graphics/Bitmap;
    .end local v12    # "newx":F
    .end local v13    # "newy":F
    .end local v22    # "xscale":F
    .end local v23    # "yscale":F
    :cond_2
    invoke-static/range {p0 .. p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 264
    const-string v24, "/Notepad"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lru/andrey/notepad/CameraActivity;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 266
    new-instance v24, Landroid/graphics/Paint;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Paint;->setDither(Z)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    const/high16 v25, -0x10000

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Paint;->setColor(I)V

    .line 270
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    sget-object v25, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    sget-object v25, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 272
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    sget-object v25, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lru/andrey/notepad/CameraActivity;->stroke:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 275
    new-instance v24, Landroid/graphics/EmbossMaskFilter;

    const/16 v25, 0x3

    move/from16 v0, v25

    new-array v0, v0, [F

    move-object/from16 v25, v0

    fill-array-data v25, :array_0

    const v26, 0x3ecccccd    # 0.4f

    const/high16 v27, 0x40c00000    # 6.0f

    const/high16 v28, 0x40600000    # 3.5f

    invoke-direct/range {v24 .. v28}, Landroid/graphics/EmbossMaskFilter;-><init>([FFFF)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->mEmboss:Landroid/graphics/MaskFilter;

    .line 277
    new-instance v24, Landroid/graphics/BlurMaskFilter;

    const/high16 v25, 0x41000000    # 8.0f

    sget-object v26, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct/range {v24 .. v26}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->mBlur:Landroid/graphics/MaskFilter;

    .line 279
    return-void

    .line 92
    .end local v5    # "back":Landroid/widget/Button;
    .end local v6    # "color":Landroid/widget/Button;
    .end local v9    # "ll":Landroid/widget/LinearLayout;
    .end local v10    # "llp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v14    # "op":Landroid/widget/Button;
    .end local v15    # "r":Landroid/content/res/Resources;
    .end local v16    # "relativeParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "relativeParams2":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v18    # "relativeParams3":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v19    # "rotate":Landroid/widget/Button;
    .end local v20    # "st":Landroid/widget/Button;
    .end local v21    # "vg":Landroid/view/ViewGroup;
    :cond_3
    new-instance v24, Lru/andrey/notepad/CameraActivity$MyView;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lru/andrey/notepad/CameraActivity$MyView;-><init>(Lru/andrey/notepad/CameraActivity;Landroid/content/Context;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    goto/16 :goto_0

    .line 275
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 803
    invoke-super {p0, p1, p2, p3}, Lru/andrey/notepad/GraphicsActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 804
    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 805
    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 806
    const v0, 0x7f050047

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 807
    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 808
    const v0, 0x7f050049

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 809
    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 810
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v2, 0x31

    const/4 v6, 0x1

    const/16 v5, 0x64

    const/16 v4, 0x34

    const/4 v3, 0x0

    .line 508
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 510
    const/16 v0, 0xb

    const v1, 0x7f05003f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x68

    invoke-interface {v0, v2, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 512
    const v0, 0x7f050024

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v6, v3, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x63

    invoke-interface {v0, v2, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 513
    const/4 v0, 0x2

    const v1, 0x7f050035

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x73

    invoke-interface {v0, v4, v1}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 515
    const/4 v0, 0x4

    const v1, 0x7f050030

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x32

    const/16 v2, 0x7a

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 517
    const/4 v0, 0x6

    const v1, 0x7f050031

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x33

    const/16 v2, 0x7a

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 519
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v0, :cond_0

    .line 520
    const/4 v0, 0x5

    const v1, 0x7f050026

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4, v5}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 522
    :cond_0
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v0, :cond_1

    .line 523
    const/16 v0, 0xc

    const v1, 0x7f05004b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4, v5}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 525
    :cond_1
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v0, :cond_2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 526
    const/16 v0, 0xd

    const v1, 0x7f05005f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4, v5}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 527
    :cond_2
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v0, :cond_3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 528
    const/16 v0, 0xe

    const v1, 0x7f050060

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4, v5}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 531
    :cond_3
    const/16 v0, 0x9

    const v1, 0x7f050025

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x36

    const/16 v2, 0x6d

    invoke-interface {v0, v1, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 532
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v0, :cond_4

    .line 533
    const/16 v0, 0xa

    const v1, 0x7f050037

    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x37

    invoke-interface {v0, v1, v5}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 545
    :cond_4
    return v6
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 14
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1053
    const/4 v10, 0x4

    if-ne p1, v10, :cond_3

    .line 1056
    iget-boolean v10, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v10, :cond_2

    .line 1058
    const-string v9, "Camera"

    .line 1059
    .local v9, "value":Ljava/lang/String;
    new-instance v5, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v5}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 1060
    .local v5, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v5, v9}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 1061
    iget v10, p0, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v5, v10}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 1062
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v5, v10}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 1063
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v10, "HH:mm MM-dd-yyyy"

    invoke-direct {v7, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1064
    .local v7, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v10, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-direct {v10, v11, v12}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 1065
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v10, v5}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 1067
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Notepad"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1068
    .local v6, "path":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1069
    .local v0, "fOut":Ljava/io/OutputStream;
    new-instance v2, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Camera"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, p0, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".png"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1073
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .local v1, "fOut":Ljava/io/OutputStream;
    :try_start_1
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    # getter for: Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v10}, Lru/andrey/notepad/CameraActivity$MyView;->access$0(Lru/andrey/notepad/CameraActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    # getter for: Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v11}, Lru/andrey/notepad/CameraActivity$MyView;->access$0(Lru/andrey/notepad/CameraActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1074
    .local v8, "toSave":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1076
    .local v4, "mCanvas":Landroid/graphics/Canvas;
    iget-boolean v10, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v10, :cond_0

    .line 1078
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->bg:Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v4, v10, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1081
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v3, v10, :cond_1

    .line 1086
    sget-object v10, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v11, 0x55

    invoke-virtual {v8, v10, v11, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1088
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 1089
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 1096
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v8    # "toSave":Landroid/graphics/Bitmap;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    :goto_1
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity;->finish()V

    .line 1103
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v9    # "value":Ljava/lang/String;
    :goto_2
    const/4 v10, 0x1

    .line 1105
    :goto_3
    return v10

    .line 1083
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "i":I
    .restart local v4    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v8    # "toSave":Landroid/graphics/Bitmap;
    .restart local v9    # "value":Ljava/lang/String;
    :cond_1
    :try_start_2
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Path;

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Paint;

    invoke-virtual {v4, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1081
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1101
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v8    # "toSave":Landroid/graphics/Bitmap;
    .end local v9    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity;->showSave()V

    goto :goto_2

    .line 1105
    :cond_3
    invoke-super/range {p0 .. p2}, Lru/andrey/notepad/GraphicsActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v10

    goto :goto_3

    .line 1091
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v5    # "om":Lru/andrey/notepad/ObjectModel;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v9    # "value":Ljava/lang/String;
    :catch_0
    move-exception v10

    goto :goto_1

    .end local v0    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v10

    move-object v0, v1

    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v9, 0x1

    .line 565
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v13}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 566
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    const/16 v11, 0xff

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 568
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 695
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v9

    :goto_0
    :pswitch_0
    return v9

    .line 571
    :pswitch_1
    new-instance v3, Lru/andrey/notepad/AmbilWarnaDialog;

    iget v10, p0, Lru/andrey/notepad/CameraActivity;->color:I

    new-instance v11, Lru/andrey/notepad/CameraActivity$7;

    invoke-direct {v11, p0}, Lru/andrey/notepad/CameraActivity$7;-><init>(Lru/andrey/notepad/CameraActivity;)V

    invoke-direct {v3, p0, v10, v11}, Lru/andrey/notepad/AmbilWarnaDialog;-><init>(Landroid/content/Context;ILru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;)V

    .line 594
    .local v3, "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    invoke-virtual {v3}, Lru/andrey/notepad/AmbilWarnaDialog;->show()V

    goto :goto_0

    .line 603
    .end local v3    # "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    :pswitch_2
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity;->setIntens()V

    goto :goto_0

    .line 606
    :pswitch_3
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    iget v11, p0, Lru/andrey/notepad/CameraActivity;->color:I

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 607
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v10}, Landroid/graphics/Paint;->getMaskFilter()Landroid/graphics/MaskFilter;

    move-result-object v10

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->mBlur:Landroid/graphics/MaskFilter;

    if-eq v10, v11, :cond_0

    .line 609
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->mBlur:Landroid/graphics/MaskFilter;

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    goto :goto_0

    .line 613
    :cond_0
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v13}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    goto :goto_0

    .line 617
    :pswitch_4
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    invoke-virtual {v10}, Lru/andrey/notepad/CameraActivity$MyView;->eraseMode()V

    goto :goto_0

    .line 620
    :pswitch_5
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    invoke-virtual {p0, v10}, Lru/andrey/notepad/CameraActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 621
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    invoke-virtual {p0, v10}, Lru/andrey/notepad/CameraActivity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 624
    :pswitch_6
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity;->setStroke()V

    goto :goto_0

    .line 627
    :pswitch_7
    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lru/andrey/notepad/DataBaseHelper;->removeObject(Ljava/lang/String;)V

    .line 628
    iput-boolean v9, p0, Lru/andrey/notepad/CameraActivity;->delMode:Z

    .line 629
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity;->finish()V

    goto :goto_0

    .line 632
    :pswitch_8
    new-instance v1, Landroid/content/Intent;

    const-string v10, "android.intent.action.VIEW"

    const-string v11, "market://search?q=pub:Power"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v1, v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 633
    .local v1, "browserIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lru/andrey/notepad/CameraActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 639
    .end local v1    # "browserIntent":Landroid/content/Intent;
    :pswitch_9
    new-instance v4, Landroid/app/Dialog;

    invoke-direct {v4, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 640
    .local v4, "dialog1":Landroid/app/Dialog;
    const v10, 0x7f030007

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->setContentView(I)V

    .line 641
    const v10, 0x7f05005f

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->setTitle(I)V

    .line 642
    invoke-virtual {v4, v12}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 643
    const v10, 0x7f070016

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    .line 644
    .local v7, "value":Landroid/widget/EditText;
    const v10, 0x7f07002d

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    .line 646
    .local v8, "value2":Landroid/widget/EditText;
    const v10, 0x7f070028

    invoke-virtual {v4, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 647
    .local v2, "button":Landroid/widget/Button;
    new-instance v10, Lru/andrey/notepad/CameraActivity$8;

    invoke-direct {v10, p0, v7, v8, v4}, Lru/andrey/notepad/CameraActivity$8;-><init>(Lru/andrey/notepad/CameraActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 667
    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 670
    .end local v2    # "button":Landroid/widget/Button;
    .end local v4    # "dialog1":Landroid/app/Dialog;
    .end local v7    # "value":Landroid/widget/EditText;
    .end local v8    # "value2":Landroid/widget/EditText;
    :pswitch_a
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 673
    :pswitch_b
    invoke-virtual {p0}, Lru/andrey/notepad/CameraActivity;->share()V

    goto/16 :goto_0

    .line 676
    :pswitch_c
    new-instance v6, Landroid/content/Intent;

    const-class v10, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v6, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 677
    .local v6, "shortcutIntent":Landroid/content/Intent;
    const-string v10, "new"

    invoke-virtual {v6, v10, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 678
    const-string v10, "name"

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 679
    const-string v10, "body"

    const-string v11, "Camera"

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 680
    const-string v10, "color"

    iget v11, p0, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 681
    const/high16 v10, 0x10000000

    invoke-virtual {v6, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 682
    const/high16 v10, 0x4000000

    invoke-virtual {v6, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 683
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 684
    .local v0, "addIntent":Landroid/content/Intent;
    const-string v10, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 685
    const-string v10, "android.intent.extra.shortcut.NAME"

    iget-object v11, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    const-string v10, "android.intent.extra.shortcut.ICON_RESOURCE"

    const-string v11, "icon64photo"

    invoke-static {p0, v11}, Lru/andrey/notepad/CameraActivity;->getDrawable(Landroid/content/Context;Ljava/lang/String;)I

    move-result v11

    invoke-static {p0, v11}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 687
    const-string v10, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-virtual {v0, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 688
    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 691
    .end local v0    # "addIntent":Landroid/content/Intent;
    .end local v6    # "shortcutIntent":Landroid/content/Intent;
    :pswitch_d
    new-instance v5, Landroid/content/Intent;

    const-class v10, Lru/andrey/notepad/MainActivity;

    invoke-direct {v5, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 692
    .local v5, "in":Landroid/content/Intent;
    invoke-virtual {p0, v5}, Lru/andrey/notepad/CameraActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 568
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_5
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v6, 0x64

    const/16 v5, 0x34

    const/16 v4, 0xe

    const/16 v3, 0xd

    const/4 v2, 0x0

    .line 551
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 552
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 553
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 554
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 556
    :cond_0
    iget-boolean v0, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    .line 559
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 1111
    invoke-super {p0}, Lru/andrey/notepad/GraphicsActivity;->onStop()V

    .line 1112
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 1114
    return-void
.end method

.method public bridge synthetic setContentView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lru/andrey/notepad/GraphicsActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setIntens()V
    .locals 5

    .prologue
    .line 758
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 760
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f050034

    invoke-virtual {p0, v3}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 763
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 765
    .local v1, "linear":Landroid/widget/LinearLayout;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 770
    new-instance v2, Landroid/widget/SeekBar;

    invoke-direct {v2, p0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 771
    .local v2, "seek":Landroid/widget/SeekBar;
    iget v3, p0, Lru/andrey/notepad/CameraActivity;->intens:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 772
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 775
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 777
    const v3, 0x7f050029

    invoke-virtual {p0, v3}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/CameraActivity$9;

    invoke-direct {v4, p0, v2}, Lru/andrey/notepad/CameraActivity$9;-><init>(Lru/andrey/notepad/CameraActivity;Landroid/widget/SeekBar;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 791
    const v3, 0x7f05002a

    invoke-virtual {p0, v3}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/CameraActivity$10;

    invoke-direct {v4, p0}, Lru/andrey/notepad/CameraActivity$10;-><init>(Lru/andrey/notepad/CameraActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 797
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 798
    return-void
.end method

.method public setStroke()V
    .locals 5

    .prologue
    .line 927
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 929
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f050032

    invoke-virtual {p0, v3}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 932
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 934
    .local v1, "linear":Landroid/widget/LinearLayout;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 939
    new-instance v2, Landroid/widget/SeekBar;

    invoke-direct {v2, p0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 940
    .local v2, "seek":Landroid/widget/SeekBar;
    iget v3, p0, Lru/andrey/notepad/CameraActivity;->stroke:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 941
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 944
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 946
    const v3, 0x7f050029

    invoke-virtual {p0, v3}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/CameraActivity$13;

    invoke-direct {v4, p0, v2}, Lru/andrey/notepad/CameraActivity$13;-><init>(Lru/andrey/notepad/CameraActivity;Landroid/widget/SeekBar;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 956
    const v3, 0x7f05002a

    invoke-virtual {p0, v3}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lru/andrey/notepad/CameraActivity$14;

    invoke-direct {v4, p0}, Lru/andrey/notepad/CameraActivity$14;-><init>(Lru/andrey/notepad/CameraActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 962
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 963
    return-void
.end method

.method public share()V
    .locals 13

    .prologue
    .line 708
    const-string v8, "NotepadSharing"

    .line 709
    .local v8, "value":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Notepad"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 710
    .local v5, "path":Ljava/lang/String;
    const/4 v0, 0x0

    .line 711
    .local v0, "fOut":Ljava/io/OutputStream;
    new-instance v2, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ".png"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 715
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .local v1, "fOut":Ljava/io/OutputStream;
    :try_start_1
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    # getter for: Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v9}, Lru/andrey/notepad/CameraActivity$MyView;->access$0(Lru/andrey/notepad/CameraActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->mv:Lru/andrey/notepad/CameraActivity$MyView;

    # getter for: Lru/andrey/notepad/CameraActivity$MyView;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v10}, Lru/andrey/notepad/CameraActivity$MyView;->access$0(Lru/andrey/notepad/CameraActivity$MyView;)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v10, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 716
    .local v7, "toSave":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 717
    .local v4, "mCanvas":Landroid/graphics/Canvas;
    const/4 v9, -0x1

    invoke-virtual {v4, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 718
    iget-boolean v9, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-nez v9, :cond_0

    .line 720
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->bg:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v4, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 723
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v3, v9, :cond_1

    .line 728
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v10, 0x55

    invoke-virtual {v7, v9, v10, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 730
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 731
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 738
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v7    # "toSave":Landroid/graphics/Bitmap;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    :goto_1
    new-instance v6, Landroid/content/Intent;

    const-string v9, "android.intent.action.SEND"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 739
    .local v6, "share":Landroid/content/Intent;
    const-string v9, "image/png"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 740
    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 741
    const-string v9, "Share Image"

    invoke-static {v6, v9}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {p0, v9}, Lru/andrey/notepad/CameraActivity;->startActivity(Landroid/content/Intent;)V

    .line 743
    return-void

    .line 725
    .end local v0    # "fOut":Ljava/io/OutputStream;
    .end local v6    # "share":Landroid/content/Intent;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v3    # "i":I
    .restart local v4    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v7    # "toSave":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_2
    iget-object v9, p0, Lru/andrey/notepad/CameraActivity;->paths:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/Path;

    iget-object v10, p0, Lru/andrey/notepad/CameraActivity;->paints:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Paint;

    invoke-virtual {v4, v9, v10}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 723
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 733
    .end local v1    # "fOut":Ljava/io/OutputStream;
    .end local v3    # "i":I
    .end local v4    # "mCanvas":Landroid/graphics/Canvas;
    .end local v7    # "toSave":Landroid/graphics/Bitmap;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    :catch_0
    move-exception v9

    goto :goto_1

    .end local v0    # "fOut":Ljava/io/OutputStream;
    .restart local v1    # "fOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v9

    move-object v0, v1

    .end local v1    # "fOut":Ljava/io/OutputStream;
    .restart local v0    # "fOut":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public showSave()V
    .locals 7

    .prologue
    .line 967
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 969
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f050033

    invoke-virtual {p0, v5}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 971
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 973
    .local v3, "linear":Landroid/widget/LinearLayout;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 975
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 977
    .local v2, "edit":Landroid/widget/EditText;
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 980
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 983
    iget-boolean v5, p0, Lru/andrey/notepad/CameraActivity;->isnew:Z

    if-eqz v5, :cond_0

    .line 984
    iget-object v5, p0, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v5}, Lru/andrey/notepad/DataBaseHelper;->getCameraCount()I

    move-result v1

    .line 988
    .local v1, "count":I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Camera "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 989
    .local v4, "names":Ljava/lang/String;
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 991
    const v5, 0x7f050029

    invoke-virtual {p0, v5}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/CameraActivity$15;

    invoke-direct {v6, p0, v1, v2}, Lru/andrey/notepad/CameraActivity$15;-><init>(Lru/andrey/notepad/CameraActivity;ILandroid/widget/EditText;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1039
    const v5, 0x7f05002a

    invoke-virtual {p0, v5}, Lru/andrey/notepad/CameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lru/andrey/notepad/CameraActivity$16;

    invoke-direct {v6, p0}, Lru/andrey/notepad/CameraActivity$16;-><init>(Lru/andrey/notepad/CameraActivity;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1047
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1048
    return-void

    .line 986
    .end local v1    # "count":I
    .end local v4    # "names":Ljava/lang/String;
    :cond_0
    iget v5, p0, Lru/andrey/notepad/CameraActivity;->num:I

    add-int/lit8 v1, v5, -0x1

    .restart local v1    # "count":I
    goto :goto_0
.end method
