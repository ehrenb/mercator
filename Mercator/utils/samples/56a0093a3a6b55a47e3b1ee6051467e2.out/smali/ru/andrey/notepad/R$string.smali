.class public final Lru/andrey/notepad/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add:I = 0x7f05002c

.field public static final additem:I = 0x7f050044

.field public static final app_name:I = 0x7f050020

.field public static final auth_client_needs_enabling_title:I = 0x7f050001

.field public static final auth_client_needs_installation_title:I = 0x7f050002

.field public static final auth_client_needs_update_title:I = 0x7f050003

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f050004

.field public static final auth_client_requested_by_msg:I = 0x7f050005

.field public static final auth_client_using_bad_version_title:I = 0x7f050000

.field public static final backup:I = 0x7f050050

.field public static final badimport:I = 0x7f050052

.field public static final bgcolor:I = 0x7f050065

.field public static final cancel:I = 0x7f05002a

.field public static final cback:I = 0x7f05004d

.field public static final cfolder:I = 0x7f050058

.field public static final cimport:I = 0x7f050054

.field public static final color:I = 0x7f050024

.field public static final colorset:I = 0x7f05005d

.field public static final common_google_play_services_enable_button:I = 0x7f050011

.field public static final common_google_play_services_enable_text:I = 0x7f050010

.field public static final common_google_play_services_enable_title:I = 0x7f05000f

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f05000a

.field public static final common_google_play_services_install_button:I = 0x7f05000e

.field public static final common_google_play_services_install_text_phone:I = 0x7f05000c

.field public static final common_google_play_services_install_text_tablet:I = 0x7f05000d

.field public static final common_google_play_services_install_title:I = 0x7f05000b

.field public static final common_google_play_services_invalid_account_text:I = 0x7f050017

.field public static final common_google_play_services_invalid_account_title:I = 0x7f050016

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f050009

.field public static final common_google_play_services_network_error_text:I = 0x7f050015

.field public static final common_google_play_services_network_error_title:I = 0x7f050014

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f050007

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f050008

.field public static final common_google_play_services_notification_ticker:I = 0x7f050006

.field public static final common_google_play_services_unknown_issue:I = 0x7f050018

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f05001b

.field public static final common_google_play_services_unsupported_text:I = 0x7f05001a

.field public static final common_google_play_services_unsupported_title:I = 0x7f050019

.field public static final common_google_play_services_update_button:I = 0x7f05001c

.field public static final common_google_play_services_update_text:I = 0x7f050013

.field public static final common_google_play_services_update_title:I = 0x7f050012

.field public static final common_signin_button_text:I = 0x7f05001d

.field public static final common_signin_button_text_long:I = 0x7f05001e

.field public static final confirm:I = 0x7f050064

.field public static final confirmpass:I = 0x7f050066

.field public static final count:I = 0x7f05005b

.field public static final delete:I = 0x7f050026

.field public static final dep:I = 0x7f050035

.field public static final doneback:I = 0x7f05005e

.field public static final entername:I = 0x7f05002b

.field public static final enterpass:I = 0x7f050062

.field public static final erase:I = 0x7f050030

.field public static final everyday:I = 0x7f050056

.field public static final everyweek:I = 0x7f05005a

.field public static final exit:I = 0x7f050068

.field public static final exportto:I = 0x7f05004f

.field public static final folder:I = 0x7f050059

.field public static final foldername:I = 0x7f050057

.field public static final gallery:I = 0x7f050045

.field public static final home:I = 0x7f05003f

.field public static final importto:I = 0x7f05004e

.field public static final lang:I = 0x7f050021

.field public static final mapps:I = 0x7f050027

.field public static final menu_settings:I = 0x7f050022

.field public static final newfolder:I = 0x7f05006c

.field public static final newnote:I = 0x7f050038

.field public static final no:I = 0x7f05006b

.field public static final note:I = 0x7f05002e

.field public static final photo:I = 0x7f050039

.field public static final phototext:I = 0x7f05003a

.field public static final pic:I = 0x7f05002f

.field public static final r15m:I = 0x7f050047

.field public static final r25m:I = 0x7f050048

.field public static final r5m:I = 0x7f050046

.field public static final r60m:I = 0x7f050049

.field public static final rchoose:I = 0x7f05004a

.field public static final record:I = 0x7f05003d

.field public static final remind:I = 0x7f05004b

.field public static final rempass:I = 0x7f050060

.field public static final rlyexit:I = 0x7f050069

.field public static final save:I = 0x7f050029

.field public static final saving:I = 0x7f05003b

.field public static final savingimg:I = 0x7f05003c

.field public static final savingrec:I = 0x7f05003e

.field public static final search:I = 0x7f05002d

.field public static final setdep:I = 0x7f050034

.field public static final setname:I = 0x7f050033

.field public static final setpass:I = 0x7f05005f

.field public static final setpassall:I = 0x7f050061

.field public static final setstroke:I = 0x7f050032

.field public static final share:I = 0x7f050025

.field public static final shareus:I = 0x7f050028

.field public static final shopcount:I = 0x7f050042

.field public static final shoplist:I = 0x7f050040

.field public static final shopname:I = 0x7f050041

.field public static final shopprice:I = 0x7f050043

.field public static final shortcut:I = 0x7f050037

.field public static final stroke:I = 0x7f050031

.field public static final sucbackup:I = 0x7f050051

.field public static final sucimport:I = 0x7f050053

.field public static final suredel:I = 0x7f050036

.field public static final sync:I = 0x7f05005c

.field public static final title_activity_main:I = 0x7f050023

.field public static final useback:I = 0x7f050055

.field public static final video:I = 0x7f050067

.field public static final wait:I = 0x7f05004c

.field public static final wallet_buy_button_place_holder:I = 0x7f05001f

.field public static final wrongpass:I = 0x7f050063

.field public static final yes:I = 0x7f05006a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
