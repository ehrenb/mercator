.class Lru/andrey/notepad/CameraTextActivity$12;
.super Ljava/lang/Object;
.source "CameraTextActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/CameraTextActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/CameraTextActivity;

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$dp:Landroid/widget/DatePicker;

.field private final synthetic val$tp:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Lru/andrey/notepad/CameraTextActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/CameraTextActivity$12;->this$0:Lru/andrey/notepad/CameraTextActivity;

    iput-object p2, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$dp:Landroid/widget/DatePicker;

    iput-object p3, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$tp:Landroid/widget/TimePicker;

    iput-object p4, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$dialog:Landroid/app/Dialog;

    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 508
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 509
    .local v0, "c":Ljava/util/Calendar;
    const/4 v4, 0x5

    iget-object v5, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 510
    const/4 v4, 0x2

    iget-object v5, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 511
    const/4 v4, 0x1

    iget-object v5, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getYear()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 512
    const/16 v4, 0xb

    iget-object v5, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 513
    const/16 v4, 0xc

    iget-object v5, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 515
    iget-object v4, p0, Lru/andrey/notepad/CameraTextActivity$12;->this$0:Lru/andrey/notepad/CameraTextActivity;

    iget-object v4, v4, Lru/andrey/notepad/CameraTextActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 516
    .local v3, "t":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    .line 517
    const-string v3, " "

    .line 518
    :cond_0
    new-instance v1, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v1}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 519
    .local v1, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PhotoText/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 520
    iget-object v4, p0, Lru/andrey/notepad/CameraTextActivity$12;->this$0:Lru/andrey/notepad/CameraTextActivity;

    iget v4, v4, Lru/andrey/notepad/CameraTextActivity;->num:I

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 521
    iget-object v4, p0, Lru/andrey/notepad/CameraTextActivity$12;->this$0:Lru/andrey/notepad/CameraTextActivity;

    iget-object v4, v4, Lru/andrey/notepad/CameraTextActivity;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 522
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 523
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 524
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 525
    iget-object v4, p0, Lru/andrey/notepad/CameraTextActivity$12;->this$0:Lru/andrey/notepad/CameraTextActivity;

    iget-object v4, v4, Lru/andrey/notepad/CameraTextActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v1}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 527
    iget-object v4, p0, Lru/andrey/notepad/CameraTextActivity$12;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->dismiss()V

    .line 529
    return-void
.end method
