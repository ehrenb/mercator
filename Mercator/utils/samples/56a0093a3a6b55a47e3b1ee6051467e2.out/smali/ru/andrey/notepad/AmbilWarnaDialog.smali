.class public Lru/andrey/notepad/AmbilWarnaDialog;
.super Ljava/lang/Object;
.source "AmbilWarnaDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;
    }
.end annotation


# instance fields
.field final currentColorHsv:[F

.field final dialog:Landroid/app/AlertDialog;

.field final listener:Lru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;

.field final viewContainer:Landroid/view/ViewGroup;

.field final viewCursor:Landroid/widget/ImageView;

.field final viewHue:Landroid/view/View;

.field final viewNewColor:Landroid/view/View;

.field final viewOldColor:Landroid/view/View;

.field final viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

.field final viewTarget:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "color"    # I
    .param p3, "listener"    # Lru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    .line 42
    iput-object p3, p0, Lru/andrey/notepad/AmbilWarnaDialog;->listener:Lru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;

    .line 43
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    invoke-static {p2, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 45
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f030002

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 46
    .local v1, "view":Landroid/view/View;
    const v0, 0x7f070020

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewHue:Landroid/view/View;

    .line 47
    const v0, 0x7f07001f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/AmbilWarnaKotak;

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

    .line 48
    const v0, 0x7f070021

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewCursor:Landroid/widget/ImageView;

    .line 49
    const v0, 0x7f070024

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewOldColor:Landroid/view/View;

    .line 50
    const v0, 0x7f070025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewNewColor:Landroid/view/View;

    .line 51
    const v0, 0x7f070022

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewTarget:Landroid/widget/ImageView;

    .line 52
    const v0, 0x7f07001e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewContainer:Landroid/view/ViewGroup;

    .line 54
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

    invoke-direct {p0}, Lru/andrey/notepad/AmbilWarnaDialog;->getHue()F

    move-result v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/AmbilWarnaKotak;->setHue(F)V

    .line 55
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewOldColor:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 56
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewNewColor:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 58
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewHue:Landroid/view/View;

    new-instance v3, Lru/andrey/notepad/AmbilWarnaDialog$1;

    invoke-direct {v3, p0}, Lru/andrey/notepad/AmbilWarnaDialog$1;-><init>(Lru/andrey/notepad/AmbilWarnaDialog;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 85
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

    new-instance v3, Lru/andrey/notepad/AmbilWarnaDialog$2;

    invoke-direct {v3, p0}, Lru/andrey/notepad/AmbilWarnaDialog$2;-><init>(Lru/andrey/notepad/AmbilWarnaDialog;)V

    invoke-virtual {v0, v3}, Lru/andrey/notepad/AmbilWarnaKotak;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 117
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x104000a

    new-instance v4, Lru/andrey/notepad/AmbilWarnaDialog$3;

    invoke-direct {v4, p0}, Lru/andrey/notepad/AmbilWarnaDialog$3;-><init>(Lru/andrey/notepad/AmbilWarnaDialog;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 126
    const/high16 v3, 0x1040000

    new-instance v4, Lru/andrey/notepad/AmbilWarnaDialog$4;

    invoke-direct {v4, p0}, Lru/andrey/notepad/AmbilWarnaDialog$4;-><init>(Lru/andrey/notepad/AmbilWarnaDialog;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 135
    new-instance v3, Lru/andrey/notepad/AmbilWarnaDialog$5;

    invoke-direct {v3, p0}, Lru/andrey/notepad/AmbilWarnaDialog$5;-><init>(Lru/andrey/notepad/AmbilWarnaDialog;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 117
    iput-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->dialog:Landroid/app/AlertDialog;

    .line 148
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->dialog:Landroid/app/AlertDialog;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog;->setView(Landroid/view/View;IIII)V

    .line 151
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v6

    .line 152
    .local v6, "vto":Landroid/view/ViewTreeObserver;
    new-instance v0, Lru/andrey/notepad/AmbilWarnaDialog$6;

    invoke-direct {v0, p0, v1}, Lru/andrey/notepad/AmbilWarnaDialog$6;-><init>(Lru/andrey/notepad/AmbilWarnaDialog;Landroid/view/View;)V

    invoke-virtual {v6, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 161
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/AmbilWarnaDialog;F)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lru/andrey/notepad/AmbilWarnaDialog;->setHue(F)V

    return-void
.end method

.method static synthetic access$1(Lru/andrey/notepad/AmbilWarnaDialog;)F
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Lru/andrey/notepad/AmbilWarnaDialog;->getHue()F

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lru/andrey/notepad/AmbilWarnaDialog;)I
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Lru/andrey/notepad/AmbilWarnaDialog;->getColor()I

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lru/andrey/notepad/AmbilWarnaDialog;F)V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0, p1}, Lru/andrey/notepad/AmbilWarnaDialog;->setSat(F)V

    return-void
.end method

.method static synthetic access$4(Lru/andrey/notepad/AmbilWarnaDialog;F)V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0, p1}, Lru/andrey/notepad/AmbilWarnaDialog;->setVal(F)V

    return-void
.end method

.method private getColor()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method private getHue()F
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private getSat()F
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method private getVal()F
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method private setHue(F)V
    .locals 2
    .param p1, "hue"    # F

    .prologue
    .line 206
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 207
    return-void
.end method

.method private setSat(F)V
    .locals 2
    .param p1, "sat"    # F

    .prologue
    .line 211
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    const/4 v1, 0x1

    aput p1, v0, v1

    .line 212
    return-void
.end method

.method private setVal(F)V
    .locals 2
    .param p1, "val"    # F

    .prologue
    .line 216
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->currentColorHsv:[F

    const/4 v1, 0x2

    aput p1, v0, v1

    .line 217
    return-void
.end method


# virtual methods
.method public getDialog()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->dialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method protected moveCursor()V
    .locals 6

    .prologue
    .line 165
    iget-object v2, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewHue:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {p0}, Lru/andrey/notepad/AmbilWarnaDialog;->getHue()F

    move-result v3

    iget-object v4, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewHue:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x43b40000    # 360.0f

    div-float/2addr v3, v4

    sub-float v1, v2, v3

    .line 166
    .local v1, "y":F
    iget-object v2, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewHue:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    .line 167
    const/4 v1, 0x0

    .line 168
    :cond_0
    iget-object v2, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewCursor:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 169
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewHue:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-double v2, v2

    iget-object v4, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewCursor:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    sub-double/2addr v2, v4

    iget-object v4, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v4

    int-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 170
    iget-object v2, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewHue:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v1

    float-to-double v2, v2

    iget-object v4, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewCursor:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    sub-double/2addr v2, v4

    iget-object v4, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v4

    int-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 171
    iget-object v2, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewCursor:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    return-void
.end method

.method protected moveTarget()V
    .locals 7

    .prologue
    .line 176
    invoke-direct {p0}, Lru/andrey/notepad/AmbilWarnaDialog;->getSat()F

    move-result v3

    iget-object v4, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

    invoke-virtual {v4}, Lru/andrey/notepad/AmbilWarnaKotak;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float v1, v3, v4

    .line 177
    .local v1, "x":F
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {p0}, Lru/andrey/notepad/AmbilWarnaDialog;->getVal()F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

    invoke-virtual {v4}, Lru/andrey/notepad/AmbilWarnaKotak;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float v2, v3, v4

    .line 178
    .local v2, "y":F
    iget-object v3, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewTarget:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 179
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

    invoke-virtual {v3}, Lru/andrey/notepad/AmbilWarnaKotak;->getLeft()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v1

    float-to-double v3, v3

    iget-object v5, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewTarget:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    sub-double/2addr v3, v5

    iget-object v5, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v5

    int-to-double v5, v5

    sub-double/2addr v3, v5

    double-to-int v3, v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 180
    iget-object v3, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewSatVal:Lru/andrey/notepad/AmbilWarnaKotak;

    invoke-virtual {v3}, Lru/andrey/notepad/AmbilWarnaKotak;->getTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v2

    float-to-double v3, v3

    iget-object v5, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewTarget:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    sub-double/2addr v3, v5

    iget-object v5, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v5

    int-to-double v5, v5

    sub-double/2addr v3, v5

    double-to-int v3, v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 181
    iget-object v3, p0, Lru/andrey/notepad/AmbilWarnaDialog;->viewTarget:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lru/andrey/notepad/AmbilWarnaDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 222
    return-void
.end method
