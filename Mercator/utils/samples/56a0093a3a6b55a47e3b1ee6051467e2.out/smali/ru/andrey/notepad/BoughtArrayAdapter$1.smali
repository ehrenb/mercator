.class Lru/andrey/notepad/BoughtArrayAdapter$1;
.super Ljava/lang/Object;
.source "BoughtArrayAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/BoughtArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/BoughtArrayAdapter;

.field private final synthetic val$ff:Lru/andrey/notepad/BuyModel;

.field private final synthetic val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/BoughtArrayAdapter;Lru/andrey/notepad/BuyModel;Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->this$0:Lru/andrey/notepad/BoughtArrayAdapter;

    iput-object p2, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$ff:Lru/andrey/notepad/BuyModel;

    iput-object p3, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iput p4, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$position:I

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$ff:Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0}, Lru/andrey/notepad/BuyModel;->getisDone()I

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iget-object v0, v0, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v1, 0x7f020051

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 79
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$ff:Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0, v3}, Lru/andrey/notepad/BuyModel;->setisDone(I)V

    .line 80
    sget-object v0, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    iget v1, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$position:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0, v3}, Lru/andrey/notepad/BuyModel;->setisDone(I)V

    .line 81
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iget-object v0, v0, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$ff:Lru/andrey/notepad/BuyModel;

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iget-object v0, v0, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iget-object v1, v1, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 91
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iget-object v0, v0, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 87
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$ff:Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0, v2}, Lru/andrey/notepad/BuyModel;->setisDone(I)V

    .line 88
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iget-object v0, v0, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$holder:Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    iget-object v1, v1, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    and-int/lit8 v1, v1, -0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 89
    sget-object v0, Lru/andrey/notepad/BuyActivity;->item:Ljava/util/ArrayList;

    iget v1, p0, Lru/andrey/notepad/BoughtArrayAdapter$1;->val$position:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/andrey/notepad/BuyModel;

    invoke-virtual {v0, v2}, Lru/andrey/notepad/BuyModel;->setisDone(I)V

    goto :goto_0
.end method
