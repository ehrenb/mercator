.class Lru/andrey/notepad/AddActivity$4;
.super Ljava/lang/Object;
.source "AddActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/AddActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/AddActivity;

.field private final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/AddActivity;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    iput-object p2, p0, Lru/andrey/notepad/AddActivity$4;->val$input:Landroid/widget/EditText;

    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 375
    iget-object v3, p0, Lru/andrey/notepad/AddActivity$4;->val$input:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 377
    .local v2, "value":Ljava/lang/String;
    new-instance v0, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v0}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 378
    .local v0, "om":Lru/andrey/notepad/ObjectModel;
    iget-object v3, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v3, v3, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 379
    iget-object v3, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    iget v3, v3, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 380
    invoke-virtual {v0, v2}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 381
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm MM-dd-yyyy"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 382
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 383
    iget-object v3, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v3, v3, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v3, v0}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 384
    iget-object v3, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v5, v5, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "notecolor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v5, v5, Lru/andrey/notepad/AddActivity;->bgcolor:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 386
    iget-object v3, p0, Lru/andrey/notepad/AddActivity$4;->this$0:Lru/andrey/notepad/AddActivity;

    invoke-virtual {v3}, Lru/andrey/notepad/AddActivity;->finish()V

    .line 387
    return-void
.end method
