.class public Lru/andrey/notepad/BoughtArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "BoughtArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lru/andrey/notepad/BuyModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Activity;

.field private final obj:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/BuyModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/BuyModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "obj":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/BuyModel;>;"
    const v0, 0x7f030013

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 22
    iput-object p1, p0, Lru/andrey/notepad/BoughtArrayAdapter;->context:Landroid/app/Activity;

    .line 23
    iput-object p2, p0, Lru/andrey/notepad/BoughtArrayAdapter;->obj:Ljava/util/ArrayList;

    .line 24
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/BoughtArrayAdapter;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lru/andrey/notepad/BoughtArrayAdapter;->obj:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 39
    move-object v4, p2

    .line 40
    .local v4, "rowView":Landroid/view/View;
    if-nez v4, :cond_4

    .line 42
    iget-object v5, p0, Lru/andrey/notepad/BoughtArrayAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 43
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030013

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 44
    new-instance v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    invoke-direct {v2}, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;-><init>()V

    .line 45
    .local v2, "holder":Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;
    const v5, 0x7f070037

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 46
    const v5, 0x7f07003e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    .line 47
    const v5, 0x7f070030

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    .line 48
    const v5, 0x7f070038

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv2:Landroid/widget/ImageView;

    .line 49
    const v5, 0x7f070039

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv3:Landroid/widget/ImageView;

    .line 51
    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 58
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v5, p0, Lru/andrey/notepad/BoughtArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/BuyModel;

    .line 59
    .local v1, "ff":Lru/andrey/notepad/BuyModel;
    iget-object v5, p0, Lru/andrey/notepad/BoughtArrayAdapter;->context:Landroid/app/Activity;

    invoke-static {v5}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 60
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getisDone()I

    move-result v5

    if-nez v5, :cond_5

    .line 62
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v6, 0x7f02003c

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 63
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v6, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v6

    and-int/lit8 v6, v6, -0x11

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 71
    :goto_1
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    new-instance v6, Lru/andrey/notepad/BoughtArrayAdapter$1;

    invoke-direct {v6, p0, v1, v2, p1}, Lru/andrey/notepad/BoughtArrayAdapter$1;-><init>(Lru/andrey/notepad/BoughtArrayAdapter;Lru/andrey/notepad/BuyModel;Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;I)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv2:Landroid/widget/ImageView;

    new-instance v6, Lru/andrey/notepad/BoughtArrayAdapter$2;

    invoke-direct {v6, p0, p1}, Lru/andrey/notepad/BoughtArrayAdapter$2;-><init>(Lru/andrey/notepad/BoughtArrayAdapter;I)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv3:Landroid/widget/ImageView;

    new-instance v6, Lru/andrey/notepad/BoughtArrayAdapter$3;

    invoke-direct {v6, p0, v0, p1}, Lru/andrey/notepad/BoughtArrayAdapter$3;-><init>(Lru/andrey/notepad/BoughtArrayAdapter;Lru/andrey/notepad/DataBaseHelper;I)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v5

    if-eq v5, v8, :cond_0

    .line 123
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Qty - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    :cond_0
    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v5

    if-eq v5, v8, :cond_1

    .line 128
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Price - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    :cond_1
    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v5

    if-eq v5, v8, :cond_2

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v5

    if-eq v5, v8, :cond_2

    .line 133
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Qty - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " price - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    :cond_2
    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getQuantity()I

    move-result v5

    if-ne v5, v8, :cond_3

    invoke-virtual {v1}, Lru/andrey/notepad/BuyModel;->getPrice()I

    move-result v5

    if-ne v5, v8, :cond_3

    .line 138
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    :cond_3
    return-object v4

    .line 55
    .end local v0    # "dh":Lru/andrey/notepad/DataBaseHelper;
    .end local v1    # "ff":Lru/andrey/notepad/BuyModel;
    .end local v2    # "holder":Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;
    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;

    .restart local v2    # "holder":Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;
    goto/16 :goto_0

    .line 67
    .restart local v0    # "dh":Lru/andrey/notepad/DataBaseHelper;
    .restart local v1    # "ff":Lru/andrey/notepad/BuyModel;
    :cond_5
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v6, 0x7f020051

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 68
    iget-object v5, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v6, v2, Lru/andrey/notepad/BoughtArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v6

    or-int/lit8 v6, v6, 0x10

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto/16 :goto_1
.end method
