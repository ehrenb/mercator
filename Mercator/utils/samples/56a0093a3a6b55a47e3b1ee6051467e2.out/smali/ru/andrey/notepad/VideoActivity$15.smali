.class Lru/andrey/notepad/VideoActivity$15;
.super Ljava/lang/Object;
.source "VideoActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/VideoActivity;->showSave()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/VideoActivity;

.field private final synthetic val$count:I

.field private final synthetic val$edit:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/VideoActivity;ILandroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/VideoActivity$15;->this$0:Lru/andrey/notepad/VideoActivity;

    iput p2, p0, Lru/andrey/notepad/VideoActivity$15;->val$count:I

    iput-object p3, p0, Lru/andrey/notepad/VideoActivity$15;->val$edit:Landroid/widget/EditText;

    .line 679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 683
    iget-object v3, p0, Lru/andrey/notepad/VideoActivity$15;->this$0:Lru/andrey/notepad/VideoActivity;

    iget-object v3, v3, Lru/andrey/notepad/VideoActivity;->text:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 684
    .local v2, "t":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 685
    const-string v2, " "

    .line 687
    :cond_0
    new-instance v0, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v0}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 688
    .local v0, "om":Lru/andrey/notepad/ObjectModel;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Video/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 689
    iget v3, p0, Lru/andrey/notepad/VideoActivity$15;->val$count:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 690
    iget-object v3, p0, Lru/andrey/notepad/VideoActivity$15;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 692
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm MM-dd-yyyy"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 693
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 694
    iget-object v3, p0, Lru/andrey/notepad/VideoActivity$15;->this$0:Lru/andrey/notepad/VideoActivity;

    iget-object v3, v3, Lru/andrey/notepad/VideoActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v3, v0}, Lru/andrey/notepad/DataBaseHelper;->addObject(Lru/andrey/notepad/ObjectModel;)V

    .line 695
    iget-object v3, p0, Lru/andrey/notepad/VideoActivity$15;->this$0:Lru/andrey/notepad/VideoActivity;

    iget-object v4, p0, Lru/andrey/notepad/VideoActivity$15;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lru/andrey/notepad/VideoActivity;->name:Ljava/lang/String;

    .line 696
    iget-object v3, p0, Lru/andrey/notepad/VideoActivity$15;->this$0:Lru/andrey/notepad/VideoActivity;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "notecolor"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lru/andrey/notepad/VideoActivity$15;->this$0:Lru/andrey/notepad/VideoActivity;

    iget-object v5, v5, Lru/andrey/notepad/VideoActivity;->bgcolor:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 697
    new-instance v3, Lru/andrey/notepad/VideoActivity$saveTask;

    iget-object v4, p0, Lru/andrey/notepad/VideoActivity$15;->this$0:Lru/andrey/notepad/VideoActivity;

    invoke-direct {v3, v4}, Lru/andrey/notepad/VideoActivity$saveTask;-><init>(Lru/andrey/notepad/VideoActivity;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lru/andrey/notepad/VideoActivity$saveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 698
    return-void
.end method
