.class Lru/andrey/notepad/PassActivity$1;
.super Ljava/lang/Object;
.source "PassActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/PassActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/PassActivity;

.field private final synthetic val$value:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/PassActivity;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    iput-object p2, p0, Lru/andrey/notepad/PassActivity$1;->val$value:Landroid/widget/EditText;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 32
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->val$value:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v5, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "pass"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v2, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 38
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 41
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 42
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    .line 124
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v2, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Camera"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 46
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 48
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 51
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 52
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    goto :goto_0

    .line 54
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v2, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PhotoText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 56
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 58
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    goto/16 :goto_0

    .line 64
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v2, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GalleryText"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 66
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 72
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    goto/16 :goto_0

    .line 74
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v2, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Record"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 76
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 78
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 81
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 82
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    goto/16 :goto_0

    .line 85
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v2, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Shop"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 87
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 89
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 92
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 93
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    goto/16 :goto_0

    .line 96
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_6
    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v2, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 98
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 100
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 103
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 104
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    goto/16 :goto_0

    .line 109
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    const-class v2, Lru/andrey/notepad/AddActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "new"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 111
    const-string v2, "name"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v2, "body"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v2, "color"

    sget-object v1, Lru/andrey/notepad/UpdateService;->rev:Ljava/util/ArrayList;

    sget v3, Lru/andrey/notepad/UpdateService;->pos:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1, v0}, Lru/andrey/notepad/PassActivity;->startActivity(Landroid/content/Intent;)V

    .line 115
    iget-object v1, p0, Lru/andrey/notepad/PassActivity$1;->this$0:Lru/andrey/notepad/PassActivity;

    invoke-virtual {v1}, Lru/andrey/notepad/PassActivity;->finish()V

    goto/16 :goto_0
.end method
