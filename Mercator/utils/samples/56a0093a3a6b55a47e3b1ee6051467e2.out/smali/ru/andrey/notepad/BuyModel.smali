.class public Lru/andrey/notepad/BuyModel;
.super Ljava/lang/Object;
.source "BuyModel.java"


# instance fields
.field private isDone:I

.field private listId:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private pos:I

.field private price:I

.field private quantity:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput v0, p0, Lru/andrey/notepad/BuyModel;->price:I

    .line 8
    iput v0, p0, Lru/andrey/notepad/BuyModel;->quantity:I

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lru/andrey/notepad/BuyModel;->isDone:I

    .line 3
    return-void
.end method


# virtual methods
.method public getListId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lru/andrey/notepad/BuyModel;->listId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lru/andrey/notepad/BuyModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPos()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lru/andrey/notepad/BuyModel;->pos:I

    return v0
.end method

.method public getPrice()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lru/andrey/notepad/BuyModel;->price:I

    return v0
.end method

.method public getQuantity()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lru/andrey/notepad/BuyModel;->quantity:I

    return v0
.end method

.method public getisDone()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lru/andrey/notepad/BuyModel;->isDone:I

    return v0
.end method

.method public setListId(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lru/andrey/notepad/BuyModel;->listId:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lru/andrey/notepad/BuyModel;->name:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setPos(I)V
    .locals 0
    .param p1, "text"    # I

    .prologue
    .line 69
    iput p1, p0, Lru/andrey/notepad/BuyModel;->pos:I

    .line 70
    return-void
.end method

.method public setPrice(I)V
    .locals 0
    .param p1, "text"    # I

    .prologue
    .line 39
    iput p1, p0, Lru/andrey/notepad/BuyModel;->price:I

    .line 40
    return-void
.end method

.method public setQuantity(I)V
    .locals 0
    .param p1, "text"    # I

    .prologue
    .line 49
    iput p1, p0, Lru/andrey/notepad/BuyModel;->quantity:I

    .line 50
    return-void
.end method

.method public setisDone(I)V
    .locals 0
    .param p1, "text"    # I

    .prologue
    .line 59
    iput p1, p0, Lru/andrey/notepad/BuyModel;->isDone:I

    .line 60
    return-void
.end method
