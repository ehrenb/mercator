.class Lru/andrey/notepad/UnZipper$UnZipTask;
.super Landroid/os/AsyncTask;
.source "UnZipper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/andrey/notepad/UnZipper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnZipTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/UnZipper;


# direct methods
.method private constructor <init>(Lru/andrey/notepad/UnZipper;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lru/andrey/notepad/UnZipper$UnZipTask;->this$0:Lru/andrey/notepad/UnZipper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/andrey/notepad/UnZipper;Lru/andrey/notepad/UnZipper$UnZipTask;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lru/andrey/notepad/UnZipper$UnZipTask;-><init>(Lru/andrey/notepad/UnZipper;)V

    return-void
.end method

.method private createDir(Ljava/io/File;)V
    .locals 3
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    .line 121
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    return-void

    .line 125
    :cond_1
    const-string v0, "UnZip"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Creating dir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not create dir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    .locals 6
    .param p1, "zipfile"    # Ljava/util/zip/ZipFile;
    .param p2, "entry"    # Ljava/util/zip/ZipEntry;
    .param p3, "outputDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    new-instance v3, Ljava/io/File;

    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lru/andrey/notepad/UnZipper$UnZipTask;->createDir(Ljava/io/File;)V

    .line 117
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p3, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .local v1, "outputFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 101
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {p0, v3}, Lru/andrey/notepad/UnZipper$UnZipTask;->createDir(Ljava/io/File;)V

    .line 104
    :cond_1
    const-string v3, "UnZip"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Extracting: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-virtual {p1, p2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 106
    .local v0, "inputStream":Ljava/io/BufferedInputStream;
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 110
    .local v2, "outputStream":Ljava/io/BufferedOutputStream;
    :try_start_0
    invoke-static {v0, v2}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 115
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v3

    .line 114
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 115
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 116
    throw v3
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 10
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 60
    aget-object v4, p1, v9

    .line 61
    .local v4, "filePath":Ljava/lang/String;
    aget-object v1, p1, v7

    .line 63
    .local v1, "destinationPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, "archive":Ljava/io/File;
    :try_start_0
    new-instance v5, Ljava/util/zip/ZipFile;

    invoke-direct {v5, v0}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 67
    .local v5, "zipfile":Ljava/util/zip/ZipFile;
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v2

    .local v2, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_0

    .line 79
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .end local v2    # "e":Ljava/util/Enumeration;
    .end local v5    # "zipfile":Ljava/util/zip/ZipFile;
    :goto_1
    return-object v6

    .line 69
    .restart local v2    # "e":Ljava/util/Enumeration;
    .restart local v5    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    :try_start_1
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/zip/ZipEntry;

    .line 70
    .local v3, "entry":Ljava/util/zip/ZipEntry;
    invoke-direct {p0, v5, v3, v1}, Lru/andrey/notepad/UnZipper$UnZipTask;->unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 73
    .end local v2    # "e":Ljava/util/Enumeration;
    .end local v3    # "entry":Ljava/util/zip/ZipEntry;
    .end local v5    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v2

    .line 75
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "UnZip"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Error while extracting file "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_1
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/UnZipper$UnZipTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 85
    iget-object v0, p0, Lru/andrey/notepad/UnZipper$UnZipTask;->this$0:Lru/andrey/notepad/UnZipper;

    # invokes: Lru/andrey/notepad/UnZipper;->setChanged()V
    invoke-static {v0}, Lru/andrey/notepad/UnZipper;->access$0(Lru/andrey/notepad/UnZipper;)V

    .line 86
    iget-object v0, p0, Lru/andrey/notepad/UnZipper$UnZipTask;->this$0:Lru/andrey/notepad/UnZipper;

    invoke-virtual {v0}, Lru/andrey/notepad/UnZipper;->notifyObservers()V

    .line 87
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lru/andrey/notepad/UnZipper$UnZipTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
