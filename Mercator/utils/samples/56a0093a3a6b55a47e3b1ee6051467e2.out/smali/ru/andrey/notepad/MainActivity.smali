.class public Lru/andrey/notepad/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/MainActivity$bannerSendTask;,
        Lru/andrey/notepad/MainActivity$bannerTask;,
        Lru/andrey/notepad/MainActivity$importTask;,
        Lru/andrey/notepad/MainActivity$mainTask;,
        Lru/andrey/notepad/MainActivity$saveTask;
    }
.end annotation


# static fields
.field private static final CAM_REQUEST:I = 0x0

.field private static final CAM_REQUEST2:I = 0x1

.field private static final IDD_BAD_IMPORT:I = 0x5

.field private static final IDD_BAD_PASS:I = 0x6

.field private static final IDD_EXIT:I = 0x7

.field private static final IDD_IMPORT_PROGRESS:I = 0x3

.field private static final IDD_SAVE_PROGRESS:I = 0x2

.field private static final IDD_SUC_BACKUP:I = 0x3

.field private static final IDD_SUC_IMPORT:I = 0x4

.field private static final MENU_ITEM_1_ACTION:I = 0x64

.field private static final MENU_ITEM_2_ACTION:I = 0x66

.field private static final MENU_ITEM_3_ACTION:I = 0x67

.field private static final MENU_ITEM_4_ACTION:I = 0x68

.field private static final MENU_ITEM_5_ACTION:I = 0x69

.field private static final MENU_ITEM_6_ACTION:I = 0x6a

.field private static final MENU_ITEM_7_ACTION:I = 0x6b

.field private static final MENU_ITEM_8_ACTION:I = 0x6c

.field private static final SELECT_PHOTO:I = 0x2

.field static arr:Lru/andrey/notepad/ObjectArrayAdapter;

.field static cambmp:Landroid/graphics/Bitmap;

.field static ctx:Landroid/content/Context;

.field static dh:Lru/andrey/notepad/DataBaseHelper;

.field static isSearch:Z

.field static lv:Landroid/widget/ListView;

.field static object:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field

.field static p:Ljava/io/File;

.field static remPos:I

.field static sobject:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final CONTEXT_MENU_ID:I

.field IMAGE_MAX_SIZE:I

.field ImportProgress:Landroid/app/ProgressDialog;

.field SaveProgress:Landroid/app/ProgressDialog;

.field ad:Landroid/widget/Button;

.field add:Landroid/widget/Button;

.field archive:Ljava/io/File;

.field bg:Landroid/widget/RelativeLayout;

.field color:Ljava/lang/String;

.field folder:Landroid/widget/Button;

.field private iconContextMenu:Lru/andrey/notepad/IconContextMenu;

.field imageUri:Landroid/net/Uri;

.field imageUrl:Ljava/lang/String;

.field lastAction:I

.field like:Landroid/widget/Button;

.field link:Ljava/lang/String;

.field menu:Landroid/widget/Button;

.field name:Ljava/lang/String;

.field outputDir:Ljava/lang/String;

.field search:Landroid/widget/EditText;

.field type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    .line 95
    const/4 v0, 0x0

    sput-boolean v0, Lru/andrey/notepad/MainActivity;->isSearch:Z

    .line 118
    const/4 v0, -0x1

    sput v0, Lru/andrey/notepad/MainActivity;->remPos:I

    .line 126
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 99
    const/4 v0, 0x1

    iput v0, p0, Lru/andrey/notepad/MainActivity;->CONTEXT_MENU_ID:I

    .line 100
    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    .line 117
    const/16 v0, 0x400

    iput v0, p0, Lru/andrey/notepad/MainActivity;->IMAGE_MAX_SIZE:I

    .line 135
    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->imageUrl:Ljava/lang/String;

    .line 86
    return-void
.end method

.method private ImageOperations(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "saveFilename"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 956
    :try_start_0
    invoke-virtual {p0, p2}, Lru/andrey/notepad/MainActivity;->fetch(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/InputStream;

    .line 957
    .local v2, "is":Ljava/io/InputStream;
    invoke-static {v2, p3}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 968
    .end local v2    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v0

    .line 960
    :catch_0
    move-exception v1

    .line 962
    .local v1, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0

    .line 965
    .end local v1    # "e":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 967
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 822
    const/16 v2, 0x400

    new-array v0, v2, [B

    .line 824
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "read":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 828
    return-void

    .line 826
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private createDir(Ljava/io/File;)V
    .locals 3
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    .line 948
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 949
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not create dir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 950
    :cond_0
    return-void
.end method

.method private decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "f"    # Ljava/io/File;

    .prologue
    .line 1043
    const-string v5, "Test"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    const/4 v0, 0x0

    .line 1048
    .local v0, "b":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1049
    .local v2, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1051
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1052
    .local v1, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    invoke-static {v1, v5, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1053
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 1055
    const/4 v4, 0x1

    .line 1056
    .local v4, "scale":I
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v6, p0, Lru/andrey/notepad/MainActivity;->IMAGE_MAX_SIZE:I

    if-gt v5, v6, :cond_0

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, p0, Lru/andrey/notepad/MainActivity;->IMAGE_MAX_SIZE:I

    if-le v5, v6, :cond_1

    .line 1058
    :cond_0
    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    iget v7, p0, Lru/andrey/notepad/MainActivity;->IMAGE_MAX_SIZE:I

    int-to-double v7, v7

    iget v9, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v10, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    int-to-double v9, v9

    div-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->log(D)D

    move-result-wide v7

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    invoke-static {v9, v10}, Ljava/lang/Math;->log(D)D

    move-result-wide v9

    div-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    long-to-int v7, v7

    int-to-double v7, v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    double-to-int v4, v5

    .line 1062
    :cond_1
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1063
    .local v3, "o2":Landroid/graphics/BitmapFactory$Options;
    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1064
    const/4 v5, 0x0

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 1065
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 1066
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 1067
    const/16 v5, 0x4000

    new-array v5, v5, [B

    iput-object v5, v3, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 1069
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fis":Ljava/io/FileInputStream;
    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1070
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    invoke-static {v1, v5, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1071
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "o":Landroid/graphics/BitmapFactory$Options;
    .end local v3    # "o2":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "scale":I
    :goto_0
    return-object v0

    .line 1073
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public static openMenu(Landroid/content/Context;Landroid/view/View;I)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "pos"    # I

    .prologue
    .line 1731
    sput p2, Lru/andrey/notepad/MainActivity;->remPos:I

    move-object v0, p0

    .line 1732
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 1733
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "ctx":Landroid/content/Context;
    invoke-virtual {p0, p1}, Landroid/app/Activity;->openContextMenu(Landroid/view/View;)V

    .line 1734
    return-void
.end method

.method private unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    .locals 5
    .param p1, "zipfile"    # Ljava/util/zip/ZipFile;
    .param p2, "entry"    # Ljava/util/zip/ZipEntry;
    .param p3, "outputDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 920
    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 922
    new-instance v3, Ljava/io/File;

    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lru/andrey/notepad/MainActivity;->createDir(Ljava/io/File;)V

    .line 944
    :goto_0
    return-void

    .line 926
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p3, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    .local v1, "outputFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 929
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {p0, v3}, Lru/andrey/notepad/MainActivity;->createDir(Ljava/io/File;)V

    .line 932
    :cond_1
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-virtual {p1, p2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 933
    .local v0, "inputStream":Ljava/io/BufferedInputStream;
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 937
    .local v2, "outputStream":Ljava/io/BufferedOutputStream;
    :try_start_0
    invoke-static {v0, v2}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 941
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 942
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_0

    .line 940
    :catchall_0
    move-exception v3

    .line 941
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 942
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 943
    throw v3
.end method

.method public static update()V
    .locals 3

    .prologue
    .line 1738
    sget-object v0, Lru/andrey/notepad/MainActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v0}, Lru/andrey/notepad/DataBaseHelper;->getNotes()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    .line 1739
    sget-object v0, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1741
    sget-boolean v0, Lru/andrey/notepad/MainActivity;->isSearch:Z

    if-eqz v0, :cond_0

    .line 1743
    new-instance v1, Lru/andrey/notepad/ObjectArrayAdapter;

    sget-object v0, Lru/andrey/notepad/MainActivity;->ctx:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    sget-object v2, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-direct {v1, v0, v2}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    .line 1751
    :goto_0
    sget-object v0, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1752
    return-void

    .line 1748
    :cond_0
    new-instance v1, Lru/andrey/notepad/ObjectArrayAdapter;

    sget-object v0, Lru/andrey/notepad/MainActivity;->ctx:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    sget-object v2, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v1, v0, v2}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    goto :goto_0
.end method


# virtual methods
.method public UnZip(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p1, "ziparchive"    # Ljava/io/File;
    .param p2, "directory"    # Ljava/lang/String;

    .prologue
    .line 867
    iput-object p1, p0, Lru/andrey/notepad/MainActivity;->archive:Ljava/io/File;

    .line 868
    iput-object p2, p0, Lru/andrey/notepad/MainActivity;->outputDir:Ljava/lang/String;

    .line 872
    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    iget-object v3, p0, Lru/andrey/notepad/MainActivity;->archive:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 873
    .local v2, "zipfile":Ljava/util/zip/ZipFile;
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_0

    .line 882
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :goto_1
    return-void

    .line 875
    .restart local v0    # "e":Ljava/util/Enumeration;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    .line 877
    .local v1, "entry":Ljava/util/zip/ZipEntry;
    iget-object v3, p0, Lru/andrey/notepad/MainActivity;->outputDir:Ljava/lang/String;

    invoke-direct {p0, v2, v1, v3}, Lru/andrey/notepad/MainActivity;->unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 880
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "entry":Ljava/util/zip/ZipEntry;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public appendSQLLog(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1438
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/NotepadBackup/database.sql"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1439
    .local v2, "logFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1443
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1454
    :cond_0
    :goto_0
    :try_start_1
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v0, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 1455
    .local v0, "buf":Ljava/io/BufferedWriter;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "#SQLITE#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1456
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V

    .line 1457
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1464
    .end local v0    # "buf":Ljava/io/BufferedWriter;
    :goto_1
    return-void

    .line 1445
    :catch_0
    move-exception v1

    .line 1448
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1459
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1462
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public checkAndCreateDirectory(Ljava/lang/String;)V
    .locals 3
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 832
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 833
    .local v0, "new_dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 835
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 837
    :cond_0
    return-void
.end method

.method public copyFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    .line 843
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    .line 845
    .local v8, "sd":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 847
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 848
    .local v10, "sourcePath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 849
    .local v7, "destinationPath":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v8, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 850
    .local v9, "source":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v8, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 851
    .local v6, "destination":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 853
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 854
    .local v1, "src":Ljava/nio/channels/FileChannel;
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 855
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 856
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 857
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 863
    .end local v0    # "dst":Ljava/nio/channels/FileChannel;
    .end local v1    # "src":Ljava/nio/channels/FileChannel;
    .end local v6    # "destination":Ljava/io/File;
    .end local v7    # "destinationPath":Ljava/lang/String;
    .end local v8    # "sd":Ljava/io/File;
    .end local v9    # "source":Ljava/io/File;
    .end local v10    # "sourcePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 861
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public fetch(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 974
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 975
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->getContent()Ljava/lang/Object;

    move-result-object v0

    .line 976
    .local v0, "content":Ljava/lang/Object;
    return-object v0
.end method

.method protected find(Ljava/lang/String;)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1468
    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1470
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1478
    new-instance v1, Lru/andrey/notepad/ObjectArrayAdapter;

    sget-object v2, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    .line 1479
    sget-object v1, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    sget-object v2, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1481
    sget-object v1, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    new-instance v2, Lru/andrey/notepad/MainActivity$15;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$15;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1660
    return-void

    .line 1472
    :cond_0
    sget-object v1, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1474
    sget-object v2, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    sget-object v1, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1470
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 13
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 982
    invoke-super/range {p0 .. p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 983
    packed-switch p1, :pswitch_data_0

    .line 1039
    :cond_0
    :goto_0
    return-void

    .line 986
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 988
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 989
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v0

    .line 991
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 992
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 994
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 995
    .local v6, "columnIndex":I
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 996
    .local v9, "filePath":Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 997
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 998
    .local v12, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x3

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1000
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1001
    .local v8, "f":Ljava/io/File;
    invoke-direct {p0, v8}, Lru/andrey/notepad/MainActivity;->decodeFile(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/MainActivity;->cambmp:Landroid/graphics/Bitmap;

    .line 1002
    new-instance v11, Landroid/content/Intent;

    const-class v0, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v11, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1003
    .local v11, "intent":Landroid/content/Intent;
    const-string v0, "new"

    const/4 v3, 0x1

    invoke-virtual {v11, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1004
    const/4 v0, 0x2

    invoke-virtual {p0, v11, v0}, Lru/andrey/notepad/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1008
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "columnIndex":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "f":Ljava/io/File;
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1010
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1011
    .restart local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 1012
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1013
    const/4 v0, 0x3

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1014
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    .line 1015
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v12, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1016
    sget-object v0, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/MainActivity;->cambmp:Landroid/graphics/Bitmap;

    .line 1017
    new-instance v10, Landroid/content/Intent;

    const-class v0, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v10, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1018
    .local v10, "in2":Landroid/content/Intent;
    const-string v0, "new"

    const/4 v3, 0x1

    invoke-virtual {v10, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1019
    invoke-virtual {p0, v10}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1023
    .end local v10    # "in2":Landroid/content/Intent;
    .end local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    :pswitch_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1025
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1026
    .restart local v12    # "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 1027
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1028
    const/4 v0, 0x3

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1029
    const/4 v0, 0x0

    iput-boolean v0, v12, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    .line 1030
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v12, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1031
    sget-object v0, Lru/andrey/notepad/MainActivity;->p:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/MainActivity;->cambmp:Landroid/graphics/Bitmap;

    .line 1032
    new-instance v10, Landroid/content/Intent;

    const-class v0, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v10, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1033
    .restart local v10    # "in2":Landroid/content/Intent;
    const-string v0, "new"

    const/4 v3, 0x1

    invoke-virtual {v10, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1034
    invoke-virtual {p0, v10}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 983
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 144
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 145
    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->setContentView(I)V

    .line 146
    const v1, 0x7f070015

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/AdView;

    .line 147
    .local v0, "mAdView":Lcom/google/android/gms/ads/AdView;
    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 156
    const v1, 0x7f070017

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    sput-object v1, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    .line 157
    invoke-static {p0}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v1

    sput-object v1, Lru/andrey/notepad/MainActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    .line 158
    sget-object v1, Lru/andrey/notepad/MainActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v1}, Lru/andrey/notepad/DataBaseHelper;->getNotes()Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    .line 159
    new-instance v1, Lru/andrey/notepad/ObjectArrayAdapter;

    sget-object v2, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v1, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    .line 160
    sget-object v1, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    sget-object v2, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 161
    const v1, 0x7f070016

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->search:Landroid/widget/EditText;

    .line 162
    sput-object p0, Lru/andrey/notepad/MainActivity;->ctx:Landroid/content/Context;

    .line 163
    const v1, 0x7f070018

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->add:Landroid/widget/Button;

    .line 164
    const v1, 0x7f070019

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->like:Landroid/widget/Button;

    .line 165
    const v1, 0x7f07001a

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->ad:Landroid/widget/Button;

    .line 166
    const v1, 0x7f07001b

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->menu:Landroid/widget/Button;

    .line 167
    const v1, 0x7f07001c

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->folder:Landroid/widget/Button;

    .line 169
    const v1, 0x7f070014

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->bg:Landroid/widget/RelativeLayout;

    .line 170
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->bg:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "color"

    const-string v4, "#dad07f"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 171
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->menu:Landroid/widget/Button;

    new-instance v2, Lru/andrey/notepad/MainActivity$1;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$1;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->folder:Landroid/widget/Button;

    new-instance v2, Lru/andrey/notepad/MainActivity$2;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$2;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    const-string v1, "/Notepad"

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->checkAndCreateDirectory(Ljava/lang/String;)V

    .line 189
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lru/andrey/notepad/UpdateService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 190
    new-instance v1, Lru/andrey/notepad/IconContextMenu;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lru/andrey/notepad/IconContextMenu;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    .line 191
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05002e

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020044

    const/16 v5, 0x64

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 192
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05002f

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02000b

    const/16 v5, 0x66

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 193
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050039

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020009

    const/16 v5, 0x67

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 194
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05003a

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020050

    const/16 v5, 0x68

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 195
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05003d

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02004d

    const/16 v5, 0x69

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 196
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050040

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020035

    const/16 v5, 0x6b

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 197
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050045

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020036

    const/16 v5, 0x6a

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 198
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050067

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020037

    const/16 v5, 0x6c

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/andrey/notepad/IconContextMenu;->addItem(Landroid/content/res/Resources;Ljava/lang/CharSequence;II)V

    .line 202
    new-instance v1, Lru/andrey/notepad/MainActivity$mainTask;

    invoke-direct {v1, p0}, Lru/andrey/notepad/MainActivity$mainTask;-><init>(Lru/andrey/notepad/MainActivity;)V

    new-array v2, v6, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Lru/andrey/notepad/MainActivity$mainTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 204
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    new-instance v2, Lru/andrey/notepad/MainActivity$3;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$3;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Lru/andrey/notepad/IconContextMenu;->setOnClickListener(Lru/andrey/notepad/IconContextMenu$IconContextMenuOnClickListener;)V

    .line 259
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->add:Landroid/widget/Button;

    new-instance v2, Lru/andrey/notepad/MainActivity$4;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$4;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->like:Landroid/widget/Button;

    new-instance v2, Lru/andrey/notepad/MainActivity$5;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$5;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    iget-object v1, p0, Lru/andrey/notepad/MainActivity;->search:Landroid/widget/EditText;

    new-instance v2, Lru/andrey/notepad/MainActivity$6;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$6;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 486
    sget-object v1, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    new-instance v2, Lru/andrey/notepad/MainActivity$7;

    invoke-direct {v2, p0}, Lru/andrey/notepad/MainActivity$7;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 665
    new-instance v1, Lru/andrey/notepad/MainActivity$bannerTask;

    invoke-direct {v1, p0}, Lru/andrey/notepad/MainActivity$bannerTask;-><init>(Lru/andrey/notepad/MainActivity;)V

    new-array v2, v6, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Lru/andrey/notepad/MainActivity$bannerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {p0}, Lru/andrey/notepad/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Hacked!"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 667
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .param p1, "id"    # I

    .prologue
    const v7, 0x7f05004c

    const/4 v6, 0x3

    const v3, 0x7f050050

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1082
    if-ne p1, v5, :cond_0

    .line 1084
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->iconContextMenu:Lru/andrey/notepad/IconContextMenu;

    const v3, 0x7f050038

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lru/andrey/notepad/IconContextMenu;->createMenu(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v2

    .line 1194
    :goto_0
    return-object v2

    .line 1086
    :cond_0
    if-ne p1, v6, :cond_1

    .line 1088
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lru/andrey/notepad/MainActivity;->ImportProgress:Landroid/app/ProgressDialog;

    .line 1089
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->ImportProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1090
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->ImportProgress:Landroid/app/ProgressDialog;

    invoke-virtual {p0, v7}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1091
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->ImportProgress:Landroid/app/ProgressDialog;

    const v3, 0x7f050054

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1092
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->ImportProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1093
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->ImportProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 1095
    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 1097
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lru/andrey/notepad/MainActivity;->SaveProgress:Landroid/app/ProgressDialog;

    .line 1098
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->SaveProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1099
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->SaveProgress:Landroid/app/ProgressDialog;

    invoke-virtual {p0, v7}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1100
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->SaveProgress:Landroid/app/ProgressDialog;

    const v3, 0x7f05004d

    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1101
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->SaveProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1102
    iget-object v2, p0, Lru/andrey/notepad/MainActivity;->SaveProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 1104
    :cond_2
    if-ne p1, v6, :cond_3

    .line 1106
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1107
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1108
    const v2, 0x7f050051

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1109
    const-string v2, "\u041e\u041a"

    new-instance v3, Lru/andrey/notepad/MainActivity$8;

    invoke-direct {v3, p0}, Lru/andrey/notepad/MainActivity$8;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1116
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1117
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 1119
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_3
    const/4 v2, 0x4

    if-ne p1, v2, :cond_4

    .line 1121
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1122
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1123
    const v2, 0x7f050053

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1124
    const-string v2, "\u041e\u041a"

    new-instance v3, Lru/andrey/notepad/MainActivity$9;

    invoke-direct {v3, p0}, Lru/andrey/notepad/MainActivity$9;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1131
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1132
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 1134
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_4
    const/4 v2, 0x5

    if-ne p1, v2, :cond_5

    .line 1136
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1137
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, v3}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1138
    const v2, 0x7f050052

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1139
    const-string v2, "\u041e\u041a"

    new-instance v3, Lru/andrey/notepad/MainActivity$10;

    invoke-direct {v3, p0}, Lru/andrey/notepad/MainActivity$10;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1146
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1147
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 1149
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_5
    const/4 v2, 0x6

    if-ne p1, v2, :cond_6

    .line 1151
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1152
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f05005f

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1153
    const v2, 0x7f050063

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1154
    const-string v2, "\u041e\u041a"

    new-instance v3, Lru/andrey/notepad/MainActivity$11;

    invoke-direct {v3, p0}, Lru/andrey/notepad/MainActivity$11;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1161
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1162
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 1164
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_6
    const/4 v2, 0x7

    if-ne p1, v2, :cond_7

    .line 1166
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1167
    .local v1, "builder7":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f050068

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1168
    const v2, 0x7f050069

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1169
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1170
    const v2, 0x7f05006a

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lru/andrey/notepad/MainActivity$12;

    invoke-direct {v3, p0}, Lru/andrey/notepad/MainActivity$12;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1177
    const-string v2, "  "

    new-instance v3, Lru/andrey/notepad/MainActivity$13;

    invoke-direct {v3, p0}, Lru/andrey/notepad/MainActivity$13;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1185
    const v2, 0x7f05006b

    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lru/andrey/notepad/MainActivity$14;

    invoke-direct {v3, p0}, Lru/andrey/notepad/MainActivity$14;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1192
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 1194
    .end local v1    # "builder7":Landroid/app/AlertDialog$Builder;
    :cond_7
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 1665
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1667
    const/16 v0, 0x6e

    const v1, 0x7f05004f

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1668
    const/16 v0, 0x6f

    const v1, 0x7f05004e

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1669
    const/16 v0, 0x70

    const v1, 0x7f05005c

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1670
    const/16 v0, 0x71

    const v1, 0x7f05005d

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1671
    const/16 v0, 0x72

    const v1, 0x7f05006c

    invoke-virtual {p0, v1}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1672
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v8, 0x0

    .line 1778
    const/4 v7, 0x4

    if-ne p1, v7, :cond_0

    .line 1780
    new-instance v3, Landroid/app/Dialog;

    invoke-direct {v3, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1781
    .local v3, "dialog":Landroid/app/Dialog;
    const v7, 0x7f03000a

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->setContentView(I)V

    .line 1782
    const v7, 0x7f050068

    invoke-virtual {p0, v7}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1783
    invoke-virtual {v3, v8}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1784
    const v7, 0x7f070028

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1785
    .local v1, "btn1":Landroid/widget/Button;
    const v7, 0x7f07002f

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1786
    .local v0, "banner":Landroid/widget/LinearLayout;
    const v7, 0x7f070031

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1787
    .local v6, "tv":Landroid/widget/TextView;
    const v7, 0x7f070030

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 1788
    .local v5, "iv":Landroid/widget/ImageView;
    const v7, 0x7f070019

    invoke-virtual {v3, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 1789
    .local v2, "btn3":Landroid/widget/Button;
    new-instance v4, Lcom/caching/loader/ImageLoader;

    invoke-direct {v4, p0}, Lcom/caching/loader/ImageLoader;-><init>(Landroid/content/Context;)V

    .line 1790
    .local v4, "il":Lcom/caching/loader/ImageLoader;
    iget-object v7, p0, Lru/andrey/notepad/MainActivity;->imageUrl:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 1792
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1793
    iget-object v7, p0, Lru/andrey/notepad/MainActivity;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1794
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "#"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lru/andrey/notepad/MainActivity;->color:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1796
    new-instance v7, Lru/andrey/notepad/MainActivity$17;

    invoke-direct {v7, p0}, Lru/andrey/notepad/MainActivity$17;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806
    new-instance v7, Lru/andrey/notepad/MainActivity$18;

    invoke-direct {v7, p0}, Lru/andrey/notepad/MainActivity$18;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1816
    iget-object v7, p0, Lru/andrey/notepad/MainActivity;->imageUrl:Ljava/lang/String;

    invoke-virtual {v4, v7, v5}, Lcom/caching/loader/ImageLoader;->DisplayImage(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 1822
    :goto_0
    new-instance v7, Lru/andrey/notepad/MainActivity$19;

    invoke-direct {v7, p0, v3}, Lru/andrey/notepad/MainActivity$19;-><init>(Lru/andrey/notepad/MainActivity;Landroid/app/Dialog;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1832
    new-instance v7, Lru/andrey/notepad/MainActivity$20;

    invoke-direct {v7, p0, v3}, Lru/andrey/notepad/MainActivity$20;-><init>(Lru/andrey/notepad/MainActivity;Landroid/app/Dialog;)V

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1841
    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 1844
    .end local v0    # "banner":Landroid/widget/LinearLayout;
    .end local v1    # "btn1":Landroid/widget/Button;
    .end local v2    # "btn3":Landroid/widget/Button;
    .end local v3    # "dialog":Landroid/app/Dialog;
    .end local v4    # "il":Lcom/caching/loader/ImageLoader;
    .end local v5    # "iv":Landroid/widget/ImageView;
    .end local v6    # "tv":Landroid/widget/TextView;
    :cond_0
    const/4 v7, 0x1

    return v7

    .line 1820
    .restart local v0    # "banner":Landroid/widget/LinearLayout;
    .restart local v1    # "btn1":Landroid/widget/Button;
    .restart local v2    # "btn3":Landroid/widget/Button;
    .restart local v3    # "dialog":Landroid/app/Dialog;
    .restart local v4    # "il":Lcom/caching/loader/ImageLoader;
    .restart local v5    # "iv":Landroid/widget/ImageView;
    .restart local v6    # "tv":Landroid/widget/TextView;
    :cond_1
    const/16 v7, 0x8

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1678
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1679
    .local v3, "intent":Landroid/content/Intent;
    const/high16 v4, 0x80000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1680
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05002c

    invoke-virtual {p0, v5}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1682
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lru/andrey/notepad/AddActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1683
    .local v2, "in":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1684
    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1726
    .end local v2    # "in":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v7

    .line 1686
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05004f

    invoke-virtual {p0, v5}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1688
    new-instance v4, Lru/andrey/notepad/MainActivity$saveTask;

    invoke-direct {v4, p0}, Lru/andrey/notepad/MainActivity$saveTask;-><init>(Lru/andrey/notepad/MainActivity;)V

    new-array v5, v6, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lru/andrey/notepad/MainActivity$saveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1690
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05004e

    invoke-virtual {p0, v5}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1692
    new-instance v4, Lru/andrey/notepad/MainActivity$importTask;

    invoke-direct {v4, p0}, Lru/andrey/notepad/MainActivity$importTask;-><init>(Lru/andrey/notepad/MainActivity;)V

    new-array v5, v6, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lru/andrey/notepad/MainActivity$importTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1694
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05005c

    invoke-virtual {p0, v5}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1696
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lru/andrey/notepad/SyncActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1697
    .restart local v2    # "in":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1699
    .end local v2    # "in":Landroid/content/Intent;
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05005d

    invoke-virtual {p0, v5}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1702
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "color"

    const-string v6, "#dad07f"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1703
    .local v0, "color":I
    new-instance v1, Lru/andrey/notepad/AmbilWarnaDialog;

    new-instance v4, Lru/andrey/notepad/MainActivity$16;

    invoke-direct {v4, p0}, Lru/andrey/notepad/MainActivity$16;-><init>(Lru/andrey/notepad/MainActivity;)V

    invoke-direct {v1, p0, v0, v4}, Lru/andrey/notepad/AmbilWarnaDialog;-><init>(Landroid/content/Context;ILru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;)V

    .line 1719
    .local v1, "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    invoke-virtual {v1}, Lru/andrey/notepad/AmbilWarnaDialog;->show()V

    goto/16 :goto_0

    .line 1721
    .end local v0    # "color":I
    .end local v1    # "dialog":Lru/andrey/notepad/AmbilWarnaDialog;
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f05006c

    invoke-virtual {p0, v5}, Lru/andrey/notepad/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1723
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lru/andrey/notepad/FolderActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v4}, Lru/andrey/notepad/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1757
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1758
    sget-object v0, Lru/andrey/notepad/MainActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v0}, Lru/andrey/notepad/DataBaseHelper;->getNotes()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    .line 1759
    sget-object v0, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1761
    sget-boolean v0, Lru/andrey/notepad/MainActivity;->isSearch:Z

    if-eqz v0, :cond_0

    .line 1763
    new-instance v0, Lru/andrey/notepad/ObjectArrayAdapter;

    sget-object v1, Lru/andrey/notepad/MainActivity;->sobject:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    .line 1771
    :goto_0
    sget-object v0, Lru/andrey/notepad/MainActivity;->lv:Landroid/widget/ListView;

    sget-object v1, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1772
    return-void

    .line 1768
    :cond_0
    new-instance v0, Lru/andrey/notepad/ObjectArrayAdapter;

    sget-object v1, Lru/andrey/notepad/MainActivity;->object:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lru/andrey/notepad/ObjectArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    sput-object v0, Lru/andrey/notepad/MainActivity;->arr:Lru/andrey/notepad/ObjectArrayAdapter;

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 889
    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    iget-object v3, p0, Lru/andrey/notepad/MainActivity;->archive:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 890
    .local v2, "zipfile":Ljava/util/zip/ZipFile;
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_0

    .line 899
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :goto_1
    return-void

    .line 892
    .restart local v0    # "e":Ljava/util/Enumeration;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    .line 894
    .local v1, "entry":Ljava/util/zip/ZipEntry;
    iget-object v3, p0, Lru/andrey/notepad/MainActivity;->outputDir:Ljava/lang/String;

    invoke-direct {p0, v2, v1, v3}, Lru/andrey/notepad/MainActivity;->unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 897
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "entry":Ljava/util/zip/ZipEntry;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public unzipArchive(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p1, "archive"    # Ljava/io/File;
    .param p2, "outputDir"    # Ljava/lang/String;

    .prologue
    .line 906
    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    invoke-direct {v2, p1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 907
    .local v2, "zipfile":Ljava/util/zip/ZipFile;
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_0

    .line 915
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :goto_1
    return-void

    .line 909
    .restart local v0    # "e":Ljava/util/Enumeration;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    .line 910
    .local v1, "entry":Ljava/util/zip/ZipEntry;
    invoke-direct {p0, v2, v1, p2}, Lru/andrey/notepad/MainActivity;->unzipEntry(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 913
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "entry":Ljava/util/zip/ZipEntry;
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v3

    goto :goto_1
.end method
