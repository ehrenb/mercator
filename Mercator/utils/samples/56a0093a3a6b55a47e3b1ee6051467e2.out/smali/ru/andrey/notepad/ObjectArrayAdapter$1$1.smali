.class Lru/andrey/notepad/ObjectArrayAdapter$1$1;
.super Ljava/lang/Object;
.source "ObjectArrayAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/ObjectArrayAdapter$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/ObjectArrayAdapter$1;

.field private final synthetic val$dh:Lru/andrey/notepad/DataBaseHelper;

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/ObjectArrayAdapter$1;Lru/andrey/notepad/DataBaseHelper;ILandroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$1;

    iput-object p2, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    iput p3, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->val$position:I

    iput-object p4, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->val$dialog:Landroid/app/Dialog;

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v0, "objs1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    iget-object v1, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v1}, Lru/andrey/notepad/DataBaseHelper;->getFolders()Ljava/util/ArrayList;

    move-result-object v0

    .line 137
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 138
    iget-object v3, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v1, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$1;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$1;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v1}, Lru/andrey/notepad/ObjectArrayAdapter$1;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$1;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v1

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/ObjectArrayAdapter;->access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->val$position:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lru/andrey/notepad/DataBaseHelper;->addFolderData(Lru/andrey/notepad/ObjectModel;Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lru/andrey/notepad/ObjectArrayAdapter$1$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 140
    return-void
.end method
