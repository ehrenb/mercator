.class public Lru/andrey/notepad/ObjectModel;
.super Ljava/lang/Object;
.source "ObjectModel.java"


# instance fields
.field private body:Ljava/lang/String;

.field private color:I

.field private date:Ljava/lang/String;

.field private fname:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private when:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lru/andrey/notepad/ObjectModel;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lru/andrey/notepad/ObjectModel;->color:I

    return v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lru/andrey/notepad/ObjectModel;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getFname()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lru/andrey/notepad/ObjectModel;->fname:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lru/andrey/notepad/ObjectModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getWhen()J
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lru/andrey/notepad/ObjectModel;->when:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lru/andrey/notepad/ObjectModel;->body:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "text"    # I

    .prologue
    .line 69
    iput p1, p0, Lru/andrey/notepad/ObjectModel;->color:I

    .line 70
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lru/andrey/notepad/ObjectModel;->date:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setFname(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lru/andrey/notepad/ObjectModel;->fname:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lru/andrey/notepad/ObjectModel;->name:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setWhen(J)V
    .locals 1
    .param p1, "text"    # J

    .prologue
    .line 59
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lru/andrey/notepad/ObjectModel;->when:Ljava/lang/Long;

    .line 60
    return-void
.end method
