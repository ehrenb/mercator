.class Lru/andrey/notepad/AddActivity$1;
.super Ljava/lang/Object;
.source "AddActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/AddActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/AddActivity;

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$dp:Landroid/widget/DatePicker;

.field private final synthetic val$tp:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Lru/andrey/notepad/AddActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/AddActivity$1;->this$0:Lru/andrey/notepad/AddActivity;

    iput-object p2, p0, Lru/andrey/notepad/AddActivity$1;->val$dp:Landroid/widget/DatePicker;

    iput-object p3, p0, Lru/andrey/notepad/AddActivity$1;->val$tp:Landroid/widget/TimePicker;

    iput-object p4, p0, Lru/andrey/notepad/AddActivity$1;->val$dialog:Landroid/app/Dialog;

    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x14

    .line 276
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 277
    .local v0, "c":Ljava/util/Calendar;
    const/4 v5, 0x5

    iget-object v6, p0, Lru/andrey/notepad/AddActivity$1;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v6}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 278
    const/4 v5, 0x2

    iget-object v6, p0, Lru/andrey/notepad/AddActivity$1;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v6}, Landroid/widget/DatePicker;->getMonth()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 279
    const/4 v5, 0x1

    iget-object v6, p0, Lru/andrey/notepad/AddActivity$1;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v6}, Landroid/widget/DatePicker;->getYear()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 280
    const/16 v5, 0xb

    iget-object v6, p0, Lru/andrey/notepad/AddActivity$1;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v6}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 281
    const/16 v5, 0xc

    iget-object v6, p0, Lru/andrey/notepad/AddActivity$1;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v6}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 283
    iget-object v5, p0, Lru/andrey/notepad/AddActivity$1;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v5, v5, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "names":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_0

    .line 287
    const/4 v5, 0x0

    invoke-virtual {v1, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 293
    .local v4, "value":Ljava/lang/String;
    :goto_0
    new-instance v2, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v2}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 294
    .local v2, "om":Lru/andrey/notepad/ObjectModel;
    iget-object v5, p0, Lru/andrey/notepad/AddActivity$1;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v5, v5, Lru/andrey/notepad/AddActivity;->main:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 295
    iget-object v5, p0, Lru/andrey/notepad/AddActivity$1;->this$0:Lru/andrey/notepad/AddActivity;

    iget v5, v5, Lru/andrey/notepad/AddActivity;->color:I

    invoke-virtual {v2, v5}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 296
    iget-object v5, p0, Lru/andrey/notepad/AddActivity$1;->this$0:Lru/andrey/notepad/AddActivity;

    iget-boolean v5, v5, Lru/andrey/notepad/AddActivity;->isnew:Z

    if-nez v5, :cond_1

    .line 297
    iget-object v5, p0, Lru/andrey/notepad/AddActivity$1;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v5, v5, Lru/andrey/notepad/AddActivity;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 301
    :goto_1
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v5, "HH:mm MM-dd-yyyy"

    invoke-direct {v3, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 302
    .local v3, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v5, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 303
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 304
    iget-object v5, p0, Lru/andrey/notepad/AddActivity$1;->this$0:Lru/andrey/notepad/AddActivity;

    iget-object v5, v5, Lru/andrey/notepad/AddActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v5, v2}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 306
    iget-object v5, p0, Lru/andrey/notepad/AddActivity$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    .line 308
    return-void

    .line 291
    .end local v2    # "om":Lru/andrey/notepad/ObjectModel;
    .end local v3    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v4    # "value":Ljava/lang/String;
    :cond_0
    move-object v4, v1

    .restart local v4    # "value":Ljava/lang/String;
    goto :goto_0

    .line 299
    .restart local v2    # "om":Lru/andrey/notepad/ObjectModel;
    :cond_1
    invoke-virtual {v2, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    goto :goto_1
.end method
