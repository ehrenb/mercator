.class Lru/andrey/notepad/FoldersArrayAdapter$1$1;
.super Ljava/lang/Object;
.source "FoldersArrayAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FoldersArrayAdapter$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FoldersArrayAdapter$1;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/FoldersArrayAdapter$1;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FoldersArrayAdapter$1$1;->this$1:Lru/andrey/notepad/FoldersArrayAdapter$1;

    iput p2, p0, Lru/andrey/notepad/FoldersArrayAdapter$1$1;->val$position:I

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 91
    iget-object v1, p0, Lru/andrey/notepad/FoldersArrayAdapter$1$1;->this$1:Lru/andrey/notepad/FoldersArrayAdapter$1;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter$1;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;
    invoke-static {v1}, Lru/andrey/notepad/FoldersArrayAdapter$1;->access$0(Lru/andrey/notepad/FoldersArrayAdapter$1;)Lru/andrey/notepad/FoldersArrayAdapter;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v1}, Lru/andrey/notepad/FoldersArrayAdapter;->access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 92
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v1, p0, Lru/andrey/notepad/FoldersArrayAdapter$1$1;->this$1:Lru/andrey/notepad/FoldersArrayAdapter$1;

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter$1;->this$0:Lru/andrey/notepad/FoldersArrayAdapter;
    invoke-static {v1}, Lru/andrey/notepad/FoldersArrayAdapter$1;->access$0(Lru/andrey/notepad/FoldersArrayAdapter$1;)Lru/andrey/notepad/FoldersArrayAdapter;

    move-result-object v1

    # getter for: Lru/andrey/notepad/FoldersArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v1}, Lru/andrey/notepad/FoldersArrayAdapter;->access$1(Lru/andrey/notepad/FoldersArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lru/andrey/notepad/FoldersArrayAdapter$1$1;->val$position:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/andrey/notepad/DataBaseHelper;->removeFolder(Ljava/lang/String;)V

    .line 93
    invoke-static {}, Lru/andrey/notepad/FolderActivity;->update()V

    .line 94
    return-void
.end method
