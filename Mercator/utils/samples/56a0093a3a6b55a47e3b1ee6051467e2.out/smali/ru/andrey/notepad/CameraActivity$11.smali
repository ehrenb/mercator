.class Lru/andrey/notepad/CameraActivity$11;
.super Ljava/lang/Object;
.source "CameraActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/CameraActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/CameraActivity;

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$dp:Landroid/widget/DatePicker;

.field private final synthetic val$tp:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Lru/andrey/notepad/CameraActivity;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/CameraActivity$11;->this$0:Lru/andrey/notepad/CameraActivity;

    iput-object p2, p0, Lru/andrey/notepad/CameraActivity$11;->val$dp:Landroid/widget/DatePicker;

    iput-object p3, p0, Lru/andrey/notepad/CameraActivity$11;->val$tp:Landroid/widget/TimePicker;

    iput-object p4, p0, Lru/andrey/notepad/CameraActivity$11;->val$dialog:Landroid/app/Dialog;

    .line 876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 880
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 881
    .local v0, "c":Ljava/util/Calendar;
    const/4 v4, 0x5

    iget-object v5, p0, Lru/andrey/notepad/CameraActivity$11;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 882
    const/4 v4, 0x2

    iget-object v5, p0, Lru/andrey/notepad/CameraActivity$11;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getMonth()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 883
    const/4 v4, 0x1

    iget-object v5, p0, Lru/andrey/notepad/CameraActivity$11;->val$dp:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getYear()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 884
    const/16 v4, 0xb

    iget-object v5, p0, Lru/andrey/notepad/CameraActivity$11;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 885
    const/16 v4, 0xc

    iget-object v5, p0, Lru/andrey/notepad/CameraActivity$11;->val$tp:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 887
    const-string v3, "Camera"

    .line 888
    .local v3, "value":Ljava/lang/String;
    new-instance v1, Lru/andrey/notepad/ObjectModel;

    invoke-direct {v1}, Lru/andrey/notepad/ObjectModel;-><init>()V

    .line 889
    .local v1, "om":Lru/andrey/notepad/ObjectModel;
    invoke-virtual {v1, v3}, Lru/andrey/notepad/ObjectModel;->setBody(Ljava/lang/String;)V

    .line 890
    iget-object v4, p0, Lru/andrey/notepad/CameraActivity$11;->this$0:Lru/andrey/notepad/CameraActivity;

    iget v4, v4, Lru/andrey/notepad/CameraActivity;->num:I

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setColor(I)V

    .line 891
    iget-object v4, p0, Lru/andrey/notepad/CameraActivity$11;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v4, v4, Lru/andrey/notepad/CameraActivity;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setName(Ljava/lang/String;)V

    .line 892
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "HH:mm MM-dd-yyyy"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 893
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lru/andrey/notepad/ObjectModel;->setDate(Ljava/lang/String;)V

    .line 894
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lru/andrey/notepad/ObjectModel;->setWhen(J)V

    .line 895
    iget-object v4, p0, Lru/andrey/notepad/CameraActivity$11;->this$0:Lru/andrey/notepad/CameraActivity;

    iget-object v4, v4, Lru/andrey/notepad/CameraActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v4, v1}, Lru/andrey/notepad/DataBaseHelper;->addRemind(Lru/andrey/notepad/ObjectModel;)V

    .line 897
    iget-object v4, p0, Lru/andrey/notepad/CameraActivity$11;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->dismiss()V

    .line 899
    return-void
.end method
