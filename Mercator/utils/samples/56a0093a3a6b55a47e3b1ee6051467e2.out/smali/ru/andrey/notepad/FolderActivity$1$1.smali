.class Lru/andrey/notepad/FolderActivity$1$1;
.super Ljava/lang/Object;
.source "FolderActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderActivity$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/FolderActivity$1;

.field private final synthetic val$dialog:Landroid/app/Dialog;

.field private final synthetic val$value:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderActivity$1;Landroid/widget/EditText;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderActivity$1$1;->this$1:Lru/andrey/notepad/FolderActivity$1;

    iput-object p2, p0, Lru/andrey/notepad/FolderActivity$1$1;->val$value:Landroid/widget/EditText;

    iput-object p3, p0, Lru/andrey/notepad/FolderActivity$1$1;->val$dialog:Landroid/app/Dialog;

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v1, p0, Lru/andrey/notepad/FolderActivity$1$1;->val$value:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 63
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm MM-dd-yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    sget-object v1, Lru/andrey/notepad/FolderActivity;->dh:Lru/andrey/notepad/DataBaseHelper;

    iget-object v2, p0, Lru/andrey/notepad/FolderActivity$1$1;->val$value:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lru/andrey/notepad/DataBaseHelper;->addFolder(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lru/andrey/notepad/FolderActivity;->update()V

    .line 66
    iget-object v1, p0, Lru/andrey/notepad/FolderActivity$1$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 68
    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_0
    return-void
.end method
