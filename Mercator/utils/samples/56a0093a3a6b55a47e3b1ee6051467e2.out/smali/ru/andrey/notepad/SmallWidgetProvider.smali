.class public Lru/andrey/notepad/SmallWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "SmallWidgetProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 17
    new-instance v4, Landroid/content/ComponentName;

    const-class v6, Lru/andrey/notepad/SmallWidgetProvider;

    invoke-direct {v4, p1, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    .local v4, "thisWidget":Landroid/content/ComponentName;
    invoke-virtual {p2, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 20
    .local v0, "allWidgetIds":[I
    array-length v7, v0

    const/4 v6, 0x0

    :goto_0
    if-lt v6, v7, :cond_0

    .line 29
    return-void

    .line 20
    :cond_0
    aget v5, v0, v6

    .line 22
    .local v5, "widgetId":I
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f030018

    invoke-direct {v3, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 23
    .local v3, "remoteViews":Landroid/widget/RemoteViews;
    new-instance v1, Landroid/content/Intent;

    const-class v8, Lru/andrey/notepad/MainActivity;

    invoke-direct {v1, p1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 24
    .local v1, "in":Landroid/content/Intent;
    const/high16 v8, 0x8000000

    invoke-static {p1, v5, v1, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 25
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    const v8, 0x7f070048

    invoke-virtual {v3, v8, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 26
    invoke-virtual {p2, v5, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 20
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method
