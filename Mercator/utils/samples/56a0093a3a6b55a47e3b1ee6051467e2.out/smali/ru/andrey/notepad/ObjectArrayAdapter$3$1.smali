.class Lru/andrey/notepad/ObjectArrayAdapter$3$1;
.super Ljava/lang/Object;
.source "ObjectArrayAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/ObjectArrayAdapter$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

.field private final synthetic val$edit:Landroid/widget/EditText;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/ObjectArrayAdapter$3;Landroid/widget/EditText;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    iput-object p2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$edit:Landroid/widget/EditText;

    iput p3, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$position:I

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    const/4 v5, 0x0

    .line 206
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "newname":Ljava/lang/String;
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 208
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v4, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$position:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 210
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 211
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v4, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$position:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 212
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "pass"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v7, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$position:I

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "pass"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, ""

    invoke-interface {v5, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 213
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$0(Lru/andrey/notepad/ObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v5, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$position:I

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "pass"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, ""

    invoke-interface {v3, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 215
    :cond_0
    iget-object v2, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->this$1:Lru/andrey/notepad/ObjectArrayAdapter$3;

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter$3;->this$0:Lru/andrey/notepad/ObjectArrayAdapter;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter$3;->access$0(Lru/andrey/notepad/ObjectArrayAdapter$3;)Lru/andrey/notepad/ObjectArrayAdapter;

    move-result-object v2

    # getter for: Lru/andrey/notepad/ObjectArrayAdapter;->obj:Ljava/util/ArrayList;
    invoke-static {v2}, Lru/andrey/notepad/ObjectArrayAdapter;->access$1(Lru/andrey/notepad/ObjectArrayAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lru/andrey/notepad/ObjectArrayAdapter$3$1;->val$position:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v2}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lru/andrey/notepad/DataBaseHelper;->updateObject(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-static {}, Lru/andrey/notepad/MainActivity;->update()V

    .line 218
    return-void
.end method
