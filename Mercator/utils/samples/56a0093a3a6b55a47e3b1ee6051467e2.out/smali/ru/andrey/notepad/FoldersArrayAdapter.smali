.class public Lru/andrey/notepad/FoldersArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FoldersArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lru/andrey/notepad/ObjectModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Activity;

.field private final obj:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Lru/andrey/notepad/ObjectModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, "obj":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    const v0, 0x7f030014

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 26
    iput-object p1, p0, Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;

    .line 27
    iput-object p2, p0, Lru/andrey/notepad/FoldersArrayAdapter;->obj:Ljava/util/ArrayList;

    .line 28
    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/FoldersArrayAdapter;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1(Lru/andrey/notepad/FoldersArrayAdapter;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lru/andrey/notepad/FoldersArrayAdapter;->obj:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v9, 0x8

    .line 47
    move-object v4, p2

    .line 48
    .local v4, "rowView":Landroid/view/View;
    const-string v5, "Test"

    const-string v6, "view"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    if-nez v4, :cond_0

    .line 51
    iget-object v5, p0, Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 52
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030014

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 53
    new-instance v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;

    invoke-direct {v2}, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;-><init>()V

    .line 54
    .local v2, "holder":Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;
    const v5, 0x7f070037

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 55
    const v5, 0x7f07003e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    .line 56
    const v5, 0x7f07002e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->str2:Landroid/widget/TextView;

    .line 57
    const v5, 0x7f070030

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    .line 58
    const v5, 0x7f070038

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv2:Landroid/widget/ImageView;

    .line 59
    const v5, 0x7f070039

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv3:Landroid/widget/ImageView;

    .line 60
    const v5, 0x7f07003c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv4:Landroid/widget/ImageView;

    .line 61
    const v5, 0x7f07003d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv5:Landroid/widget/ImageView;

    .line 62
    const v5, 0x7f07003a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv6:Landroid/widget/ImageView;

    .line 63
    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 70
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v5, p0, Lru/andrey/notepad/FoldersArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/andrey/notepad/ObjectModel;

    .line 71
    .local v1, "ff":Lru/andrey/notepad/ObjectModel;
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->str1:Landroid/widget/TextView;

    invoke-virtual {v1}, Lru/andrey/notepad/ObjectModel;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv1:Landroid/widget/ImageView;

    const v6, 0x7f020034

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 75
    iget-object v5, p0, Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;

    invoke-static {v5}, Lru/andrey/notepad/DataBaseHelper;->getHelper(Landroid/content/Context;)Lru/andrey/notepad/DataBaseHelper;

    move-result-object v0

    .line 77
    .local v0, "dh":Lru/andrey/notepad/DataBaseHelper;
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv4:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    iget-object v6, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->str2:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lru/andrey/notepad/FoldersArrayAdapter;->context:Landroid/app/Activity;

    const v8, 0x7f05005b

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, p0, Lru/andrey/notepad/FoldersArrayAdapter;->obj:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v5}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lru/andrey/notepad/DataBaseHelper;->getFoldersCount(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv2:Landroid/widget/ImageView;

    new-instance v6, Lru/andrey/notepad/FoldersArrayAdapter$1;

    invoke-direct {v6, p0, p1}, Lru/andrey/notepad/FoldersArrayAdapter$1;-><init>(Lru/andrey/notepad/FoldersArrayAdapter;I)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv5:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv6:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 112
    iget-object v5, v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;->iv3:Landroid/widget/ImageView;

    new-instance v6, Lru/andrey/notepad/FoldersArrayAdapter$2;

    invoke-direct {v6, p0, p1}, Lru/andrey/notepad/FoldersArrayAdapter$2;-><init>(Lru/andrey/notepad/FoldersArrayAdapter;I)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    return-object v4

    .line 67
    .end local v0    # "dh":Lru/andrey/notepad/DataBaseHelper;
    .end local v1    # "ff":Lru/andrey/notepad/ObjectModel;
    .end local v2    # "holder":Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;
    :cond_0
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;

    .restart local v2    # "holder":Lru/andrey/notepad/FoldersArrayAdapter$ViewHolder;
    goto/16 :goto_0
.end method
