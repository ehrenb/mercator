.class public Lru/andrey/notepad/LineEditText;
.super Landroid/widget/EditText;
.source "LineEditText.java"


# instance fields
.field private mPaint:Landroid/graphics/Paint;

.field private mRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/LineEditText;->mRect:Landroid/graphics/Rect;

    .line 19
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lru/andrey/notepad/LineEditText;->mPaint:Landroid/graphics/Paint;

    .line 20
    iget-object v0, p0, Lru/andrey/notepad/LineEditText;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 21
    iget-object v0, p0, Lru/andrey/notepad/LineEditText;->mPaint:Landroid/graphics/Paint;

    const v1, -0x7d95d000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 22
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    mul-int/lit8 v8, v0, 0x5

    .line 32
    .local v8, "height":I
    const/4 v7, 0x0

    .line 33
    .local v7, "curHeight":I
    iget-object v9, p0, Lru/andrey/notepad/LineEditText;->mRect:Landroid/graphics/Rect;

    .line 34
    .local v9, "r":Landroid/graphics/Rect;
    iget-object v5, p0, Lru/andrey/notepad/LineEditText;->mPaint:Landroid/graphics/Paint;

    .line 35
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v9}, Lru/andrey/notepad/LineEditText;->getLineBounds(ILandroid/graphics/Rect;)I

    move-result v6

    .line 36
    .local v6, "baseline":I
    add-int/lit8 v7, v6, 0x1

    :goto_0
    if-lt v7, v8, :cond_0

    .line 40
    invoke-super {p0, p1}, Landroid/widget/EditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 41
    return-void

    .line 38
    :cond_0
    iget v0, v9, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    int-to-float v2, v7

    iget v0, v9, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    int-to-float v4, v7

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 36
    invoke-virtual {p0}, Lru/andrey/notepad/LineEditText;->getLineHeight()I

    move-result v0

    add-int/2addr v7, v0

    goto :goto_0
.end method
