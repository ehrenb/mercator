.class Lru/andrey/notepad/FolderDataActivity$8;
.super Ljava/lang/Object;
.source "FolderDataActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderDataActivity;->find(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/FolderDataActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    .line 1228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/FolderDataActivity$8;)Lru/andrey/notepad/FolderDataActivity;
    .locals 1

    .prologue
    .line 1228
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 1233
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1235
    new-instance v1, Landroid/app/Dialog;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-direct {v1, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1236
    .local v1, "dialog":Landroid/app/Dialog;
    const v4, 0x7f030006

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 1237
    const v4, 0x7f050066

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setTitle(I)V

    .line 1238
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1239
    const v4, 0x7f070016

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 1240
    .local v3, "value":Landroid/widget/EditText;
    const v4, 0x7f070028

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1241
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lru/andrey/notepad/FolderDataActivity$8$1;

    invoke-direct {v4, p0, v3, p3, v1}, Lru/andrey/notepad/FolderDataActivity$8$1;-><init>(Lru/andrey/notepad/FolderDataActivity$8;Landroid/widget/EditText;ILandroid/app/Dialog;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1328
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 1405
    .end local v0    # "button":Landroid/widget/Button;
    .end local v1    # "dialog":Landroid/app/Dialog;
    .end local v3    # "value":Landroid/widget/EditText;
    :goto_0
    return-void

    .line 1332
    :cond_0
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawing"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1334
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/PictureActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1335
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1336
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1337
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1338
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1339
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1341
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Camera"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1343
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/CameraActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1344
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1345
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1346
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1347
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1348
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1350
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PhotoText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1352
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/CameraTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1353
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1354
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1355
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1356
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1357
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1359
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GalleryText"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1361
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/GalleryTextActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1362
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1363
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1364
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1365
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1366
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1368
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Record"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1370
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/RecordActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1371
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1372
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1373
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1374
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1375
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1377
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_5
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Shop"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1379
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/BuyActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1380
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1381
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1382
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1383
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1384
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1386
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Video"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1388
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/VideoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1389
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1390
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1391
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1392
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1393
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1397
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    const-class v5, Lru/andrey/notepad/AddActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1398
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "new"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1399
    const-string v5, "name"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1400
    const-string v5, "body"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1401
    const-string v5, "color"

    sget-object v4, Lru/andrey/notepad/FolderDataActivity;->sobject:Ljava/util/ArrayList;

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/andrey/notepad/ObjectModel;

    invoke-virtual {v4}, Lru/andrey/notepad/ObjectModel;->getColor()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1402
    iget-object v4, p0, Lru/andrey/notepad/FolderDataActivity$8;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-virtual {v4, v2}, Lru/andrey/notepad/FolderDataActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
