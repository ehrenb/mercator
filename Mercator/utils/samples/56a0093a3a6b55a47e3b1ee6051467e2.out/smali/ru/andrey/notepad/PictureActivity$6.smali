.class Lru/andrey/notepad/PictureActivity$6;
.super Ljava/lang/Object;
.source "PictureActivity.java"

# interfaces
.implements Lru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/PictureActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/PictureActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/PictureActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Lru/andrey/notepad/AmbilWarnaDialog;)V
    .locals 0
    .param p1, "dialog"    # Lru/andrey/notepad/AmbilWarnaDialog;

    .prologue
    .line 527
    return-void
.end method

.method public onOk(Lru/andrey/notepad/AmbilWarnaDialog;I)V
    .locals 4
    .param p1, "dialog"    # Lru/andrey/notepad/AmbilWarnaDialog;
    .param p2, "col"    # I

    .prologue
    .line 511
    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    iput p2, v2, Lru/andrey/notepad/PictureActivity;->color:I

    .line 512
    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v2

    iget-object v3, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    iget v3, v3, Lru/andrey/notepad/PictureActivity;->color:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 513
    const/4 v0, 0x0

    .line 514
    .local v0, "calc1":I
    const/4 v1, 0x0

    .line 515
    .local v1, "calc2":I
    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    iget v0, v2, Lru/andrey/notepad/PictureActivity;->intens:I

    .line 516
    mul-int/lit8 v1, v0, 0x2

    .line 517
    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    # getter for: Lru/andrey/notepad/PictureActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/PictureActivity;->access$1(Lru/andrey/notepad/PictureActivity;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 518
    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lru/andrey/notepad/PictureActivity;->erase:Z

    .line 519
    iget-object v2, p0, Lru/andrey/notepad/PictureActivity$6;->this$0:Lru/andrey/notepad/PictureActivity;

    iget-object v2, v2, Lru/andrey/notepad/PictureActivity;->clear:Landroid/widget/Button;

    const v3, 0x7f020043

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 521
    return-void
.end method
