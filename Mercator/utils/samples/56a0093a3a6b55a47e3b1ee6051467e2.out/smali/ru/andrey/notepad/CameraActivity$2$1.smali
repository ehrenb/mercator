.class Lru/andrey/notepad/CameraActivity$2$1;
.super Ljava/lang/Object;
.source "CameraActivity.java"

# interfaces
.implements Lru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/CameraActivity$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/andrey/notepad/CameraActivity$2;


# direct methods
.method constructor <init>(Lru/andrey/notepad/CameraActivity$2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Lru/andrey/notepad/AmbilWarnaDialog;)V
    .locals 0
    .param p1, "dialog"    # Lru/andrey/notepad/AmbilWarnaDialog;

    .prologue
    .line 155
    return-void
.end method

.method public onOk(Lru/andrey/notepad/AmbilWarnaDialog;I)V
    .locals 4
    .param p1, "dialog"    # Lru/andrey/notepad/AmbilWarnaDialog;
    .param p2, "col"    # I

    .prologue
    .line 139
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    # getter for: Lru/andrey/notepad/CameraActivity$2;->this$0:Lru/andrey/notepad/CameraActivity;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity$2;->access$0(Lru/andrey/notepad/CameraActivity$2;)Lru/andrey/notepad/CameraActivity;

    move-result-object v2

    iput p2, v2, Lru/andrey/notepad/CameraActivity;->color:I

    .line 140
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    # getter for: Lru/andrey/notepad/CameraActivity$2;->this$0:Lru/andrey/notepad/CameraActivity;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity$2;->access$0(Lru/andrey/notepad/CameraActivity$2;)Lru/andrey/notepad/CameraActivity;

    move-result-object v2

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v2

    iget-object v3, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    # getter for: Lru/andrey/notepad/CameraActivity$2;->this$0:Lru/andrey/notepad/CameraActivity;
    invoke-static {v3}, Lru/andrey/notepad/CameraActivity$2;->access$0(Lru/andrey/notepad/CameraActivity$2;)Lru/andrey/notepad/CameraActivity;

    move-result-object v3

    iget v3, v3, Lru/andrey/notepad/CameraActivity;->color:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "calc1":I
    const/4 v1, 0x0

    .line 143
    .local v1, "calc2":I
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    # getter for: Lru/andrey/notepad/CameraActivity$2;->this$0:Lru/andrey/notepad/CameraActivity;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity$2;->access$0(Lru/andrey/notepad/CameraActivity$2;)Lru/andrey/notepad/CameraActivity;

    move-result-object v2

    iget v0, v2, Lru/andrey/notepad/CameraActivity;->intens:I

    .line 144
    mul-int/lit8 v1, v0, 0x2

    .line 145
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    # getter for: Lru/andrey/notepad/CameraActivity$2;->this$0:Lru/andrey/notepad/CameraActivity;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity$2;->access$0(Lru/andrey/notepad/CameraActivity$2;)Lru/andrey/notepad/CameraActivity;

    move-result-object v2

    # getter for: Lru/andrey/notepad/CameraActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity;->access$1(Lru/andrey/notepad/CameraActivity;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 146
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    # getter for: Lru/andrey/notepad/CameraActivity$2;->this$0:Lru/andrey/notepad/CameraActivity;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity$2;->access$0(Lru/andrey/notepad/CameraActivity$2;)Lru/andrey/notepad/CameraActivity;

    move-result-object v2

    const/4 v3, 0x0

    iput-boolean v3, v2, Lru/andrey/notepad/CameraActivity;->erase:Z

    .line 147
    iget-object v2, p0, Lru/andrey/notepad/CameraActivity$2$1;->this$1:Lru/andrey/notepad/CameraActivity$2;

    # getter for: Lru/andrey/notepad/CameraActivity$2;->this$0:Lru/andrey/notepad/CameraActivity;
    invoke-static {v2}, Lru/andrey/notepad/CameraActivity$2;->access$0(Lru/andrey/notepad/CameraActivity$2;)Lru/andrey/notepad/CameraActivity;

    move-result-object v2

    iget-object v2, v2, Lru/andrey/notepad/CameraActivity;->clear:Landroid/widget/Button;

    const v3, 0x7f020043

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 149
    return-void
.end method
