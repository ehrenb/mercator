.class Lru/andrey/notepad/FolderObjectArrayAdapter$1;
.super Ljava/lang/Object;
.source "FolderObjectArrayAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderObjectArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

.field private final synthetic val$dh:Lru/andrey/notepad/DataBaseHelper;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderObjectArrayAdapter;Lru/andrey/notepad/DataBaseHelper;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    iput-object p2, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    iput p3, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->val$position:I

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lru/andrey/notepad/FolderObjectArrayAdapter$1;)Lru/andrey/notepad/FolderObjectArrayAdapter;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 123
    new-instance v0, Landroid/app/Dialog;

    iget-object v3, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v3}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 124
    .local v0, "dialog":Landroid/app/Dialog;
    const v3, 0x7f030008

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setContentView(I)V

    .line 125
    const v3, 0x7f050059

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setTitle(I)V

    .line 126
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 127
    const v3, 0x7f070017

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 128
    .local v1, "lv":Landroid/widget/ListView;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v2, "objs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/andrey/notepad/ObjectModel;>;"
    iget-object v3, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    invoke-virtual {v3}, Lru/andrey/notepad/DataBaseHelper;->getFolders()Ljava/util/ArrayList;

    move-result-object v2

    .line 130
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 131
    new-instance v3, Lru/andrey/notepad/SimpleArrayAdapter;

    iget-object v4, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->this$0:Lru/andrey/notepad/FolderObjectArrayAdapter;

    # getter for: Lru/andrey/notepad/FolderObjectArrayAdapter;->context:Landroid/app/Activity;
    invoke-static {v4}, Lru/andrey/notepad/FolderObjectArrayAdapter;->access$0(Lru/andrey/notepad/FolderObjectArrayAdapter;)Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lru/andrey/notepad/SimpleArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 133
    new-instance v3, Lru/andrey/notepad/FolderObjectArrayAdapter$1$1;

    iget-object v4, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->val$dh:Lru/andrey/notepad/DataBaseHelper;

    iget v5, p0, Lru/andrey/notepad/FolderObjectArrayAdapter$1;->val$position:I

    invoke-direct {v3, p0, v4, v5, v0}, Lru/andrey/notepad/FolderObjectArrayAdapter$1$1;-><init>(Lru/andrey/notepad/FolderObjectArrayAdapter$1;Lru/andrey/notepad/DataBaseHelper;ILandroid/app/Dialog;)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 145
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 146
    return-void
.end method
