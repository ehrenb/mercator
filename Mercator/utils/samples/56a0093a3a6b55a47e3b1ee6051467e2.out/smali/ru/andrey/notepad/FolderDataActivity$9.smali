.class Lru/andrey/notepad/FolderDataActivity$9;
.super Ljava/lang/Object;
.source "FolderDataActivity.java"

# interfaces
.implements Lru/andrey/notepad/AmbilWarnaDialog$OnAmbilWarnaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/andrey/notepad/FolderDataActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/andrey/notepad/FolderDataActivity;


# direct methods
.method constructor <init>(Lru/andrey/notepad/FolderDataActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lru/andrey/notepad/FolderDataActivity$9;->this$0:Lru/andrey/notepad/FolderDataActivity;

    .line 1449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Lru/andrey/notepad/AmbilWarnaDialog;)V
    .locals 0
    .param p1, "dialog"    # Lru/andrey/notepad/AmbilWarnaDialog;

    .prologue
    .line 1462
    return-void
.end method

.method public onOk(Lru/andrey/notepad/AmbilWarnaDialog;I)V
    .locals 4
    .param p1, "dialog"    # Lru/andrey/notepad/AmbilWarnaDialog;
    .param p2, "col"    # I

    .prologue
    .line 1453
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$9;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "color"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1454
    iget-object v0, p0, Lru/andrey/notepad/FolderDataActivity$9;->this$0:Lru/andrey/notepad/FolderDataActivity;

    iget-object v0, v0, Lru/andrey/notepad/FolderDataActivity;->bg:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lru/andrey/notepad/FolderDataActivity$9;->this$0:Lru/andrey/notepad/FolderDataActivity;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "color"

    const-string v3, "#dad07f"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1456
    return-void
.end method
