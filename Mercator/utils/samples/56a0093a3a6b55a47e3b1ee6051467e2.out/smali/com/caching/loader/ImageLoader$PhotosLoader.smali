.class Lcom/caching/loader/ImageLoader$PhotosLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/caching/loader/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PhotosLoader"
.end annotation


# instance fields
.field photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

.field final synthetic this$0:Lcom/caching/loader/ImageLoader;


# direct methods
.method constructor <init>(Lcom/caching/loader/ImageLoader;Lcom/caching/loader/ImageLoader$PhotoToLoad;)V
    .locals 0
    .param p2, "photoToLoad"    # Lcom/caching/loader/ImageLoader$PhotoToLoad;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->this$0:Lcom/caching/loader/ImageLoader;

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    iput-object p2, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    .line 172
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 179
    :try_start_0
    iget-object v3, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->this$0:Lcom/caching/loader/ImageLoader;

    iget-object v4, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    invoke-virtual {v3, v4}, Lcom/caching/loader/ImageLoader;->imageViewReused(Lcom/caching/loader/ImageLoader$PhotoToLoad;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v3, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->this$0:Lcom/caching/loader/ImageLoader;

    iget-object v4, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    iget-object v4, v4, Lcom/caching/loader/ImageLoader$PhotoToLoad;->url:Ljava/lang/String;

    # invokes: Lcom/caching/loader/ImageLoader;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v3, v4}, Lcom/caching/loader/ImageLoader;->access$0(Lcom/caching/loader/ImageLoader;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 182
    .local v1, "bmp":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->this$0:Lcom/caching/loader/ImageLoader;

    iget-object v3, v3, Lcom/caching/loader/ImageLoader;->memoryCache:Lcom/caching/loader/MemoryCache;

    iget-object v4, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    iget-object v4, v4, Lcom/caching/loader/ImageLoader$PhotoToLoad;->url:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Lcom/caching/loader/MemoryCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 183
    iget-object v3, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->this$0:Lcom/caching/loader/ImageLoader;

    iget-object v4, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    invoke-virtual {v3, v4}, Lcom/caching/loader/ImageLoader;->imageViewReused(Lcom/caching/loader/ImageLoader$PhotoToLoad;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 185
    new-instance v0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;

    iget-object v3, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->this$0:Lcom/caching/loader/ImageLoader;

    iget-object v4, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    invoke-direct {v0, v3, v1, v4}, Lcom/caching/loader/ImageLoader$BitmapDisplayer;-><init>(Lcom/caching/loader/ImageLoader;Landroid/graphics/Bitmap;Lcom/caching/loader/ImageLoader$PhotoToLoad;)V

    .line 186
    .local v0, "bd":Lcom/caching/loader/ImageLoader$BitmapDisplayer;
    iget-object v3, p0, Lcom/caching/loader/ImageLoader$PhotosLoader;->this$0:Lcom/caching/loader/ImageLoader;

    iget-object v3, v3, Lcom/caching/loader/ImageLoader;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 188
    .end local v0    # "bd":Lcom/caching/loader/ImageLoader$BitmapDisplayer;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v2

    .line 190
    .local v2, "th":Ljava/lang/Throwable;
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
