.class Lcom/caching/loader/ImageLoader$BitmapDisplayer;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/caching/loader/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BitmapDisplayer"
.end annotation


# instance fields
.field bitmap:Landroid/graphics/Bitmap;

.field photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

.field final synthetic this$0:Lcom/caching/loader/ImageLoader;


# direct methods
.method public constructor <init>(Lcom/caching/loader/ImageLoader;Landroid/graphics/Bitmap;Lcom/caching/loader/ImageLoader$PhotoToLoad;)V
    .locals 0
    .param p2, "b"    # Landroid/graphics/Bitmap;
    .param p3, "p"    # Lcom/caching/loader/ImageLoader$PhotoToLoad;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->this$0:Lcom/caching/loader/ImageLoader;

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object p2, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->bitmap:Landroid/graphics/Bitmap;

    .line 212
    iput-object p3, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    .line 213
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->this$0:Lcom/caching/loader/ImageLoader;

    iget-object v1, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    invoke-virtual {v0, v1}, Lcom/caching/loader/ImageLoader;->imageViewReused(Lcom/caching/loader/ImageLoader$PhotoToLoad;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    iget-object v0, v0, Lcom/caching/loader/ImageLoader$PhotoToLoad;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/caching/loader/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/caching/loader/ImageLoader$PhotoToLoad;

    iget-object v0, v0, Lcom/caching/loader/ImageLoader$PhotoToLoad;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
