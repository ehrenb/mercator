.class public interface abstract Lio/realm/an;
.super Ljava/lang/Object;
.source "ScheduledRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$alternativeApkPath()Ljava/lang/String;
.end method

.method public abstract realmGet$appAction()Ljava/lang/String;
.end method

.method public abstract realmGet$icon()Ljava/lang/String;
.end method

.method public abstract realmGet$isDownloading()Z
.end method

.method public abstract realmGet$mainObbMd5()Ljava/lang/String;
.end method

.method public abstract realmGet$mainObbName()Ljava/lang/String;
.end method

.method public abstract realmGet$mainObbPath()Ljava/lang/String;
.end method

.method public abstract realmGet$md5()Ljava/lang/String;
.end method

.method public abstract realmGet$name()Ljava/lang/String;
.end method

.method public abstract realmGet$packageName()Ljava/lang/String;
.end method

.method public abstract realmGet$patchObbMd5()Ljava/lang/String;
.end method

.method public abstract realmGet$patchObbName()Ljava/lang/String;
.end method

.method public abstract realmGet$patchObbPath()Ljava/lang/String;
.end method

.method public abstract realmGet$path()Ljava/lang/String;
.end method

.method public abstract realmGet$storeName()Ljava/lang/String;
.end method

.method public abstract realmGet$verCode()I
.end method

.method public abstract realmGet$versionName()Ljava/lang/String;
.end method

.method public abstract realmSet$alternativeApkPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$appAction(Ljava/lang/String;)V
.end method

.method public abstract realmSet$icon(Ljava/lang/String;)V
.end method

.method public abstract realmSet$isDownloading(Z)V
.end method

.method public abstract realmSet$mainObbMd5(Ljava/lang/String;)V
.end method

.method public abstract realmSet$mainObbName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$mainObbPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$md5(Ljava/lang/String;)V
.end method

.method public abstract realmSet$name(Ljava/lang/String;)V
.end method

.method public abstract realmSet$packageName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$patchObbMd5(Ljava/lang/String;)V
.end method

.method public abstract realmSet$patchObbName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$patchObbPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$path(Ljava/lang/String;)V
.end method

.method public abstract realmSet$storeName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$verCode(I)V
.end method

.method public abstract realmSet$versionName(Ljava/lang/String;)V
.end method
