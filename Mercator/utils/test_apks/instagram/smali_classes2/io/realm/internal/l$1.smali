.class final Lio/realm/internal/l$1;
.super Ljava/lang/Object;
.source "Row.java"

# interfaces
.implements Lio/realm/internal/l;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 131
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JD)V
    .locals 2

    .prologue
    .line 226
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JF)V
    .locals 2

    .prologue
    .line 221
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JJ)V
    .locals 2

    .prologue
    .line 211
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 236
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/util/Date;)V
    .locals 2

    .prologue
    .line 231
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JZ)V
    .locals 2

    .prologue
    .line 216
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(J[B)V
    .locals 2

    .prologue
    .line 241
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(J)Z
    .locals 2

    .prologue
    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(JJ)V
    .locals 2

    .prologue
    .line 246
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(J)Z
    .locals 2

    .prologue
    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(J)V
    .locals 2

    .prologue
    .line 201
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public e(J)Lio/realm/RealmFieldType;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f(J)J
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g(J)Z
    .locals 2

    .prologue
    .line 156
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public h(J)F
    .locals 2

    .prologue
    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public i(J)D
    .locals 2

    .prologue
    .line 166
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public j(J)Ljava/util/Date;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public k(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public l(J)[B
    .locals 2

    .prologue
    .line 181
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public m(J)Lio/realm/internal/LinkView;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public n(J)V
    .locals 2

    .prologue
    .line 251
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t access a row that hasn\'t been loaded or represents \'null\', make sure the instance is loaded and is valid by calling \'RealmObject.isLoaded() && RealmObject.isValid()\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
