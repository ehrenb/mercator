.class public interface abstract Lio/realm/s;
.super Ljava/lang/Object;
.source "PaymentConfirmationRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$payerId()Ljava/lang/String;
.end method

.method public abstract realmGet$paymentConfirmationId()Ljava/lang/String;
.end method

.method public abstract realmGet$productId()I
.end method

.method public abstract realmGet$status()Ljava/lang/String;
.end method

.method public abstract realmSet$payerId(Ljava/lang/String;)V
.end method

.method public abstract realmSet$paymentConfirmationId(Ljava/lang/String;)V
.end method

.method public abstract realmSet$productId(I)V
.end method

.method public abstract realmSet$status(Ljava/lang/String;)V
.end method
