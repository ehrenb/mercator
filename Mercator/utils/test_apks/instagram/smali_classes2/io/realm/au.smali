.class public interface abstract Lio/realm/au;
.super Ljava/lang/Object;
.source "UpdateRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$alternativeApkPath()Ljava/lang/String;
.end method

.method public abstract realmGet$apkPath()Ljava/lang/String;
.end method

.method public abstract realmGet$appId()J
.end method

.method public abstract realmGet$excluded()Z
.end method

.method public abstract realmGet$fileSize()D
.end method

.method public abstract realmGet$icon()Ljava/lang/String;
.end method

.method public abstract realmGet$label()Ljava/lang/String;
.end method

.method public abstract realmGet$mainObbMd5()Ljava/lang/String;
.end method

.method public abstract realmGet$mainObbName()Ljava/lang/String;
.end method

.method public abstract realmGet$mainObbPath()Ljava/lang/String;
.end method

.method public abstract realmGet$md5()Ljava/lang/String;
.end method

.method public abstract realmGet$packageName()Ljava/lang/String;
.end method

.method public abstract realmGet$patchObbMd5()Ljava/lang/String;
.end method

.method public abstract realmGet$patchObbName()Ljava/lang/String;
.end method

.method public abstract realmGet$patchObbPath()Ljava/lang/String;
.end method

.method public abstract realmGet$timestamp()J
.end method

.method public abstract realmGet$trustedBadge()Ljava/lang/String;
.end method

.method public abstract realmGet$updateVersionCode()I
.end method

.method public abstract realmGet$updateVersionName()Ljava/lang/String;
.end method

.method public abstract realmGet$versionCode()I
.end method

.method public abstract realmSet$alternativeApkPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$apkPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$appId(J)V
.end method

.method public abstract realmSet$excluded(Z)V
.end method

.method public abstract realmSet$fileSize(D)V
.end method

.method public abstract realmSet$icon(Ljava/lang/String;)V
.end method

.method public abstract realmSet$label(Ljava/lang/String;)V
.end method

.method public abstract realmSet$mainObbMd5(Ljava/lang/String;)V
.end method

.method public abstract realmSet$mainObbName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$mainObbPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$md5(Ljava/lang/String;)V
.end method

.method public abstract realmSet$packageName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$patchObbMd5(Ljava/lang/String;)V
.end method

.method public abstract realmSet$patchObbName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$patchObbPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$timestamp(J)V
.end method

.method public abstract realmSet$trustedBadge(Ljava/lang/String;)V
.end method

.method public abstract realmSet$updateVersionCode(I)V
.end method

.method public abstract realmSet$updateVersionName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$versionCode(I)V
.end method
