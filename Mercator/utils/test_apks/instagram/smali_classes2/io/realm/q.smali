.class public interface abstract Lio/realm/q;
.super Ljava/lang/Object;
.source "PaymentAuthorizationRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$payerId()Ljava/lang/String;
.end method

.method public abstract realmGet$paymentId()I
.end method

.method public abstract realmGet$redirectUrl()Ljava/lang/String;
.end method

.method public abstract realmGet$status()Ljava/lang/String;
.end method

.method public abstract realmGet$url()Ljava/lang/String;
.end method

.method public abstract realmSet$payerId(Ljava/lang/String;)V
.end method

.method public abstract realmSet$paymentId(I)V
.end method

.method public abstract realmSet$redirectUrl(Ljava/lang/String;)V
.end method

.method public abstract realmSet$status(Ljava/lang/String;)V
.end method

.method public abstract realmSet$url(Ljava/lang/String;)V
.end method
