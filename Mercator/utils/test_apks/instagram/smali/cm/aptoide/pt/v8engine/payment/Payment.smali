.class public interface abstract Lcm/aptoide/pt/v8engine/payment/Payment;
.super Ljava/lang/Object;
.source "Payment.java"


# virtual methods
.method public abstract getAuthorization()Lcm/aptoide/pt/v8engine/payment/Authorization;
.end method

.method public abstract getDescription()Ljava/lang/String;
.end method

.method public abstract getId()I
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPrice()Lcm/aptoide/pt/v8engine/payment/Price;
.end method

.method public abstract getProduct()Lcm/aptoide/pt/v8engine/payment/Product;
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract isAuthorizationRequired()Z
.end method

.method public abstract process()Lrx/a;
.end method

.method public abstract setAuthorization(Lcm/aptoide/pt/v8engine/payment/Authorization;)V
.end method
