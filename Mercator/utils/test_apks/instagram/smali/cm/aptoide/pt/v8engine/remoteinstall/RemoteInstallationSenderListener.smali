.class public interface abstract Lcm/aptoide/pt/v8engine/remoteinstall/RemoteInstallationSenderListener;
.super Ljava/lang/Object;
.source "RemoteInstallationSenderListener.java"


# virtual methods
.method public abstract onAppSendSuccess()V
.end method

.method public abstract onAppSendUnsuccess()V
.end method

.method public abstract onAptoideTVServiceFound(Lcm/aptoide/pt/v8engine/remoteinstall/ReceiverDevice;)V
.end method

.method public abstract onAptoideTVServiceLost(Lcm/aptoide/pt/v8engine/remoteinstall/ReceiverDevice;)V
.end method

.method public abstract onDiscoveryStarted()V
.end method

.method public abstract onDiscoveryStopped()V
.end method

.method public abstract onNoNetworkAccess()V
.end method
