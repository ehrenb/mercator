.class public interface abstract Lcm/aptoide/pt/downloadmanager/interfaces/DownloadSettingsInterface;
.super Ljava/lang/Object;
.source "DownloadSettingsInterface.java"


# virtual methods
.method public abstract getButton1Icon()I
.end method

.method public abstract getButton1Text(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getDownloadDir()Ljava/lang/String;
.end method

.method public abstract getMainIcon()I
.end method

.method public abstract getMaxCacheSize()J
.end method

.method public abstract getObbDir()Ljava/lang/String;
.end method
