.class public interface abstract Lcm/aptoide/pt/v8engine/interfaces/Scrollable;
.super Ljava/lang/Object;
.source "Scrollable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcm/aptoide/pt/v8engine/interfaces/Scrollable$Position;
    }
.end annotation


# virtual methods
.method public abstract itemAdded(I)V
.end method

.method public abstract itemChanged(I)V
.end method

.method public abstract itemRemoved(I)V
.end method

.method public abstract scroll(Lcm/aptoide/pt/v8engine/interfaces/Scrollable$Position;)V
.end method
