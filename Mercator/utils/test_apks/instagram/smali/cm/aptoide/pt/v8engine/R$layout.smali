.class public final Lcm/aptoide/pt/v8engine/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/v8engine/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f040000

.field public static final abc_action_bar_up_container:I = 0x7f040001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f040002

.field public static final abc_action_menu_item_layout:I = 0x7f040003

.field public static final abc_action_menu_layout:I = 0x7f040004

.field public static final abc_action_mode_bar:I = 0x7f040005

.field public static final abc_action_mode_close_item_material:I = 0x7f040006

.field public static final abc_activity_chooser_view:I = 0x7f040007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f040008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f040009

.field public static final abc_alert_dialog_material:I = 0x7f04000a

.field public static final abc_dialog_title_material:I = 0x7f04000b

.field public static final abc_expanded_menu_layout:I = 0x7f04000c

.field public static final abc_list_menu_item_checkbox:I = 0x7f04000d

.field public static final abc_list_menu_item_icon:I = 0x7f04000e

.field public static final abc_list_menu_item_layout:I = 0x7f04000f

.field public static final abc_list_menu_item_radio:I = 0x7f040010

.field public static final abc_popup_menu_header_item_layout:I = 0x7f040011

.field public static final abc_popup_menu_item_layout:I = 0x7f040012

.field public static final abc_screen_content_include:I = 0x7f040013

.field public static final abc_screen_simple:I = 0x7f040014

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f040015

.field public static final abc_screen_toolbar:I = 0x7f040016

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f040017

.field public static final abc_search_view:I = 0x7f040018

.field public static final abc_select_dialog_material:I = 0x7f040019

.field public static final active_donwload_row_layout:I = 0x7f04001a

.field public static final active_downloads_header_row:I = 0x7f04001b

.field public static final activity_app_view:I = 0x7f04001c

.field public static final activity_create_store:I = 0x7f04001d

.field public static final activity_create_user:I = 0x7f04001e

.field public static final activity_feed_back:I = 0x7f04001f

.field public static final activity_main:I = 0x7f040020

.field public static final activity_main_open_gl:I = 0x7f040021

.field public static final activity_main_store_list:I = 0x7f040022

.field public static final activity_payment:I = 0x7f040023

.field public static final activity_web_authorization:I = 0x7f040024

.field public static final add_more_stores_row:I = 0x7f040025

.field public static final app_bar_layout:I = 0x7f040026

.field public static final app_manual_reviewed_message_layout:I = 0x7f040027

.field public static final apps_timeline_card_header:I = 0x7f040028

.field public static final appview_flags_layout:I = 0x7f040029

.field public static final appview_rate_and_flag_app_layout:I = 0x7f04002a

.field public static final appview_rating_bar:I = 0x7f04002b

.field public static final appview_rating_card:I = 0x7f04002c

.field public static final appview_rating_indicator:I = 0x7f04002d

.field public static final brick_app_item:I = 0x7f04002e

.field public static final brick_app_item_list:I = 0x7f04002f

.field public static final button:I = 0x7f040030

.field public static final com_facebook_activity_layout:I = 0x7f040031

.field public static final com_facebook_device_auth_dialog_fragment:I = 0x7f040032

.field public static final com_facebook_login_fragment:I = 0x7f040033

.field public static final com_facebook_smart_device_dialog_fragment:I = 0x7f040034

.field public static final com_facebook_tooltip_bubble:I = 0x7f040035

.field public static final comment_layout:I = 0x7f040036

.field public static final comments_displayable_group:I = 0x7f040037

.field public static final comments_read_more_layout:I = 0x7f040038

.field public static final completed_donwload_row_layout:I = 0x7f040039

.field public static final create_store_displayable_layout:I = 0x7f04003a

.field public static final create_store_themes_recyclerview:I = 0x7f04003b

.field public static final credentials_edit_texts:I = 0x7f04003c

.field public static final custom_snackbar:I = 0x7f04003d

.field public static final design_bottom_navigation_item:I = 0x7f04003e

.field public static final design_bottom_sheet_dialog:I = 0x7f04003f

.field public static final design_layout_snackbar:I = 0x7f040040

.field public static final design_layout_snackbar_include:I = 0x7f040041

.field public static final design_layout_tab_icon:I = 0x7f040042

.field public static final design_layout_tab_text:I = 0x7f040043

.field public static final design_menu_item_action_area:I = 0x7f040044

.field public static final design_navigation_item:I = 0x7f040045

.field public static final design_navigation_item_header:I = 0x7f040046

.field public static final design_navigation_item_separator:I = 0x7f040047

.field public static final design_navigation_item_subheader:I = 0x7f040048

.field public static final design_navigation_menu:I = 0x7f040049

.field public static final design_navigation_menu_item:I = 0x7f04004a

.field public static final design_text_input_password_icon:I = 0x7f04004b

.field public static final dialog_about:I = 0x7f04004c

.field public static final dialog_add_pvt_store:I = 0x7f04004d

.field public static final dialog_add_store:I = 0x7f04004e

.field public static final dialog_choose_avatar_layout:I = 0x7f04004f

.field public static final dialog_comment_on_review:I = 0x7f040050

.field public static final dialog_install_warning:I = 0x7f040051

.field public static final dialog_rate_app:I = 0x7f040052

.field public static final dialog_remote_install:I = 0x7f040053

.field public static final dialog_requestpin:I = 0x7f040054

.field public static final displayable_app_view_comments:I = 0x7f040055

.field public static final displayable_app_view_description:I = 0x7f040056

.field public static final displayable_app_view_developer:I = 0x7f040057

.field public static final displayable_app_view_flag_this:I = 0x7f040058

.field public static final displayable_app_view_flag_trusted:I = 0x7f040059

.field public static final displayable_app_view_images:I = 0x7f04005a

.field public static final displayable_app_view_install:I = 0x7f04005b

.field public static final displayable_app_view_other_versions:I = 0x7f04005c

.field public static final displayable_app_view_rate_and_comment:I = 0x7f04005d

.field public static final displayable_app_view_rate_result:I = 0x7f04005e

.field public static final displayable_app_view_rate_this:I = 0x7f04005f

.field public static final displayable_app_view_subscription:I = 0x7f040060

.field public static final displayable_app_view_suggested_app:I = 0x7f040061

.field public static final displayable_app_view_suggested_apps:I = 0x7f040062

.field public static final displayable_empty:I = 0x7f040063

.field public static final displayable_grid_add_store_comment:I = 0x7f040064

.field public static final displayable_grid_app:I = 0x7f040065

.field public static final displayable_grid_display:I = 0x7f040066

.field public static final displayable_grid_footer:I = 0x7f040067

.field public static final displayable_grid_footer_text:I = 0x7f040068

.field public static final displayable_grid_header:I = 0x7f040069

.field public static final displayable_grid_latest_store_comments:I = 0x7f04006a

.field public static final displayable_grid_scheduled_download:I = 0x7f04006b

.field public static final displayable_grid_sponsored:I = 0x7f04006c

.field public static final displayable_grid_store:I = 0x7f04006d

.field public static final displayable_grid_store_subscribed:I = 0x7f04006e

.field public static final displayable_latest_store_review:I = 0x7f04006f

.field public static final displayable_list_app:I = 0x7f040070

.field public static final displayable_rate_and_review:I = 0x7f040071

.field public static final displayable_recommended_store:I = 0x7f040072

.field public static final displayable_row_review:I = 0x7f040073

.field public static final displayable_scheduled_download_row:I = 0x7f040074

.field public static final displayable_social_timeline_app_update:I = 0x7f040075

.field public static final displayable_social_timeline_article:I = 0x7f040076

.field public static final displayable_social_timeline_feature:I = 0x7f040077

.field public static final displayable_social_timeline_recommendation:I = 0x7f040078

.field public static final displayable_social_timeline_similar:I = 0x7f040079

.field public static final displayable_social_timeline_social_article:I = 0x7f04007a

.field public static final displayable_social_timeline_social_article_preview:I = 0x7f04007b

.field public static final displayable_social_timeline_social_install:I = 0x7f04007c

.field public static final displayable_social_timeline_social_install_preview:I = 0x7f04007d

.field public static final displayable_social_timeline_social_recommendation:I = 0x7f04007e

.field public static final displayable_social_timeline_social_recommendation_preview:I = 0x7f04007f

.field public static final displayable_social_timeline_social_store_latest_apps:I = 0x7f040080

.field public static final displayable_social_timeline_social_store_latest_apps_preview:I = 0x7f040081

.field public static final displayable_social_timeline_social_video:I = 0x7f040082

.field public static final displayable_social_timeline_social_video_preview:I = 0x7f040083

.field public static final displayable_social_timeline_store_latest_apps:I = 0x7f040084

.field public static final displayable_social_timeline_video:I = 0x7f040085

.field public static final displayable_store_meta:I = 0x7f040086

.field public static final displayable_to_delete:I = 0x7f040087

.field public static final drawer_header:I = 0x7f040088

.field public static final exclude_update_uninstall_menu:I = 0x7f040089

.field public static final flag_this_app_card_fake_layout:I = 0x7f04008a

.field public static final flag_this_app_card_freeze_layout:I = 0x7f04008b

.field public static final flag_this_app_card_good_layout:I = 0x7f04008c

.field public static final flag_this_app_card_need_license_layout:I = 0x7f04008d

.field public static final flag_this_app_card_virus_layout:I = 0x7f04008e

.field public static final fragment_app_view:I = 0x7f04008f

.field public static final fragment_app_view_description:I = 0x7f040090

.field public static final fragment_aptoide_preferences:I = 0x7f040091

.field public static final fragment_other_versions:I = 0x7f040092

.field public static final fragment_rate_and_reviews:I = 0x7f040093

.field public static final fragment_screenshots_viewer:I = 0x7f040094

.field public static final fragment_user_interests:I = 0x7f040095

.field public static final fragment_with_toolbar:I = 0x7f040096

.field public static final fragment_with_toolbar_and_swipe:I = 0x7f040097

.field public static final frame_layout:I = 0x7f040098

.field public static final global_search_fragment:I = 0x7f040099

.field public static final incl_no_search_results_layout:I = 0x7f04009a

.field public static final installed_row:I = 0x7f04009b

.field public static final layout_dialog_badge:I = 0x7f04009c

.field public static final layout_dialog_permissions:I = 0x7f04009d

.field public static final logged_in_first_screen:I = 0x7f04009e

.field public static final logged_in_second_screen:I = 0x7f04009f

.field public static final login_activity_layout:I = 0x7f0400a0

.field public static final messenger_button_send_blue_large:I = 0x7f0400a1

.field public static final messenger_button_send_blue_round:I = 0x7f0400a2

.field public static final messenger_button_send_blue_small:I = 0x7f0400a3

.field public static final messenger_button_send_white_large:I = 0x7f0400a4

.field public static final messenger_button_send_white_round:I = 0x7f0400a5

.field public static final messenger_button_send_white_small:I = 0x7f0400a6

.field public static final mini_top_comment:I = 0x7f0400a7

.field public static final my_account_activity:I = 0x7f0400a8

.field public static final my_store_displayable_layout:I = 0x7f0400a9

.field public static final my_stores_layout_fragment:I = 0x7f0400aa

.field public static final notification_action:I = 0x7f0400ab

.field public static final notification_action_tombstone:I = 0x7f0400ac

.field public static final notification_media_action:I = 0x7f0400ad

.field public static final notification_media_cancel_action:I = 0x7f0400ae

.field public static final notification_template_big_media:I = 0x7f0400af

.field public static final notification_template_big_media_custom:I = 0x7f0400b0

.field public static final notification_template_big_media_narrow:I = 0x7f0400b1

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0400b2

.field public static final notification_template_custom_big:I = 0x7f0400b3

.field public static final notification_template_icon_group:I = 0x7f0400b4

.field public static final notification_template_lines_media:I = 0x7f0400b5

.field public static final notification_template_media:I = 0x7f0400b6

.field public static final notification_template_media_custom:I = 0x7f0400b7

.field public static final notification_template_part_chronometer:I = 0x7f0400b8

.field public static final notification_template_part_time:I = 0x7f0400b9

.field public static final official_app_displayable_layout:I = 0x7f0400ba

.field public static final other_version_row:I = 0x7f0400bb

.field public static final partial_app_view_collapsing_toolbar:I = 0x7f0400bc

.field public static final partial_social_timeline_thumbnail:I = 0x7f0400bd

.field public static final partial_view_comment_separator:I = 0x7f0400be

.field public static final partial_view_empty_frame:I = 0x7f0400bf

.field public static final partial_view_error:I = 0x7f0400c0

.field public static final partial_view_incl_no_network:I = 0x7f0400c1

.field public static final partial_view_no_apps_downloaded:I = 0x7f0400c2

.field public static final partial_view_progress_bar:I = 0x7f0400c3

.field public static final payment_item:I = 0x7f0400c4

.field public static final preference:I = 0x7f0400c5

.field public static final preference_category:I = 0x7f0400c6

.field public static final preference_dialog_edittext:I = 0x7f0400c7

.field public static final preference_information:I = 0x7f0400c8

.field public static final preference_list_fragment:I = 0x7f0400c9

.field public static final preference_recyclerview:I = 0x7f0400ca

.field public static final preference_widget_checkbox:I = 0x7f0400cb

.field public static final preference_widget_switch_compat:I = 0x7f0400cc

.field public static final psts_tab:I = 0x7f0400cd

.field public static final pushnotificationlayout:I = 0x7f0400ce

.field public static final recycler_fragment:I = 0x7f0400cf

.field public static final recycler_fragment_downloads:I = 0x7f0400d0

.field public static final recycler_fragment_with_toolbar:I = 0x7f0400d1

.field public static final recycler_swipe_fragment:I = 0x7f0400d2

.field public static final recycler_swipe_fragment_with_toolbar:I = 0x7f0400d3

.field public static final rollback_row:I = 0x7f0400d4

.field public static final row_adult_switch:I = 0x7f0400d5

.field public static final row_description:I = 0x7f0400d6

.field public static final row_excluded_update:I = 0x7f0400d7

.field public static final row_item_screenshots_big:I = 0x7f0400d8

.field public static final row_item_screenshots_gallery:I = 0x7f0400d9

.field public static final row_permission:I = 0x7f0400da

.field public static final row_progress_bar:I = 0x7f0400db

.field public static final row_remote_install:I = 0x7f0400dc

.field public static final search_app_row:I = 0x7f0400dd

.field public static final searchview_layout:I = 0x7f0400de

.field public static final select_dialog_item_material:I = 0x7f0400df

.field public static final select_dialog_multichoice_material:I = 0x7f0400e0

.field public static final select_dialog_singlechoice_material:I = 0x7f0400e1

.field public static final separator_horizontal:I = 0x7f0400e2

.field public static final separator_vertical:I = 0x7f0400e3

.field public static final sign_up_activity_layout:I = 0x7f0400e4

.field public static final social_bar_timeline:I = 0x7f0400e5

.field public static final social_fragment_layout:I = 0x7f0400e6

.field public static final social_timeline_latest_app:I = 0x7f0400e7

.field public static final social_timeline_like_user_preview:I = 0x7f0400e8

.field public static final social_timeline_social_store_follow:I = 0x7f0400e9

.field public static final store_activity:I = 0x7f0400ea

.field public static final store_activity_with_logo:I = 0x7f0400eb

.field public static final store_recycler_fragment:I = 0x7f0400ec

.field public static final suggested_app_search:I = 0x7f0400ed

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0400ee

.field public static final timeline_follow_user:I = 0x7f0400ef

.field public static final timeline_follows_info:I = 0x7f0400f0

.field public static final timeline_login_header_layout:I = 0x7f0400f1

.field public static final toolbar:I = 0x7f0400f2

.field public static final toolbar_readmore:I = 0x7f0400f3

.field public static final update_row:I = 0x7f0400f4

.field public static final updates_header_row:I = 0x7f0400f5

.field public static final view_like_button:I = 0x7f0400f6

.field public static final view_pager:I = 0x7f0400f7

.field public static final white_message_displayable:I = 0x7f0400f8

.field public static final wizard_layout:I = 0x7f0400f9

.field public static final wizard_page_one:I = 0x7f0400fa

.field public static final wizard_page_three:I = 0x7f0400fb

.field public static final wizard_page_two:I = 0x7f0400fc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
