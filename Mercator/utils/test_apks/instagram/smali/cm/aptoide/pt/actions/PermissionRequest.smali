.class public interface abstract Lcm/aptoide/pt/actions/PermissionRequest;
.super Ljava/lang/Object;
.source "PermissionRequest.java"


# virtual methods
.method public abstract requestAccessToAccounts(Lrx/b/a;Lrx/b/a;)V
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation
.end method

.method public abstract requestAccessToAccounts(ZLrx/b/a;Lrx/b/a;)V
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation
.end method

.method public abstract requestAccessToExternalFileSystem(Lrx/b/a;Lrx/b/a;)V
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation
.end method

.method public abstract requestAccessToExternalFileSystem(ZLrx/b/a;Lrx/b/a;)V
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation
.end method

.method public abstract requestDownloadAccess(Lrx/b/a;Lrx/b/a;)V
.end method
