.class public final Lcm/aptoide/pt/downloadmanager/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/downloadmanager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final WidgetProvider_timestamp_days_ago:I = 0x7f070042

.field public static final WidgetProvider_timestamp_hour_ago:I = 0x7f070043

.field public static final WidgetProvider_timestamp_hours_ago:I = 0x7f070044

.field public static final WidgetProvider_timestamp_just_now:I = 0x7f070045

.field public static final WidgetProvider_timestamp_minutes_ago:I = 0x7f070046

.field public static final WidgetProvider_timestamp_month_ago:I = 0x7f070047

.field public static final WidgetProvider_timestamp_months_ago:I = 0x7f070048

.field public static final WidgetProvider_timestamp_today:I = 0x7f070049

.field public static final WidgetProvider_timestamp_week_ago2:I = 0x7f07004a

.field public static final WidgetProvider_timestamp_weeks_ago:I = 0x7f07004b

.field public static final WidgetProvider_timestamp_year_ago:I = 0x7f07004c

.field public static final WidgetProvider_timestamp_years_ago:I = 0x7f07004d

.field public static final WidgetProvider_timestamp_yesterday:I = 0x7f07004e

.field public static final X_download_number:I = 0x7f07004f

.field public static final _downloads:I = 0x7f070050

.field public static final abTest1_label_1:I = 0x7f070051

.field public static final abTest1_label_2:I = 0x7f070052

.field public static final abTest1_label_3:I = 0x7f070053

.field public static final abc_action_bar_home_description:I = 0x7f070000

.field public static final abc_action_bar_home_description_format:I = 0x7f070001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070002

.field public static final abc_action_bar_up_description:I = 0x7f070003

.field public static final abc_action_menu_overflow_description:I = 0x7f070004

.field public static final abc_action_mode_done:I = 0x7f070005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070006

.field public static final abc_activitychooserview_choose_application:I = 0x7f070007

.field public static final abc_capital_off:I = 0x7f070008

.field public static final abc_capital_on:I = 0x7f070009

.field public static final abc_font_family_body_1_material:I = 0x7f0703d3

.field public static final abc_font_family_body_2_material:I = 0x7f0703d4

.field public static final abc_font_family_button_material:I = 0x7f0703d5

.field public static final abc_font_family_caption_material:I = 0x7f0703d6

.field public static final abc_font_family_display_1_material:I = 0x7f0703d7

.field public static final abc_font_family_display_2_material:I = 0x7f0703d8

.field public static final abc_font_family_display_3_material:I = 0x7f0703d9

.field public static final abc_font_family_display_4_material:I = 0x7f0703da

.field public static final abc_font_family_headline_material:I = 0x7f0703db

.field public static final abc_font_family_menu_material:I = 0x7f0703dc

.field public static final abc_font_family_subhead_material:I = 0x7f0703dd

.field public static final abc_font_family_title_material:I = 0x7f0703de

.field public static final abc_search_hint:I = 0x7f07000a

.field public static final abc_searchview_description_clear:I = 0x7f07000b

.field public static final abc_searchview_description_query:I = 0x7f07000c

.field public static final abc_searchview_description_search:I = 0x7f07000d

.field public static final abc_searchview_description_submit:I = 0x7f07000e

.field public static final abc_searchview_description_voice:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f070010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070011

.field public static final abc_toolbar_collapse_description:I = 0x7f070012

.field public static final about_mail:I = 0x7f070054

.field public static final about_site:I = 0x7f070055

.field public static final about_us:I = 0x7f070056

.field public static final account:I = 0x7f070059

.field public static final active:I = 0x7f07005b

.field public static final additional_permissions:I = 0x7f07005f

.field public static final adult_pin_wrong:I = 0x7f070060

.field public static final all_stores:I = 0x7f070063

.field public static final allow_root:I = 0x7f070064

.field public static final and:I = 0x7f070065

.field public static final apk_not_found:I = 0x7f070066

.field public static final app_trusted:I = 0x7f070068

.field public static final app_warning:I = 0x7f070069

.field public static final application_signature_analysis:I = 0x7f07006a

.field public static final application_signature_blacklisted:I = 0x7f07006b

.field public static final application_signature_matched:I = 0x7f07006c

.field public static final application_signature_not_matched:I = 0x7f07006d

.field public static final appview_additional_information_title:I = 0x7f070073

.field public static final appview_comment_reply_button_text:I = 0x7f070074

.field public static final appview_header_trusted_text:I = 0x7f070079

.field public static final appview_other_versions_in_others_stores:I = 0x7f07007c

.field public static final appview_rate_Success:I = 0x7f07007d

.field public static final appview_rate_this_app_text:I = 0x7f07007f

.field public static final appview_rating_card_text:I = 0x7f070080

.field public static final appview_recommended_for_you_title:I = 0x7f070082

.field public static final appview_subscribe_store_button_text:I = 0x7f070083

.field public static final appview_subscribed_store_button_text:I = 0x7f070084

.field public static final aptoide_downloading:I = 0x7f070085

.field public static final aptoide_timeline:I = 0x7f070088

.field public static final are_you_adult:I = 0x7f07008c

.field public static final asksetadultpinmessage:I = 0x7f07008d

.field public static final at_time:I = 0x7f07008e

.field public static final backup_apps:I = 0x7f07008f

.field public static final billing_result:I = 0x7f070092

.field public static final button_more:I = 0x7f070098

.field public static final buy:I = 0x7f070099

.field public static final cache_using_X_mb:I = 0x7f07009a

.field public static final cancel:I = 0x7f07009b

.field public static final cannot_uninstall_self:I = 0x7f07009c

.field public static final casting_vote:I = 0x7f0700a0

.field public static final change_payment:I = 0x7f0700a2

.field public static final change_theme:I = 0x7f0700a3

.field public static final change_theme_soon:I = 0x7f0700a4

.field public static final clear_cache_sucess:I = 0x7f0700a7

.field public static final clearcache_sum:I = 0x7f0700a9

.field public static final clearcache_sum_jolla:I = 0x7f0700aa

.field public static final clearcontent_sum:I = 0x7f0700ac

.field public static final clearcontent_sum_jolla:I = 0x7f0700ad

.field public static final clearcontent_title:I = 0x7f0700ae

.field public static final comment:I = 0x7f0700b0

.field public static final comment_submitted:I = 0x7f0700b3

.field public static final comments:I = 0x7f0700b4

.field public static final compared_with_another_marketplace:I = 0x7f0700b7

.field public static final completed:I = 0x7f0700b8

.field public static final confirm_carrier_payment:I = 0x7f0700b9

.field public static final connection_error:I = 0x7f0700ba

.field public static final continue_option:I = 0x7f0700bb

.field public static final cpuAbi:I = 0x7f0700bc

.field public static final createReviewButtonText:I = 0x7f0700bd

.field public static final create_review:I = 0x7f0700c6

.field public static final critical:I = 0x7f0700e2

.field public static final data_usage_constraint:I = 0x7f0700e5

.field public static final description:I = 0x7f0700e8

.field public static final developer:I = 0x7f0700ea

.field public static final dialog_version_size:I = 0x7f0700f5

.field public static final disable_social_timeline:I = 0x7f0700f6

.field public static final dont_show_again:I = 0x7f070106

.field public static final downgrade_requires_uninstall:I = 0x7f070108

.field public static final download_completed:I = 0x7f07010b

.field public static final download_paused:I = 0x7f07010c

.field public static final download_pending:I = 0x7f07010d

.field public static final download_progress:I = 0x7f07010e

.field public static final download_queue:I = 0x7f07010f

.field public static final download_retry:I = 0x7f070110

.field public static final drawer_social_media_title:I = 0x7f070113

.field public static final email:I = 0x7f070118

.field public static final email_copied:I = 0x7f070119

.field public static final empty_search:I = 0x7f07011a

.field public static final error503:I = 0x7f07011c

.field public static final error_410:I = 0x7f07011d

.field public static final error_APK_1:I = 0x7f07011e

.field public static final error_APK_4:I = 0x7f07011f

.field public static final error_APK_Description:I = 0x7f070120

.field public static final error_AUTH_1:I = 0x7f070121

.field public static final error_AUTH_100:I = 0x7f070122

.field public static final error_AUTH_101:I = 0x7f070123

.field public static final error_AUTH_102:I = 0x7f070124

.field public static final error_AUTH_103:I = 0x7f070125

.field public static final error_AUTH_2:I = 0x7f070126

.field public static final error_AUTH_3:I = 0x7f070127

.field public static final error_AUTH_4:I = 0x7f070128

.field public static final error_IARG_1:I = 0x7f070129

.field public static final error_IARG_100:I = 0x7f07012a

.field public static final error_IARG_101:I = 0x7f07012b

.field public static final error_IARG_102:I = 0x7f07012c

.field public static final error_IARG_103:I = 0x7f07012d

.field public static final error_IARG_104:I = 0x7f07012e

.field public static final error_IARG_105:I = 0x7f07012f

.field public static final error_IARG_106:I = 0x7f070130

.field public static final error_IARG_107:I = 0x7f070131

.field public static final error_IARG_108:I = 0x7f070132

.field public static final error_IARG_109:I = 0x7f070133

.field public static final error_IARG_110:I = 0x7f070134

.field public static final error_IARG_2:I = 0x7f070135

.field public static final error_IARG_200:I = 0x7f070136

.field public static final error_IARG_201:I = 0x7f070137

.field public static final error_IARG_3:I = 0x7f070138

.field public static final error_IARG_300:I = 0x7f070139

.field public static final error_IARG_301:I = 0x7f07013a

.field public static final error_IARG_302:I = 0x7f07013b

.field public static final error_IARG_4:I = 0x7f07013c

.field public static final error_IARG_5:I = 0x7f07013d

.field public static final error_IARG_6:I = 0x7f07013e

.field public static final error_MARG_1:I = 0x7f07013f

.field public static final error_MARG_10:I = 0x7f070140

.field public static final error_MARG_100:I = 0x7f070141

.field public static final error_MARG_101:I = 0x7f070142

.field public static final error_MARG_102:I = 0x7f070143

.field public static final error_MARG_103:I = 0x7f070144

.field public static final error_MARG_104:I = 0x7f070145

.field public static final error_MARG_105:I = 0x7f070146

.field public static final error_MARG_106:I = 0x7f070147

.field public static final error_MARG_107:I = 0x7f070148

.field public static final error_MARG_108:I = 0x7f070149

.field public static final error_MARG_109:I = 0x7f07014a

.field public static final error_MARG_11:I = 0x7f07014b

.field public static final error_MARG_110:I = 0x7f07014c

.field public static final error_MARG_111:I = 0x7f07014d

.field public static final error_MARG_112:I = 0x7f07014e

.field public static final error_MARG_113:I = 0x7f07014f

.field public static final error_MARG_2:I = 0x7f070150

.field public static final error_MARG_200:I = 0x7f070151

.field public static final error_MARG_201:I = 0x7f070152

.field public static final error_MARG_202:I = 0x7f070153

.field public static final error_MARG_3:I = 0x7f070154

.field public static final error_MARG_4:I = 0x7f070155

.field public static final error_MARG_5:I = 0x7f070156

.field public static final error_MARG_6:I = 0x7f070157

.field public static final error_MARG_7:I = 0x7f070158

.field public static final error_MARG_8:I = 0x7f070159

.field public static final error_MARG_9:I = 0x7f07015a

.field public static final error_REPO_1:I = 0x7f07015b

.field public static final error_REPO_2:I = 0x7f07015c

.field public static final error_REPO_3:I = 0x7f07015d

.field public static final error_REPO_4_no_store:I = 0x7f07015e

.field public static final error_REPO_7:I = 0x7f07015f

.field public static final error_SYS_1:I = 0x7f070160

.field public static final error_SYS_2:I = 0x7f070161

.field public static final error_SYS_3:I = 0x7f070162

.field public static final error_SYS_4:I = 0x7f070163

.field public static final error_SYS_Billing:I = 0x7f070164

.field public static final error_WOP_10:I = 0x7f070165

.field public static final error_WOP_11:I = 0x7f070166

.field public static final error_WOP_6:I = 0x7f070167

.field public static final error_WOP_7:I = 0x7f070168

.field public static final error_WOP_8:I = 0x7f070169

.field public static final error_WOP_9:I = 0x7f07016a

.field public static final error_occured_paying:I = 0x7f07016d

.field public static final error_occured_retry_later:I = 0x7f07016e

.field public static final error_occured_uploading:I = 0x7f07016f

.field public static final exclude_update:I = 0x7f070171

.field public static final facebook:I = 0x7f070173

.field public static final facebook_friends_list_using_timeline:I = 0x7f070175

.field public static final facebook_friends_list_using_timeline_empty:I = 0x7f070176

.field public static final facebook_join_friends:I = 0x7f070178

.field public static final facebook_logout_and_login:I = 0x7f070179

.field public static final facebook_start_timeline:I = 0x7f07017a

.field public static final facebook_timeline_friends_invited:I = 0x7f07017b

.field public static final failed_auth_code:I = 0x7f07017c

.field public static final feedback_include_logs:I = 0x7f07017f

.field public static final feedback_no_email:I = 0x7f070180

.field public static final feedback_send_button:I = 0x7f070182

.field public static final feedback_subject:I = 0x7f070183

.field public static final feedback_to_write2:I = 0x7f070184

.field public static final finished_install:I = 0x7f070187

.field public static final flag_added:I = 0x7f070189

.field public static final flag_fake:I = 0x7f07018a

.field public static final flag_freeze:I = 0x7f07018b

.field public static final flag_good:I = 0x7f07018c

.field public static final flag_license:I = 0x7f07018d

.field public static final flag_this_app:I = 0x7f07018f

.field public static final flag_this_app_button_text:I = 0x7f070190

.field public static final flag_this_app_fake:I = 0x7f070191

.field public static final flag_this_app_freeze:I = 0x7f070192

.field public static final flag_this_app_good:I = 0x7f070193

.field public static final flag_this_app_need_licence:I = 0x7f070194

.field public static final flag_this_app_virus:I = 0x7f070195

.field public static final flag_virus:I = 0x7f070196

.field public static final followed_stores:I = 0x7f070199

.field public static final friends_installs:I = 0x7f0701a2

.field public static final friends_to_invite:I = 0x7f0701a3

.field public static final general_downloads_dialog_no_download_rule_message:I = 0x7f0701a5

.field public static final general_downloads_dialog_only_mobile_message:I = 0x7f0701a6

.field public static final general_downloads_dialog_only_wifi_message:I = 0x7f0701a7

.field public static final get_latest:I = 0x7f0701a8

.field public static final has_updates:I = 0x7f0701a9

.field public static final having_some_trouble:I = 0x7f0701aa

.field public static final hidden_adult:I = 0x7f0701ae

.field public static final hide_post:I = 0x7f0701af

.field public static final hide_post_info:I = 0x7f0701b0

.field public static final highlighted_apps:I = 0x7f0701b2

.field public static final ignoreUpdateText:I = 0x7f0701b4

.field public static final ignored:I = 0x7f0701b6

.field public static final insert_store_name:I = 0x7f0701c0

.field public static final install:I = 0x7f0701c1

.field public static final install_update:I = 0x7f0701c6

.field public static final installapp_alrt:I = 0x7f0701c7

.field public static final installed_tab:I = 0x7f0701c8

.field public static final installed_this:I = 0x7f0701c9

.field public static final installing:I = 0x7f0701ca

.field public static final invalid_apk:I = 0x7f0701cc

.field public static final invite_friends:I = 0x7f0701cd

.field public static final ip_blacklisted:I = 0x7f0701ce

.field public static final like:I = 0x7f0701d6

.field public static final likes:I = 0x7f0701d7

.field public static final make_review_title:I = 0x7f0701dd

.field public static final manually_reviewed:I = 0x7f0701de

.field public static final menu_context_remove:I = 0x7f0701e2

.field public static final menu_context_reparse:I = 0x7f0701e3

.field public static final menu_search_item_go_to_store:I = 0x7f0701e4

.field public static final more_editors_choice:I = 0x7f0701e7

.field public static final more_friends:I = 0x7f0701e8

.field public static final more_info:I = 0x7f0701e9

.field public static final more_reviews:I = 0x7f0701ea

.field public static final more_versions:I = 0x7f0701eb

.field public static final my_account:I = 0x7f0701f0

.field public static final na:I = 0x7f0701f2

.field public static final navigation_drawer_signup_login:I = 0x7f0701f3

.field public static final new_updates:I = 0x7f0701f6

.field public static final no:I = 0x7f0701fb

.field public static final noIgnoreUpdateText:I = 0x7f0701fc

.field public static final no_apps_found:I = 0x7f0701fd

.field public static final no_comment_be_the_first:I = 0x7f0701fe

.field public static final no_description:I = 0x7f0701ff

.field public static final no_email_and_pass_error_message:I = 0x7f070200

.field public static final no_email_error_message:I = 0x7f070201

.field public static final no_excluded_updates:I = 0x7f070202

.field public static final no_excluded_updates_selected:I = 0x7f070204

.field public static final no_new_updates_available:I = 0x7f070205

.field public static final no_pass_error_message:I = 0x7f070206

.field public static final no_payments_available:I = 0x7f070207

.field public static final no_permissions_required:I = 0x7f070208

.field public static final no_sch_downloads:I = 0x7f07020b

.field public static final no_search_results_message:I = 0x7f07020c

.field public static final no_updates_available_retoric:I = 0x7f07020d

.field public static final notification_social_timeline:I = 0x7f070213

.field public static final notification_timeline_activity:I = 0x7f070214

.field public static final notification_timeline_new_comments:I = 0x7f070215

.field public static final notification_timeline_new_likes:I = 0x7f070216

.field public static final notification_timeline_posts:I = 0x7f070217

.field public static final one_new_update:I = 0x7f070219

.field public static final order_popup_title3:I = 0x7f07021f

.field public static final other_stores:I = 0x7f070220

.field public static final out_of_space_error:I = 0x7f0703bc

.field public static final paidapp_not_found:I = 0x7f070225

.field public static final pause_download:I = 0x7f070229

.field public static final permissions:I = 0x7f07022a

.field public static final permissions_na:I = 0x7f07022b

.field public static final please_wait:I = 0x7f07022f

.field public static final privacy_copied:I = 0x7f070231

.field public static final privacy_policy:I = 0x7f070232

.field public static final private_key:I = 0x7f070233

.field public static final public_key:I = 0x7f070236

.field public static final read_less:I = 0x7f07023c

.field public static final read_more:I = 0x7f07023d

.field public static final reason_failed:I = 0x7f07023e

.field public static final reason_manual:I = 0x7f07023f

.field public static final reason_scanned:I = 0x7f070240

.field public static final reason_signature:I = 0x7f070241

.field public static final reason_third_party:I = 0x7f070242

.field public static final reason_unknown:I = 0x7f070243

.field public static final recommended_for_you:I = 0x7f070244

.field public static final remaining_time:I = 0x7f070248

.field public static final remote_in_nospace:I = 0x7f070249

.field public static final remove_mature_pin_summary:I = 0x7f070257

.field public static final remove_mature_pin_title:I = 0x7f070258

.field public static final reply:I = 0x7f070259

.field public static final reply_to:I = 0x7f07025a

.field public static final request_adult_pin:I = 0x7f07025b

.field public static final restart_aptoide:I = 0x7f07025c

.field public static final restore:I = 0x7f07025d

.field public static final results_in_store:I = 0x7f07025f

.field public static final results_subscribed:I = 0x7f070260

.field public static final resume_download:I = 0x7f070261

.field public static final retrieving_update:I = 0x7f070262

.field public static final review_addictive:I = 0x7f070263

.field public static final review_bad:I = 0x7f070264

.field public static final review_by:I = 0x7f070265

.field public static final review_final_score:I = 0x7f070266

.field public static final review_final_verdict:I = 0x7f070267

.field public static final review_finish:I = 0x7f070268

.field public static final review_get_app:I = 0x7f070269

.field public static final review_good:I = 0x7f07026a

.field public static final review_speed:I = 0x7f07026c

.field public static final review_stability:I = 0x7f07026d

.field public static final review_success:I = 0x7f07026e

.field public static final review_title:I = 0x7f07026f

.field public static final review_usability:I = 0x7f070270

.field public static final reviewed_by:I = 0x7f070271

.field public static final reviews:I = 0x7f070272

.field public static final rollback_downgraded:I = 0x7f070275

.field public static final rollback_empty:I = 0x7f070276

.field public static final rollback_installed:I = 0x7f070277

.field public static final rollback_uninstalled:I = 0x7f070278

.field public static final rollback_updated:I = 0x7f070279

.field public static final rollback_updated_at:I = 0x7f07027a

.field public static final root:I = 0x7f07027b

.field public static final root_access_dialog:I = 0x7f07027c

.field public static final scanned_manually_by_aptoide_team:I = 0x7f07027e

.field public static final scanned_verified_by_tester:I = 0x7f07027f

.field public static final scanned_with_av:I = 0x7f070280

.field public static final schDown_install:I = 0x7f070281

.field public static final schDown_installselected:I = 0x7f070282

.field public static final schDown_invertselection:I = 0x7f070283

.field public static final schDown_nodownloadselect:I = 0x7f070284

.field public static final schDown_removeselected:I = 0x7f070285

.field public static final schDwnBtn:I = 0x7f070286

.field public static final screenCode:I = 0x7f070287

.field public static final sd_error:I = 0x7f070288

.field public static final sd_error_jolla:I = 0x7f070289

.field public static final search_for_apps:I = 0x7f07028b

.field public static final search_menu_title:I = 0x7f07003e

.field public static final search_minimum_chars:I = 0x7f07028c

.field public static final search_other_stores:I = 0x7f07028d

.field public static final see_less:I = 0x7f07028e

.field public static final see_more:I = 0x7f07028f

.field public static final see_top_stores:I = 0x7f070290

.field public static final select_friends_to_invite:I = 0x7f070291

.field public static final server_error:I = 0x7f070292

.field public static final set_mature_pin_summary:I = 0x7f070293

.field public static final set_mature_pin_title:I = 0x7f070294

.field public static final set_sliders:I = 0x7f070295

.field public static final setpin:I = 0x7f070296

.field public static final setting_3g:I = 0x7f070297

.field public static final setting_4g:I = 0x7f070298

.field public static final setting_allUpdates_filtering:I = 0x7f070299

.field public static final setting_appfiltersum:I = 0x7f07029a

.field public static final setting_appfiltertitle:I = 0x7f07029b

.field public static final setting_auto_update_sum:I = 0x7f07029c

.field public static final setting_auto_update_title:I = 0x7f07029d

.field public static final setting_category_autoupdate:I = 0x7f0703c1

.field public static final setting_category_autoupdate_title:I = 0x7f0703c3

.field public static final setting_clear_memory:I = 0x7f07029e

.field public static final setting_dont_show_mature_content:I = 0x7f07029f

.field public static final setting_esgl_version:I = 0x7f0702a0

.field public static final setting_ethernet:I = 0x7f0702a1

.field public static final setting_general_download_rules:I = 0x7f0702a2

.field public static final setting_hwspecssum:I = 0x7f0702a3

.field public static final setting_hwspecstitle:I = 0x7f0702a4

.field public static final setting_icon_download_rules:I = 0x7f0702a5

.field public static final setting_mature_filtering:I = 0x7f0702a6

.field public static final setting_mobile:I = 0x7f0702a7

.field public static final setting_schdwninstallsum:I = 0x7f0702a8

.field public static final setting_schdwninstalltitle:I = 0x7f0702a9

.field public static final setting_schdwntitle:I = 0x7f0702aa

.field public static final setting_screen_size:I = 0x7f0702ab

.field public static final setting_sdk_version:I = 0x7f0702ac

.field public static final setting_sum_general_download_rules:I = 0x7f0702ad

.field public static final setting_sum_icon_download_rules:I = 0x7f0702ae

.field public static final setting_updates:I = 0x7f0702af

.field public static final setting_updates_notification_sum:I = 0x7f0702b0

.field public static final setting_updates_notification_title:I = 0x7f0702b1

.field public static final setting_wifi:I = 0x7f0702b2

.field public static final settings:I = 0x7f0702b3

.field public static final settings_dwn_network_rules:I = 0x7f0702b4

.field public static final settings_maxFileCache_sum:I = 0x7f0702b5

.field public static final settings_maxFileCache_title:I = 0x7f0702b6

.field public static final settings_title_bar:I = 0x7f0702b7

.field public static final share:I = 0x7f0702b8

.field public static final share_on_timeline:I = 0x7f0702b9

.field public static final share_options_share_external:I = 0x7f0703c4

.field public static final share_options_share_in_timeline:I = 0x7f0703c5

.field public static final show_all_updates:I = 0x7f0702bb

.field public static final simple_error_occured:I = 0x7f0702bd

.field public static final size:I = 0x7f0702bf

.field public static final skip:I = 0x7f0702c0

.field public static final social_timeline_notifications:I = 0x7f0702c6

.field public static final social_timeline_notifications_sum2:I = 0x7f0702c7

.field public static final sponsored:I = 0x7f0702d9

.field public static final srch_hint:I = 0x7f0702dc

.field public static final stand_by:I = 0x7f0703c9

.field public static final starting_download:I = 0x7f0702dd

.field public static final status_bar_notification_info_overflow:I = 0x7f07003f

.field public static final store:I = 0x7f0702e0

.field public static final store_already_added:I = 0x7f0702e1

.field public static final store_name:I = 0x7f0702e3

.field public static final store_password:I = 0x7f0702e4

.field public static final store_privacy:I = 0x7f0702e5

.field public static final store_username:I = 0x7f0702e7

.field public static final subscribe_apps_official_store:I = 0x7f0702ea

.field public static final subscribe_pvt_store:I = 0x7f0702eb

.field public static final subscribe_store:I = 0x7f0702ec

.field public static final suggested_app:I = 0x7f0702ef

.field public static final theme:I = 0x7f0702f3

.field public static final timeline_ad:I = 0x7f0702f6

.field public static final timeline_email_how_to_join:I = 0x7f0702f7

.field public static final timeline_email_invitation:I = 0x7f0702f8

.field public static final timeline_email_step1:I = 0x7f0702f9

.field public static final timeline_email_step2:I = 0x7f0702fa

.field public static final timeline_empty_no_friends_send_email:I = 0x7f0702fb

.field public static final timeline_empty_send_email_to_friends:I = 0x7f0702fc

.field public static final timeline_is_empty_no_friends:I = 0x7f0702ff

.field public static final timeline_is_empty_send_email:I = 0x7f070300

.field public static final timeline_is_empty_start_invite:I = 0x7f070301

.field public static final timeline_like:I = 0x7f070302

.field public static final timeline_not_compatible:I = 0x7f070304

.field public static final timeout:I = 0x7f070306

.field public static final title_caps:I = 0x7f070308

.field public static final trusted_app:I = 0x7f070315

.field public static final twitter:I = 0x7f070316

.field public static final unhide_post:I = 0x7f07031b

.field public static final unknown:I = 0x7f07031d

.field public static final unlike:I = 0x7f07031f

.field public static final unsubscribe:I = 0x7f070320

.field public static final unsubscribe_yes_no:I = 0x7f070321

.field public static final unsubscribed_:I = 0x7f070322

.field public static final unsubscribing_store_message:I = 0x7f070323

.field public static final update_all:I = 0x7f070325

.field public static final update_self_msg:I = 0x7f070326

.field public static final update_self_title:I = 0x7f070327

.field public static final updatesTabAdapterUninstall:I = 0x7f070329

.field public static final updates_tab:I = 0x7f07032c

.field public static final updates_tab_adapter_uninstalled:I = 0x7f07032d

.field public static final upload_app:I = 0x7f07032f

.field public static final upload_app_backup_apps_installed:I = 0x7f070330

.field public static final upload_app_backup_apps_not_installed:I = 0x7f070331

.field public static final version:I = 0x7f07033d

.field public static final version_placeholder:I = 0x7f07033f

.field public static final vote_down:I = 0x7f070341

.field public static final vote_submitted:I = 0x7f070342

.field public static final vote_up:I = 0x7f070343

.field public static final votes:I = 0x7f070344

.field public static final warning:I = 0x7f070345

.field public static final website:I = 0x7f070347

.field public static final website_copied:I = 0x7f070348

.field public static final whats_new:I = 0x7f070349

.field public static final wizard_description_01:I = 0x7f07034b

.field public static final wizard_description_02:I = 0x7f07034c

.field public static final wizard_description_03_1:I = 0x7f07034d

.field public static final wizard_description_03_2:I = 0x7f07034e

.field public static final wizard_new_and_improved:I = 0x7f070354

.field public static final wizard_new_and_improved_bullet_1:I = 0x7f070355

.field public static final wizard_new_and_improved_bullet_2:I = 0x7f070356

.field public static final wizard_new_and_improved_bullet_3:I = 0x7f070357

.field public static final wizard_new_and_improved_bullet_4:I = 0x7f070358

.field public static final wizard_new_homepage:I = 0x7f070359

.field public static final wizard_new_top:I = 0x7f07035a

.field public static final wizard_next:I = 0x7f07035b

.field public static final wizard_prev:I = 0x7f07035d

.field public static final wizard_reviews:I = 0x7f07035f

.field public static final wizard_reviews_title:I = 0x7f070360

.field public static final wizard_subtitle_03_1:I = 0x7f070364

.field public static final wizard_subtitle_03_2:I = 0x7f070365

.field public static final wizard_title_01:I = 0x7f070366

.field public static final wizard_title_02:I = 0x7f070367

.field public static final wizard_title_03:I = 0x7f070368

.field public static final write_a_comment:I = 0x7f07036f

.field public static final write_one_con:I = 0x7f070370

.field public static final write_one_pro:I = 0x7f070371

.field public static final write_your_comment:I = 0x7f070372

.field public static final write_your_final_verdict:I = 0x7f070373

.field public static final yes:I = 0x7f070395


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
