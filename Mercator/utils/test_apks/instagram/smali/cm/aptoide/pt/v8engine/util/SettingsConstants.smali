.class public Lcm/aptoide/pt/v8engine/util/SettingsConstants;
.super Ljava/lang/Object;
.source "SettingsConstants.java"


# static fields
.field public static final ABOUT_DIALOG:Ljava/lang/CharSequence;

.field public static final ADULT_CHECK_BOX:Ljava/lang/CharSequence;

.field public static final ADULT_PIN:Ljava/lang/CharSequence;

.field public static final CHECK_AUTO_UPDATE:Ljava/lang/CharSequence;

.field public static final CHECK_AUTO_UPDATE_CATEGORY:Ljava/lang/CharSequence;

.field public static final CLEAR_CACHE:Ljava/lang/CharSequence;

.field public static final FILTER_APPS:Ljava/lang/CharSequence;

.field public static final HARDWARE_SPECS:Ljava/lang/CharSequence;

.field public static final MAX_FILE_CACHE:Ljava/lang/CharSequence;

.field public static final ROOT:Ljava/lang/CharSequence;

.field public static final THEME:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "matureChkBox"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->ADULT_CHECK_BOX:Ljava/lang/CharSequence;

    .line 14
    const-string v0, "Maturepin"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->ADULT_PIN:Ljava/lang/CharSequence;

    .line 15
    const-string v0, "clearcache"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->CLEAR_CACHE:Ljava/lang/CharSequence;

    .line 16
    const-string v0, "maxFileCache"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->MAX_FILE_CACHE:Ljava/lang/CharSequence;

    .line 17
    const-string v0, "root"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->ROOT:Ljava/lang/CharSequence;

    .line 18
    const-string v0, "hwspecs"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->HARDWARE_SPECS:Ljava/lang/CharSequence;

    .line 19
    const-string v0, "hwspecsChkBox"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->FILTER_APPS:Ljava/lang/CharSequence;

    .line 20
    const-string v0, "theme"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->THEME:Ljava/lang/CharSequence;

    .line 21
    const-string v0, "aboutDialog"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->ABOUT_DIALOG:Ljava/lang/CharSequence;

    .line 22
    const-string v0, "checkautoupdate"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->CHECK_AUTO_UPDATE:Ljava/lang/CharSequence;

    .line 23
    const-string v0, "checkautoupdatecategory"

    sput-object v0, Lcm/aptoide/pt/v8engine/util/SettingsConstants;->CHECK_AUTO_UPDATE_CATEGORY:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
