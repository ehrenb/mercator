.class public final Lcm/aptoide/pt/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final WidgetProvider_timestamp_days_ago:I = 0x7f070042

.field public static final WidgetProvider_timestamp_hour_ago:I = 0x7f070043

.field public static final WidgetProvider_timestamp_hours_ago:I = 0x7f070044

.field public static final WidgetProvider_timestamp_just_now:I = 0x7f070045

.field public static final WidgetProvider_timestamp_minutes_ago:I = 0x7f070046

.field public static final WidgetProvider_timestamp_month_ago:I = 0x7f070047

.field public static final WidgetProvider_timestamp_months_ago:I = 0x7f070048

.field public static final WidgetProvider_timestamp_today:I = 0x7f070049

.field public static final WidgetProvider_timestamp_week_ago2:I = 0x7f07004a

.field public static final WidgetProvider_timestamp_weeks_ago:I = 0x7f07004b

.field public static final WidgetProvider_timestamp_year_ago:I = 0x7f07004c

.field public static final WidgetProvider_timestamp_years_ago:I = 0x7f07004d

.field public static final WidgetProvider_timestamp_yesterday:I = 0x7f07004e

.field public static final X_download_number:I = 0x7f07004f

.field public static final _downloads:I = 0x7f070050

.field public static final abTest1_label_1:I = 0x7f070051

.field public static final abTest1_label_2:I = 0x7f070052

.field public static final abTest1_label_3:I = 0x7f070053

.field public static final abc_action_bar_home_description:I = 0x7f070000

.field public static final abc_action_bar_home_description_format:I = 0x7f070001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070002

.field public static final abc_action_bar_up_description:I = 0x7f070003

.field public static final abc_action_menu_overflow_description:I = 0x7f070004

.field public static final abc_action_mode_done:I = 0x7f070005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070006

.field public static final abc_activitychooserview_choose_application:I = 0x7f070007

.field public static final abc_capital_off:I = 0x7f070008

.field public static final abc_capital_on:I = 0x7f070009

.field public static final abc_font_family_body_1_material:I = 0x7f0703d3

.field public static final abc_font_family_body_2_material:I = 0x7f0703d4

.field public static final abc_font_family_button_material:I = 0x7f0703d5

.field public static final abc_font_family_caption_material:I = 0x7f0703d6

.field public static final abc_font_family_display_1_material:I = 0x7f0703d7

.field public static final abc_font_family_display_2_material:I = 0x7f0703d8

.field public static final abc_font_family_display_3_material:I = 0x7f0703d9

.field public static final abc_font_family_display_4_material:I = 0x7f0703da

.field public static final abc_font_family_headline_material:I = 0x7f0703db

.field public static final abc_font_family_menu_material:I = 0x7f0703dc

.field public static final abc_font_family_subhead_material:I = 0x7f0703dd

.field public static final abc_font_family_title_material:I = 0x7f0703de

.field public static final abc_search_hint:I = 0x7f07000a

.field public static final abc_searchview_description_clear:I = 0x7f07000b

.field public static final abc_searchview_description_query:I = 0x7f07000c

.field public static final abc_searchview_description_search:I = 0x7f07000d

.field public static final abc_searchview_description_submit:I = 0x7f07000e

.field public static final abc_searchview_description_voice:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f070010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070011

.field public static final abc_toolbar_collapse_description:I = 0x7f070012

.field public static final about_mail:I = 0x7f070054

.field public static final about_site:I = 0x7f070055

.field public static final about_us:I = 0x7f070056

.field public static final accept:I = 0x7f070013

.field public static final accept_terms:I = 0x7f070057

.field public static final accept_terms_signup:I = 0x7f070398

.field public static final access_to_get_accounts_rationale:I = 0x7f070058

.field public static final account:I = 0x7f070059

.field public static final action:I = 0x7f07005a

.field public static final active:I = 0x7f07005b

.field public static final activity_payment_more_payments_button:I = 0x7f070399

.field public static final add_store:I = 0x7f07005c

.field public static final add_store_dialog_title:I = 0x7f0703cd

.field public static final add_store_dialog_top_stores:I = 0x7f0703ce

.field public static final add_store_dialog_top_stores_small_text:I = 0x7f0703cf

.field public static final added_to_scheduled:I = 0x7f07005d

.field public static final additional_information:I = 0x7f07005e

.field public static final additional_permissions:I = 0x7f07005f

.field public static final addressbook_2nd_msg:I = 0x7f07039a

.field public static final addressbook_3rd_msg:I = 0x7f07039b

.field public static final addressbook_allow_friends_find:I = 0x7f07039c

.field public static final addressbook_connect_with_friends_message:I = 0x7f07039d

.field public static final addressbook_data_about:I = 0x7f07039e

.field public static final addressbook_increase_network_message:I = 0x7f07039f

.field public static final addressbook_insuccess_connection:I = 0x7f0703a0

.field public static final addressbook_invite_friends:I = 0x7f0703a1

.field public static final addressbook_phone_number:I = 0x7f0703a2

.field public static final addressbook_share_phone:I = 0x7f0703a3

.field public static final addressbook_success:I = 0x7f0703a4

.field public static final addressbook_success_connected_friends:I = 0x7f0703a5

.field public static final addressbook_sync:I = 0x7f0703a6

.field public static final addressbook_thanks_for_connecting:I = 0x7f0703a7

.field public static final adult_pin_wrong:I = 0x7f070060

.field public static final adventure:I = 0x7f070061

.field public static final all:I = 0x7f070062

.field public static final all_stores:I = 0x7f070063

.field public static final allow_root:I = 0x7f070064

.field public static final and:I = 0x7f070065

.field public static final apk_not_found:I = 0x7f070066

.field public static final app_name:I = 0x7f0703df

.field public static final app_size:I = 0x7f070067

.field public static final app_trusted:I = 0x7f070068

.field public static final app_warning:I = 0x7f070069

.field public static final appbar_scrolling_view_behavior:I = 0x7f0703e0

.field public static final application_signature_analysis:I = 0x7f07006a

.field public static final application_signature_blacklisted:I = 0x7f07006b

.field public static final application_signature_matched:I = 0x7f07006c

.field public static final application_signature_not_matched:I = 0x7f07006d

.field public static final applications:I = 0x7f07006e

.field public static final apps:I = 0x7f07006f

.field public static final apps_for_kids:I = 0x7f070070

.field public static final apps_timeline:I = 0x7f070071

.field public static final appstimeline_update_app:I = 0x7f070072

.field public static final appview_additional_information_title:I = 0x7f070073

.field public static final appview_comment_reply_button_text:I = 0x7f070074

.field public static final appview_follow_store_button_text:I = 0x7f070075

.field public static final appview_followers_count_text:I = 0x7f070076

.field public static final appview_good_app_description:I = 0x7f070077

.field public static final appview_good_app_guaranteed:I = 0x7f070078

.field public static final appview_header_trusted_text:I = 0x7f070079

.field public static final appview_latest_version_text:I = 0x7f07007a

.field public static final appview_other_versions:I = 0x7f07007b

.field public static final appview_other_versions_in_others_stores:I = 0x7f07007c

.field public static final appview_rate_Success:I = 0x7f07007d

.field public static final appview_rate_this_app:I = 0x7f07007e

.field public static final appview_rate_this_app_text:I = 0x7f07007f

.field public static final appview_rating_card_text:I = 0x7f070080

.field public static final appview_read_all:I = 0x7f070081

.field public static final appview_recommended_for_you_title:I = 0x7f070082

.field public static final appview_subscribe_store_button_text:I = 0x7f070083

.field public static final appview_subscribed_store_button_text:I = 0x7f070084

.field public static final aptoide_downloading:I = 0x7f070085

.field public static final aptoide_email:I = 0x7f070086

.field public static final aptoide_publishers:I = 0x7f070087

.field public static final aptoide_timeline:I = 0x7f070088

.field public static final aptoide_url:I = 0x7f070089

.field public static final arcade:I = 0x7f07008a

.field public static final arcade_action:I = 0x7f07008b

.field public static final are_you_adult:I = 0x7f07008c

.field public static final asksetadultpinmessage:I = 0x7f07008d

.field public static final at_time:I = 0x7f07008e

.field public static final authenticator_account_type:I = 0x7f0703e1

.field public static final backup_apps:I = 0x7f07008f

.field public static final be_the_first_to_rate_this_app:I = 0x7f070090

.field public static final be_the_first_to_send_feedback_about_this_store:I = 0x7f070091

.field public static final billing_result:I = 0x7f070092

.field public static final board:I = 0x7f070093

.field public static final books_reference:I = 0x7f070094

.field public static final bottom_sheet_behavior:I = 0x7f0703e2

.field public static final brain_puzzle:I = 0x7f070095

.field public static final business:I = 0x7f070096

.field public static final button_header_more:I = 0x7f070097

.field public static final button_more:I = 0x7f070098

.field public static final buy:I = 0x7f070099

.field public static final cache_using_X_mb:I = 0x7f07009a

.field public static final cancel:I = 0x7f07009b

.field public static final cannot_uninstall_self:I = 0x7f07009c

.field public static final card:I = 0x7f07009d

.field public static final cards_casino:I = 0x7f07009e

.field public static final casino:I = 0x7f07009f

.field public static final casting_vote:I = 0x7f0700a0

.field public static final casual:I = 0x7f0700a1

.field public static final change_payment:I = 0x7f0700a2

.field public static final change_theme:I = 0x7f0700a3

.field public static final change_theme_soon:I = 0x7f0700a4

.field public static final character_counter_pattern:I = 0x7f0703e3

.field public static final clear:I = 0x7f0700a5

.field public static final clear_cache_dialog_message:I = 0x7f0700a6

.field public static final clear_cache_sucess:I = 0x7f0700a7

.field public static final clear_cache_summary:I = 0x7f0700a8

.field public static final clearcache_sum:I = 0x7f0700a9

.field public static final clearcache_sum_jolla:I = 0x7f0700aa

.field public static final clearcache_title:I = 0x7f0700ab

.field public static final clearcontent_sum:I = 0x7f0700ac

.field public static final clearcontent_sum_jolla:I = 0x7f0700ad

.field public static final clearcontent_title:I = 0x7f0700ae

.field public static final com_crashlytics_android_build_id:I = 0x7f0703e4

.field public static final com_facebook_device_auth_instructions:I = 0x7f070014

.field public static final com_facebook_image_download_unknown_error:I = 0x7f070015

.field public static final com_facebook_internet_permission_error_message:I = 0x7f070016

.field public static final com_facebook_internet_permission_error_title:I = 0x7f070017

.field public static final com_facebook_like_button_liked:I = 0x7f070018

.field public static final com_facebook_like_button_not_liked:I = 0x7f070019

.field public static final com_facebook_loading:I = 0x7f07001a

.field public static final com_facebook_loginview_cancel_action:I = 0x7f07001b

.field public static final com_facebook_loginview_log_in_button:I = 0x7f07001c

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f07001d

.field public static final com_facebook_loginview_log_out_action:I = 0x7f07001e

.field public static final com_facebook_loginview_log_out_button:I = 0x7f07001f

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f070020

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f070021

.field public static final com_facebook_send_button_text:I = 0x7f070022

.field public static final com_facebook_share_button_text:I = 0x7f070023

.field public static final com_facebook_smart_device_instructions_0:I = 0x7f0703e5

.field public static final com_facebook_smart_device_instructions_1:I = 0x7f0703e6

.field public static final com_facebook_smart_device_instructions_2:I = 0x7f0703e7

.field public static final com_facebook_smart_device_instructions_3:I = 0x7f0703e8

.field public static final com_facebook_smart_device_instructions_or:I = 0x7f0703e9

.field public static final com_facebook_smart_login_confirmation_cancel:I = 0x7f0703ea

.field public static final com_facebook_smart_login_confirmation_continue_as:I = 0x7f0703eb

.field public static final com_facebook_smart_login_confirmation_title:I = 0x7f0703ec

.field public static final com_facebook_tooltip_default:I = 0x7f070024

.field public static final comics:I = 0x7f0700af

.field public static final comment:I = 0x7f0700b0

.field public static final comment_on_store:I = 0x7f0700b1

.field public static final comment_store:I = 0x7f0700b2

.field public static final comment_store_title:I = 0x7f0703a8

.field public static final comment_submitted:I = 0x7f0700b3

.field public static final comments:I = 0x7f0700b4

.field public static final common_google_play_services_enable_button:I = 0x7f070025

.field public static final common_google_play_services_enable_text:I = 0x7f070026

.field public static final common_google_play_services_enable_title:I = 0x7f070027

.field public static final common_google_play_services_install_button:I = 0x7f070028

.field public static final common_google_play_services_install_text:I = 0x7f070029

.field public static final common_google_play_services_install_title:I = 0x7f07002a

.field public static final common_google_play_services_notification_ticker:I = 0x7f07002b

.field public static final common_google_play_services_unknown_issue:I = 0x7f07002c

.field public static final common_google_play_services_unsupported_text:I = 0x7f07002d

.field public static final common_google_play_services_update_button:I = 0x7f07002e

.field public static final common_google_play_services_update_text:I = 0x7f07002f

.field public static final common_google_play_services_update_title:I = 0x7f070030

.field public static final common_google_play_services_updating_text:I = 0x7f070031

.field public static final common_google_play_services_wear_update_text:I = 0x7f070032

.field public static final common_open_on_phone:I = 0x7f070033

.field public static final common_signin_button_text:I = 0x7f070034

.field public static final common_signin_button_text_long:I = 0x7f070035

.field public static final communication:I = 0x7f0700b5

.field public static final community:I = 0x7f0700b6

.field public static final compared_with_another_marketplace:I = 0x7f0700b7

.field public static final completed:I = 0x7f0700b8

.field public static final confirm_carrier_payment:I = 0x7f0700b9

.field public static final connect_with_facebook:I = 0x7f0703a9

.field public static final connect_with_google:I = 0x7f0703aa

.field public static final connection_error:I = 0x7f0700ba

.field public static final content_authority:I = 0x7f0703ed

.field public static final continue_option:I = 0x7f0700bb

.field public static final cpuAbi:I = 0x7f0700bc

.field public static final createReviewButtonText:I = 0x7f0700bd

.field public static final create_calendar_message:I = 0x7f070036

.field public static final create_calendar_title:I = 0x7f070037

.field public static final create_password:I = 0x7f0703ab

.field public static final create_profile_logged_in_activity_title:I = 0x7f0700be

.field public static final create_profile_pub_pri:I = 0x7f0700bf

.field public static final create_profile_pub_pri_make_priv:I = 0x7f0700c0

.field public static final create_profile_pub_pri_sub_text_1:I = 0x7f0700c1

.field public static final create_profile_pub_pri_sub_text_2:I = 0x7f0700c2

.field public static final create_profile_pub_pri_sub_text_3:I = 0x7f0700c3

.field public static final create_profile_pub_pri_suc_login:I = 0x7f0700c4

.field public static final create_profile_pub_pri_suc_login_sub_text:I = 0x7f0700c5

.field public static final create_review:I = 0x7f0700c6

.field public static final create_store_choose_image:I = 0x7f0700c7

.field public static final create_store_create:I = 0x7f0700c8

.field public static final create_store_description_hint:I = 0x7f0700c9

.field public static final create_store_description_title:I = 0x7f0700ca

.field public static final create_store_displayable_button:I = 0x7f0700cb

.field public static final create_store_displayable_created_store_long_term_message:I = 0x7f0703ac

.field public static final create_store_displayable_created_store_message:I = 0x7f0703ee

.field public static final create_store_displayable_created_store_short_term_message:I = 0x7f0703ad

.field public static final create_store_displayable_empty_description_message:I = 0x7f0703ae

.field public static final create_store_displayable_explore_button:I = 0x7f0700cc

.field public static final create_store_displayable_explore_long_term_button:I = 0x7f0703af

.field public static final create_store_displayable_explore_message:I = 0x7f0700cd

.field public static final create_store_displayable_message:I = 0x7f0700ce

.field public static final create_store_header:I = 0x7f0700cf

.field public static final create_store_name:I = 0x7f0700d0

.field public static final create_store_name_inserted:I = 0x7f0700d1

.field public static final create_store_skip:I = 0x7f0700d2

.field public static final create_store_store_created:I = 0x7f0700d3

.field public static final create_store_theme:I = 0x7f0700d4

.field public static final create_store_title:I = 0x7f0700d5

.field public static final create_user_bad_photo:I = 0x7f0700d6

.field public static final create_user_choose_username:I = 0x7f0700d7

.field public static final create_user_create_button:I = 0x7f0700d8

.field public static final create_user_dialog_camera:I = 0x7f0700d9

.field public static final create_user_dialog_cancel:I = 0x7f0700da

.field public static final create_user_dialog_gallery:I = 0x7f0700db

.field public static final create_user_header:I = 0x7f0700dc

.field public static final create_user_hint:I = 0x7f0700dd

.field public static final create_user_random_photo:I = 0x7f0700de

.field public static final create_user_take_picture:I = 0x7f0700df

.field public static final create_user_title:I = 0x7f0700e0

.field public static final create_user_username:I = 0x7f0700e1

.field public static final credits:I = 0x7f0703ef

.field public static final critical:I = 0x7f0700e2

.field public static final customtabs_open_native_app:I = 0x7f0700e3

.field public static final dark:I = 0x7f0700e4

.field public static final data_usage_constraint:I = 0x7f0700e5

.field public static final debug_menu_ad_information:I = 0x7f070038

.field public static final debug_menu_creative_preview:I = 0x7f070039

.field public static final debug_menu_title:I = 0x7f07003a

.field public static final debug_menu_troubleshooting:I = 0x7f07003b

.field public static final decline:I = 0x7f07003c

.field public static final default_account_type:I = 0x7f0700e6

.field public static final demo:I = 0x7f0700e7

.field public static final description:I = 0x7f0700e8

.field public static final description_not_available:I = 0x7f0700e9

.field public static final developer:I = 0x7f0700ea

.field public static final developer_email:I = 0x7f0700eb

.field public static final developer_privacy_policy:I = 0x7f0700ec

.field public static final developer_website:I = 0x7f0700ed

.field public static final dialog_install_warning_careful_text:I = 0x7f0700ee

.field public static final dialog_install_warning_credibility_text:I = 0x7f0700ef

.field public static final dialog_install_warning_get_trusted_version_button:I = 0x7f0700f0

.field public static final dialog_install_warning_proceed_button:I = 0x7f0700f1

.field public static final dialog_install_warning_search_for_trusted_app_button:I = 0x7f0700f2

.field public static final dialog_install_warning_trusted_app_button:I = 0x7f0700f3

.field public static final dialog_install_warning_trusted_version_button:I = 0x7f0700f4

.field public static final dialog_version_size:I = 0x7f0700f5

.field public static final disable_social_timeline:I = 0x7f0700f6

.field public static final displayable_social_timeline_app_has_update:I = 0x7f0700f7

.field public static final displayable_social_timeline_app_update:I = 0x7f0700f8

.field public static final displayable_social_timeline_app_update_button:I = 0x7f0700f9

.field public static final displayable_social_timeline_app_update_error:I = 0x7f0700fa

.field public static final displayable_social_timeline_app_update_name:I = 0x7f0700fb

.field public static final displayable_social_timeline_app_update_updated:I = 0x7f0700fc

.field public static final displayable_social_timeline_app_update_updating:I = 0x7f0700fd

.field public static final displayable_social_timeline_app_update_version:I = 0x7f0700fe

.field public static final displayable_social_timeline_article_get_app_button:I = 0x7f0700ff

.field public static final displayable_social_timeline_article_related_to:I = 0x7f070100

.field public static final displayable_social_timeline_recommendation_atptoide_team_recommends:I = 0x7f070101

.field public static final displayable_social_timeline_recommendation_similar_and:I = 0x7f070102

.field public static final displayable_social_timeline_recommendation_similar_to:I = 0x7f070103

.field public static final displayable_social_timeline_similar_to:I = 0x7f070104

.field public static final displayable_social_timeline_store_latest_apps_headline:I = 0x7f070105

.field public static final done:I = 0x7f0703b0

.field public static final dont_show_again:I = 0x7f070106

.field public static final downgrade:I = 0x7f070107

.field public static final downgrade_requires_uninstall:I = 0x7f070108

.field public static final downgrade_warning_dialog:I = 0x7f070109

.field public static final downgrading_msg:I = 0x7f07010a

.field public static final download_completed:I = 0x7f07010b

.field public static final download_paused:I = 0x7f07010c

.field public static final download_pending:I = 0x7f07010d

.field public static final download_progress:I = 0x7f07010e

.field public static final download_queue:I = 0x7f07010f

.field public static final download_retry:I = 0x7f070110

.field public static final downloads:I = 0x7f070111

.field public static final downloads_count:I = 0x7f070112

.field public static final drawer_social_media_title:I = 0x7f070113

.field public static final edit_store:I = 0x7f0703b1

.field public static final edit_store_header:I = 0x7f070114

.field public static final edit_store_title:I = 0x7f070115

.field public static final education:I = 0x7f070116

.field public static final educational:I = 0x7f070117

.field public static final email:I = 0x7f070118

.field public static final email_copied:I = 0x7f070119

.field public static final empty_search:I = 0x7f07011a

.field public static final entertainment:I = 0x7f07011b

.field public static final error503:I = 0x7f07011c

.field public static final error_410:I = 0x7f07011d

.field public static final error_APK_1:I = 0x7f07011e

.field public static final error_APK_4:I = 0x7f07011f

.field public static final error_APK_Description:I = 0x7f070120

.field public static final error_AUTH_1:I = 0x7f070121

.field public static final error_AUTH_100:I = 0x7f070122

.field public static final error_AUTH_101:I = 0x7f070123

.field public static final error_AUTH_102:I = 0x7f070124

.field public static final error_AUTH_103:I = 0x7f070125

.field public static final error_AUTH_2:I = 0x7f070126

.field public static final error_AUTH_3:I = 0x7f070127

.field public static final error_AUTH_4:I = 0x7f070128

.field public static final error_IARG_1:I = 0x7f070129

.field public static final error_IARG_100:I = 0x7f07012a

.field public static final error_IARG_101:I = 0x7f07012b

.field public static final error_IARG_102:I = 0x7f07012c

.field public static final error_IARG_103:I = 0x7f07012d

.field public static final error_IARG_104:I = 0x7f07012e

.field public static final error_IARG_105:I = 0x7f07012f

.field public static final error_IARG_106:I = 0x7f070130

.field public static final error_IARG_107:I = 0x7f070131

.field public static final error_IARG_108:I = 0x7f070132

.field public static final error_IARG_109:I = 0x7f070133

.field public static final error_IARG_110:I = 0x7f070134

.field public static final error_IARG_2:I = 0x7f070135

.field public static final error_IARG_200:I = 0x7f070136

.field public static final error_IARG_201:I = 0x7f070137

.field public static final error_IARG_3:I = 0x7f070138

.field public static final error_IARG_300:I = 0x7f070139

.field public static final error_IARG_301:I = 0x7f07013a

.field public static final error_IARG_302:I = 0x7f07013b

.field public static final error_IARG_4:I = 0x7f07013c

.field public static final error_IARG_5:I = 0x7f07013d

.field public static final error_IARG_6:I = 0x7f07013e

.field public static final error_MARG_1:I = 0x7f07013f

.field public static final error_MARG_10:I = 0x7f070140

.field public static final error_MARG_100:I = 0x7f070141

.field public static final error_MARG_101:I = 0x7f070142

.field public static final error_MARG_102:I = 0x7f070143

.field public static final error_MARG_103:I = 0x7f070144

.field public static final error_MARG_104:I = 0x7f070145

.field public static final error_MARG_105:I = 0x7f070146

.field public static final error_MARG_106:I = 0x7f070147

.field public static final error_MARG_107:I = 0x7f070148

.field public static final error_MARG_108:I = 0x7f070149

.field public static final error_MARG_109:I = 0x7f07014a

.field public static final error_MARG_11:I = 0x7f07014b

.field public static final error_MARG_110:I = 0x7f07014c

.field public static final error_MARG_111:I = 0x7f07014d

.field public static final error_MARG_112:I = 0x7f07014e

.field public static final error_MARG_113:I = 0x7f07014f

.field public static final error_MARG_2:I = 0x7f070150

.field public static final error_MARG_200:I = 0x7f070151

.field public static final error_MARG_201:I = 0x7f070152

.field public static final error_MARG_202:I = 0x7f070153

.field public static final error_MARG_3:I = 0x7f070154

.field public static final error_MARG_4:I = 0x7f070155

.field public static final error_MARG_5:I = 0x7f070156

.field public static final error_MARG_6:I = 0x7f070157

.field public static final error_MARG_7:I = 0x7f070158

.field public static final error_MARG_8:I = 0x7f070159

.field public static final error_MARG_9:I = 0x7f07015a

.field public static final error_REPO_1:I = 0x7f07015b

.field public static final error_REPO_2:I = 0x7f07015c

.field public static final error_REPO_3:I = 0x7f07015d

.field public static final error_REPO_4_no_store:I = 0x7f07015e

.field public static final error_REPO_7:I = 0x7f07015f

.field public static final error_SYS_1:I = 0x7f070160

.field public static final error_SYS_2:I = 0x7f070161

.field public static final error_SYS_3:I = 0x7f070162

.field public static final error_SYS_4:I = 0x7f070163

.field public static final error_SYS_Billing:I = 0x7f070164

.field public static final error_WOP_10:I = 0x7f070165

.field public static final error_WOP_11:I = 0x7f070166

.field public static final error_WOP_6:I = 0x7f070167

.field public static final error_WOP_7:I = 0x7f070168

.field public static final error_WOP_8:I = 0x7f070169

.field public static final error_WOP_9:I = 0x7f07016a

.field public static final error_invalid_grant:I = 0x7f07016b

.field public static final error_occured:I = 0x7f07016c

.field public static final error_occured_paying:I = 0x7f07016d

.field public static final error_occured_retry_later:I = 0x7f07016e

.field public static final error_occured_uploading:I = 0x7f07016f

.field public static final essential_apps:I = 0x7f070170

.field public static final exclude_update:I = 0x7f070171

.field public static final excluded_updates:I = 0x7f070172

.field public static final facebook:I = 0x7f070173

.field public static final facebook_app_id:I = 0x7f0703f0

.field public static final facebook_email_permission_regected_message:I = 0x7f070174

.field public static final facebook_friends_list_using_timeline:I = 0x7f070175

.field public static final facebook_friends_list_using_timeline_empty:I = 0x7f070176

.field public static final facebook_grant_permission_button:I = 0x7f070177

.field public static final facebook_join_friends:I = 0x7f070178

.field public static final facebook_logout_and_login:I = 0x7f070179

.field public static final facebook_start_timeline:I = 0x7f07017a

.field public static final facebook_timeline_friends_invited:I = 0x7f07017b

.field public static final failed_auth_code:I = 0x7f07017c

.field public static final fake_app:I = 0x7f07017d

.field public static final family:I = 0x7f07017e

.field public static final feedback_include_logs:I = 0x7f07017f

.field public static final feedback_no_email:I = 0x7f070180

.field public static final feedback_not_valid:I = 0x7f070181

.field public static final feedback_send_button:I = 0x7f070182

.field public static final feedback_subject:I = 0x7f070183

.field public static final feedback_to_write2:I = 0x7f070184

.field public static final fields_cannot_empty:I = 0x7f070185

.field public static final finance:I = 0x7f070186

.field public static final finished_install:I = 0x7f070187

.field public static final five_numeric:I = 0x7f070188

.field public static final flag_added:I = 0x7f070189

.field public static final flag_fake:I = 0x7f07018a

.field public static final flag_freeze:I = 0x7f07018b

.field public static final flag_good:I = 0x7f07018c

.field public static final flag_license:I = 0x7f07018d

.field public static final flag_results:I = 0x7f07018e

.field public static final flag_this_app:I = 0x7f07018f

.field public static final flag_this_app_button_text:I = 0x7f070190

.field public static final flag_this_app_fake:I = 0x7f070191

.field public static final flag_this_app_freeze:I = 0x7f070192

.field public static final flag_this_app_good:I = 0x7f070193

.field public static final flag_this_app_need_licence:I = 0x7f070194

.field public static final flag_this_app_virus:I = 0x7f070195

.field public static final flag_virus:I = 0x7f070196

.field public static final follow:I = 0x7f070197

.field public static final follow_friends:I = 0x7f0703b2

.field public static final followed:I = 0x7f070198

.field public static final followed_stores:I = 0x7f070199

.field public static final font_family_black:I = 0x7f0703f1

.field public static final font_family_condensed:I = 0x7f0703f2

.field public static final font_family_light:I = 0x7f0703f3

.field public static final font_family_medium:I = 0x7f0703f4

.field public static final font_family_regular:I = 0x7f0703f5

.field public static final font_family_thin:I = 0x7f0703f6

.field public static final font_fontFamily_medium:I = 0x7f0703d2

.field public static final forgot_passwd:I = 0x7f07019a

.field public static final four_numeric:I = 0x7f07019b

.field public static final fragment_download_no_connection:I = 0x7f07019c

.field public static final fragment_download_no_connection_subtitle:I = 0x7f07019d

.field public static final fragment_social_timeline_aptoide_team:I = 0x7f07019e

.field public static final fragment_social_timeline_general_error:I = 0x7f07019f

.field public static final fragment_social_timeline_no_connection:I = 0x7f0701a0

.field public static final freed_space:I = 0x7f0701a1

.field public static final friends_installs:I = 0x7f0701a2

.field public static final friends_to_invite:I = 0x7f0701a3

.field public static final games:I = 0x7f0701a4

.field public static final general_downloads_dialog_no_download_rule_message:I = 0x7f0701a5

.field public static final general_downloads_dialog_only_mobile_message:I = 0x7f0701a6

.field public static final general_downloads_dialog_only_wifi_message:I = 0x7f0701a7

.field public static final get_latest:I = 0x7f0701a8

.field public static final google:I = 0x7f0703b3

.field public static final has_updates:I = 0x7f0701a9

.field public static final having_some_trouble:I = 0x7f0701aa

.field public static final health:I = 0x7f0701ab

.field public static final health_fitness:I = 0x7f0701ac

.field public static final helpful:I = 0x7f0701ad

.field public static final hidden_adult:I = 0x7f0701ae

.field public static final hide_post:I = 0x7f0701af

.field public static final hide_post_info:I = 0x7f0701b0

.field public static final highlighted:I = 0x7f0701b1

.field public static final highlighted_apps:I = 0x7f0701b2

.field public static final home_title:I = 0x7f0701b3

.field public static final ignoreUpdateText:I = 0x7f0701b4

.field public static final ignore_update:I = 0x7f0701b5

.field public static final ignored:I = 0x7f0701b6

.field public static final image_requirements_error_max_file_size:I = 0x7f0701b7

.field public static final image_requirements_error_max_height:I = 0x7f0701b8

.field public static final image_requirements_error_max_width:I = 0x7f0701b9

.field public static final image_requirements_error_min_height:I = 0x7f0701ba

.field public static final image_requirements_error_min_width:I = 0x7f0701bb

.field public static final image_requirements_error_popup_title:I = 0x7f0701bc

.field public static final image_requirements_popup_message:I = 0x7f0701bd

.field public static final include_system_apps_summary:I = 0x7f0701be

.field public static final include_system_apps_title:I = 0x7f0701bf

.field public static final insert_store_name:I = 0x7f0701c0

.field public static final install:I = 0x7f0701c1

.field public static final install_app_inner:I = 0x7f0701c2

.field public static final install_app_outter_pt1:I = 0x7f0701c3

.field public static final install_app_outter_pt2:I = 0x7f0701c4

.field public static final install_on_tv_mobile_error:I = 0x7f0701c5

.field public static final install_update:I = 0x7f0701c6

.field public static final installapp_alrt:I = 0x7f0701c7

.field public static final installed_tab:I = 0x7f0701c8

.field public static final installed_this:I = 0x7f0701c9

.field public static final installing:I = 0x7f0701ca

.field public static final installing_msg:I = 0x7f0701cb

.field public static final invalid_apk:I = 0x7f0701cc

.field public static final invite_friends:I = 0x7f0701cd

.field public static final ip_blacklisted:I = 0x7f0701ce

.field public static final item_payment_approving_text:I = 0x7f0703b4

.field public static final item_payment_button_register:I = 0x7f0703b5

.field public static final item_payment_button_use:I = 0x7f0703b6

.field public static final join_company:I = 0x7f0703b7

.field public static final just_reviews:I = 0x7f0701cf

.field public static final latest_applications:I = 0x7f0701d0

.field public static final latest_comments:I = 0x7f0701d1

.field public static final latest_reviews:I = 0x7f0701d2

.field public static final libraries_demo:I = 0x7f0701d3

.field public static final lifestyle:I = 0x7f0701d4

.field public static final light:I = 0x7f0701d5

.field public static final like:I = 0x7f0701d6

.field public static final likes:I = 0x7f0701d7

.field public static final link_device_webinstall:I = 0x7f0701d8

.field public static final local_top_apps:I = 0x7f0701d9

.field public static final logged_as:I = 0x7f0701da

.field public static final login:I = 0x7f0701db

.field public static final login_company:I = 0x7f0703b8

.field public static final login_successful:I = 0x7f0701dc

.field public static final make_review_title:I = 0x7f0701dd

.field public static final manually_reviewed:I = 0x7f0701de

.field public static final media_video:I = 0x7f0701df

.field public static final medical:I = 0x7f0701e0

.field public static final menu_card_timeline_clear:I = 0x7f0701e1

.field public static final menu_context_remove:I = 0x7f0701e2

.field public static final menu_context_reparse:I = 0x7f0701e3

.field public static final menu_search_item_go_to_store:I = 0x7f0701e4

.field public static final menu_select_all:I = 0x7f0701e5

.field public static final menu_select_none:I = 0x7f0701e6

.field public static final messenger_send_button_text:I = 0x7f07003d

.field public static final more_editors_choice:I = 0x7f0701e7

.field public static final more_friends:I = 0x7f0701e8

.field public static final more_info:I = 0x7f0701e9

.field public static final more_reviews:I = 0x7f0701ea

.field public static final more_versions:I = 0x7f0701eb

.field public static final multimedia:I = 0x7f0701ec

.field public static final music:I = 0x7f0701ed

.field public static final music_audio:I = 0x7f0701ee

.field public static final music_video:I = 0x7f0701ef

.field public static final my_account:I = 0x7f0701f0

.field public static final my_store_created_displayable_message:I = 0x7f0701f1

.field public static final na:I = 0x7f0701f2

.field public static final navigation_drawer_signup_login:I = 0x7f0701f3

.field public static final needs_license:I = 0x7f0701f4

.field public static final needs_permission_to_fs:I = 0x7f0701f5

.field public static final new_updates:I = 0x7f0701f6

.field public static final newer_version_available:I = 0x7f0701f7

.field public static final news_magazines:I = 0x7f0701f8

.field public static final news_weather:I = 0x7f0701f9

.field public static final next:I = 0x7f0701fa

.field public static final no:I = 0x7f0701fb

.field public static final noIgnoreUpdateText:I = 0x7f0701fc

.field public static final no_apps_found:I = 0x7f0701fd

.field public static final no_comment_be_the_first:I = 0x7f0701fe

.field public static final no_description:I = 0x7f0701ff

.field public static final no_email_and_pass_error_message:I = 0x7f070200

.field public static final no_email_error_message:I = 0x7f070201

.field public static final no_excluded_updates:I = 0x7f070202

.field public static final no_excluded_updates_msg:I = 0x7f070203

.field public static final no_excluded_updates_selected:I = 0x7f070204

.field public static final no_new_updates_available:I = 0x7f070205

.field public static final no_pass_error_message:I = 0x7f070206

.field public static final no_payments_available:I = 0x7f070207

.field public static final no_permissions_required:I = 0x7f070208

.field public static final no_photo_inserted:I = 0x7f070209

.field public static final no_rollbacks_msg:I = 0x7f07020a

.field public static final no_sch_downloads:I = 0x7f07020b

.field public static final no_search_results_message:I = 0x7f07020c

.field public static final no_updates_available_retoric:I = 0x7f07020d

.field public static final no_username_inserted:I = 0x7f07020e

.field public static final not_available:I = 0x7f07020f

.field public static final not_helpful:I = 0x7f070210

.field public static final not_now:I = 0x7f0703b9

.field public static final nothing_inserted_store:I = 0x7f070211

.field public static final nothing_inserted_user:I = 0x7f070212

.field public static final notification_social_timeline:I = 0x7f070213

.field public static final notification_timeline_activity:I = 0x7f070214

.field public static final notification_timeline_new_comments:I = 0x7f070215

.field public static final notification_timeline_new_likes:I = 0x7f070216

.field public static final notification_timeline_posts:I = 0x7f070217

.field public static final one_account_allowed:I = 0x7f070218

.field public static final one_new_update:I = 0x7f070219

.field public static final one_numeric:I = 0x7f07021a

.field public static final open:I = 0x7f07021b

.field public static final open_app_inner:I = 0x7f07021c

.field public static final open_apps_manager:I = 0x7f07021d

.field public static final or:I = 0x7f07021e

.field public static final order_popup_title3:I = 0x7f07021f

.field public static final other_stores:I = 0x7f070220

.field public static final other_versions_downloads_count_text:I = 0x7f070221

.field public static final other_versions_on_different_stores:I = 0x7f070222

.field public static final other_versions_partial_title_1:I = 0x7f070223

.field public static final other_versions_partial_title_2:I = 0x7f070224

.field public static final out_of_space_dialog_message:I = 0x7f0703ba

.field public static final out_of_space_dialog_title:I = 0x7f0703bb

.field public static final out_of_space_error:I = 0x7f0703bc

.field public static final paidapp_not_found:I = 0x7f070225

.field public static final password:I = 0x7f070226

.field public static final password_validation_text:I = 0x7f070227

.field public static final pause_all_downloads:I = 0x7f070228

.field public static final pause_download:I = 0x7f070229

.field public static final permissions:I = 0x7f07022a

.field public static final permissions_na:I = 0x7f07022b

.field public static final personalization:I = 0x7f07022c

.field public static final photography:I = 0x7f07022d

.field public static final play_it:I = 0x7f07022e

.field public static final please_wait:I = 0x7f07022f

.field public static final please_wait_upload:I = 0x7f070230

.field public static final privacy_copied:I = 0x7f070231

.field public static final privacy_policy:I = 0x7f070232

.field public static final private_followers_message:I = 0x7f0703bd

.field public static final private_following_message:I = 0x7f0703be

.field public static final private_key:I = 0x7f070233

.field public static final private_profile_create_store:I = 0x7f070234

.field public static final productivity:I = 0x7f070235

.field public static final public_key:I = 0x7f070236

.field public static final puzzle:I = 0x7f070237

.field public static final racing:I = 0x7f070238

.field public static final rate:I = 0x7f070239

.field public static final rate_app:I = 0x7f07023a

.field public static final rate_this_app:I = 0x7f07023b

.field public static final read_less:I = 0x7f07023c

.field public static final read_more:I = 0x7f07023d

.field public static final reason_failed:I = 0x7f07023e

.field public static final reason_manual:I = 0x7f07023f

.field public static final reason_scanned:I = 0x7f070240

.field public static final reason_signature:I = 0x7f070241

.field public static final reason_third_party:I = 0x7f070242

.field public static final reason_unknown:I = 0x7f070243

.field public static final recommended_for_you:I = 0x7f070244

.field public static final recommended_stores:I = 0x7f070245

.field public static final recover_password:I = 0x7f0703bf

.field public static final reference:I = 0x7f070246

.field public static final register:I = 0x7f070247

.field public static final remaining_time:I = 0x7f070248

.field public static final remote_in_nospace:I = 0x7f070249

.field public static final remote_install_empty_list:I = 0x7f07024a

.field public static final remote_install_fail:I = 0x7f07024b

.field public static final remote_install_help:I = 0x7f07024c

.field public static final remote_install_help_url:I = 0x7f07024d

.field public static final remote_install_menu_title:I = 0x7f07024e

.field public static final remote_install_nodevices:I = 0x7f07024f

.field public static final remote_install_notinstallated:I = 0x7f070250

.field public static final remote_install_nowifi:I = 0x7f070251

.field public static final remote_install_nowifi_tip:I = 0x7f070252

.field public static final remote_install_retry:I = 0x7f070253

.field public static final remote_install_success:I = 0x7f070254

.field public static final remote_install_tip:I = 0x7f070255

.field public static final remote_install_title:I = 0x7f070256

.field public static final remove_mature_pin_summary:I = 0x7f070257

.field public static final remove_mature_pin_title:I = 0x7f070258

.field public static final reply:I = 0x7f070259

.field public static final reply_to:I = 0x7f07025a

.field public static final request_adult_pin:I = 0x7f07025b

.field public static final restart_aptoide:I = 0x7f07025c

.field public static final restore:I = 0x7f07025d

.field public static final restore_updates:I = 0x7f07025e

.field public static final results_in_store:I = 0x7f07025f

.field public static final results_subscribed:I = 0x7f070260

.field public static final resume_download:I = 0x7f070261

.field public static final retrieving_update:I = 0x7f070262

.field public static final review_addictive:I = 0x7f070263

.field public static final review_bad:I = 0x7f070264

.field public static final review_by:I = 0x7f070265

.field public static final review_final_score:I = 0x7f070266

.field public static final review_final_verdict:I = 0x7f070267

.field public static final review_finish:I = 0x7f070268

.field public static final review_get_app:I = 0x7f070269

.field public static final review_good:I = 0x7f07026a

.field public static final review_optional:I = 0x7f07026b

.field public static final review_speed:I = 0x7f07026c

.field public static final review_stability:I = 0x7f07026d

.field public static final review_success:I = 0x7f07026e

.field public static final review_title:I = 0x7f07026f

.field public static final review_usability:I = 0x7f070270

.field public static final reviewed_by:I = 0x7f070271

.field public static final reviews:I = 0x7f070272

.field public static final reviews_expand_button:I = 0x7f0703c0

.field public static final role_playing:I = 0x7f070273

.field public static final rollback:I = 0x7f070274

.field public static final rollback_downgraded:I = 0x7f070275

.field public static final rollback_empty:I = 0x7f070276

.field public static final rollback_installed:I = 0x7f070277

.field public static final rollback_uninstalled:I = 0x7f070278

.field public static final rollback_updated:I = 0x7f070279

.field public static final rollback_updated_at:I = 0x7f07027a

.field public static final root:I = 0x7f07027b

.field public static final root_access_dialog:I = 0x7f07027c

.field public static final save_edit_store:I = 0x7f07027d

.field public static final scanned_manually_by_aptoide_team:I = 0x7f07027e

.field public static final scanned_verified_by_tester:I = 0x7f07027f

.field public static final scanned_with_av:I = 0x7f070280

.field public static final schDown_install:I = 0x7f070281

.field public static final schDown_installselected:I = 0x7f070282

.field public static final schDown_invertselection:I = 0x7f070283

.field public static final schDown_nodownloadselect:I = 0x7f070284

.field public static final schDown_removeselected:I = 0x7f070285

.field public static final schDwnBtn:I = 0x7f070286

.field public static final screenCode:I = 0x7f070287

.field public static final sd_error:I = 0x7f070288

.field public static final sd_error_jolla:I = 0x7f070289

.field public static final search:I = 0x7f07028a

.field public static final search_for_apps:I = 0x7f07028b

.field public static final search_menu_title:I = 0x7f07003e

.field public static final search_minimum_chars:I = 0x7f07028c

.field public static final search_other_stores:I = 0x7f07028d

.field public static final see_less:I = 0x7f07028e

.field public static final see_more:I = 0x7f07028f

.field public static final see_top_stores:I = 0x7f070290

.field public static final select_friends_to_invite:I = 0x7f070291

.field public static final server_error:I = 0x7f070292

.field public static final set_mature_pin_summary:I = 0x7f070293

.field public static final set_mature_pin_title:I = 0x7f070294

.field public static final set_sliders:I = 0x7f070295

.field public static final setpin:I = 0x7f070296

.field public static final setting_3g:I = 0x7f070297

.field public static final setting_4g:I = 0x7f070298

.field public static final setting_allUpdates_filtering:I = 0x7f070299

.field public static final setting_appfiltersum:I = 0x7f07029a

.field public static final setting_appfiltertitle:I = 0x7f07029b

.field public static final setting_auto_update_sum:I = 0x7f07029c

.field public static final setting_auto_update_title:I = 0x7f07029d

.field public static final setting_category_autoupdate:I = 0x7f0703c1

.field public static final setting_category_autoupdate_message:I = 0x7f0703c2

.field public static final setting_category_autoupdate_title:I = 0x7f0703c3

.field public static final setting_clear_memory:I = 0x7f07029e

.field public static final setting_dont_show_mature_content:I = 0x7f07029f

.field public static final setting_esgl_version:I = 0x7f0702a0

.field public static final setting_ethernet:I = 0x7f0702a1

.field public static final setting_general_download_rules:I = 0x7f0702a2

.field public static final setting_hwspecssum:I = 0x7f0702a3

.field public static final setting_hwspecstitle:I = 0x7f0702a4

.field public static final setting_icon_download_rules:I = 0x7f0702a5

.field public static final setting_mature_filtering:I = 0x7f0702a6

.field public static final setting_mobile:I = 0x7f0702a7

.field public static final setting_schdwninstallsum:I = 0x7f0702a8

.field public static final setting_schdwninstalltitle:I = 0x7f0702a9

.field public static final setting_schdwntitle:I = 0x7f0702aa

.field public static final setting_screen_size:I = 0x7f0702ab

.field public static final setting_sdk_version:I = 0x7f0702ac

.field public static final setting_sum_general_download_rules:I = 0x7f0702ad

.field public static final setting_sum_icon_download_rules:I = 0x7f0702ae

.field public static final setting_updates:I = 0x7f0702af

.field public static final setting_updates_notification_sum:I = 0x7f0702b0

.field public static final setting_updates_notification_title:I = 0x7f0702b1

.field public static final setting_wifi:I = 0x7f0702b2

.field public static final settings:I = 0x7f0702b3

.field public static final settings_dwn_network_rules:I = 0x7f0702b4

.field public static final settings_maxFileCache_sum:I = 0x7f0702b5

.field public static final settings_maxFileCache_title:I = 0x7f0702b6

.field public static final settings_title_bar:I = 0x7f0702b7

.field public static final share:I = 0x7f0702b8

.field public static final share_on_timeline:I = 0x7f0702b9

.field public static final share_options_share_external:I = 0x7f0703c4

.field public static final share_options_share_in_timeline:I = 0x7f0703c5

.field public static final shopping:I = 0x7f0702ba

.field public static final show_all_updates:I = 0x7f0702bb

.field public static final sign_out:I = 0x7f0702bc

.field public static final simple_error_occured:I = 0x7f0702bd

.field public static final simulation:I = 0x7f0702be

.field public static final size:I = 0x7f0702bf

.field public static final skip:I = 0x7f0702c0

.field public static final social:I = 0x7f0702c1

.field public static final social_facebook_screen_title:I = 0x7f0702c2

.field public static final social_timeline:I = 0x7f0702c3

.field public static final social_timeline_followers_fragment_title:I = 0x7f0702c4

.field public static final social_timeline_following_fragment_title:I = 0x7f0702c5

.field public static final social_timeline_notifications:I = 0x7f0702c6

.field public static final social_timeline_notifications_sum2:I = 0x7f0702c7

.field public static final social_timeline_recommends:I = 0x7f0702c8

.field public static final social_timeline_share_bar_followers:I = 0x7f0702c9

.field public static final social_timeline_share_bar_following:I = 0x7f0702ca

.field public static final social_timeline_share_bar_login:I = 0x7f0702cb

.field public static final social_timeline_share_dialog_installed_and_recommended:I = 0x7f0702cc

.field public static final social_timeline_share_dialog_opt_in_text_1:I = 0x7f0702cd

.field public static final social_timeline_share_dialog_opt_in_text_2:I = 0x7f0702ce

.field public static final social_timeline_share_dialog_opt_out_text_1:I = 0x7f0702cf

.field public static final social_timeline_share_dialog_opt_out_text_2:I = 0x7f0702d0

.field public static final social_timeline_share_dialog_title:I = 0x7f0702d1

.field public static final social_timeline_share_soon:I = 0x7f0702d2

.field public static final social_timeline_share_title:I = 0x7f0702d3

.field public static final social_timeline_share_title_checkbox:I = 0x7f0702d4

.field public static final social_timeline_share_title_checkbox_fb:I = 0x7f0702d5

.field public static final social_timeline_shared_by:I = 0x7f0703c6

.field public static final social_timeline_users_private:I = 0x7f0703c7

.field public static final social_timeline_who_liked:I = 0x7f0703c8

.field public static final social_timeline_you_will_share:I = 0x7f0702d6

.field public static final social_twitter_screen_title:I = 0x7f0702d7

.field public static final software_libraries:I = 0x7f0702d8

.field public static final sponsored:I = 0x7f0702d9

.field public static final sports:I = 0x7f0702da

.field public static final sports_games:I = 0x7f0702db

.field public static final spot_share:I = 0x7f0703d1

.field public static final srch_hint:I = 0x7f0702dc

.field public static final stand_by:I = 0x7f0703c9

.field public static final starting_download:I = 0x7f0702dd

.field public static final status_bar_notification_info_overflow:I = 0x7f07003f

.field public static final storage_access_permission_request_message:I = 0x7f0702de

.field public static final storage_dialog_title:I = 0x7f0702df

.field public static final store:I = 0x7f0702e0

.field public static final store_added_long_time_suggestion_message:I = 0x7f0703ca

.field public static final store_added_short_time_suggestion_message:I = 0x7f0703cb

.field public static final store_already_added:I = 0x7f0702e1

.field public static final store_followed:I = 0x7f0702e2

.field public static final store_name:I = 0x7f0702e3

.field public static final store_password:I = 0x7f0702e4

.field public static final store_picture_message:I = 0x7f070040

.field public static final store_picture_title:I = 0x7f070041

.field public static final store_privacy:I = 0x7f0702e5

.field public static final store_suspended_message:I = 0x7f0703d0

.field public static final store_upload_photo_failed:I = 0x7f0702e6

.field public static final store_username:I = 0x7f0702e7

.field public static final stores:I = 0x7f0702e8

.field public static final strategy:I = 0x7f0702e9

.field public static final subscribe_apps_official_store:I = 0x7f0702ea

.field public static final subscribe_pvt_store:I = 0x7f0702eb

.field public static final subscribe_store:I = 0x7f0702ec

.field public static final subscribers:I = 0x7f0702ed

.field public static final successful:I = 0x7f0702ee

.field public static final suggested_app:I = 0x7f0702ef

.field public static final summer_apps:I = 0x7f0702f0

.field public static final tap_a_star:I = 0x7f0702f1

.field public static final thank_you_for_your_opinion:I = 0x7f0702f2

.field public static final theme:I = 0x7f0702f3

.field public static final themes:I = 0x7f0702f4

.field public static final three_numeric:I = 0x7f0702f5

.field public static final timeline:I = 0x7f0703cc

.field public static final timeline_ad:I = 0x7f0702f6

.field public static final timeline_email_how_to_join:I = 0x7f0702f7

.field public static final timeline_email_invitation:I = 0x7f0702f8

.field public static final timeline_email_step1:I = 0x7f0702f9

.field public static final timeline_email_step2:I = 0x7f0702fa

.field public static final timeline_empty_no_friends_send_email:I = 0x7f0702fb

.field public static final timeline_empty_send_email_to_friends:I = 0x7f0702fc

.field public static final timeline_followed:I = 0x7f0702fd

.field public static final timeline_followers:I = 0x7f0702fe

.field public static final timeline_is_empty_no_friends:I = 0x7f0702ff

.field public static final timeline_is_empty_send_email:I = 0x7f070300

.field public static final timeline_is_empty_start_invite:I = 0x7f070301

.field public static final timeline_like:I = 0x7f070302

.field public static final timeline_login_message:I = 0x7f070303

.field public static final timeline_not_compatible:I = 0x7f070304

.field public static final timeline_stats_followers:I = 0x7f070305

.field public static final timeout:I = 0x7f070306

.field public static final title:I = 0x7f070307

.field public static final title_caps:I = 0x7f070308

.field public static final tools:I = 0x7f070309

.field public static final top_apps:I = 0x7f07030a

.field public static final top_apps_in_store:I = 0x7f07030b

.field public static final top_comments:I = 0x7f07030c

.field public static final top_games:I = 0x7f07030d

.field public static final top_stores_fragment_title:I = 0x7f07030e

.field public static final transport:I = 0x7f07030f

.field public static final transportation:I = 0x7f070310

.field public static final travel:I = 0x7f070311

.field public static final travel_local:I = 0x7f070312

.field public static final trending:I = 0x7f070313

.field public static final trivia:I = 0x7f070314

.field public static final trusted_app:I = 0x7f070315

.field public static final twitter:I = 0x7f070316

.field public static final two_numeric:I = 0x7f070317

.field public static final unfollow:I = 0x7f070318

.field public static final unfollow_yes_no:I = 0x7f070319

.field public static final unfollowing_store_message:I = 0x7f07031a

.field public static final unhide_post:I = 0x7f07031b

.field public static final uninstall:I = 0x7f07031c

.field public static final unknown:I = 0x7f07031d

.field public static final unknown_error:I = 0x7f07031e

.field public static final unlike:I = 0x7f07031f

.field public static final unsubscribe:I = 0x7f070320

.field public static final unsubscribe_yes_no:I = 0x7f070321

.field public static final unsubscribed_:I = 0x7f070322

.field public static final unsubscribing_store_message:I = 0x7f070323

.field public static final update:I = 0x7f070324

.field public static final update_all:I = 0x7f070325

.field public static final update_self_msg:I = 0x7f070326

.field public static final update_self_title:I = 0x7f070327

.field public static final updates:I = 0x7f070328

.field public static final updatesTabAdapterUninstall:I = 0x7f070329

.field public static final updates_filter_setting_title:I = 0x7f07032a

.field public static final updates_filter_summary:I = 0x7f07032b

.field public static final updates_tab:I = 0x7f07032c

.field public static final updates_tab_adapter_uninstalled:I = 0x7f07032d

.field public static final updating_msg:I = 0x7f07032e

.field public static final upload_app:I = 0x7f07032f

.field public static final upload_app_backup_apps_installed:I = 0x7f070330

.field public static final upload_app_backup_apps_not_installed:I = 0x7f070331

.field public static final upload_dialog_gallery:I = 0x7f070332

.field public static final upload_dialog_photo:I = 0x7f070333

.field public static final upload_dialog_title:I = 0x7f070334

.field public static final user_cancelled:I = 0x7f070335

.field public static final user_created:I = 0x7f070336

.field public static final user_interests_adult_gambling:I = 0x7f070337

.field public static final user_interests_header:I = 0x7f070338

.field public static final user_interests_photography_video:I = 0x7f070339

.field public static final user_interests_title:I = 0x7f07033a

.field public static final user_upload_photo_failed:I = 0x7f07033b

.field public static final username:I = 0x7f07033c

.field public static final v7_preference_off:I = 0x7f0703f7

.field public static final v7_preference_on:I = 0x7f0703f8

.field public static final version:I = 0x7f07033d

.field public static final version_number:I = 0x7f07033e

.field public static final version_placeholder:I = 0x7f07033f

.field public static final virus:I = 0x7f070340

.field public static final vote_down:I = 0x7f070341

.field public static final vote_submitted:I = 0x7f070342

.field public static final vote_up:I = 0x7f070343

.field public static final votes:I = 0x7f070344

.field public static final warning:I = 0x7f070345

.field public static final weather:I = 0x7f070346

.field public static final website:I = 0x7f070347

.field public static final website_copied:I = 0x7f070348

.field public static final whats_new:I = 0x7f070349

.field public static final wizard_be_a_part_of_aptoide:I = 0x7f07034a

.field public static final wizard_description_01:I = 0x7f07034b

.field public static final wizard_description_02:I = 0x7f07034c

.field public static final wizard_description_03_1:I = 0x7f07034d

.field public static final wizard_description_03_2:I = 0x7f07034e

.field public static final wizard_description_part_one_viewpager_one:I = 0x7f07034f

.field public static final wizard_description_part_one_viewpager_three:I = 0x7f070350

.field public static final wizard_description_part_three_viewpager_three:I = 0x7f070351

.field public static final wizard_description_part_two_viewpager_one:I = 0x7f070352

.field public static final wizard_description_part_two_viewpager_three:I = 0x7f070353

.field public static final wizard_new_and_improved:I = 0x7f070354

.field public static final wizard_new_and_improved_bullet_1:I = 0x7f070355

.field public static final wizard_new_and_improved_bullet_2:I = 0x7f070356

.field public static final wizard_new_and_improved_bullet_3:I = 0x7f070357

.field public static final wizard_new_and_improved_bullet_4:I = 0x7f070358

.field public static final wizard_new_homepage:I = 0x7f070359

.field public static final wizard_new_top:I = 0x7f07035a

.field public static final wizard_next:I = 0x7f07035b

.field public static final wizard_or_login:I = 0x7f07035c

.field public static final wizard_prev:I = 0x7f07035d

.field public static final wizard_register_now_button:I = 0x7f07035e

.field public static final wizard_reviews:I = 0x7f07035f

.field public static final wizard_reviews_title:I = 0x7f070360

.field public static final wizard_skip:I = 0x7f070361

.field public static final wizard_sub_title_viewpager_one:I = 0x7f070362

.field public static final wizard_sub_title_viewpager_two:I = 0x7f070363

.field public static final wizard_subtitle_03_1:I = 0x7f070364

.field public static final wizard_subtitle_03_2:I = 0x7f070365

.field public static final wizard_title_01:I = 0x7f070366

.field public static final wizard_title_02:I = 0x7f070367

.field public static final wizard_title_03:I = 0x7f070368

.field public static final wizard_title_viewpager_three:I = 0x7f070369

.field public static final wizard_title_viewpager_two:I = 0x7f07036a

.field public static final wizard_welcome_message:I = 0x7f07036b

.field public static final word:I = 0x7f07036c

.field public static final word_this:I = 0x7f07036d

.field public static final working_well:I = 0x7f07036e

.field public static final write_a_comment:I = 0x7f07036f

.field public static final write_one_con:I = 0x7f070370

.field public static final write_one_pro:I = 0x7f070371

.field public static final write_your_comment:I = 0x7f070372

.field public static final write_your_final_verdict:I = 0x7f070373

.field public static final ws_eror_WOP_:I = 0x7f070374

.field public static final ws_eror_WOP_1:I = 0x7f070375

.field public static final ws_error_API_1:I = 0x7f070376

.field public static final ws_error_AUTH_1:I = 0x7f070377

.field public static final ws_error_AUTH_100:I = 0x7f070378

.field public static final ws_error_AUTH_101:I = 0x7f070379

.field public static final ws_error_AUTH_102:I = 0x7f07037a

.field public static final ws_error_AUTH_103:I = 0x7f07037b

.field public static final ws_error_AUTH_2:I = 0x7f07037c

.field public static final ws_error_AUTH_7:I = 0x7f07037d

.field public static final ws_error_IARG_106:I = 0x7f07037e

.field public static final ws_error_IARG_111:I = 0x7f07037f

.field public static final ws_error_IARG_200:I = 0x7f070380

.field public static final ws_error_IARG_5:I = 0x7f070381

.field public static final ws_error_MARG_109:I = 0x7f070382

.field public static final ws_error_MARG_110:I = 0x7f070383

.field public static final ws_error_MARG_111:I = 0x7f070384

.field public static final ws_error_MARG_112:I = 0x7f070385

.field public static final ws_error_MARG_12:I = 0x7f070386

.field public static final ws_error_MARG_200:I = 0x7f070387

.field public static final ws_error_MARG_203:I = 0x7f070388

.field public static final ws_error_MARG_204:I = 0x7f070389

.field public static final ws_error_QUOTA_2:I = 0x7f07038a

.field public static final ws_error_REPO_4:I = 0x7f07038b

.field public static final ws_error_REPO_5:I = 0x7f07038c

.field public static final ws_error_REPO_6:I = 0x7f07038d

.field public static final ws_error_REPO_7:I = 0x7f07038e

.field public static final ws_error_WOP_10:I = 0x7f07038f

.field public static final ws_error_WOP_11:I = 0x7f070390

.field public static final ws_error_WOP_2:I = 0x7f070391

.field public static final ws_error_WOP_3:I = 0x7f070392

.field public static final ws_error_WOP_9:I = 0x7f070393

.field public static final ws_error_invalid_grant:I = 0x7f070394

.field public static final yes:I = 0x7f070395

.field public static final you_need_to_be_logged_in:I = 0x7f070396

.field public static final zero:I = 0x7f070397


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
