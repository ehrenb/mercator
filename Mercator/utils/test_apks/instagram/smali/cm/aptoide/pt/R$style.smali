.class public final Lcm/aptoide/pt/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AlertDialogAptoide:I = 0x7f0900a5

.field public static final AlertDialog_AppCompat:I = 0x7f0900a3

.field public static final AlertDialog_AppCompat_Light:I = 0x7f0900a4

.field public static final AlertTextAppearanceAptoide:I = 0x7f0900a6

.field public static final Animation_AppCompat_Dialog:I = 0x7f0900a7

.field public static final Animation_AppCompat_DropDownUp:I = 0x7f0900a8

.field public static final Animation_Design_BottomSheetDialog:I = 0x7f0900a9

.field public static final AppBaseTheme:I = 0x7f0900aa

.field public static final AppBaseThemeDark:I = 0x7f0900ac

.field public static final AppBaseTheme_Payment:I = 0x7f0900ab

.field public static final AppViewHeaderTextAppView:I = 0x7f0900ad

.field public static final AppViewWidget:I = 0x7f090030

.field public static final AppViewWidgetFirst:I = 0x7f0900ae

.field public static final AptoideAppView:I = 0x7f090002

.field public static final AptoideThemeDefault:I = 0x7f0900b0

.field public static final AptoideThemeDefaultAmber:I = 0x7f0900b1

.field public static final AptoideThemeDefaultAmberDark:I = 0x7f0900b2

.field public static final AptoideThemeDefaultBlack:I = 0x7f0900b3

.field public static final AptoideThemeDefaultBlackDark:I = 0x7f0900b4

.field public static final AptoideThemeDefaultBlue:I = 0x7f0900b5

.field public static final AptoideThemeDefaultBlueDark:I = 0x7f0900b6

.field public static final AptoideThemeDefaultBluegrey:I = 0x7f0900b7

.field public static final AptoideThemeDefaultBluegreyDark:I = 0x7f0900b8

.field public static final AptoideThemeDefaultBrown:I = 0x7f0900b9

.field public static final AptoideThemeDefaultBrownDark:I = 0x7f0900ba

.field public static final AptoideThemeDefaultDark:I = 0x7f0900bb

.field public static final AptoideThemeDefaultDeepPurple:I = 0x7f0900bc

.field public static final AptoideThemeDefaultDeepPurpleDark:I = 0x7f0900bd

.field public static final AptoideThemeDefaultDimgray:I = 0x7f0900be

.field public static final AptoideThemeDefaultDimgrayDark:I = 0x7f0900bf

.field public static final AptoideThemeDefaultGold:I = 0x7f0900c0

.field public static final AptoideThemeDefaultGoldDark:I = 0x7f0900c1

.field public static final AptoideThemeDefaultGreen:I = 0x7f0900c2

.field public static final AptoideThemeDefaultGreenDark:I = 0x7f0900c3

.field public static final AptoideThemeDefaultGreenapple:I = 0x7f0900c4

.field public static final AptoideThemeDefaultGreenappleDark:I = 0x7f0900c5

.field public static final AptoideThemeDefaultGrey:I = 0x7f0900c6

.field public static final AptoideThemeDefaultGreyDark:I = 0x7f0900c7

.field public static final AptoideThemeDefaultHappyblue:I = 0x7f0900c8

.field public static final AptoideThemeDefaultHappyblueDark:I = 0x7f0900c9

.field public static final AptoideThemeDefaultIndigo:I = 0x7f0900ca

.field public static final AptoideThemeDefaultIndigoDark:I = 0x7f0900cb

.field public static final AptoideThemeDefaultLightblue:I = 0x7f0900cc

.field public static final AptoideThemeDefaultLightblueDark:I = 0x7f0900cd

.field public static final AptoideThemeDefaultLightgreen:I = 0x7f0900ce

.field public static final AptoideThemeDefaultLightgreenDark:I = 0x7f0900cf

.field public static final AptoideThemeDefaultLightsky:I = 0x7f0900d0

.field public static final AptoideThemeDefaultLightskyDark:I = 0x7f0900d1

.field public static final AptoideThemeDefaultLime:I = 0x7f0900d2

.field public static final AptoideThemeDefaultLimeDark:I = 0x7f0900d3

.field public static final AptoideThemeDefaultMagenta:I = 0x7f0900d4

.field public static final AptoideThemeDefaultMagentaDark:I = 0x7f0900d5

.field public static final AptoideThemeDefaultMaroon:I = 0x7f0900d6

.field public static final AptoideThemeDefaultMaroonDark:I = 0x7f0900d7

.field public static final AptoideThemeDefaultMidnight:I = 0x7f0900d8

.field public static final AptoideThemeDefaultMidnightDark:I = 0x7f0900d9

.field public static final AptoideThemeDefaultOrange:I = 0x7f0900da

.field public static final AptoideThemeDefaultOrangeDark:I = 0x7f0900db

.field public static final AptoideThemeDefaultPink:I = 0x7f0900dc

.field public static final AptoideThemeDefaultPinkDark:I = 0x7f0900dd

.field public static final AptoideThemeDefaultRed:I = 0x7f0900de

.field public static final AptoideThemeDefaultRedDark:I = 0x7f0900df

.field public static final AptoideThemeDefaultSeagreen:I = 0x7f0900e0

.field public static final AptoideThemeDefaultSeagreenDark:I = 0x7f0900e1

.field public static final AptoideThemeDefaultSilver:I = 0x7f0900e2

.field public static final AptoideThemeDefaultSilverDark:I = 0x7f0900e3

.field public static final AptoideThemeDefaultSlategray:I = 0x7f0900e4

.field public static final AptoideThemeDefaultSlategrayDark:I = 0x7f0900e5

.field public static final AptoideThemeDefaultSpringgreen:I = 0x7f0900e6

.field public static final AptoideThemeDefaultSpringgreenDark:I = 0x7f0900e7

.field public static final AptoideThemeDefaultTeal:I = 0x7f0900e8

.field public static final AptoideThemeDefaultTealDark:I = 0x7f0900e9

.field public static final AptoideThemeDefaultYellow:I = 0x7f0900ea

.field public static final AptoideThemeDefaultYellowDark:I = 0x7f0900eb

.field public static final Aptoide_PreferenceThemeOverlay:I = 0x7f0900af

.field public static final AutoCompleteTextView:I = 0x7f0900ec

.field public static final Base_AlertDialog_AppCompat:I = 0x7f0900ed

.field public static final Base_AlertDialog_AppCompat_Light:I = 0x7f0900ee

.field public static final Base_Animation_AppCompat_Dialog:I = 0x7f0900ef

.field public static final Base_Animation_AppCompat_DropDownUp:I = 0x7f0900f0

.field public static final Base_CardView:I = 0x7f0900f1

.field public static final Base_DialogWindowTitleBackground_AppCompat:I = 0x7f0900f3

.field public static final Base_DialogWindowTitle_AppCompat:I = 0x7f0900f2

.field public static final Base_TextAppearance_AppCompat:I = 0x7f090042

.field public static final Base_TextAppearance_AppCompat_Body1:I = 0x7f090043

.field public static final Base_TextAppearance_AppCompat_Body2:I = 0x7f090044

.field public static final Base_TextAppearance_AppCompat_Button:I = 0x7f090026

.field public static final Base_TextAppearance_AppCompat_Caption:I = 0x7f090045

.field public static final Base_TextAppearance_AppCompat_Display1:I = 0x7f090046

.field public static final Base_TextAppearance_AppCompat_Display2:I = 0x7f090047

.field public static final Base_TextAppearance_AppCompat_Display3:I = 0x7f090048

.field public static final Base_TextAppearance_AppCompat_Display4:I = 0x7f090049

.field public static final Base_TextAppearance_AppCompat_Headline:I = 0x7f09004a

.field public static final Base_TextAppearance_AppCompat_Inverse:I = 0x7f09000d

.field public static final Base_TextAppearance_AppCompat_Large:I = 0x7f09004b

.field public static final Base_TextAppearance_AppCompat_Large_Inverse:I = 0x7f09000e

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f09004c

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f09004d

.field public static final Base_TextAppearance_AppCompat_Medium:I = 0x7f09004e

.field public static final Base_TextAppearance_AppCompat_Medium_Inverse:I = 0x7f09000f

.field public static final Base_TextAppearance_AppCompat_Menu:I = 0x7f09004f

.field public static final Base_TextAppearance_AppCompat_SearchResult:I = 0x7f0900f4

.field public static final Base_TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f090050

.field public static final Base_TextAppearance_AppCompat_SearchResult_Title:I = 0x7f090051

.field public static final Base_TextAppearance_AppCompat_Small:I = 0x7f090052

.field public static final Base_TextAppearance_AppCompat_Small_Inverse:I = 0x7f090010

.field public static final Base_TextAppearance_AppCompat_Subhead:I = 0x7f090053

.field public static final Base_TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f090011

.field public static final Base_TextAppearance_AppCompat_Title:I = 0x7f090054

.field public static final Base_TextAppearance_AppCompat_Title_Inverse:I = 0x7f090012

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f090098

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f090055

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f090056

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f090057

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f090058

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f090059

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f09005a

.field public static final Base_TextAppearance_AppCompat_Widget_Button:I = 0x7f09005b

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f090099

.field public static final Base_TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f0900f5

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f09005c

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f09005d

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f09005e

.field public static final Base_TextAppearance_AppCompat_Widget_Switch:I = 0x7f09005f

.field public static final Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f090060

.field public static final Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f0900f6

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f090061

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f090062

.field public static final Base_ThemeOverlay_AppCompat:I = 0x7f0900ff

.field public static final Base_ThemeOverlay_AppCompat_ActionBar:I = 0x7f090100

.field public static final Base_ThemeOverlay_AppCompat_Dark:I = 0x7f090101

.field public static final Base_ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f090102

.field public static final Base_ThemeOverlay_AppCompat_Dialog:I = 0x7f090015

.field public static final Base_ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f090103

.field public static final Base_ThemeOverlay_AppCompat_Light:I = 0x7f090104

.field public static final Base_Theme_AppCompat:I = 0x7f090063

.field public static final Base_Theme_AppCompat_CompactMenu:I = 0x7f0900f7

.field public static final Base_Theme_AppCompat_Dialog:I = 0x7f090013

.field public static final Base_Theme_AppCompat_DialogWhenLarge:I = 0x7f090003

.field public static final Base_Theme_AppCompat_Dialog_Alert:I = 0x7f0900f8

.field public static final Base_Theme_AppCompat_Dialog_FixedSize:I = 0x7f0900f9

.field public static final Base_Theme_AppCompat_Dialog_MinWidth:I = 0x7f0900fa

.field public static final Base_Theme_AppCompat_Light:I = 0x7f090064

.field public static final Base_Theme_AppCompat_Light_DarkActionBar:I = 0x7f0900fb

.field public static final Base_Theme_AppCompat_Light_Dialog:I = 0x7f090014

.field public static final Base_Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f090004

.field public static final Base_Theme_AppCompat_Light_Dialog_Alert:I = 0x7f0900fc

.field public static final Base_Theme_AppCompat_Light_Dialog_FixedSize:I = 0x7f0900fd

.field public static final Base_Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f0900fe

.field public static final Base_V11_ThemeOverlay_AppCompat_Dialog:I = 0x7f090018

.field public static final Base_V11_Theme_AppCompat_Dialog:I = 0x7f090016

.field public static final Base_V11_Theme_AppCompat_Light_Dialog:I = 0x7f090017

.field public static final Base_V12_Widget_AppCompat_AutoCompleteTextView:I = 0x7f090022

.field public static final Base_V12_Widget_AppCompat_EditText:I = 0x7f090023

.field public static final Base_V21_ThemeOverlay_AppCompat_Dialog:I = 0x7f090069

.field public static final Base_V21_Theme_AppCompat:I = 0x7f090065

.field public static final Base_V21_Theme_AppCompat_Dialog:I = 0x7f090066

.field public static final Base_V21_Theme_AppCompat_Light:I = 0x7f090067

.field public static final Base_V21_Theme_AppCompat_Light_Dialog:I = 0x7f090068

.field public static final Base_V22_Theme_AppCompat:I = 0x7f090096

.field public static final Base_V22_Theme_AppCompat_Light:I = 0x7f090097

.field public static final Base_V23_Theme_AppCompat:I = 0x7f09009a

.field public static final Base_V23_Theme_AppCompat_Light:I = 0x7f09009b

.field public static final Base_V7_ThemeOverlay_AppCompat_Dialog:I = 0x7f090109

.field public static final Base_V7_Theme_AppCompat:I = 0x7f090105

.field public static final Base_V7_Theme_AppCompat_Dialog:I = 0x7f090106

.field public static final Base_V7_Theme_AppCompat_Light:I = 0x7f090107

.field public static final Base_V7_Theme_AppCompat_Light_Dialog:I = 0x7f090108

.field public static final Base_V7_Widget_AppCompat_AutoCompleteTextView:I = 0x7f09010a

.field public static final Base_V7_Widget_AppCompat_EditText:I = 0x7f09010b

.field public static final Base_Widget_AppCompat_ActionBar:I = 0x7f09010c

.field public static final Base_Widget_AppCompat_ActionBar_Solid:I = 0x7f09010d

.field public static final Base_Widget_AppCompat_ActionBar_TabBar:I = 0x7f09010e

.field public static final Base_Widget_AppCompat_ActionBar_TabText:I = 0x7f09006a

.field public static final Base_Widget_AppCompat_ActionBar_TabView:I = 0x7f09006b

.field public static final Base_Widget_AppCompat_ActionButton:I = 0x7f09006c

.field public static final Base_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f09006d

.field public static final Base_Widget_AppCompat_ActionButton_Overflow:I = 0x7f09006e

.field public static final Base_Widget_AppCompat_ActionMode:I = 0x7f09010f

.field public static final Base_Widget_AppCompat_ActivityChooserView:I = 0x7f090110

.field public static final Base_Widget_AppCompat_AutoCompleteTextView:I = 0x7f090024

.field public static final Base_Widget_AppCompat_Button:I = 0x7f09006f

.field public static final Base_Widget_AppCompat_ButtonBar:I = 0x7f090073

.field public static final Base_Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f090112

.field public static final Base_Widget_AppCompat_Button_Borderless:I = 0x7f090070

.field public static final Base_Widget_AppCompat_Button_Borderless_Colored:I = 0x7f090071

.field public static final Base_Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f090111

.field public static final Base_Widget_AppCompat_Button_Colored:I = 0x7f09009c

.field public static final Base_Widget_AppCompat_Button_Small:I = 0x7f090072

.field public static final Base_Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f090074

.field public static final Base_Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f090075

.field public static final Base_Widget_AppCompat_CompoundButton_Switch:I = 0x7f090113

.field public static final Base_Widget_AppCompat_DrawerArrowToggle:I = 0x7f090000

.field public static final Base_Widget_AppCompat_DrawerArrowToggle_Common:I = 0x7f090114

.field public static final Base_Widget_AppCompat_DropDownItem_Spinner:I = 0x7f090076

.field public static final Base_Widget_AppCompat_EditText:I = 0x7f090025

.field public static final Base_Widget_AppCompat_ImageButton:I = 0x7f090077

.field public static final Base_Widget_AppCompat_Light_ActionBar:I = 0x7f090115

.field public static final Base_Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f090116

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f090117

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f090078

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f090079

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f09007a

.field public static final Base_Widget_AppCompat_Light_PopupMenu:I = 0x7f09007b

.field public static final Base_Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f09007c

.field public static final Base_Widget_AppCompat_ListMenuView:I = 0x7f090118

.field public static final Base_Widget_AppCompat_ListPopupWindow:I = 0x7f09007d

.field public static final Base_Widget_AppCompat_ListView:I = 0x7f09007e

.field public static final Base_Widget_AppCompat_ListView_DropDown:I = 0x7f09007f

.field public static final Base_Widget_AppCompat_ListView_Menu:I = 0x7f090080

.field public static final Base_Widget_AppCompat_PopupMenu:I = 0x7f090081

.field public static final Base_Widget_AppCompat_PopupMenu_Overflow:I = 0x7f090082

.field public static final Base_Widget_AppCompat_PopupWindow:I = 0x7f090119

.field public static final Base_Widget_AppCompat_ProgressBar:I = 0x7f090019

.field public static final Base_Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f09001a

.field public static final Base_Widget_AppCompat_RatingBar:I = 0x7f090083

.field public static final Base_Widget_AppCompat_RatingBar_Indicator:I = 0x7f09009d

.field public static final Base_Widget_AppCompat_RatingBar_Small:I = 0x7f09009e

.field public static final Base_Widget_AppCompat_SearchView:I = 0x7f09011a

.field public static final Base_Widget_AppCompat_SearchView_ActionBar:I = 0x7f09011b

.field public static final Base_Widget_AppCompat_SeekBar:I = 0x7f090084

.field public static final Base_Widget_AppCompat_SeekBar_Discrete:I = 0x7f09011c

.field public static final Base_Widget_AppCompat_Spinner:I = 0x7f090085

.field public static final Base_Widget_AppCompat_Spinner_Underlined:I = 0x7f090005

.field public static final Base_Widget_AppCompat_TextView_SpinnerItem:I = 0x7f090086

.field public static final Base_Widget_AppCompat_Toolbar:I = 0x7f09011d

.field public static final Base_Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f090087

.field public static final Base_Widget_Design_AppBarLayout:I = 0x7f09011e

.field public static final Base_Widget_Design_TabLayout:I = 0x7f09011f

.field public static final CardView:I = 0x7f09009f

.field public static final CardView_Dark:I = 0x7f090120

.field public static final CardView_Light:I = 0x7f090121

.field public static final DrawerArrowStyle:I = 0x7f090122

.field public static final FinskyLightDialogTheme:I = 0x7f090123

.field public static final FragmentRateAndReviewsCount:I = 0x7f090124

.field public static final FragmentRateAndReviewsLabel:I = 0x7f0900a0

.field public static final FragmentRateAndReviewsProgressBar:I = 0x7f090031

.field public static final MenuDrawer:I = 0x7f090125

.field public static final MenuDrawer_Widget:I = 0x7f090126

.field public static final MenuDrawer_Widget_Title:I = 0x7f090127

.field public static final MessengerButton:I = 0x7f090128

.field public static final MessengerButtonText:I = 0x7f09012f

.field public static final MessengerButtonText_Blue:I = 0x7f090130

.field public static final MessengerButtonText_Blue_Large:I = 0x7f090131

.field public static final MessengerButtonText_Blue_Small:I = 0x7f090132

.field public static final MessengerButtonText_White:I = 0x7f090133

.field public static final MessengerButtonText_White_Large:I = 0x7f090134

.field public static final MessengerButtonText_White_Small:I = 0x7f090135

.field public static final MessengerButton_Blue:I = 0x7f090129

.field public static final MessengerButton_Blue_Large:I = 0x7f09012a

.field public static final MessengerButton_Blue_Small:I = 0x7f09012b

.field public static final MessengerButton_White:I = 0x7f09012c

.field public static final MessengerButton_White_Large:I = 0x7f09012d

.field public static final MessengerButton_White_Small:I = 0x7f09012e

.field public static final OtherVersionsHeaderTextAppView:I = 0x7f090136

.field public static final Platform_AppCompat:I = 0x7f09001b

.field public static final Platform_AppCompat_Light:I = 0x7f09001c

.field public static final Platform_ThemeOverlay_AppCompat:I = 0x7f090088

.field public static final Platform_ThemeOverlay_AppCompat_Dark:I = 0x7f090089

.field public static final Platform_ThemeOverlay_AppCompat_Light:I = 0x7f09008a

.field public static final Platform_V11_AppCompat:I = 0x7f09001d

.field public static final Platform_V11_AppCompat_Light:I = 0x7f09001e

.field public static final Platform_V14_AppCompat:I = 0x7f090027

.field public static final Platform_V14_AppCompat_Light:I = 0x7f090028

.field public static final Platform_V21_AppCompat:I = 0x7f09008b

.field public static final Platform_V21_AppCompat_Light:I = 0x7f09008c

.field public static final Platform_V25_AppCompat:I = 0x7f0900a1

.field public static final Platform_V25_AppCompat_Light:I = 0x7f0900a2

.field public static final Platform_Widget_AppCompat_Spinner:I = 0x7f09001f

.field public static final Preference:I = 0x7f090137

.field public static final PreferenceFragment:I = 0x7f090032

.field public static final PreferenceFragmentList:I = 0x7f090033

.field public static final PreferenceThemeOverlay:I = 0x7f09013f

.field public static final Preference_Category:I = 0x7f090138

.field public static final Preference_CheckBoxPreference:I = 0x7f090139

.field public static final Preference_DialogPreference:I = 0x7f09013a

.field public static final Preference_DialogPreference_EditTextPreference:I = 0x7f09013b

.field public static final Preference_Information:I = 0x7f09013c

.field public static final Preference_PreferenceScreen:I = 0x7f09013d

.field public static final Preference_SwitchPreferenceCompat:I = 0x7f09013e

.field public static final PurchaseDialog:I = 0x7f090140

.field public static final RatingBarExtraSmall:I = 0x7f090141

.field public static final RatingBarLargeSelectable:I = 0x7f090142

.field public static final RatingBarMedium:I = 0x7f090143

.field public static final RatingBarReviewDialog:I = 0x7f090144

.field public static final RatingBarSmall:I = 0x7f090145

.field public static final RollBackSectionItemStyle:I = 0x7f090146

.field public static final RollBackSectionTextViewStyle:I = 0x7f090147

.field public static final RtlOverlay_DialogWindowTitle_AppCompat:I = 0x7f090034

.field public static final RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I = 0x7f090035

.field public static final RtlOverlay_Widget_AppCompat_DialogTitle_Icon:I = 0x7f090036

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem:I = 0x7f090037

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I = 0x7f090038

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I = 0x7f090039

.field public static final RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I = 0x7f09003f

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown:I = 0x7f09003a

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I = 0x7f09003b

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I = 0x7f09003c

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I = 0x7f09003d

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I = 0x7f09003e

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton:I = 0x7f090040

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton_Overflow:I = 0x7f090041

.field public static final StoreWhiteButton:I = 0x7f090148

.field public static final TextAppearance_AppCompat:I = 0x7f090149

.field public static final TextAppearance_AppCompat_Body1:I = 0x7f09014a

.field public static final TextAppearance_AppCompat_Body2:I = 0x7f09014b

.field public static final TextAppearance_AppCompat_Button:I = 0x7f09014c

.field public static final TextAppearance_AppCompat_Caption:I = 0x7f09014d

.field public static final TextAppearance_AppCompat_Display1:I = 0x7f09014e

.field public static final TextAppearance_AppCompat_Display2:I = 0x7f09014f

.field public static final TextAppearance_AppCompat_Display3:I = 0x7f090150

.field public static final TextAppearance_AppCompat_Display4:I = 0x7f090151

.field public static final TextAppearance_AppCompat_Headline:I = 0x7f090152

.field public static final TextAppearance_AppCompat_Inverse:I = 0x7f090153

.field public static final TextAppearance_AppCompat_Large:I = 0x7f090154

.field public static final TextAppearance_AppCompat_Large_Inverse:I = 0x7f090155

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f090156

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f090157

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f090158

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f090159

.field public static final TextAppearance_AppCompat_Medium:I = 0x7f09015a

.field public static final TextAppearance_AppCompat_Medium_Inverse:I = 0x7f09015b

.field public static final TextAppearance_AppCompat_Menu:I = 0x7f09015c

.field public static final TextAppearance_AppCompat_Notification:I = 0x7f090029

.field public static final TextAppearance_AppCompat_Notification_Info:I = 0x7f09008d

.field public static final TextAppearance_AppCompat_Notification_Info_Media:I = 0x7f09008e

.field public static final TextAppearance_AppCompat_Notification_Line2:I = 0x7f09015d

.field public static final TextAppearance_AppCompat_Notification_Line2_Media:I = 0x7f09015e

.field public static final TextAppearance_AppCompat_Notification_Media:I = 0x7f09008f

.field public static final TextAppearance_AppCompat_Notification_Time:I = 0x7f090090

.field public static final TextAppearance_AppCompat_Notification_Time_Media:I = 0x7f090091

.field public static final TextAppearance_AppCompat_Notification_Title:I = 0x7f09002a

.field public static final TextAppearance_AppCompat_Notification_Title_Media:I = 0x7f090092

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f09015f

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f090160

.field public static final TextAppearance_AppCompat_Small:I = 0x7f090161

.field public static final TextAppearance_AppCompat_Small_Inverse:I = 0x7f090162

.field public static final TextAppearance_AppCompat_Subhead:I = 0x7f090163

.field public static final TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f090164

.field public static final TextAppearance_AppCompat_Title:I = 0x7f090165

.field public static final TextAppearance_AppCompat_Title_Inverse:I = 0x7f090166

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f090167

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f090168

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f090169

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f09016a

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f09016b

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f09016c

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f09016d

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f09016e

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f09016f

.field public static final TextAppearance_AppCompat_Widget_Button:I = 0x7f090170

.field public static final TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f090171

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f090172

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f090173

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f090174

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f090175

.field public static final TextAppearance_AppCompat_Widget_Switch:I = 0x7f090176

.field public static final TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f090177

.field public static final TextAppearance_Aptoide_Caption:I = 0x7f090178

.field public static final TextAppearance_Aptoide_Caption_Inverse:I = 0x7f090179

.field public static final TextAppearance_Aptoide_Subhead:I = 0x7f09017a

.field public static final TextAppearance_Aptoide_Title:I = 0x7f09017b

.field public static final TextAppearance_Aptoide_Title_Inverse:I = 0x7f09017c

.field public static final TextAppearance_Design_CollapsingToolbar_Expanded:I = 0x7f09017d

.field public static final TextAppearance_Design_Counter:I = 0x7f09017e

.field public static final TextAppearance_Design_Counter_Overflow:I = 0x7f09017f

.field public static final TextAppearance_Design_Error:I = 0x7f090180

.field public static final TextAppearance_Design_Hint:I = 0x7f090181

.field public static final TextAppearance_Design_Snackbar_Message:I = 0x7f090182

.field public static final TextAppearance_Design_Tab:I = 0x7f090183

.field public static final TextAppearance_StatusBar_EventContent:I = 0x7f09002b

.field public static final TextAppearance_StatusBar_EventContent_Info:I = 0x7f09002c

.field public static final TextAppearance_StatusBar_EventContent_Line2:I = 0x7f09002d

.field public static final TextAppearance_StatusBar_EventContent_Time:I = 0x7f09002e

.field public static final TextAppearance_StatusBar_EventContent_Title:I = 0x7f09002f

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f090184

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f090185

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f090186

.field public static final TextViewAppLabel:I = 0x7f090187

.field public static final ThemeOverlay_AppCompat:I = 0x7f09019e

.field public static final ThemeOverlay_AppCompat_ActionBar:I = 0x7f09019f

.field public static final ThemeOverlay_AppCompat_Dark:I = 0x7f0901a0

.field public static final ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f0901a1

.field public static final ThemeOverlay_AppCompat_Dialog:I = 0x7f0901a2

.field public static final ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f0901a3

.field public static final ThemeOverlay_AppCompat_Light:I = 0x7f0901a4

.field public static final Theme_AppCompat:I = 0x7f090188

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f090189

.field public static final Theme_AppCompat_DayNight:I = 0x7f090006

.field public static final Theme_AppCompat_DayNight_DarkActionBar:I = 0x7f090007

.field public static final Theme_AppCompat_DayNight_Dialog:I = 0x7f090008

.field public static final Theme_AppCompat_DayNight_DialogWhenLarge:I = 0x7f09000b

.field public static final Theme_AppCompat_DayNight_Dialog_Alert:I = 0x7f090009

.field public static final Theme_AppCompat_DayNight_Dialog_MinWidth:I = 0x7f09000a

.field public static final Theme_AppCompat_DayNight_NoActionBar:I = 0x7f09000c

.field public static final Theme_AppCompat_Dialog:I = 0x7f09018a

.field public static final Theme_AppCompat_DialogWhenLarge:I = 0x7f09018d

.field public static final Theme_AppCompat_Dialog_Alert:I = 0x7f09018b

.field public static final Theme_AppCompat_Dialog_MinWidth:I = 0x7f09018c

.field public static final Theme_AppCompat_Light:I = 0x7f09018e

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f09018f

.field public static final Theme_AppCompat_Light_Dialog:I = 0x7f090190

.field public static final Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f090193

.field public static final Theme_AppCompat_Light_Dialog_Alert:I = 0x7f090191

.field public static final Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f090192

.field public static final Theme_AppCompat_Light_NoActionBar:I = 0x7f090194

.field public static final Theme_AppCompat_NoActionBar:I = 0x7f090195

.field public static final Theme_Design:I = 0x7f090196

.field public static final Theme_Design_BottomSheetDialog:I = 0x7f090197

.field public static final Theme_Design_Light:I = 0x7f090198

.field public static final Theme_Design_Light_BottomSheetDialog:I = 0x7f090199

.field public static final Theme_Design_Light_NoActionBar:I = 0x7f09019a

.field public static final Theme_Design_NoActionBar:I = 0x7f09019b

.field public static final Theme_IAPTheme:I = 0x7f09019c

.field public static final Theme_Transparent:I = 0x7f09019d

.field public static final TimelineCommentsDialog:I = 0x7f0901a5

.field public static final Toolbar:I = 0x7f0901a6

.field public static final ToolbarAmber:I = 0x7f0901a7

.field public static final ToolbarAmberTransparent:I = 0x7f0901a8

.field public static final ToolbarBlack:I = 0x7f0901a9

.field public static final ToolbarBlackTransparent:I = 0x7f0901aa

.field public static final ToolbarBluegrey:I = 0x7f0901ab

.field public static final ToolbarBluegreyTransparent:I = 0x7f0901ac

.field public static final ToolbarBrown:I = 0x7f0901ad

.field public static final ToolbarBrownTransparent:I = 0x7f0901ae

.field public static final ToolbarDark:I = 0x7f0901af

.field public static final ToolbarDeeppurple:I = 0x7f0901b0

.field public static final ToolbarDeeppurpleTransparent:I = 0x7f0901b1

.field public static final ToolbarGreen:I = 0x7f0901b2

.field public static final ToolbarGreenTransparent:I = 0x7f0901b3

.field public static final ToolbarGrey:I = 0x7f0901b4

.field public static final ToolbarGreyTransparent:I = 0x7f0901b5

.field public static final ToolbarIndigo:I = 0x7f0901b6

.field public static final ToolbarIndigoTransparent:I = 0x7f0901b7

.field public static final ToolbarLightblue:I = 0x7f0901b8

.field public static final ToolbarLightblueTransparent:I = 0x7f0901b9

.field public static final ToolbarLightgreen:I = 0x7f0901ba

.field public static final ToolbarLightgreenTransparent:I = 0x7f0901bb

.field public static final ToolbarLime:I = 0x7f0901bc

.field public static final ToolbarLimeTransparent:I = 0x7f0901bd

.field public static final ToolbarOrange:I = 0x7f0901be

.field public static final ToolbarOrangeTransparent:I = 0x7f0901bf

.field public static final ToolbarPink:I = 0x7f0901c0

.field public static final ToolbarPinkTransparent:I = 0x7f0901c1

.field public static final ToolbarRed:I = 0x7f0901c2

.field public static final ToolbarRedTransparent:I = 0x7f0901c3

.field public static final ToolbarTeal:I = 0x7f0901c4

.field public static final ToolbarTealTransparent:I = 0x7f0901c5

.field public static final WidgetButtonCarrierBillingFortumo:I = 0x7f09022b

.field public static final WidgetButtonCarrierBillingUnitel:I = 0x7f09022c

.field public static final WidgetButtonCustom:I = 0x7f09022d

.field public static final WidgetButtonCustomAmber:I = 0x7f09022e

.field public static final WidgetButtonCustomAptoide:I = 0x7f09022f

.field public static final WidgetButtonCustomAptoideAmber:I = 0x7f090230

.field public static final WidgetButtonCustomAptoideBlack:I = 0x7f090231

.field public static final WidgetButtonCustomAptoideBluegrey:I = 0x7f090232

.field public static final WidgetButtonCustomAptoideBrown:I = 0x7f090233

.field public static final WidgetButtonCustomAptoideDeepPurple:I = 0x7f090234

.field public static final WidgetButtonCustomAptoideGreen:I = 0x7f090235

.field public static final WidgetButtonCustomAptoideGrey:I = 0x7f090236

.field public static final WidgetButtonCustomAptoideIndigo:I = 0x7f090237

.field public static final WidgetButtonCustomAptoideLightblue:I = 0x7f090238

.field public static final WidgetButtonCustomAptoideLightgreen:I = 0x7f090239

.field public static final WidgetButtonCustomAptoideLime:I = 0x7f09023a

.field public static final WidgetButtonCustomAptoideOrange:I = 0x7f09023b

.field public static final WidgetButtonCustomAptoidePink:I = 0x7f09023c

.field public static final WidgetButtonCustomAptoideRed:I = 0x7f09023d

.field public static final WidgetButtonCustomAptoideTeal:I = 0x7f09023e

.field public static final WidgetButtonCustomAptoideTwoLines:I = 0x7f09023f

.field public static final WidgetButtonCustomAptoideWhite:I = 0x7f090240

.field public static final WidgetButtonCustomBlack:I = 0x7f090241

.field public static final WidgetButtonCustomBluegrey:I = 0x7f090242

.field public static final WidgetButtonCustomBrown:I = 0x7f090243

.field public static final WidgetButtonCustomDeepPurple:I = 0x7f090244

.field public static final WidgetButtonCustomGray:I = 0x7f090245

.field public static final WidgetButtonCustomGreen:I = 0x7f090246

.field public static final WidgetButtonCustomGrey:I = 0x7f090247

.field public static final WidgetButtonCustomIndigo:I = 0x7f090248

.field public static final WidgetButtonCustomLightblue:I = 0x7f090249

.field public static final WidgetButtonCustomLightgreen:I = 0x7f09024a

.field public static final WidgetButtonCustomLime:I = 0x7f09024b

.field public static final WidgetButtonCustomOrange:I = 0x7f09024c

.field public static final WidgetButtonCustomPink:I = 0x7f09024d

.field public static final WidgetButtonCustomRed:I = 0x7f09024e

.field public static final WidgetButtonCustomTeal:I = 0x7f09024f

.field public static final WidgetButtonPaypal:I = 0x7f090250

.field public static final WidgetButtonVisa:I = 0x7f090251

.field public static final Widget_AppCompat_ActionBar:I = 0x7f0901c6

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f0901c7

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f0901c8

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f0901c9

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f0901ca

.field public static final Widget_AppCompat_ActionButton:I = 0x7f0901cb

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0901cc

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f0901cd

.field public static final Widget_AppCompat_ActionMode:I = 0x7f0901ce

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f0901cf

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f0901d0

.field public static final Widget_AppCompat_Button:I = 0x7f0901d1

.field public static final Widget_AppCompat_ButtonBar:I = 0x7f0901d7

.field public static final Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f0901d8

.field public static final Widget_AppCompat_Button_Borderless:I = 0x7f0901d2

.field public static final Widget_AppCompat_Button_Borderless_Colored:I = 0x7f0901d3

.field public static final Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f0901d4

.field public static final Widget_AppCompat_Button_Colored:I = 0x7f0901d5

.field public static final Widget_AppCompat_Button_Small:I = 0x7f0901d6

.field public static final Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f0901d9

.field public static final Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f0901da

.field public static final Widget_AppCompat_CompoundButton_Switch:I = 0x7f0901db

.field public static final Widget_AppCompat_DrawerArrowToggle:I = 0x7f0901dc

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f0901dd

.field public static final Widget_AppCompat_EditText:I = 0x7f0901de

.field public static final Widget_AppCompat_ImageButton:I = 0x7f0901df

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f0901e0

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0901e1

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f0901e2

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0901e3

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f0901e4

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f0901e5

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f0901e6

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f0901e7

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f0901e8

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f0901e9

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f0901ea

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f0901eb

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f0901ec

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f0901ed

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f0901ee

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f0901ef

.field public static final Widget_AppCompat_Light_ListPopupWindow:I = 0x7f0901f0

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f0901f1

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f0901f2

.field public static final Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f0901f3

.field public static final Widget_AppCompat_Light_SearchView:I = 0x7f0901f4

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f0901f5

.field public static final Widget_AppCompat_ListMenuView:I = 0x7f0901f6

.field public static final Widget_AppCompat_ListPopupWindow:I = 0x7f0901f7

.field public static final Widget_AppCompat_ListView:I = 0x7f0901f8

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f0901f9

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f0901fa

.field public static final Widget_AppCompat_NotificationActionContainer:I = 0x7f090093

.field public static final Widget_AppCompat_NotificationActionText:I = 0x7f090094

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f0901fb

.field public static final Widget_AppCompat_PopupMenu_Overflow:I = 0x7f0901fc

.field public static final Widget_AppCompat_PopupWindow:I = 0x7f0901fd

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f0901fe

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f0901ff

.field public static final Widget_AppCompat_RatingBar:I = 0x7f090200

.field public static final Widget_AppCompat_RatingBar_Indicator:I = 0x7f090201

.field public static final Widget_AppCompat_RatingBar_Small:I = 0x7f090202

.field public static final Widget_AppCompat_SearchView:I = 0x7f090203

.field public static final Widget_AppCompat_SearchView_ActionBar:I = 0x7f090204

.field public static final Widget_AppCompat_SeekBar:I = 0x7f090205

.field public static final Widget_AppCompat_SeekBar_Discrete:I = 0x7f090206

.field public static final Widget_AppCompat_Spinner:I = 0x7f090207

.field public static final Widget_AppCompat_Spinner_DropDown:I = 0x7f090208

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f090209

.field public static final Widget_AppCompat_Spinner_Underlined:I = 0x7f09020a

.field public static final Widget_AppCompat_TextView_SpinnerItem:I = 0x7f09020b

.field public static final Widget_AppCompat_Toolbar:I = 0x7f09020c

.field public static final Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f09020d

.field public static final Widget_Aptoide_Button:I = 0x7f09020e

.field public static final Widget_Aptoide_Button_Borderless:I = 0x7f09020f

.field public static final Widget_Aptoide_Button_Borderless_Colored:I = 0x7f090210

.field public static final Widget_Aptoide_Button_Colored:I = 0x7f090211

.field public static final Widget_Aptoide_Card:I = 0x7f090212

.field public static final Widget_Aptoide_Card_Body:I = 0x7f090213

.field public static final Widget_Aptoide_Card_Button:I = 0x7f090214

.field public static final Widget_Aptoide_Card_Headline:I = 0x7f090215

.field public static final Widget_Aptoide_Card_Subtitle:I = 0x7f090216

.field public static final Widget_Aptoide_Card_Subtitle_Large:I = 0x7f090217

.field public static final Widget_Aptoide_Card_Thumbnail:I = 0x7f090218

.field public static final Widget_Aptoide_Card_Title:I = 0x7f090219

.field public static final Widget_Aptoide_Card_Title_Feature:I = 0x7f09021a

.field public static final Widget_Aptoide_Card_Title_Large:I = 0x7f09021b

.field public static final Widget_Aptoide_Card_Title_Large_Black:I = 0x7f09021c

.field public static final Widget_Aptoide_Card_Title_LatestApps:I = 0x7f09021d

.field public static final Widget_Aptoide_DropDown_Borderless:I = 0x7f09021e

.field public static final Widget_Aptoide_RatingBar_GridItem:I = 0x7f09021f

.field public static final Widget_Aptoide_defaultColorText:I = 0x7f090220

.field public static final Widget_Design_AppBarLayout:I = 0x7f090095

.field public static final Widget_Design_BottomNavigationView:I = 0x7f090221

.field public static final Widget_Design_BottomSheet_Modal:I = 0x7f090222

.field public static final Widget_Design_CollapsingToolbar:I = 0x7f090223

.field public static final Widget_Design_CoordinatorLayout:I = 0x7f090224

.field public static final Widget_Design_FloatingActionButton:I = 0x7f090225

.field public static final Widget_Design_NavigationView:I = 0x7f090226

.field public static final Widget_Design_ScrimInsetsFrameLayout:I = 0x7f090227

.field public static final Widget_Design_Snackbar:I = 0x7f090228

.field public static final Widget_Design_TabLayout:I = 0x7f090001

.field public static final Widget_Design_TextInputLayout:I = 0x7f090229

.field public static final Widget_Styled_ActionBar_Aptoide:I = 0x7f09022a

.field public static final WizardPageSubtitle:I = 0x7f090252

.field public static final WizardPageTitle:I = 0x7f090253

.field public static final backgroundCardStyleDark:I = 0x7f090254

.field public static final backgroundCardStyleLight:I = 0x7f090255

.field public static final backgroundTabsStyleDark:I = 0x7f090256

.field public static final backgroundTabsStyleLight:I = 0x7f090257

.field public static final badgeReasonStyle:I = 0x7f090258

.field public static final captionOnly:I = 0x7f090259

.field public static final com_facebook_activity_theme:I = 0x7f09025a

.field public static final com_facebook_auth_dialog:I = 0x7f09025b

.field public static final com_facebook_auth_dialog_instructions_textview:I = 0x7f09025c

.field public static final com_facebook_button:I = 0x7f09025d

.field public static final com_facebook_button_like:I = 0x7f09025e

.field public static final com_facebook_button_send:I = 0x7f09025f

.field public static final com_facebook_button_share:I = 0x7f090260

.field public static final com_facebook_loginview_default_style:I = 0x7f090261

.field public static final com_facebook_loginview_silver_style:I = 0x7f090262

.field public static final customRowStyle:I = 0x7f090263

.field public static final customRowStyleHomeLayout:I = 0x7f090264

.field public static final focusableBackground:I = 0x7f090020

.field public static final focusableRowBackground:I = 0x7f090021

.field public static final headerSeparatorButtonStyle:I = 0x7f090265

.field public static final headerSeparatorButtonStyleAptoide:I = 0x7f090266

.field public static final navigationDrawerColorDark:I = 0x7f090267

.field public static final navigationDrawerColorLight:I = 0x7f090268

.field public static final titleLabel:I = 0x7f090269

.field public static final titleStyle:I = 0x7f09026a

.field public static final tooltip_bubble_text:I = 0x7f09026b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
