.class public final Lcm/aptoide/pt/v8engine/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/v8engine/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final FeedBackCheckBox:I = 0x7f0e00c4

.field public static final FeedBackSendButton:I = 0x7f0e00c5

.field public static final FeedBackSubject:I = 0x7f0e00c2

.field public static final FeedBacktext:I = 0x7f0e00c3

.field public static final PushNotificationImageView:I = 0x7f0e02ac

.field public static final accept_terms:I = 0x7f0e02c8

.field public static final action0:I = 0x7f0e0272

.field public static final actionContainer:I = 0x7f0e0181

.field public static final action_bar:I = 0x7f0e0074

.field public static final action_bar_activity_content:I = 0x7f0e0000

.field public static final action_bar_container:I = 0x7f0e0073

.field public static final action_bar_root:I = 0x7f0e006f

.field public static final action_bar_spinner:I = 0x7f0e0001

.field public static final action_bar_subtitle:I = 0x7f0e0054

.field public static final action_bar_title:I = 0x7f0e0053

.field public static final action_btn:I = 0x7f0e0187

.field public static final action_container:I = 0x7f0e026f

.field public static final action_context_bar:I = 0x7f0e0075

.field public static final action_divider:I = 0x7f0e0276

.field public static final action_image:I = 0x7f0e0270

.field public static final action_menu_divider:I = 0x7f0e0002

.field public static final action_menu_presenter:I = 0x7f0e0003

.field public static final action_mode_bar:I = 0x7f0e0071

.field public static final action_mode_bar_stub:I = 0x7f0e0070

.field public static final action_mode_close_button:I = 0x7f0e0055

.field public static final action_search:I = 0x7f0e030c

.field public static final action_text:I = 0x7f0e0271

.field public static final actions:I = 0x7f0e027f

.field public static final activity_boa_compra_authorization_web_view:I = 0x7f0e00df

.field public static final activity_chooser_view_content:I = 0x7f0e0056

.field public static final activity_payment_body:I = 0x7f0e00d1

.field public static final activity_payment_buttons:I = 0x7f0e00da

.field public static final activity_payment_buy_button:I = 0x7f0e00dc

.field public static final activity_payment_cancel_button:I = 0x7f0e00db

.field public static final activity_payment_global_progress_bar:I = 0x7f0e00dd

.field public static final activity_payment_header:I = 0x7f0e00cd

.field public static final activity_payment_list:I = 0x7f0e00d8

.field public static final activity_payment_list_container:I = 0x7f0e00d7

.field public static final activity_payment_list_progress_bar:I = 0x7f0e00de

.field public static final activity_payment_more_payments_button:I = 0x7f0e00d6

.field public static final activity_payment_no_payments_text:I = 0x7f0e00d9

.field public static final activity_payment_product_description:I = 0x7f0e00d0

.field public static final activity_payment_product_icon:I = 0x7f0e00ce

.field public static final activity_payment_product_name:I = 0x7f0e00cf

.field public static final activity_payment_selected_divider:I = 0x7f0e00d5

.field public static final activity_selected_payment_checked_image:I = 0x7f0e00d4

.field public static final activity_selected_payment_name:I = 0x7f0e00d2

.field public static final activity_selected_payment_price:I = 0x7f0e00d3

.field public static final activity_web_authorization_preogress_bar:I = 0x7f0e00e0

.field public static final add:I = 0x7f0e001c

.field public static final add_more_stores:I = 0x7f0e00e1

.field public static final added_date:I = 0x7f0e01c3

.field public static final added_date_pos1:I = 0x7f0e011c

.field public static final added_date_pos2:I = 0x7f0e011d

.field public static final added_time:I = 0x7f0e01af

.field public static final additional_info:I = 0x7f0e0169

.field public static final additional_info_layout:I = 0x7f0e016a

.field public static final adjust_height:I = 0x7f0e0038

.field public static final adjust_width:I = 0x7f0e0039

.field public static final adult_content:I = 0x7f0e02b4

.field public static final adult_content_layout:I = 0x7f0e02b3

.field public static final alertTitle:I = 0x7f0e0062

.field public static final all:I = 0x7f0e0034

.field public static final always:I = 0x7f0e003a

.field public static final apk_id:I = 0x7f0e02b8

.field public static final app_bar:I = 0x7f0e0217

.field public static final app_bar_layout:I = 0x7f0e00e2

.field public static final app_downloads:I = 0x7f0e028c

.field public static final app_error:I = 0x7f0e0122

.field public static final app_icon:I = 0x7f0e0084

.field public static final app_image:I = 0x7f0e0288

.field public static final app_info:I = 0x7f0e02dc

.field public static final app_install_button:I = 0x7f0e028e

.field public static final app_installed_version:I = 0x7f0e02f1

.field public static final app_name:I = 0x7f0e0086

.field public static final app_name_layout:I = 0x7f0e0293

.field public static final app_rating:I = 0x7f0e028a

.field public static final app_size:I = 0x7f0e028d

.field public static final app_state:I = 0x7f0e02b1

.field public static final app_store_layout:I = 0x7f0e01a3

.field public static final app_update_version:I = 0x7f0e02b2

.field public static final app_version:I = 0x7f0e01d2

.field public static final app_view_description_coordinator_layout:I = 0x7f0e0212

.field public static final app_view_fragment_coordinator_layout:I = 0x7f0e0211

.field public static final app_view_suggested:I = 0x7f0e01a9

.field public static final appbar:I = 0x7f0e008d

.field public static final apps:I = 0x7f0e01fe

.field public static final appview_avg_rating:I = 0x7f0e0102

.field public static final appview_rating_bar1:I = 0x7f0e00fc

.field public static final appview_rating_bar2:I = 0x7f0e00f9

.field public static final appview_rating_bar3:I = 0x7f0e00f6

.field public static final appview_rating_bar4:I = 0x7f0e00f3

.field public static final appview_rating_bar5:I = 0x7f0e00f0

.field public static final appview_rating_bar_avg:I = 0x7f0e0103

.field public static final appview_rating_bar_number1:I = 0x7f0e00fb

.field public static final appview_rating_bar_number2:I = 0x7f0e00f8

.field public static final appview_rating_bar_number3:I = 0x7f0e00f5

.field public static final appview_rating_bar_number4:I = 0x7f0e00f2

.field public static final appview_rating_bar_number5:I = 0x7f0e00ef

.field public static final appview_rating_bar_rating_number1:I = 0x7f0e0101

.field public static final appview_rating_bar_rating_number2:I = 0x7f0e0100

.field public static final appview_rating_bar_rating_number3:I = 0x7f0e00ff

.field public static final appview_rating_bar_rating_number4:I = 0x7f0e00fe

.field public static final appview_rating_bar_rating_number5:I = 0x7f0e00fd

.field public static final appview_rating_bar_star1:I = 0x7f0e00fa

.field public static final appview_rating_bar_star2:I = 0x7f0e00f7

.field public static final appview_rating_bar_star3:I = 0x7f0e00f4

.field public static final appview_rating_bar_star4:I = 0x7f0e00f1

.field public static final appview_rating_bar_star5:I = 0x7f0e00ee

.field public static final appview_suggested_recycler_view:I = 0x7f0e01aa

.field public static final aptoide_logo:I = 0x7f0e0304

.field public static final aptoide_version:I = 0x7f0e013a

.field public static final aptoidetv_installed_text:I = 0x7f0e0162

.field public static final auto:I = 0x7f0e0023

.field public static final autoCompleteTextViewTo:I = 0x7f0e02c6

.field public static final automatic:I = 0x7f0e004e

.field public static final avatar:I = 0x7f0e01bd

.field public static final badge_icon:I = 0x7f0e0291

.field public static final badge_img:I = 0x7f0e0299

.field public static final badge_text:I = 0x7f0e029a

.field public static final badges_layout:I = 0x7f0e0295

.field public static final beginning:I = 0x7f0e0036

.field public static final bold:I = 0x7f0e003f

.field public static final bottom:I = 0x7f0e0024

.field public static final bottom_separator_horizontal:I = 0x7f0e01a8

.field public static final bottom_view:I = 0x7f0e02c3

.field public static final box_count:I = 0x7f0e004b

.field public static final brick_app_item:I = 0x7f0e0106

.field public static final btn_access_token:I = 0x7f0e0267

.field public static final btn_close_screenshots_window:I = 0x7f0e0236

.field public static final btn_flag_this_app:I = 0x7f0e00eb

.field public static final btn_invalidate_token:I = 0x7f0e0269

.field public static final btn_refresh_token:I = 0x7f0e0268

.field public static final btn_show_hide_pass:I = 0x7f0e012a

.field public static final btn_user_name:I = 0x7f0e0266

.field public static final button:I = 0x7f0e004c

.field public static final buttonPanel:I = 0x7f0e005d

.field public static final button_camera:I = 0x7f0e0147

.field public static final button_dialog_add_store:I = 0x7f0e0145

.field public static final button_gallery:I = 0x7f0e0148

.field public static final button_login:I = 0x7f0e0260

.field public static final button_logout:I = 0x7f0e0265

.field public static final button_register:I = 0x7f0e025f

.field public static final button_top_stores:I = 0x7f0e0146

.field public static final buttons_layout:I = 0x7f0e023b

.field public static final cancel:I = 0x7f0e0149

.field public static final cancel_action:I = 0x7f0e0273

.field public static final cancel_button:I = 0x7f0e010d

.field public static final card:I = 0x7f0e01df

.field public static final card_custom_view_line_separator:I = 0x7f0e00e9

.field public static final card_date:I = 0x7f0e00e8

.field public static final card_image:I = 0x7f0e00e4

.field public static final card_subtitle:I = 0x7f0e00e7

.field public static final card_title:I = 0x7f0e00e6

.field public static final card_user_avatar:I = 0x7f0e00e5

.field public static final center:I = 0x7f0e0025

.field public static final centerHorizontalShim:I = 0x7f0e0306

.field public static final centerVerticalShim:I = 0x7f0e0305

.field public static final center_horizontal:I = 0x7f0e0026

.field public static final center_vertical:I = 0x7f0e0027

.field public static final change_ordering:I = 0x7f0e0232

.field public static final check_box:I = 0x7f0e01b3

.field public static final checkbox:I = 0x7f0e006b

.field public static final chosen_account_content_view:I = 0x7f0e0206

.field public static final chronometer:I = 0x7f0e027b

.field public static final clip_horizontal:I = 0x7f0e0030

.field public static final clip_vertical:I = 0x7f0e0031

.field public static final collapseActionView:I = 0x7f0e003b

.field public static final collapsing_toolbar:I = 0x7f0e0218

.field public static final com_facebook_body_frame:I = 0x7f0e0114

.field public static final com_facebook_button_xout:I = 0x7f0e0116

.field public static final com_facebook_device_auth_instructions:I = 0x7f0e010c

.field public static final com_facebook_fragment_container:I = 0x7f0e0109

.field public static final com_facebook_login_activity_progress_bar:I = 0x7f0e010e

.field public static final com_facebook_smart_instructions_0:I = 0x7f0e010f

.field public static final com_facebook_smart_instructions_1:I = 0x7f0e0110

.field public static final com_facebook_smart_instructions_2:I = 0x7f0e0111

.field public static final com_facebook_smart_instructions_3:I = 0x7f0e0112

.field public static final com_facebook_smart_instructions_or:I = 0x7f0e0113

.field public static final com_facebook_tooltip_bubble_view_bottom_pointer:I = 0x7f0e0118

.field public static final com_facebook_tooltip_bubble_view_text_body:I = 0x7f0e0117

.field public static final com_facebook_tooltip_bubble_view_top_pointer:I = 0x7f0e0115

.field public static final comment:I = 0x7f0e011f

.field public static final comment_button:I = 0x7f0e014c

.field public static final comment_store_button:I = 0x7f0e01ac

.field public static final comment_title:I = 0x7f0e01c2

.field public static final comments:I = 0x7f0e01b2

.field public static final comments_layout:I = 0x7f0e0191

.field public static final confirmIgnoreUpdateUninstall:I = 0x7f0e020a

.field public static final confirmation_code:I = 0x7f0e010a

.field public static final contact_layout:I = 0x7f0e013f

.field public static final container:I = 0x7f0e02bf

.field public static final content:I = 0x7f0e01bc

.field public static final contentPanel:I = 0x7f0e0063

.field public static final counts_layout:I = 0x7f0e021b

.field public static final create_store_action:I = 0x7f0e00b8

.field public static final create_store_choose_name_title:I = 0x7f0e0094

.field public static final create_store_header_textview:I = 0x7f0e008f

.field public static final create_store_image:I = 0x7f0e0091

.field public static final create_store_image_action:I = 0x7f0e0090

.field public static final create_store_layout:I = 0x7f0e0124

.field public static final create_store_name:I = 0x7f0e0095

.field public static final create_store_random_photo:I = 0x7f0e0093

.field public static final create_store_skip:I = 0x7f0e00b7

.field public static final create_store_take_picture_text:I = 0x7f0e0092

.field public static final create_store_theme_amber:I = 0x7f0e00a7

.field public static final create_store_theme_black:I = 0x7f0e00ad

.field public static final create_store_theme_blue_grey:I = 0x7f0e00af

.field public static final create_store_theme_brown:I = 0x7f0e00a9

.field public static final create_store_theme_check_amber:I = 0x7f0e00a8

.field public static final create_store_theme_check_black:I = 0x7f0e00ae

.field public static final create_store_theme_check_blue_grey:I = 0x7f0e00b0

.field public static final create_store_theme_check_brown:I = 0x7f0e00aa

.field public static final create_store_theme_check_deep_purple:I = 0x7f0e00b2

.field public static final create_store_theme_check_default:I = 0x7f0e0098

.field public static final create_store_theme_check_green:I = 0x7f0e009c

.field public static final create_store_theme_check_grey:I = 0x7f0e00b6

.field public static final create_store_theme_check_indigo:I = 0x7f0e00a0

.field public static final create_store_theme_check_light_green:I = 0x7f0e00b4

.field public static final create_store_theme_check_lightblue:I = 0x7f0e00ac

.field public static final create_store_theme_check_lime:I = 0x7f0e00a6

.field public static final create_store_theme_check_orange:I = 0x7f0e009a

.field public static final create_store_theme_check_pink:I = 0x7f0e00a4

.field public static final create_store_theme_check_red:I = 0x7f0e009e

.field public static final create_store_theme_check_teal:I = 0x7f0e00a2

.field public static final create_store_theme_deep_purple:I = 0x7f0e00b1

.field public static final create_store_theme_default:I = 0x7f0e0097

.field public static final create_store_theme_green:I = 0x7f0e009b

.field public static final create_store_theme_grey:I = 0x7f0e00b5

.field public static final create_store_theme_indigo:I = 0x7f0e009f

.field public static final create_store_theme_light_green:I = 0x7f0e00b3

.field public static final create_store_theme_lightblue:I = 0x7f0e00ab

.field public static final create_store_theme_lime:I = 0x7f0e00a5

.field public static final create_store_theme_orange:I = 0x7f0e0099

.field public static final create_store_theme_pink:I = 0x7f0e00a3

.field public static final create_store_theme_red:I = 0x7f0e009d

.field public static final create_store_theme_teal:I = 0x7f0e00a1

.field public static final create_user_choose_username:I = 0x7f0e00bf

.field public static final create_user_create_profile:I = 0x7f0e00c1

.field public static final create_user_header_textview:I = 0x7f0e00b9

.field public static final create_user_image:I = 0x7f0e00bc

.field public static final create_user_image_action:I = 0x7f0e00bb

.field public static final create_user_images:I = 0x7f0e00ba

.field public static final create_user_random_photo:I = 0x7f0e00be

.field public static final create_user_take_picture_text:I = 0x7f0e00bd

.field public static final create_user_username_inserted:I = 0x7f0e00c0

.field public static final created_store_text:I = 0x7f0e026d

.field public static final credits:I = 0x7f0e013e

.field public static final custom:I = 0x7f0e0069

.field public static final customPanel:I = 0x7f0e0068

.field public static final custom_snackbar_layout:I = 0x7f0e012b

.field public static final dark:I = 0x7f0e0044

.field public static final data_container:I = 0x7f0e0214

.field public static final decor_content_parent:I = 0x7f0e0072

.field public static final default_activity_button:I = 0x7f0e0059

.field public static final description:I = 0x7f0e0168

.field public static final descriptionContent:I = 0x7f0e01ff

.field public static final descriptionText:I = 0x7f0e0156

.field public static final description_edit:I = 0x7f0e0200

.field public static final design_bottom_sheet:I = 0x7f0e0133

.field public static final design_menu_item_action_area:I = 0x7f0e0138

.field public static final design_menu_item_action_area_stub:I = 0x7f0e0137

.field public static final design_menu_item_text:I = 0x7f0e0136

.field public static final design_navigation_view:I = 0x7f0e0135

.field public static final deviceNameText:I = 0x7f0e02be

.field public static final dialog_app_info:I = 0x7f0e0256

.field public static final dialog_app_layout:I = 0x7f0e0253

.field public static final dialog_app_name:I = 0x7f0e0255

.field public static final dialog_appview_icon:I = 0x7f0e0254

.field public static final dialog_install_warning_credibility_text:I = 0x7f0e014e

.field public static final dialog_install_warning_proceed_button:I = 0x7f0e0150

.field public static final dialog_install_warning_rank_text:I = 0x7f0e014d

.field public static final dialog_install_warning_trusted_app_button:I = 0x7f0e014f

.field public static final dialog_ok_button:I = 0x7f0e0251

.field public static final dialog_permissions:I = 0x7f0e0252

.field public static final dialog_scrollview:I = 0x7f0e0257

.field public static final dialog_table_permissions:I = 0x7f0e0258

.field public static final disableHome:I = 0x7f0e0010

.field public static final display_always:I = 0x7f0e004f

.field public static final displayable_social_timeline_app_update:I = 0x7f0e01da

.field public static final displayable_social_timeline_app_update_button:I = 0x7f0e01dc

.field public static final displayable_social_timeline_app_update_card:I = 0x7f0e01d3

.field public static final displayable_social_timeline_app_update_card_card_subtitle:I = 0x7f0e01d7

.field public static final displayable_social_timeline_app_update_card_image:I = 0x7f0e01d5

.field public static final displayable_social_timeline_app_update_card_title:I = 0x7f0e01d6

.field public static final displayable_social_timeline_app_update_error:I = 0x7f0e01dd

.field public static final displayable_social_timeline_app_update_header:I = 0x7f0e01d4

.field public static final displayable_social_timeline_app_update_icon:I = 0x7f0e01d8

.field public static final displayable_social_timeline_app_update_name:I = 0x7f0e01d9

.field public static final displayable_social_timeline_app_update_version:I = 0x7f0e01db

.field public static final displayable_social_timeline_article_header:I = 0x7f0e01e0

.field public static final displayable_social_timeline_likes_preview_container:I = 0x7f0e02ca

.field public static final displayable_social_timeline_recommendation_card_content:I = 0x7f0e01e5

.field public static final displayable_social_timeline_recommendation_card_icon:I = 0x7f0e01e2

.field public static final displayable_social_timeline_recommendation_card_subtitle:I = 0x7f0e01e4

.field public static final displayable_social_timeline_recommendation_card_title:I = 0x7f0e01e3

.field public static final displayable_social_timeline_recommendation_get_app_button:I = 0x7f0e01e9

.field public static final displayable_social_timeline_recommendation_icon:I = 0x7f0e01e6

.field public static final displayable_social_timeline_recommendation_name:I = 0x7f0e01e7

.field public static final displayable_social_timeline_recommendation_similar_apps:I = 0x7f0e01e8

.field public static final displayable_social_timeline_store_latest_apps_card_image:I = 0x7f0e01f7

.field public static final displayable_social_timeline_store_latest_apps_card_subtitle:I = 0x7f0e01f9

.field public static final displayable_social_timeline_store_latest_apps_card_title:I = 0x7f0e01f8

.field public static final displayable_social_timeline_store_latest_apps_container:I = 0x7f0e01ee

.field public static final displayable_social_timeline_store_latest_apps_header:I = 0x7f0e01f6

.field public static final displayable_social_timeline_video_header:I = 0x7f0e01fa

.field public static final download_details_layout:I = 0x7f0e0088

.field public static final download_progress:I = 0x7f0e0180

.field public static final download_progress_layout:I = 0x7f0e017e

.field public static final downloading_progress:I = 0x7f0e0087

.field public static final downloads:I = 0x7f0e01ae

.field public static final downloads_count:I = 0x7f0e0297

.field public static final downloads_count_in_store:I = 0x7f0e0296

.field public static final downloads_version_layout:I = 0x7f0e028b

.field public static final drawer_layout:I = 0x7f0e00c6

.field public static final edit_query:I = 0x7f0e0076

.field public static final edit_store_btn:I = 0x7f0e0201

.field public static final edit_store_description:I = 0x7f0e0096

.field public static final edit_store_password:I = 0x7f0e0142

.field public static final edit_store_uri:I = 0x7f0e0144

.field public static final edit_store_username:I = 0x7f0e0141

.field public static final email_label:I = 0x7f0e016c

.field public static final emptyListLayout:I = 0x7f0e015b

.field public static final emptyText:I = 0x7f0e015c

.field public static final empty_data:I = 0x7f0e0213

.field public static final empty_review_icon:I = 0x7f0e0198

.field public static final empty_review_text:I = 0x7f0e0199

.field public static final empty_reviews_layout:I = 0x7f0e0197

.field public static final end:I = 0x7f0e0028

.field public static final end_padder:I = 0x7f0e0285

.field public static final enterAlways:I = 0x7f0e0017

.field public static final enterAlwaysCollapsed:I = 0x7f0e0018

.field public static final errorLayout:I = 0x7f0e015f

.field public static final error_image:I = 0x7f0e02a2

.field public static final error_text:I = 0x7f0e029c

.field public static final error_text2:I = 0x7f0e029d

.field public static final eta:I = 0x7f0e008a

.field public static final everywhere:I = 0x7f0e023d

.field public static final exitUntilCollapsed:I = 0x7f0e0019

.field public static final expand_activities_button:I = 0x7f0e0057

.field public static final expanded_menu:I = 0x7f0e006a

.field public static final explore_button:I = 0x7f0e026e

.field public static final fab:I = 0x7f0e0234

.field public static final fabAdd:I = 0x7f0e02af

.field public static final fabAddStore:I = 0x7f0e02da

.field public static final fake_app_count:I = 0x7f0e0178

.field public static final fake_app_count_label:I = 0x7f0e0177

.field public static final fake_app_layout:I = 0x7f0e0176

.field public static final fb_login_button:I = 0x7f0e025c

.field public static final featured_graphic:I = 0x7f0e0107

.field public static final file_size:I = 0x7f0e0298

.field public static final fill:I = 0x7f0e0032

.field public static final fill_horizontal:I = 0x7f0e0033

.field public static final fill_vertical:I = 0x7f0e0029

.field public static final five_rate_star_count:I = 0x7f0e021e

.field public static final five_rate_star_label:I = 0x7f0e021d

.field public static final five_rate_star_layout:I = 0x7f0e021c

.field public static final five_rate_star_progress:I = 0x7f0e021f

.field public static final fixed:I = 0x7f0e0046

.field public static final flags_layout:I = 0x7f0e00ea

.field public static final follow_btn:I = 0x7f0e01fc

.field public static final follow_store_btn:I = 0x7f0e01a7

.field public static final follow_store_layout:I = 0x7f0e02e8

.field public static final followers:I = 0x7f0e02ea

.field public static final followers_following_numbers:I = 0x7f0e02e2

.field public static final followers_number:I = 0x7f0e02e4

.field public static final followers_tv:I = 0x7f0e02e3

.field public static final following:I = 0x7f0e02ec

.field public static final following_number:I = 0x7f0e02e7

.field public static final following_tv:I = 0x7f0e02e6

.field public static final forgot_password:I = 0x7f0e0261

.field public static final four_rate_star_count:I = 0x7f0e0222

.field public static final four_rate_star_label:I = 0x7f0e0221

.field public static final four_rate_star_layout:I = 0x7f0e0220

.field public static final four_rate_star_progress:I = 0x7f0e0223

.field public static final fragment_placeholder:I = 0x7f0e00cb

.field public static final g_sign_in_button:I = 0x7f0e025d

.field public static final generic_error:I = 0x7f0e029b

.field public static final go_to_store:I = 0x7f0e0323

.field public static final good_app_layout:I = 0x7f0e017c

.field public static final help_btn:I = 0x7f0e0161

.field public static final helpful_btn:I = 0x7f0e01c5

.field public static final helpful_layout:I = 0x7f0e01c4

.field public static final home:I = 0x7f0e0004

.field public static final homeAsUp:I = 0x7f0e0011

.field public static final horizontal_bar_1:I = 0x7f0e0230

.field public static final horizontal_separator:I = 0x7f0e02e5

.field public static final ic_action:I = 0x7f0e02b0

.field public static final ic_action_cancel:I = 0x7f0e0183

.field public static final ic_action_pause:I = 0x7f0e0184

.field public static final ic_action_resume:I = 0x7f0e0185

.field public static final ic_search_button:I = 0x7f0e0240

.field public static final ic_trusted_search:I = 0x7f0e02c1

.field public static final icon:I = 0x7f0e005b

.field public static final icon_frame:I = 0x7f0e02a8

.field public static final icon_group:I = 0x7f0e0280

.field public static final icon_only:I = 0x7f0e0041

.field public static final ifRoom:I = 0x7f0e003c

.field public static final image:I = 0x7f0e0058

.field public static final imageView:I = 0x7f0e02bd

.field public static final image_category:I = 0x7f0e01b0

.field public static final img_update_layout:I = 0x7f0e02f3

.field public static final info:I = 0x7f0e027c

.field public static final inline:I = 0x7f0e004d

.field public static final input_layout_review:I = 0x7f0e0154

.field public static final input_layout_title:I = 0x7f0e014a

.field public static final input_review:I = 0x7f0e0153

.field public static final input_text:I = 0x7f0e014b

.field public static final input_title:I = 0x7f0e0152

.field public static final install_and_latest_version_layout:I = 0x7f0e0186

.field public static final install_message:I = 0x7f0e0286

.field public static final installedItemFrame:I = 0x7f0e0241

.field public static final is_excluded:I = 0x7f0e02b6

.field public static final is_selected:I = 0x7f0e01d0

.field public static final italic:I = 0x7f0e0040

.field public static final item_payment_approving_text:I = 0x7f0e02a7

.field public static final item_payment_button_register:I = 0x7f0e02a6

.field public static final item_payment_button_use:I = 0x7f0e02a3

.field public static final item_payment_description:I = 0x7f0e02a5

.field public static final item_payment_name:I = 0x7f0e02a4

.field public static final item_touch_helper_previous_elevation:I = 0x7f0e0005

.field public static final iv_password:I = 0x7f0e0128

.field public static final iv_signature:I = 0x7f0e0249

.field public static final just_reviews:I = 0x7f0e0233

.field public static final large:I = 0x7f0e0051

.field public static final largeLabel:I = 0x7f0e0131

.field public static final latest_available_icon:I = 0x7f0e018e

.field public static final latest_available_layout:I = 0x7f0e018c

.field public static final latest_available_text:I = 0x7f0e018d

.field public static final latest_version_layout:I = 0x7f0e018b

.field public static final left:I = 0x7f0e002a

.field public static final leftSeparator:I = 0x7f0e02eb

.field public static final light:I = 0x7f0e0045

.field public static final line1:I = 0x7f0e0281

.field public static final line3:I = 0x7f0e0283

.field public static final list:I = 0x7f0e02a9

.field public static final listLayout:I = 0x7f0e0159

.field public static final listMode:I = 0x7f0e000d

.field public static final listView:I = 0x7f0e015a

.field public static final list_container:I = 0x7f0e0215

.field public static final list_item:I = 0x7f0e005a

.field public static final listview_background_shape:I = 0x7f0e030b

.field public static final loading_selected_layout:I = 0x7f0e01cf

.field public static final logged_as:I = 0x7f0e0264

.field public static final logged_in_continue:I = 0x7f0e025a

.field public static final logged_in_more_info_button:I = 0x7f0e0259

.field public static final logged_in_private_button:I = 0x7f0e025b

.field public static final login_button:I = 0x7f0e02ee

.field public static final login_link:I = 0x7f0e030a

.field public static final login_register_layout:I = 0x7f0e0308

.field public static final main_icon:I = 0x7f0e02de

.field public static final main_info_layout:I = 0x7f0e0289

.field public static final main_store_coordinator_layout:I = 0x7f0e00ca

.field public static final manual_reviewed_message_layout:I = 0x7f0e00ec

.field public static final media_actions:I = 0x7f0e0275

.field public static final media_layout:I = 0x7f0e01f0

.field public static final menu_card_timeline_clear:I = 0x7f0e0311

.field public static final menu_clear:I = 0x7f0e0312

.field public static final menu_install:I = 0x7f0e031f

.field public static final menu_install_selected:I = 0x7f0e0320

.field public static final menu_remote_install:I = 0x7f0e0310

.field public static final menu_remove:I = 0x7f0e0321

.field public static final menu_restore_updates:I = 0x7f0e031c

.field public static final menu_schedule:I = 0x7f0e030e

.field public static final menu_select_all:I = 0x7f0e031d

.field public static final menu_select_none:I = 0x7f0e031e

.field public static final menu_share:I = 0x7f0e030d

.field public static final menu_uninstall:I = 0x7f0e030f

.field public static final message:I = 0x7f0e0163

.field public static final message_white_bg:I = 0x7f0e02f9

.field public static final messenger_send_button:I = 0x7f0e0262

.field public static final middle:I = 0x7f0e0037

.field public static final mini:I = 0x7f0e0035

.field public static final more:I = 0x7f0e01b1

.field public static final more_layout:I = 0x7f0e0165

.field public static final multiply:I = 0x7f0e001d

.field public static final name:I = 0x7f0e01ad

.field public static final names_layout:I = 0x7f0e02e0

.field public static final nav_view:I = 0x7f0e00c7

.field public static final navigation_header_container:I = 0x7f0e0134

.field public static final navigation_item_backup_apps:I = 0x7f0e0318

.field public static final navigation_item_excluded_updates:I = 0x7f0e0316

.field public static final navigation_item_facebook:I = 0x7f0e0319

.field public static final navigation_item_my_account:I = 0x7f0e0313

.field public static final navigation_item_rollback:I = 0x7f0e0314

.field public static final navigation_item_setting_scheduled_downloads:I = 0x7f0e0315

.field public static final navigation_item_settings:I = 0x7f0e0317

.field public static final navigation_item_twitter:I = 0x7f0e031a

.field public static final needs_licence_count:I = 0x7f0e0175

.field public static final needs_licence_count_label:I = 0x7f0e0174

.field public static final needs_licence_layout:I = 0x7f0e0173

.field public static final never:I = 0x7f0e003d

.field public static final never_display:I = 0x7f0e0050

.field public static final no_apps_downloaded:I = 0x7f0e02a1

.field public static final no_connection_text:I = 0x7f0e0160

.field public static final no_connextion_text:I = 0x7f0e02a0

.field public static final no_network_connection:I = 0x7f0e029f

.field public static final no_search_results_layout:I = 0x7f0e023e

.field public static final none:I = 0x7f0e0012

.field public static final normal:I = 0x7f0e000e

.field public static final not_helpful_btn:I = 0x7f0e01c7

.field public static final not_helpful_layout:I = 0x7f0e01c6

.field public static final not_latest_available_text:I = 0x7f0e0190

.field public static final notification_background:I = 0x7f0e027e

.field public static final notification_main_column:I = 0x7f0e0278

.field public static final notification_main_column_container:I = 0x7f0e0277

.field public static final number_fake_flags:I = 0x7f0e020c

.field public static final number_freeze_flags:I = 0x7f0e020d

.field public static final number_good_flags:I = 0x7f0e020e

.field public static final number_licence_flags:I = 0x7f0e020f

.field public static final number_of_apps:I = 0x7f0e02d7

.field public static final number_of_followers:I = 0x7f0e02d6

.field public static final number_virus_flags:I = 0x7f0e0210

.field public static final numberslayout:I = 0x7f0e00ed

.field public static final one_rate_star_count:I = 0x7f0e022e

.field public static final one_rate_star_label:I = 0x7f0e022d

.field public static final one_rate_star_layout:I = 0x7f0e022c

.field public static final one_rate_star_progress:I = 0x7f0e022f

.field public static final open_graph:I = 0x7f0e0048

.field public static final opengllayout:I = 0x7f0e00c8

.field public static final or_message:I = 0x7f0e025e

.field public static final ordering_buttons_layout:I = 0x7f0e0231

.field public static final other_versions:I = 0x7f0e0189

.field public static final other_versions_coordinator_layout:I = 0x7f0e0216

.field public static final other_versions_title:I = 0x7f0e0219

.field public static final outer_layout:I = 0x7f0e0119

.field public static final outter_layout:I = 0x7f0e01fb

.field public static final overflow:I = 0x7f0e02c0

.field public static final page:I = 0x7f0e0049

.field public static final pager:I = 0x7f0e02f8

.field public static final parallax:I = 0x7f0e002e

.field public static final parentPanel:I = 0x7f0e005f

.field public static final partial_social_timeline_thumbnail:I = 0x7f0e01ef

.field public static final partial_social_timeline_thumbnail_divider:I = 0x7f0e01e1

.field public static final partial_social_timeline_thumbnail_divider2:I = 0x7f0e01de

.field public static final partial_social_timeline_thumbnail_get_app_button:I = 0x7f0e01f5

.field public static final partial_social_timeline_thumbnail_image:I = 0x7f0e01f2

.field public static final partial_social_timeline_thumbnail_related_to:I = 0x7f0e01f4

.field public static final partial_social_timeline_thumbnail_title:I = 0x7f0e01f1

.field public static final password:I = 0x7f0e0129

.field public static final pause_cancel_button:I = 0x7f0e008c

.field public static final payment_activity_overlay:I = 0x7f0e00cc

.field public static final permission_description:I = 0x7f0e02b5

.field public static final permission_name:I = 0x7f0e02bb

.field public static final permissions_label:I = 0x7f0e016e

.field public static final pin:I = 0x7f0e002f

.field public static final pininput:I = 0x7f0e0164

.field public static final play_button:I = 0x7f0e01f3

.field public static final privacy_policy_label:I = 0x7f0e016d

.field public static final profile_email_text:I = 0x7f0e0208

.field public static final profile_image:I = 0x7f0e0207

.field public static final profile_name_text:I = 0x7f0e0209

.field public static final progress:I = 0x7f0e008b

.field public static final progressBar:I = 0x7f0e0157

.field public static final progress_bar:I = 0x7f0e010b

.field public static final progress_bar_endless:I = 0x7f0e02bc

.field public static final progress_bar_is_installing:I = 0x7f0e01d1

.field public static final progress_circular:I = 0x7f0e0006

.field public static final progress_horizontal:I = 0x7f0e0007

.field public static final psts_tab_title:I = 0x7f0e02ab

.field public static final radio:I = 0x7f0e006d

.field public static final rate_and_reviews_coordinator_layout:I = 0x7f0e021a

.field public static final rate_button:I = 0x7f0e0155

.field public static final rate_this_button:I = 0x7f0e019b

.field public static final rate_this_button2:I = 0x7f0e019a

.field public static final rating:I = 0x7f0e01bb

.field public static final ratingCard:I = 0x7f0e0105

.field public static final rating_bar:I = 0x7f0e0151

.field public static final rating_flags_layout:I = 0x7f0e016f

.field public static final rating_layout:I = 0x7f0e0192

.field public static final rating_value:I = 0x7f0e019f

.field public static final ratingbar:I = 0x7f0e0108

.field public static final ratingbar_appview:I = 0x7f0e01a0

.field public static final read_all_button:I = 0x7f0e0193

.field public static final read_more_button:I = 0x7f0e0121

.field public static final recommended_store_action:I = 0x7f0e01ca

.field public static final recommended_store_apps:I = 0x7f0e01ce

.field public static final recommended_store_info:I = 0x7f0e01cb

.field public static final recommended_store_layout:I = 0x7f0e01c9

.field public static final recommended_store_name:I = 0x7f0e01cc

.field public static final recommended_store_users:I = 0x7f0e01cd

.field public static final recycler_view:I = 0x7f0e0120

.field public static final refreshButton:I = 0x7f0e0158

.field public static final registerButton:I = 0x7f0e0309

.field public static final reply_layout:I = 0x7f0e011e

.field public static final resume_download:I = 0x7f0e0123

.field public static final retry:I = 0x7f0e029e

.field public static final reviewButtonLayout:I = 0x7f0e0242

.field public static final reviewer:I = 0x7f0e01be

.field public static final right:I = 0x7f0e002b

.field public static final right_icon:I = 0x7f0e027d

.field public static final right_side:I = 0x7f0e0279

.field public static final row_app_download_indicator:I = 0x7f0e0083

.field public static final score:I = 0x7f0e01ba

.field public static final screen:I = 0x7f0e001e

.field public static final screen_shots_pager:I = 0x7f0e0235

.field public static final screenshot_image_big:I = 0x7f0e02b9

.field public static final screenshot_image_item:I = 0x7f0e02ba

.field public static final screenshots_list:I = 0x7f0e017d

.field public static final scroll:I = 0x7f0e001a

.field public static final scrollIndicatorDown:I = 0x7f0e0067

.field public static final scrollIndicatorUp:I = 0x7f0e0064

.field public static final scrollView:I = 0x7f0e0065

.field public static final scroll_child:I = 0x7f0e013d

.field public static final scrollable:I = 0x7f0e0047

.field public static final scroller:I = 0x7f0e02c7

.field public static final scrollview:I = 0x7f0e013b

.field public static final search_badge:I = 0x7f0e0078

.field public static final search_bar:I = 0x7f0e0077

.field public static final search_button:I = 0x7f0e0079

.field public static final search_close_btn:I = 0x7f0e007e

.field public static final search_edit_frame:I = 0x7f0e007a

.field public static final search_go_btn:I = 0x7f0e0080

.field public static final search_mag_icon:I = 0x7f0e007b

.field public static final search_plate:I = 0x7f0e007c

.field public static final search_results_layout:I = 0x7f0e023a

.field public static final search_src_text:I = 0x7f0e007d

.field public static final search_store:I = 0x7f0e02c4

.field public static final search_text:I = 0x7f0e023f

.field public static final search_time:I = 0x7f0e02c2

.field public static final search_voice_btn:I = 0x7f0e0081

.field public static final secondary_icon:I = 0x7f0e02df

.field public static final see_more_comments:I = 0x7f0e0167

.field public static final select_dialog_listview:I = 0x7f0e0082

.field public static final send_feedback:I = 0x7f0e031b

.field public static final separator:I = 0x7f0e0126

.field public static final separator_vertical:I = 0x7f0e02e1

.field public static final share_in_timeline:I = 0x7f0e017f

.field public static final shortcut:I = 0x7f0e006c

.field public static final showCustom:I = 0x7f0e0013

.field public static final showHome:I = 0x7f0e0014

.field public static final showTitle:I = 0x7f0e0015

.field public static final show_replies_btn:I = 0x7f0e01c8

.field public static final site:I = 0x7f0e0140

.field public static final site_layout:I = 0x7f0e013c

.field public static final small:I = 0x7f0e0052

.field public static final smallLabel:I = 0x7f0e0130

.field public static final snackbar_action:I = 0x7f0e012f

.field public static final snackbar_dismiss_action:I = 0x7f0e012e

.field public static final snackbar_image:I = 0x7f0e012c

.field public static final snackbar_text:I = 0x7f0e012d

.field public static final snap:I = 0x7f0e001b

.field public static final social_channels:I = 0x7f0e0202

.field public static final social_comment:I = 0x7f0e02ce

.field public static final social_fragment_layout:I = 0x7f0e02d1

.field public static final social_header:I = 0x7f0e00e3

.field public static final social_like:I = 0x7f0e02cd

.field public static final social_like_button:I = 0x7f0e02cf

.field public static final social_number_of_comments:I = 0x7f0e02d0

.field public static final social_number_of_likes:I = 0x7f0e02cc

.field public static final social_preview_checkbox:I = 0x7f0e01ec

.field public static final social_privacy_terms:I = 0x7f0e01eb

.field public static final social_share:I = 0x7f0e02cb

.field public static final social_shared_by:I = 0x7f0e01ea

.field public static final social_shared_store_avatar:I = 0x7f0e02d4

.field public static final social_text_privacy:I = 0x7f0e01ed

.field public static final social_timeline_latest_app:I = 0x7f0e02d2

.field public static final social_timeline_like_user_preview:I = 0x7f0e02d3

.field public static final spacer:I = 0x7f0e005e

.field public static final speed:I = 0x7f0e0089

.field public static final split_action_bar:I = 0x7f0e0008

.field public static final sponsored:I = 0x7f0e01b5

.field public static final sponsored_label:I = 0x7f0e02dd

.field public static final src_atop:I = 0x7f0e001f

.field public static final src_in:I = 0x7f0e0020

.field public static final src_over:I = 0x7f0e0021

.field public static final standard:I = 0x7f0e0042

.field public static final start:I = 0x7f0e002c

.field public static final status_bar_latest_event_content:I = 0x7f0e0274

.field public static final storeName:I = 0x7f0e01c0

.field public static final store_activity_coordinator_layout:I = 0x7f0e02d8

.field public static final store_avatar:I = 0x7f0e01a4

.field public static final store_avatar_row:I = 0x7f0e01b7

.field public static final store_followers:I = 0x7f0e0294

.field public static final store_icon:I = 0x7f0e026b

.field public static final store_info:I = 0x7f0e02d5

.field public static final store_layout:I = 0x7f0e01a2

.field public static final store_main_layout_row:I = 0x7f0e01b6

.field public static final store_main_layout_row_subcribed:I = 0x7f0e01b9

.field public static final store_name:I = 0x7f0e01a5

.field public static final store_name_row:I = 0x7f0e01b8

.field public static final store_number_users:I = 0x7f0e01a6

.field public static final store_recycler_coordinator_layout:I = 0x7f0e02db

.field public static final store_version_name:I = 0x7f0e018f

.field public static final submenuarrow:I = 0x7f0e006e

.field public static final submitCreateUser:I = 0x7f0e02c9

.field public static final submit_area:I = 0x7f0e007f

.field public static final subscribed:I = 0x7f0e023c

.field public static final subscribers:I = 0x7f0e01fd

.field public static final swipe_container:I = 0x7f0e0239

.field public static final switchWidget:I = 0x7f0e02aa

.field public static final tabMode:I = 0x7f0e000f

.field public static final tabs:I = 0x7f0e02d9

.field public static final text:I = 0x7f0e0284

.field public static final text1:I = 0x7f0e02ad

.field public static final text2:I = 0x7f0e0282

.field public static final textSpacerNoButtons:I = 0x7f0e0066

.field public static final textView:I = 0x7f0e0143

.field public static final textView2:I = 0x7f0e026c

.field public static final textView3:I = 0x7f0e01ab

.field public static final text_input_password_toggle:I = 0x7f0e0139

.field public static final text_progress:I = 0x7f0e0182

.field public static final text_update_layout:I = 0x7f0e02f4

.field public static final themes_list:I = 0x7f0e0125

.field public static final three_rate_star_count:I = 0x7f0e0226

.field public static final three_rate_star_label:I = 0x7f0e0225

.field public static final three_rate_star_layout:I = 0x7f0e0224

.field public static final three_rate_star_progress:I = 0x7f0e0227

.field public static final time:I = 0x7f0e027a

.field public static final timeSinceModified:I = 0x7f0e01bf

.field public static final timeline_login_layout:I = 0x7f0e02ed

.field public static final timeline_stats_layout:I = 0x7f0e02e9

.field public static final tipLayout:I = 0x7f0e015d

.field public static final tipText:I = 0x7f0e015e

.field public static final title:I = 0x7f0e005c

.field public static final title_template:I = 0x7f0e0061

.field public static final to_text_input_layout:I = 0x7f0e02c5

.field public static final toolbal_swipe_recycler_coordinator_layout:I = 0x7f0e02ae

.field public static final toolbar:I = 0x7f0e008e

.field public static final toolbar_and_swipe_coordinator_layout:I = 0x7f0e0238

.field public static final toolbar_login:I = 0x7f0e0263

.field public static final top:I = 0x7f0e002d

.field public static final topPanel:I = 0x7f0e0060

.field public static final top_comments_list:I = 0x7f0e0195

.field public static final top_comments_progress:I = 0x7f0e0196

.field public static final top_comments_title:I = 0x7f0e0194

.field public static final top_separator_horizontal:I = 0x7f0e01a1

.field public static final touch_outside:I = 0x7f0e0132

.field public static final tr_manual:I = 0x7f0e024d

.field public static final tr_scanned:I = 0x7f0e024b

.field public static final tr_signature:I = 0x7f0e0248

.field public static final tr_third_party:I = 0x7f0e0246

.field public static final tr_unknown:I = 0x7f0e024f

.field public static final transition_current_scene:I = 0x7f0e0009

.field public static final transition_scene_layoutid_cache:I = 0x7f0e000a

.field public static final trusted_header_layout:I = 0x7f0e0243

.field public static final tvUpdateUninstall:I = 0x7f0e020b

.field public static final tv_number_of_rates:I = 0x7f0e0104

.field public static final tv_reason_manual_qa:I = 0x7f0e024e

.field public static final tv_reason_scanned_passed:I = 0x7f0e024c

.field public static final tv_reason_signature_validation:I = 0x7f0e024a

.field public static final tv_reason_thirdparty_validated:I = 0x7f0e0247

.field public static final tv_reason_unknown:I = 0x7f0e0250

.field public static final twitch_button:I = 0x7f0e0204

.field public static final twitter_button:I = 0x7f0e0205

.field public static final two_rate_star_count:I = 0x7f0e022a

.field public static final two_rate_star_label:I = 0x7f0e0229

.field public static final two_rate_star_layout:I = 0x7f0e0228

.field public static final two_rate_star_progress:I = 0x7f0e022b

.field public static final unknown:I = 0x7f0e004a

.field public static final unknown_header_layout:I = 0x7f0e0245

.field public static final up:I = 0x7f0e000b

.field public static final updateButtonLayout:I = 0x7f0e02f0

.field public static final updateRowRelativeLayout:I = 0x7f0e02ef

.field public static final update_layout:I = 0x7f0e02f2

.field public static final useLogo:I = 0x7f0e0016

.field public static final user_icon:I = 0x7f0e011a

.field public static final user_interests_header_textview:I = 0x7f0e0237

.field public static final user_name:I = 0x7f0e011b

.field public static final username:I = 0x7f0e0127

.field public static final users_icon:I = 0x7f0e019d

.field public static final users_icon_layout:I = 0x7f0e019c

.field public static final users_voted:I = 0x7f0e019e

.field public static final vCircle:I = 0x7f0e02f6

.field public static final vDotsView:I = 0x7f0e02f5

.field public static final vHeart:I = 0x7f0e02f7

.field public static final version:I = 0x7f0e01b4

.field public static final version_and_info_layout:I = 0x7f0e028f

.field public static final version_code:I = 0x7f0e02b7

.field public static final version_date:I = 0x7f0e0292

.field public static final version_name:I = 0x7f0e0290

.field public static final version_separator_vertical:I = 0x7f0e018a

.field public static final versions:I = 0x7f0e0322

.field public static final versions_layout:I = 0x7f0e0188

.field public static final vertical_separator:I = 0x7f0e0287

.field public static final view:I = 0x7f0e0085

.field public static final viewPager:I = 0x7f0e02fa

.field public static final view_offset_helper:I = 0x7f0e000c

.field public static final virus_count:I = 0x7f0e017b

.field public static final virus_count_label:I = 0x7f0e017a

.field public static final virus_layout:I = 0x7f0e0179

.field public static final visualizer:I = 0x7f0e00c9

.field public static final warning_header_layout:I = 0x7f0e0244

.field public static final website_label:I = 0x7f0e016b

.field public static final wide:I = 0x7f0e0043

.field public static final widgetLayout:I = 0x7f0e026a

.field public static final withText:I = 0x7f0e003e

.field public static final wizard_indicator_group:I = 0x7f0e02fd

.field public static final wizard_indicator_one:I = 0x7f0e02fe

.field public static final wizard_indicator_three:I = 0x7f0e0300

.field public static final wizard_indicator_two:I = 0x7f0e02ff

.field public static final wizard_navigation_menu:I = 0x7f0e02fb

.field public static final wizard_next_arrow_icon:I = 0x7f0e0303

.field public static final wizard_next_clickable_space:I = 0x7f0e0301

.field public static final wizard_page_3_linear_layout_1:I = 0x7f0e0307

.field public static final wizard_skip:I = 0x7f0e02fc

.field public static final wizard_skip_text:I = 0x7f0e0302

.field public static final working_well_count:I = 0x7f0e0172

.field public static final working_well_count_label:I = 0x7f0e0171

.field public static final working_well_layout:I = 0x7f0e0170

.field public static final wrap_content:I = 0x7f0e0022

.field public static final write_comment:I = 0x7f0e0166

.field public static final write_reply_btn:I = 0x7f0e01c1

.field public static final youtube_or_facebook_button:I = 0x7f0e0203


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
