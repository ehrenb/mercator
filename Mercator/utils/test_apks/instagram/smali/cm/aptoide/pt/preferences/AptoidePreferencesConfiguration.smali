.class public interface abstract Lcm/aptoide/pt/preferences/AptoidePreferencesConfiguration;
.super Ljava/lang/Object;
.source "AptoidePreferencesConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcm/aptoide/pt/preferences/AptoidePreferencesConfiguration$SocialLogin;
    }
.end annotation


# virtual methods
.method public abstract getAccountType()Ljava/lang/String;
.end method

.method public abstract getApkCachePath()Ljava/lang/String;
.end method

.method public abstract getAppId()Ljava/lang/String;
.end method

.method public abstract getAutoUpdateUrl()Ljava/lang/String;
.end method

.method public abstract getAutoUpdatesSyncAdapterAuthority()Ljava/lang/String;
.end method

.method public abstract getCachePath()Ljava/lang/String;
.end method

.method public abstract getContentAuthority()Ljava/lang/String;
.end method

.method public abstract getDefaultStore()Ljava/lang/String;
.end method

.method public abstract getDefaultTheme()Ljava/lang/String;
.end method

.method public abstract getDefaultThemeRes()I
.end method

.method public abstract getExtraId()Ljava/lang/String;
.end method

.method public abstract getFeedbackEmail()Ljava/lang/String;
.end method

.method public abstract getIcon()I
.end method

.method public abstract getImagesCachePath()Ljava/lang/String;
.end method

.method public abstract getMarketName()Ljava/lang/String;
.end method

.method public abstract getPartnerDimension()Ljava/lang/String;
.end method

.method public abstract getPartnerId()Ljava/lang/String;
.end method

.method public abstract getPushNotificationReceiverClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract getSearchAuthority()Ljava/lang/String;
.end method

.method public abstract getTimeLinePostsSyncAdapterAuthority()Ljava/lang/String;
.end method

.method public abstract getTimelineActivitySyncAdapterAuthority()Ljava/lang/String;
.end method

.method public abstract getUserAvatarCachePath()Ljava/lang/String;
.end method

.method public abstract getVerticalDimension()Ljava/lang/String;
.end method

.method public abstract isAlwaysUpdate()Z
.end method

.method public abstract isCreateStoreAndSetUserPrivacyAvailable()Z
.end method

.method public abstract isLoginAvailable(Lcm/aptoide/pt/preferences/AptoidePreferencesConfiguration$SocialLogin;)Z
.end method
