.class public final enum Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;
.super Ljava/lang/Enum;
.source "GetAdsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Location"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum appview:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum aptoidesdk:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum firstinstall:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum homepage:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum middleappview:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum search:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum secondinstall:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

.field public static final enum secondtry:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 214
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "homepage"

    const-string v2, "native-aptoide:homepage"

    invoke-direct {v0, v1, v4, v2}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->homepage:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 215
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "appview"

    const-string v2, "native-aptoide:appview"

    invoke-direct {v0, v1, v5, v2}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->appview:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 216
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "middleappview"

    const-string v2, "native-aptoide:middleappview"

    invoke-direct {v0, v1, v6, v2}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->middleappview:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 217
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "search"

    const-string v2, "native-aptoide:search"

    invoke-direct {v0, v1, v7, v2}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->search:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 218
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "secondinstall"

    const-string v2, "native-aptoide:secondinstall"

    invoke-direct {v0, v1, v8, v2}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->secondinstall:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 219
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "secondtry"

    const/4 v2, 0x5

    const-string v3, "native-aptoide:secondtry"

    invoke-direct {v0, v1, v2, v3}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->secondtry:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 220
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "aptoidesdk"

    const/4 v2, 0x6

    const-string v3, "sdk-aptoide:generic"

    invoke-direct {v0, v1, v2, v3}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->aptoidesdk:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 221
    new-instance v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    const-string v1, "firstinstall"

    const/4 v2, 0x7

    const-string v3, "native-aptoide:first-install"

    invoke-direct {v0, v1, v2, v3}, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->firstinstall:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    .line 213
    const/16 v0, 0x8

    new-array v0, v0, [Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    sget-object v1, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->homepage:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v1, v0, v4

    sget-object v1, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->appview:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v1, v0, v5

    sget-object v1, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->middleappview:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v1, v0, v6

    sget-object v1, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->search:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v1, v0, v7

    sget-object v1, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->secondinstall:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->secondtry:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->aptoidesdk:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->firstinstall:Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    aput-object v2, v0, v1

    sput-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->$VALUES:[Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 225
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 226
    iput-object p3, p0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->value:Ljava/lang/String;

    .line 227
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;
    .locals 1

    .prologue
    .line 213
    const-class v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    return-object v0
.end method

.method public static values()[Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->$VALUES:[Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    invoke-virtual {v0}, [Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcm/aptoide/pt/dataprovider/ws/v2/aptwords/GetAdsRequest$Location;->value:Ljava/lang/String;

    return-object v0
.end method
