.class public final Lcm/aptoide/pt/v8engine/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/v8engine/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_background_aptoide:I = 0x7f020000

.field public static final ab_stacked_transparent_light_holo:I = 0x7f020001

.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f020002

.field public static final abc_action_bar_item_background_material:I = 0x7f020003

.field public static final abc_btn_borderless_material:I = 0x7f020004

.field public static final abc_btn_check_material:I = 0x7f020005

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f020006

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f020007

.field public static final abc_btn_colored_material:I = 0x7f020008

.field public static final abc_btn_default_mtrl_shape:I = 0x7f020009

.field public static final abc_btn_radio_material:I = 0x7f02000a

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f02000b

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f02000c

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f02000d

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f02000e

.field public static final abc_cab_background_internal_bg:I = 0x7f02000f

.field public static final abc_cab_background_top_material:I = 0x7f020010

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f020011

.field public static final abc_control_background_material:I = 0x7f020012

.field public static final abc_dialog_material_background:I = 0x7f020013

.field public static final abc_edit_text_material:I = 0x7f020014

.field public static final abc_ic_ab_back_material:I = 0x7f020015

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f020016

.field public static final abc_ic_clear_material:I = 0x7f020017

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f020018

.field public static final abc_ic_go_search_api_material:I = 0x7f020019

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f02001a

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f02001b

.field public static final abc_ic_menu_overflow_material:I = 0x7f02001c

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f02001d

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f02001e

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f02001f

.field public static final abc_ic_search_api_material:I = 0x7f020020

.field public static final abc_ic_star_black_16dp:I = 0x7f020021

.field public static final abc_ic_star_black_36dp:I = 0x7f020022

.field public static final abc_ic_star_black_48dp:I = 0x7f020023

.field public static final abc_ic_star_half_black_16dp:I = 0x7f020024

.field public static final abc_ic_star_half_black_36dp:I = 0x7f020025

.field public static final abc_ic_star_half_black_48dp:I = 0x7f020026

.field public static final abc_ic_voice_search_api_material:I = 0x7f020027

.field public static final abc_item_background_holo_dark:I = 0x7f020028

.field public static final abc_item_background_holo_light:I = 0x7f020029

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f02002a

.field public static final abc_list_focused_holo:I = 0x7f02002b

.field public static final abc_list_longpressed_holo:I = 0x7f02002c

.field public static final abc_list_pressed_holo_dark:I = 0x7f02002d

.field public static final abc_list_pressed_holo_light:I = 0x7f02002e

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f02002f

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020030

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020031

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020032

.field public static final abc_list_selector_holo_dark:I = 0x7f020033

.field public static final abc_list_selector_holo_light:I = 0x7f020034

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f020035

.field public static final abc_popup_background_mtrl_mult:I = 0x7f020036

.field public static final abc_ratingbar_indicator_material:I = 0x7f020037

.field public static final abc_ratingbar_material:I = 0x7f020038

.field public static final abc_ratingbar_small_material:I = 0x7f020039

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f02003a

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f02003b

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f02003c

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f02003d

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f02003e

.field public static final abc_seekbar_thumb_material:I = 0x7f02003f

.field public static final abc_seekbar_tick_mark_material:I = 0x7f020040

.field public static final abc_seekbar_track_material:I = 0x7f020041

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f020042

.field public static final abc_spinner_textfield_background_material:I = 0x7f020043

.field public static final abc_switch_thumb_material:I = 0x7f020044

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f020045

.field public static final abc_tab_indicator_material:I = 0x7f020046

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f020047

.field public static final abc_text_cursor_material:I = 0x7f020048

.field public static final abc_text_select_handle_left_mtrl_dark:I = 0x7f020049

.field public static final abc_text_select_handle_left_mtrl_light:I = 0x7f02004a

.field public static final abc_text_select_handle_middle_mtrl_dark:I = 0x7f02004b

.field public static final abc_text_select_handle_middle_mtrl_light:I = 0x7f02004c

.field public static final abc_text_select_handle_right_mtrl_dark:I = 0x7f02004d

.field public static final abc_text_select_handle_right_mtrl_light:I = 0x7f02004e

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f02004f

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f020050

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f020051

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020052

.field public static final abc_textfield_search_material:I = 0x7f020053

.field public static final abc_vector_test:I = 0x7f020054

.field public static final amber_btn_default_disabled_focused_holo_light:I = 0x7f020055

.field public static final amber_btn_default_disabled_holo_light:I = 0x7f020056

.field public static final amber_btn_default_focused_holo_light:I = 0x7f020057

.field public static final amber_btn_default_holo_light:I = 0x7f020058

.field public static final amber_btn_default_normal_holo_light:I = 0x7f020059

.field public static final amber_btn_default_pressed_holo_light:I = 0x7f02005a

.field public static final app_count_mini_icon:I = 0x7f02005b

.field public static final app_header_bgd_aptoide_dark:I = 0x7f02005c

.field public static final app_header_bgd_aptoide_light:I = 0x7f02005d

.field public static final app_rating_progress_bar_layout:I = 0x7f02005e

.field public static final app_view_header_alfa_gradient:I = 0x7f02005f

.field public static final app_view_header_gradient:I = 0x7f020060

.field public static final appstimeline_appupdate_shop_icon:I = 0x7f020061

.field public static final appstimeline_article_icon:I = 0x7f020062

.field public static final appstimeline_recommendation_compass_icon:I = 0x7f020063

.field public static final appstimeline_video_play_icon:I = 0x7f020064

.field public static final aptoide_btn_default_disabled_focused_holo_light:I = 0x7f020065

.field public static final aptoide_btn_default_disabled_holo_light:I = 0x7f020066

.field public static final aptoide_btn_default_focused_holo_light:I = 0x7f020067

.field public static final aptoide_btn_default_holo_light:I = 0x7f020068

.field public static final aptoide_btn_default_normal_holo_light:I = 0x7f020069

.field public static final aptoide_btn_default_pressed_holo_light:I = 0x7f02006a

.field public static final aptoide_white_btn_default_disabled_focused_holo_light:I = 0x7f02006b

.field public static final aptoide_white_btn_default_disabled_holo_light:I = 0x7f02006c

.field public static final aptoide_white_btn_default_focused_holo_light:I = 0x7f02006d

.field public static final aptoide_white_btn_default_holo_light:I = 0x7f02006e

.field public static final aptoide_white_btn_default_normal_holo_light:I = 0x7f02006f

.field public static final aptoide_white_btn_default_pressed_holo_light:I = 0x7f020070

.field public static final arrow_down:I = 0x7f020071

.field public static final arrow_up:I = 0x7f020072

.field public static final background_card_dark:I = 0x7f020073

.field public static final background_card_light:I = 0x7f020074

.field public static final background_card_overlay:I = 0x7f020075

.field public static final background_tabs_dark:I = 0x7f020076

.field public static final background_tabs_light:I = 0x7f020077

.field public static final bad:I = 0x7f020078

.field public static final black_btn_default_disabled_focused_holo_light:I = 0x7f020079

.field public static final black_btn_default_disabled_holo_light:I = 0x7f02007a

.field public static final black_btn_default_focused_holo_light:I = 0x7f02007b

.field public static final black_btn_default_holo_light:I = 0x7f02007c

.field public static final black_btn_default_normal_holo_light:I = 0x7f02007d

.field public static final black_btn_default_pressed_holo_light:I = 0x7f02007e

.field public static final black_card:I = 0x7f02007f

.field public static final blue_gray_btn_default_disabled_focused_holo_light:I = 0x7f020080

.field public static final blue_gray_btn_default_disabled_holo_light:I = 0x7f020081

.field public static final blue_gray_btn_default_focused_holo_light:I = 0x7f020082

.field public static final blue_gray_btn_default_normal_holo_light:I = 0x7f020083

.field public static final blue_gray_btn_default_pressed_holo_light:I = 0x7f020084

.field public static final blue_grey_btn_default_holo_light:I = 0x7f020085

.field public static final border_grey:I = 0x7f020086

.field public static final brown_btn_default_disabled_focused_holo_light:I = 0x7f020087

.field public static final brown_btn_default_disabled_holo_light:I = 0x7f020088

.field public static final brown_btn_default_focused_holo_light:I = 0x7f020089

.field public static final brown_btn_default_holo_light:I = 0x7f02008a

.field public static final brown_btn_default_normal_holo_light:I = 0x7f02008b

.field public static final brown_btn_default_pressed_holo_light:I = 0x7f02008c

.field public static final btn_movie_play_normal:I = 0x7f02008d

.field public static final button:I = 0x7f02008e

.field public static final button_border_amber:I = 0x7f02008f

.field public static final button_border_black:I = 0x7f020090

.field public static final button_border_blue:I = 0x7f020091

.field public static final button_border_bluegrey:I = 0x7f020092

.field public static final button_border_brown:I = 0x7f020093

.field public static final button_border_deeppurple:I = 0x7f020094

.field public static final button_border_dimgray:I = 0x7f020095

.field public static final button_border_gold:I = 0x7f020096

.field public static final button_border_green:I = 0x7f020097

.field public static final button_border_grey:I = 0x7f020098

.field public static final button_border_indigo:I = 0x7f020099

.field public static final button_border_lightblue:I = 0x7f02009a

.field public static final button_border_lightgreen:I = 0x7f02009b

.field public static final button_border_lightsky:I = 0x7f02009c

.field public static final button_border_lime:I = 0x7f02009d

.field public static final button_border_magenta:I = 0x7f02009e

.field public static final button_border_maroon:I = 0x7f02009f

.field public static final button_border_midnight:I = 0x7f0200a0

.field public static final button_border_orange:I = 0x7f0200a1

.field public static final button_border_orange2:I = 0x7f0200a2

.field public static final button_border_orange3:I = 0x7f0200a3

.field public static final button_border_pink:I = 0x7f0200a4

.field public static final button_border_red:I = 0x7f0200a5

.field public static final button_border_seagreen:I = 0x7f0200a6

.field public static final button_border_silver:I = 0x7f0200a7

.field public static final button_border_slategray:I = 0x7f0200a8

.field public static final button_border_springgreen:I = 0x7f0200a9

.field public static final button_border_teal:I = 0x7f0200aa

.field public static final button_disable:I = 0x7f0200ab

.field public static final button_focused:I = 0x7f0200ac

.field public static final button_pressed:I = 0x7f0200ad

.field public static final check_mdpi:I = 0x7f0200ae

.field public static final circle:I = 0x7f0200af

.field public static final circle_mask:I = 0x7f0200b0

.field public static final close_mdpi:I = 0x7f0200b1

.field public static final com_facebook_auth_dialog_background:I = 0x7f0200b2

.field public static final com_facebook_auth_dialog_cancel_background:I = 0x7f0200b3

.field public static final com_facebook_auth_dialog_header_background:I = 0x7f0200b4

.field public static final com_facebook_button_background:I = 0x7f0200b5

.field public static final com_facebook_button_icon:I = 0x7f0200b6

.field public static final com_facebook_button_icon_blue:I = 0x7f0200b7

.field public static final com_facebook_button_icon_white:I = 0x7f0200b8

.field public static final com_facebook_button_like_background:I = 0x7f0200b9

.field public static final com_facebook_button_like_icon_selected:I = 0x7f0200ba

.field public static final com_facebook_button_login_silver_background:I = 0x7f0200bb

.field public static final com_facebook_button_send_background:I = 0x7f0200bc

.field public static final com_facebook_button_send_icon_blue:I = 0x7f0200bd

.field public static final com_facebook_button_send_icon_white:I = 0x7f0200be

.field public static final com_facebook_close:I = 0x7f0200bf

.field public static final com_facebook_favicon_blue:I = 0x7f0200c0

.field public static final com_facebook_profile_picture_blank_portrait:I = 0x7f0200c1

.field public static final com_facebook_profile_picture_blank_square:I = 0x7f0200c2

.field public static final com_facebook_send_button_icon:I = 0x7f0200c3

.field public static final com_facebook_tooltip_black_background:I = 0x7f0200c4

.field public static final com_facebook_tooltip_black_bottomnub:I = 0x7f0200c5

.field public static final com_facebook_tooltip_black_topnub:I = 0x7f0200c6

.field public static final com_facebook_tooltip_black_xout:I = 0x7f0200c7

.field public static final com_facebook_tooltip_blue_background:I = 0x7f0200c8

.field public static final com_facebook_tooltip_blue_bottomnub:I = 0x7f0200c9

.field public static final com_facebook_tooltip_blue_topnub:I = 0x7f0200ca

.field public static final com_facebook_tooltip_blue_xout:I = 0x7f0200cb

.field public static final common_full_open_on_phone:I = 0x7f0200cc

.field public static final common_google_signin_btn_icon_dark:I = 0x7f0200cd

.field public static final common_google_signin_btn_icon_dark_disabled:I = 0x7f0200ce

.field public static final common_google_signin_btn_icon_dark_focused:I = 0x7f0200cf

.field public static final common_google_signin_btn_icon_dark_normal:I = 0x7f0200d0

.field public static final common_google_signin_btn_icon_dark_pressed:I = 0x7f0200d1

.field public static final common_google_signin_btn_icon_light:I = 0x7f0200d2

.field public static final common_google_signin_btn_icon_light_disabled:I = 0x7f0200d3

.field public static final common_google_signin_btn_icon_light_focused:I = 0x7f0200d4

.field public static final common_google_signin_btn_icon_light_normal:I = 0x7f0200d5

.field public static final common_google_signin_btn_icon_light_pressed:I = 0x7f0200d6

.field public static final common_google_signin_btn_text_dark:I = 0x7f0200d7

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0200d8

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0200d9

.field public static final common_google_signin_btn_text_dark_normal:I = 0x7f0200da

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0200db

.field public static final common_google_signin_btn_text_light:I = 0x7f0200dc

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0200dd

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0200de

.field public static final common_google_signin_btn_text_light_normal:I = 0x7f0200df

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0200e0

.field public static final con_red:I = 0x7f0200e1

.field public static final create_review_darkgrey:I = 0x7f0200e2

.field public static final create_store_avatar:I = 0x7f0200e3

.field public static final create_store_theme_check:I = 0x7f0200e4

.field public static final create_store_theme_shape_amber:I = 0x7f0200e5

.field public static final create_store_theme_shape_black:I = 0x7f0200e6

.field public static final create_store_theme_shape_blue_grey:I = 0x7f0200e7

.field public static final create_store_theme_shape_brown:I = 0x7f0200e8

.field public static final create_store_theme_shape_deep_purple:I = 0x7f0200e9

.field public static final create_store_theme_shape_default:I = 0x7f0200ea

.field public static final create_store_theme_shape_green:I = 0x7f0200eb

.field public static final create_store_theme_shape_grey:I = 0x7f0200ec

.field public static final create_store_theme_shape_indigo:I = 0x7f0200ed

.field public static final create_store_theme_shape_light_green:I = 0x7f0200ee

.field public static final create_store_theme_shape_lightblue:I = 0x7f0200ef

.field public static final create_store_theme_shape_lime:I = 0x7f0200f0

.field public static final create_store_theme_shape_orange:I = 0x7f0200f1

.field public static final create_store_theme_shape_pink:I = 0x7f0200f2

.field public static final create_store_theme_shape_red:I = 0x7f0200f3

.field public static final create_store_theme_shape_teal:I = 0x7f0200f4

.field public static final create_user_avatar:I = 0x7f0200f5

.field public static final create_user_camera:I = 0x7f0200f6

.field public static final create_user_camera_background_shape:I = 0x7f0200f7

.field public static final cross:I = 0x7f0200f8

.field public static final custom_categ_black:I = 0x7f0200f9

.field public static final custom_categ_blue:I = 0x7f0200fa

.field public static final custom_categ_dimgray:I = 0x7f0200fb

.field public static final custom_categ_gold:I = 0x7f0200fc

.field public static final custom_categ_green:I = 0x7f0200fd

.field public static final custom_categ_lightsky:I = 0x7f0200fe

.field public static final custom_categ_magenta:I = 0x7f0200ff

.field public static final custom_categ_maroon:I = 0x7f020100

.field public static final custom_categ_midnight:I = 0x7f020101

.field public static final custom_categ_orange:I = 0x7f020102

.field public static final custom_categ_pink:I = 0x7f020103

.field public static final custom_categ_red:I = 0x7f020104

.field public static final custom_categ_seagreen:I = 0x7f020105

.field public static final custom_categ_silver:I = 0x7f020106

.field public static final custom_categ_slategray:I = 0x7f020107

.field public static final custom_categ_springgreen:I = 0x7f020108

.field public static final custom_sponsored_label:I = 0x7f020109

.field public static final dark_card:I = 0x7f02010a

.field public static final dark_progress_bar:I = 0x7f02010b

.field public static final deep_purple_btn_default_disabled_focused_holo_light:I = 0x7f02010c

.field public static final deep_purple_btn_default_disabled_holo_light:I = 0x7f02010d

.field public static final deep_purple_btn_default_focused_holo_light:I = 0x7f02010e

.field public static final deep_purple_btn_default_holo_light:I = 0x7f02010f

.field public static final deep_purple_btn_default_normal_holo_light:I = 0x7f020110

.field public static final deep_purple_btn_default_pressed_holo_light:I = 0x7f020111

.field public static final design_bottom_navigation_item_background:I = 0x7f020112

.field public static final design_fab_background:I = 0x7f020113

.field public static final design_ic_visibility:I = 0x7f020114

.field public static final design_snackbar_background:I = 0x7f020115

.field public static final dialog_background_dark:I = 0x7f020116

.field public static final dialog_bg:I = 0x7f020117

.field public static final dialog_bg_2:I = 0x7f020118

.field public static final dialog_full_holo_light:I = 0x7f020119

.field public static final dialog_remote_install_bg:I = 0x7f02011a

.field public static final displayable_app_view_suggested_app_border:I = 0x7f02011b

.field public static final displayable_review_button:I = 0x7f02011c

.field public static final edittext_bg_round_corner:I = 0x7f02011d

.field public static final empty_star:I = 0x7f02011e

.field public static final error1:I = 0x7f02011f

.field public static final error2:I = 0x7f020120

.field public static final error3:I = 0x7f020121

.field public static final error_410:I = 0x7f020122

.field public static final facebook_btn_default_disabled_focused_holo_light:I = 0x7f020123

.field public static final facebook_btn_default_disabled_holo_light:I = 0x7f020124

.field public static final facebook_btn_default_focused_holo_light:I = 0x7f020125

.field public static final facebook_btn_default_holo_light:I = 0x7f020126

.field public static final facebook_btn_default_normal_holo_light:I = 0x7f020127

.field public static final facebook_btn_default_pressed_holo_light:I = 0x7f020128

.field public static final facebook_logo:I = 0x7f020129

.field public static final facebook_profile_pic:I = 0x7f02012a

.field public static final fake:I = 0x7f02012b

.field public static final fake_app:I = 0x7f02012c

.field public static final flag_license_copiar:I = 0x7f02012d

.field public static final flag_license_copiar_copy:I = 0x7f02012e

.field public static final flags_background:I = 0x7f02012f

.field public static final flat_selector:I = 0x7f020130

.field public static final forma_1:I = 0x7f020131

.field public static final fortumo_btn_default_disabled_focused_holo_light:I = 0x7f020132

.field public static final fortumo_btn_default_disabled_holo_light:I = 0x7f020133

.field public static final fortumo_btn_default_focused_holo_light:I = 0x7f020134

.field public static final fortumo_btn_default_holo_light:I = 0x7f020135

.field public static final fortumo_btn_default_normal_holo_light:I = 0x7f020136

.field public static final fortumo_btn_default_pressed_holo_light:I = 0x7f020137

.field public static final freeze:I = 0x7f020138

.field public static final full_star:I = 0x7f020139

.field public static final generic_image:I = 0x7f02013a

.field public static final good:I = 0x7f02013b

.field public static final good_flag:I = 0x7f02013c

.field public static final gradient_black:I = 0x7f02013d

.field public static final gradient_blue:I = 0x7f02013e

.field public static final gradient_dimgray:I = 0x7f02013f

.field public static final gradient_gold:I = 0x7f020140

.field public static final gradient_green:I = 0x7f020141

.field public static final gradient_lightsky:I = 0x7f020142

.field public static final gradient_magenta:I = 0x7f020143

.field public static final gradient_maroon:I = 0x7f020144

.field public static final gradient_midnight:I = 0x7f020145

.field public static final gradient_orange:I = 0x7f020146

.field public static final gradient_pink:I = 0x7f020147

.field public static final gradient_red:I = 0x7f020148

.field public static final gradient_seagreen:I = 0x7f020149

.field public static final gradient_silver:I = 0x7f02014a

.field public static final gradient_slategray:I = 0x7f02014b

.field public static final gradient_springgreen:I = 0x7f02014c

.field public static final gray_btn_default_disabled_focused_holo_light:I = 0x7f02014d

.field public static final gray_btn_default_disabled_holo_light:I = 0x7f02014e

.field public static final gray_btn_default_focused_holo_light:I = 0x7f02014f

.field public static final gray_btn_default_holo_light:I = 0x7f020150

.field public static final gray_btn_default_normal_holo_light:I = 0x7f020151

.field public static final gray_btn_default_pressed_holo_light:I = 0x7f020152

.field public static final gray_card:I = 0x7f020153

.field public static final gray_item_decorator:I = 0x7f020154

.field public static final green_btn_default_disabled_focused_holo_light:I = 0x7f020155

.field public static final green_btn_default_disabled_holo_light:I = 0x7f020156

.field public static final green_btn_default_focused_holo_light:I = 0x7f020157

.field public static final green_btn_default_holo_light:I = 0x7f020158

.field public static final green_btn_default_normal_holo_light:I = 0x7f020159

.field public static final green_btn_default_pressed_holo_light:I = 0x7f02015a

.field public static final grey_btn_default_disabled_focused_holo_light:I = 0x7f02015b

.field public static final grey_btn_default_disabled_holo_light:I = 0x7f02015c

.field public static final grey_btn_default_focused_holo_light:I = 0x7f02015d

.field public static final grey_btn_default_holo_light:I = 0x7f02015e

.field public static final grey_btn_default_normal_holo_light:I = 0x7f02015f

.field public static final grey_btn_default_pressed_holo_light:I = 0x7f020160

.field public static final grey_circular_placeholder:I = 0x7f020161

.field public static final grey_circular_placeholder_store:I = 0x7f020162

.field public static final grid_item_star_empty_small:I = 0x7f020163

.field public static final grid_item_star_full_small:I = 0x7f020164

.field public static final grid_item_star_half_small:I = 0x7f020165

.field public static final half_circle_white:I = 0x7f020166

.field public static final half_star:I = 0x7f020167

.field public static final heart_off:I = 0x7f020168

.field public static final heart_on:I = 0x7f020169

.field public static final highlight_overlay:I = 0x7f02016a

.field public static final ic_action_about:I = 0x7f02016b

.field public static final ic_action_about_dark:I = 0x7f02016c

.field public static final ic_action_accept:I = 0x7f02016d

.field public static final ic_action_accept_dark:I = 0x7f02016e

.field public static final ic_action_accounts:I = 0x7f02016f

.field public static final ic_action_accounts_dark:I = 0x7f020170

.field public static final ic_action_backup_custom:I = 0x7f020171

.field public static final ic_action_bad_dark:I = 0x7f020172

.field public static final ic_action_cancel:I = 0x7f020173

.field public static final ic_action_cancel_dark:I = 0x7f020174

.field public static final ic_action_cancel_small:I = 0x7f020175

.field public static final ic_action_cancel_small_dark:I = 0x7f020176

.field public static final ic_action_comments_dark:I = 0x7f020177

.field public static final ic_action_comments_light:I = 0x7f020178

.field public static final ic_action_credit_card_dark:I = 0x7f020179

.field public static final ic_action_discard:I = 0x7f02017a

.field public static final ic_action_discard_dark:I = 0x7f02017b

.field public static final ic_action_download:I = 0x7f02017c

.field public static final ic_action_download_dark:I = 0x7f02017d

.field public static final ic_action_email_dark:I = 0x7f02017e

.field public static final ic_action_facebook:I = 0x7f02017f

.field public static final ic_action_facebook_like:I = 0x7f020180

.field public static final ic_action_fortumo:I = 0x7f020181

.field public static final ic_action_go_to_today_dark:I = 0x7f020182

.field public static final ic_action_good_dark:I = 0x7f020183

.field public static final ic_action_home_collapse:I = 0x7f020184

.field public static final ic_action_home_collapse_dark:I = 0x7f020185

.field public static final ic_action_home_expand:I = 0x7f020186

.field public static final ic_action_home_expand_dark:I = 0x7f020187

.field public static final ic_action_important:I = 0x7f020188

.field public static final ic_action_important_dark:I = 0x7f020189

.field public static final ic_action_new:I = 0x7f02018a

.field public static final ic_action_new_dark:I = 0x7f02018b

.field public static final ic_action_next_item_dark:I = 0x7f02018c

.field public static final ic_action_next_item_light:I = 0x7f02018d

.field public static final ic_action_overflow:I = 0x7f02018e

.field public static final ic_action_paypal_dark:I = 0x7f02018f

.field public static final ic_action_play:I = 0x7f020190

.field public static final ic_action_play_dark:I = 0x7f020191

.field public static final ic_action_search:I = 0x7f020192

.field public static final ic_action_secure_dark:I = 0x7f020193

.field public static final ic_action_secure_light:I = 0x7f020194

.field public static final ic_action_send_now_dark:I = 0x7f020195

.field public static final ic_action_send_now_light:I = 0x7f020196

.field public static final ic_action_settings_dark:I = 0x7f020197

.field public static final ic_action_settings_light:I = 0x7f020198

.field public static final ic_action_share:I = 0x7f020199

.field public static final ic_action_time:I = 0x7f02019a

.field public static final ic_action_time_dark:I = 0x7f02019b

.field public static final ic_action_timeline_dark:I = 0x7f02019c

.field public static final ic_action_timeline_light:I = 0x7f02019d

.field public static final ic_action_twitter:I = 0x7f02019e

.field public static final ic_action_undo:I = 0x7f02019f

.field public static final ic_action_undo_dark:I = 0x7f0201a0

.field public static final ic_action_unitel2:I = 0x7f0201a1

.field public static final ic_action_update:I = 0x7f0201a2

.field public static final ic_action_update_dark:I = 0x7f0201a3

.field public static final ic_action_web_site_dark:I = 0x7f0201a4

.field public static final ic_add:I = 0x7f0201a5

.field public static final ic_appview_download_icon:I = 0x7f0201a6

.field public static final ic_appview_version:I = 0x7f0201a7

.field public static final ic_appview_version_green:I = 0x7f0201a8

.field public static final ic_appview_version_white:I = 0x7f0201a9

.field public static final ic_arrow_back:I = 0x7f0201aa

.field public static final ic_avatar_apps:I = 0x7f0201ab

.field public static final ic_badge_antivirus:I = 0x7f0201ac

.field public static final ic_badge_critical:I = 0x7f0201ad

.field public static final ic_badge_flag:I = 0x7f0201ae

.field public static final ic_badge_market:I = 0x7f0201af

.field public static final ic_badge_signature:I = 0x7f0201b0

.field public static final ic_badge_trusted:I = 0x7f0201b1

.field public static final ic_badge_unknown:I = 0x7f0201b2

.field public static final ic_badge_warning:I = 0x7f0201b3

.field public static final ic_chat:I = 0x7f0201b4

.field public static final ic_check:I = 0x7f0201b5

.field public static final ic_check_amber:I = 0x7f0201b6

.field public static final ic_check_black:I = 0x7f0201b7

.field public static final ic_check_blue_grey:I = 0x7f0201b8

.field public static final ic_check_brown:I = 0x7f0201b9

.field public static final ic_check_deep_purple:I = 0x7f0201ba

.field public static final ic_check_default:I = 0x7f0201bb

.field public static final ic_check_green:I = 0x7f0201bc

.field public static final ic_check_grey:I = 0x7f0201bd

.field public static final ic_check_indigo:I = 0x7f0201be

.field public static final ic_check_light_blue:I = 0x7f0201bf

.field public static final ic_check_light_green:I = 0x7f0201c0

.field public static final ic_check_lime:I = 0x7f0201c1

.field public static final ic_check_orange:I = 0x7f0201c2

.field public static final ic_check_pink:I = 0x7f0201c3

.field public static final ic_check_red:I = 0x7f0201c4

.field public static final ic_check_teal:I = 0x7f0201c5

.field public static final ic_check_white:I = 0x7f0201c6

.field public static final ic_close_btn:I = 0x7f0201c7

.field public static final ic_close_white:I = 0x7f0201c8

.field public static final ic_dimension:I = 0x7f0201c9

.field public static final ic_down_arrow:I = 0x7f0201ca

.field public static final ic_download_icon:I = 0x7f0201cb

.field public static final ic_drawable_left_facebook:I = 0x7f0201cc

.field public static final ic_drawer:I = 0x7f0201cd

.field public static final ic_drawer_accounts_expand:I = 0x7f0201ce

.field public static final ic_drawer_schedule:I = 0x7f0201cf

.field public static final ic_drawer_schedule_light:I = 0x7f0201d0

.field public static final ic_email:I = 0x7f0201d1

.field public static final ic_hide_password:I = 0x7f0201d2

.field public static final ic_latest_version:I = 0x7f0201d3

.field public static final ic_locked_appview:I = 0x7f0201d4

.field public static final ic_mail_appview:I = 0x7f0201d5

.field public static final ic_manager:I = 0x7f0201d6

.field public static final ic_map_appview:I = 0x7f0201d7

.field public static final ic_more_arrow_right:I = 0x7f0201d8

.field public static final ic_pause:I = 0x7f0201d9

.field public static final ic_pencil:I = 0x7f0201da

.field public static final ic_permissions_appview:I = 0x7f0201db

.field public static final ic_play:I = 0x7f0201dc

.field public static final ic_plus_amber:I = 0x7f0201dd

.field public static final ic_plus_black:I = 0x7f0201de

.field public static final ic_plus_blue_grey:I = 0x7f0201df

.field public static final ic_plus_brown:I = 0x7f0201e0

.field public static final ic_plus_deep_purple:I = 0x7f0201e1

.field public static final ic_plus_green:I = 0x7f0201e2

.field public static final ic_plus_grey:I = 0x7f0201e3

.field public static final ic_plus_indigo:I = 0x7f0201e4

.field public static final ic_plus_light_blue:I = 0x7f0201e5

.field public static final ic_plus_light_green:I = 0x7f0201e6

.field public static final ic_plus_lime:I = 0x7f0201e7

.field public static final ic_plus_orange:I = 0x7f0201e8

.field public static final ic_plus_orange_700:I = 0x7f0201e9

.field public static final ic_plus_pink:I = 0x7f0201ea

.field public static final ic_plus_red:I = 0x7f0201eb

.field public static final ic_plus_teal:I = 0x7f0201ec

.field public static final ic_plus_white:I = 0x7f0201ed

.field public static final ic_refresh:I = 0x7f0201ee

.field public static final ic_reply:I = 0x7f0201ef

.field public static final ic_reply_white_18dp:I = 0x7f0201f0

.field public static final ic_schedule:I = 0x7f0201f1

.field public static final ic_search:I = 0x7f0201f2

.field public static final ic_search_icon:I = 0x7f0201f3

.field public static final ic_search_no_results:I = 0x7f0201f4

.field public static final ic_see_more_comments:I = 0x7f0201f5

.field public static final ic_send_feedback:I = 0x7f0201f6

.field public static final ic_show_password:I = 0x7f0201f7

.field public static final ic_stat_aptoide_notification:I = 0x7f0201f8

.field public static final ic_stat_facebook_notification:I = 0x7f0201f9

.field public static final ic_store:I = 0x7f0201fa

.field public static final ic_timestamp:I = 0x7f0201fb

.field public static final ic_trusted:I = 0x7f0201fc

.field public static final ic_up_arrow:I = 0x7f0201fd

.field public static final ic_update_version:I = 0x7f0201fe

.field public static final ic_user_icon:I = 0x7f0201ff

.field public static final ic_user_icon_black:I = 0x7f020200

.field public static final ic_web_site:I = 0x7f020201

.field public static final ic_widget_launcher:I = 0x7f020202

.field public static final icon_closed_eye:I = 0x7f020203

.field public static final icon_logged_in:I = 0x7f020204

.field public static final icon_open_eye:I = 0x7f020205

.field public static final icon_password:I = 0x7f020206

.field public static final icon_store:I = 0x7f020207

.field public static final icon_timeline:I = 0x7f020208

.field public static final icon_user:I = 0x7f020209

.field public static final img_navdrawer_wallpaper:I = 0x7f02020a

.field public static final img_refresh:I = 0x7f02020b

.field public static final img_tvicon:I = 0x7f02020c

.field public static final indicator_dot_grey:I = 0x7f02020d

.field public static final indicator_dot_white:I = 0x7f02020e

.field public static final indigo_btn_default_disabled_focused_holo_light:I = 0x7f02020f

.field public static final indigo_btn_default_disabled_holo_light:I = 0x7f020210

.field public static final indigo_btn_default_focused_holo_light:I = 0x7f020211

.field public static final indigo_btn_default_holo_light:I = 0x7f020212

.field public static final indigo_btn_default_normal_holo_light:I = 0x7f020213

.field public static final indigo_btn_default_pressed_holo_light:I = 0x7f020214

.field public static final key:I = 0x7f020215

.field public static final layer_1:I = 0x7f020216

.field public static final layer_11:I = 0x7f020217

.field public static final layer_11_copy_3:I = 0x7f020218

.field public static final layer_13:I = 0x7f020219

.field public static final layer_13_copy:I = 0x7f02021a

.field public static final layer_14:I = 0x7f02021b

.field public static final layer_14_copy:I = 0x7f02021c

.field public static final layer_2:I = 0x7f02021d

.field public static final left_wizard_arrow:I = 0x7f02021e

.field public static final left_wizard_arrow_dark:I = 0x7f02021f

.field public static final light_blue_btn_default_disabled_focused_holo_light:I = 0x7f020220

.field public static final light_blue_btn_default_disabled_holo_light:I = 0x7f020221

.field public static final light_blue_btn_default_focused_holo_light:I = 0x7f020222

.field public static final light_blue_btn_default_normal_holo_light:I = 0x7f020223

.field public static final light_blue_btn_default_pressed_holo_light:I = 0x7f020224

.field public static final light_green_btn_default_disabled_focused_holo_light:I = 0x7f020225

.field public static final light_green_btn_default_disabled_holo_light:I = 0x7f020226

.field public static final light_green_btn_default_focused_holo_light:I = 0x7f020227

.field public static final light_green_btn_default_holo_light:I = 0x7f020228

.field public static final light_green_btn_default_normal_holo_light:I = 0x7f020229

.field public static final light_green_btn_default_pressed_holo_light:I = 0x7f02022a

.field public static final lightblue_btn_default_holo_light:I = 0x7f02022b

.field public static final lime_btn_default_disabled_focused_holo_light:I = 0x7f02022c

.field public static final lime_btn_default_disabled_holo_light:I = 0x7f02022d

.field public static final lime_btn_default_focused_holo_light:I = 0x7f02022e

.field public static final lime_btn_default_holo_light:I = 0x7f02022f

.field public static final lime_btn_default_normal_holo_light:I = 0x7f020230

.field public static final lime_btn_default_pressed_holo_light:I = 0x7f020231

.field public static final list_selector_background:I = 0x7f020232

.field public static final list_selector_background_disabled:I = 0x7f020233

.field public static final list_selector_background_focus:I = 0x7f020234

.field public static final list_selector_background_longpress:I = 0x7f020235

.field public static final list_selector_background_pressed:I = 0x7f020236

.field public static final list_selector_background_transition:I = 0x7f020237

.field public static final logo_aptoide:I = 0x7f020238

.field public static final logo_toolbar:I = 0x7f020239

.field public static final media_pause:I = 0x7f02023a

.field public static final media_play:I = 0x7f02023b

.field public static final messenger_bubble_large_blue:I = 0x7f02023c

.field public static final messenger_bubble_large_white:I = 0x7f02023d

.field public static final messenger_bubble_small_blue:I = 0x7f02023e

.field public static final messenger_bubble_small_white:I = 0x7f02023f

.field public static final messenger_button_blue_bg_round:I = 0x7f020240

.field public static final messenger_button_blue_bg_selector:I = 0x7f020241

.field public static final messenger_button_send_round_shadow:I = 0x7f020242

.field public static final messenger_button_white_bg_round:I = 0x7f020243

.field public static final messenger_button_white_bg_selector:I = 0x7f020244

.field public static final navigation_empty_icon:I = 0x7f020245

.field public static final needs_license:I = 0x7f020246

.field public static final notification_action_background:I = 0x7f020247

.field public static final notification_bg:I = 0x7f020248

.field public static final notification_bg_low:I = 0x7f020249

.field public static final notification_bg_low_normal:I = 0x7f02024a

.field public static final notification_bg_low_pressed:I = 0x7f02024b

.field public static final notification_bg_normal:I = 0x7f02024c

.field public static final notification_bg_normal_pressed:I = 0x7f02024d

.field public static final notification_icon_background:I = 0x7f02024e

.field public static final notification_template_icon_bg:I = 0x7f0202db

.field public static final notification_template_icon_low_bg:I = 0x7f0202dc

.field public static final notification_tile_bg:I = 0x7f02024f

.field public static final notify_panel_notification_icon_bg:I = 0x7f020250

.field public static final number_of_apps:I = 0x7f020251

.field public static final oculo:I = 0x7f020252

.field public static final orange_btn_default_disabled_focused_holo_light:I = 0x7f020253

.field public static final orange_btn_default_disabled_holo_light:I = 0x7f020254

.field public static final orange_btn_default_focused_holo_light:I = 0x7f020255

.field public static final orange_btn_default_holo_light:I = 0x7f020256

.field public static final orange_btn_default_normal_holo_light:I = 0x7f020257

.field public static final orange_btn_default_pressed_holo_light:I = 0x7f020258

.field public static final orange_button:I = 0x7f020259

.field public static final oval_rectangle_shape:I = 0x7f02025a

.field public static final overlay_focused:I = 0x7f02025b

.field public static final overlay_pressed:I = 0x7f02025c

.field public static final paypal_btn_default_disabled_focused_holo_light:I = 0x7f02025d

.field public static final paypal_btn_default_disabled_holo_light:I = 0x7f02025e

.field public static final paypal_btn_default_focused_holo_light:I = 0x7f02025f

.field public static final paypal_btn_default_holo_light:I = 0x7f020260

.field public static final paypal_btn_default_normal_holo_light:I = 0x7f020261

.field public static final paypal_btn_default_pressed_holo_light:I = 0x7f020262

.field public static final pink_btn_default_disabled_focused_holo_light:I = 0x7f020263

.field public static final pink_btn_default_disabled_holo_light:I = 0x7f020264

.field public static final pink_btn_default_focused_holo_light:I = 0x7f020265

.field public static final pink_btn_default_holo_light:I = 0x7f020266

.field public static final pink_btn_default_normal_holo_light:I = 0x7f020267

.field public static final pink_btn_default_pressed_holo_light:I = 0x7f020268

.field public static final placeholder_144x240:I = 0x7f020269

.field public static final placeholder_256x160:I = 0x7f02026a

.field public static final placeholder_300x300:I = 0x7f02026b

.field public static final placeholder_705x345:I = 0x7f02026c

.field public static final pro_green:I = 0x7f02026d

.field public static final progress_bar:I = 0x7f02026e

.field public static final progress_bar_background:I = 0x7f02026f

.field public static final psts_background_tab:I = 0x7f020270

.field public static final rating_bar:I = 0x7f020271

.field public static final rating_bar_big:I = 0x7f020272

.field public static final rating_bar_big_empty:I = 0x7f020273

.field public static final rating_bar_big_full:I = 0x7f020274

.field public static final rating_bar_extra_small:I = 0x7f020275

.field public static final rating_bar_grid_item:I = 0x7f020276

.field public static final rating_bar_medium:I = 0x7f020277

.field public static final rating_bar_review:I = 0x7f020278

.field public static final rating_bar_small:I = 0x7f020279

.field public static final rating_shape:I = 0x7f02027a

.field public static final red_btn_default_disabled_focused_holo_light:I = 0x7f02027b

.field public static final red_btn_default_disabled_holo_light:I = 0x7f02027c

.field public static final red_btn_default_focused_holo_light:I = 0x7f02027d

.field public static final red_btn_default_holo_light:I = 0x7f02027e

.field public static final red_btn_default_normal_holo_light:I = 0x7f02027f

.field public static final red_btn_default_pressed_holo_light:I = 0x7f020280

.field public static final review_icon:I = 0x7f020281

.field public static final right_wizard_arrow:I = 0x7f020282

.field public static final right_wizard_arrow_dark:I = 0x7f020283

.field public static final round_circle_gradient:I = 0x7f020284

.field public static final round_circle_white:I = 0x7f020285

.field public static final rounded_corners_bg:I = 0x7f020286

.field public static final rounded_corners_dark:I = 0x7f020287

.field public static final rounded_corners_dark_gray:I = 0x7f020288

.field public static final rounded_corners_primary:I = 0x7f020289

.field public static final rounded_corners_primary2:I = 0x7f02028a

.field public static final rounded_corners_primary3:I = 0x7f02028b

.field public static final rounded_corners_transparent_white:I = 0x7f02028c

.field public static final rounded_corners_white:I = 0x7f02028d

.field public static final search_background:I = 0x7f02028e

.field public static final search_button_background:I = 0x7f02028f

.field public static final search_button_shape_layout:I = 0x7f020290

.field public static final search_buttons_background:I = 0x7f020291

.field public static final search_row_bottom_line:I = 0x7f020292

.field public static final search_row_store_background:I = 0x7f020293

.field public static final search_widget:I = 0x7f020294

.field public static final selected_payment_icon:I = 0x7f020295

.field public static final shape_gray_border:I = 0x7f020296

.field public static final social_icon:I = 0x7f020297

.field public static final stat_sys_download:I = 0x7f020298

.field public static final stat_sys_download_anim0:I = 0x7f020299

.field public static final stat_sys_download_anim1:I = 0x7f02029a

.field public static final stat_sys_download_anim2:I = 0x7f02029b

.field public static final stat_sys_download_anim3:I = 0x7f02029c

.field public static final stat_sys_download_anim4:I = 0x7f02029d

.field public static final stat_sys_download_anim5:I = 0x7f02029e

.field public static final tabs_pattern:I = 0x7f02029f

.field public static final tabs_pattern_dark:I = 0x7f0202a0

.field public static final teal_btn_default_disabled_focused_holo_light:I = 0x7f0202a1

.field public static final teal_btn_default_disabled_holo_light:I = 0x7f0202a2

.field public static final teal_btn_default_focused_holo_light:I = 0x7f0202a3

.field public static final teal_btn_default_holo_light:I = 0x7f0202a4

.field public static final teal_btn_default_normal_holo_light:I = 0x7f0202a5

.field public static final teal_btn_default_pressed_holo_light:I = 0x7f0202a6

.field public static final thumbs_down:I = 0x7f0202a7

.field public static final thumbs_up:I = 0x7f0202a8

.field public static final timeline_arrow_get_app:I = 0x7f0202a9

.field public static final timeline_arrow_get_app_dark:I = 0x7f0202aa

.field public static final timeline_comment:I = 0x7f0202ab

.field public static final timeline_heart:I = 0x7f0202ac

.field public static final timeline_post_background_dark:I = 0x7f0202ad

.field public static final timeline_post_background_light:I = 0x7f0202ae

.field public static final timeline_share:I = 0x7f0202af

.field public static final timeline_update_app_dark:I = 0x7f0202b0

.field public static final twitch_logo:I = 0x7f0202b1

.field public static final twitter_logo:I = 0x7f0202b2

.field public static final unitel_btn_default_disabled_focused_holo_light:I = 0x7f0202b3

.field public static final unitel_btn_default_disabled_holo_light:I = 0x7f0202b4

.field public static final unitel_btn_default_focused_holo_light:I = 0x7f0202b5

.field public static final unitel_btn_default_holo_light:I = 0x7f0202b6

.field public static final unitel_btn_default_normal_holo_light:I = 0x7f0202b7

.field public static final unitel_btn_default_pressed_holo_light:I = 0x7f0202b8

.field public static final update_darkgrey:I = 0x7f0202b9

.field public static final upload_camera:I = 0x7f0202ba

.field public static final upload_dialog_gallery:I = 0x7f0202bb

.field public static final user_account_grey:I = 0x7f0202bc

.field public static final user_account_white:I = 0x7f0202bd

.field public static final user_default:I = 0x7f0202be

.field public static final user_shape:I = 0x7f0202bf

.field public static final user_shape_mini_icon:I = 0x7f0202c0

.field public static final vector_smart_object:I = 0x7f0202c1

.field public static final virus:I = 0x7f0202c2

.field public static final virus_flag:I = 0x7f0202c3

.field public static final visa_btn_default_disabled_focused_holo_light:I = 0x7f0202c4

.field public static final visa_btn_default_disabled_holo_light:I = 0x7f0202c5

.field public static final visa_btn_default_focused_holo_light:I = 0x7f0202c6

.field public static final visa_btn_default_holo_light:I = 0x7f0202c7

.field public static final visa_btn_default_normal_holo_light:I = 0x7f0202c8

.field public static final visa_btn_default_pressed_holo_light:I = 0x7f0202c9

.field public static final white_border_circle:I = 0x7f0202ca

.field public static final white_border_square:I = 0x7f0202cb

.field public static final white_card:I = 0x7f0202cc

.field public static final white_star:I = 0x7f0202cd

.field public static final white_star_review:I = 0x7f0202ce

.field public static final wizard_apps:I = 0x7f0202cf

.field public static final wizard_apps_feed:I = 0x7f0202d0

.field public static final wizard_community:I = 0x7f0202d1

.field public static final wizard_custom_indicator:I = 0x7f0202d2

.field public static final wizard_custom_register_button:I = 0x7f0202d3

.field public static final wizard_magnify:I = 0x7f0202d4

.field public static final wizard_next_arrow:I = 0x7f0202d5

.field public static final working_well:I = 0x7f0202d6

.field public static final working_well_btn:I = 0x7f0202d7

.field public static final yellow_star:I = 0x7f0202d8

.field public static final yellow_star_review:I = 0x7f0202d9

.field public static final youtube_logo:I = 0x7f0202da


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
