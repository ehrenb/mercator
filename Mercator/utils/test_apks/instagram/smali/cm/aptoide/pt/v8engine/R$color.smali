.class public final Lcm/aptoide/pt/v8engine/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/pt/v8engine/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c00f5

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c00f6

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0c00f7

.field public static final abc_color_highlight_material:I = 0x7f0c00f8

.field public static final abc_hint_foreground_material_dark:I = 0x7f0c00f9

.field public static final abc_hint_foreground_material_light:I = 0x7f0c00fa

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0001

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c00fb

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c00fc

.field public static final abc_primary_text_material_dark:I = 0x7f0c00fd

.field public static final abc_primary_text_material_light:I = 0x7f0c00fe

.field public static final abc_search_url_text:I = 0x7f0c00ff

.field public static final abc_search_url_text_normal:I = 0x7f0c0002

.field public static final abc_search_url_text_pressed:I = 0x7f0c0003

.field public static final abc_search_url_text_selected:I = 0x7f0c0004

.field public static final abc_secondary_text_material_dark:I = 0x7f0c0100

.field public static final abc_secondary_text_material_light:I = 0x7f0c0101

.field public static final abc_tint_btn_checkable:I = 0x7f0c0102

.field public static final abc_tint_default:I = 0x7f0c0103

.field public static final abc_tint_edittext:I = 0x7f0c0104

.field public static final abc_tint_seek_thumb:I = 0x7f0c0105

.field public static final abc_tint_spinner:I = 0x7f0c0106

.field public static final abc_tint_switch_thumb:I = 0x7f0c0107

.field public static final abc_tint_switch_track:I = 0x7f0c0108

.field public static final accent_material_dark:I = 0x7f0c0005

.field public static final accent_material_light:I = 0x7f0c0006

.field public static final activity_payment_dark_gray:I = 0x7f0c0007

.field public static final activity_payment_gray:I = 0x7f0c0008

.field public static final activity_payment_green:I = 0x7f0c0009

.field public static final activity_payment_light_gray:I = 0x7f0c000a

.field public static final almost_black:I = 0x7f0c000b

.field public static final amber:I = 0x7f0c000c

.field public static final amber_700:I = 0x7f0c000d

.field public static final app_view_follow_orange:I = 0x7f0c000e

.field public static final app_view_gray:I = 0x7f0c000f

.field public static final appstimeline_grey:I = 0x7f0c0010

.field public static final appstimeline_recommends_title:I = 0x7f0c0011

.field public static final appview_rating_text:I = 0x7f0c0012

.field public static final aptoide_color_focused:I = 0x7f0c0013

.field public static final aptoide_color_pressed:I = 0x7f0c0014

.field public static final aptoide_lite:I = 0x7f0c0015

.field public static final aptoide_orange:I = 0x7f0c0016

.field public static final aptoide_orange_700:I = 0x7f0c0017

.field public static final aptoide_tv:I = 0x7f0c0018

.field public static final background_floating_material_dark:I = 0x7f0c0019

.field public static final background_floating_material_light:I = 0x7f0c001a

.field public static final background_holo_light:I = 0x7f0c001b

.field public static final background_material_dark:I = 0x7f0c001c

.field public static final background_material_light:I = 0x7f0c001d

.field public static final background_window:I = 0x7f0c001e

.field public static final black:I = 0x7f0c001f

.field public static final black_alfa_gradient_end:I = 0x7f0c0020

.field public static final black_alfa_gradient_start:I = 0x7f0c0021

.field public static final black_gradient_end:I = 0x7f0c0022

.field public static final black_gradient_start:I = 0x7f0c0023

.field public static final blue:I = 0x7f0c0024

.field public static final blue_700:I = 0x7f0c0025

.field public static final bluegrey:I = 0x7f0c0026

.field public static final bluegrey_700:I = 0x7f0c0027

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c0028

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c0029

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c002a

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c002b

.field public static final bright_foreground_material_dark:I = 0x7f0c002c

.field public static final bright_foreground_material_light:I = 0x7f0c002d

.field public static final brown:I = 0x7f0c002e

.field public static final brown_700:I = 0x7f0c002f

.field public static final button_material_dark:I = 0x7f0c0030

.field public static final button_material_light:I = 0x7f0c0031

.field public static final card_store_title:I = 0x7f0c0032

.field public static final cardview_dark_background:I = 0x7f0c0033

.field public static final cardview_light_background:I = 0x7f0c0034

.field public static final cardview_shadow_end_color:I = 0x7f0c0035

.field public static final cardview_shadow_start_color:I = 0x7f0c0036

.field public static final com_facebook_blue:I = 0x7f0c0037

.field public static final com_facebook_button_background_color:I = 0x7f0c0038

.field public static final com_facebook_button_background_color_disabled:I = 0x7f0c0039

.field public static final com_facebook_button_background_color_focused:I = 0x7f0c003a

.field public static final com_facebook_button_background_color_focused_disabled:I = 0x7f0c003b

.field public static final com_facebook_button_background_color_pressed:I = 0x7f0c003c

.field public static final com_facebook_button_background_color_selected:I = 0x7f0c003d

.field public static final com_facebook_button_border_color_focused:I = 0x7f0c003e

.field public static final com_facebook_button_login_silver_background_color:I = 0x7f0c003f

.field public static final com_facebook_button_login_silver_background_color_pressed:I = 0x7f0c0040

.field public static final com_facebook_button_send_background_color:I = 0x7f0c0041

.field public static final com_facebook_button_send_background_color_pressed:I = 0x7f0c0042

.field public static final com_facebook_button_text_color:I = 0x7f0c0109

.field public static final com_facebook_device_auth_text:I = 0x7f0c0043

.field public static final com_facebook_likeboxcountview_border_color:I = 0x7f0c0044

.field public static final com_facebook_likeboxcountview_text_color:I = 0x7f0c0045

.field public static final com_facebook_likeview_text_color:I = 0x7f0c0046

.field public static final com_facebook_messenger_blue:I = 0x7f0c0047

.field public static final com_facebook_send_button_text_color:I = 0x7f0c010a

.field public static final com_facebook_share_button_text_color:I = 0x7f0c0048

.field public static final com_smart_login_code:I = 0x7f0c0049

.field public static final comment_gray:I = 0x7f0c004a

.field public static final comments_footer:I = 0x7f0c004b

.field public static final common_google_signin_btn_text_dark:I = 0x7f0c010b

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f0c004c

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0c004d

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0c004e

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0c004f

.field public static final common_google_signin_btn_text_light:I = 0x7f0c010c

.field public static final common_google_signin_btn_text_light_default:I = 0x7f0c0050

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0c0051

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0c0052

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0c0053

.field public static final create_store_edit_text_background:I = 0x7f0c0054

.field public static final create_store_edit_text_color:I = 0x7f0c0055

.field public static final create_store_hint_color:I = 0x7f0c0056

.field public static final create_store_title_color:I = 0x7f0c0057

.field public static final crimson:I = 0x7f0c0058

.field public static final dark_custom_gray:I = 0x7f0c0059

.field public static final dark_gray:I = 0x7f0c005a

.field public static final darker_gray:I = 0x7f0c005b

.field public static final deeppurple:I = 0x7f0c005c

.field public static final deeppurple_700:I = 0x7f0c005d

.field public static final default_color:I = 0x7f0c005e

.field public static final default_color2:I = 0x7f0c005f

.field public static final default_color_700:I = 0x7f0c0060

.field public static final default_progress_bar_color:I = 0x7f0c0061

.field public static final design_bottom_navigation_shadow_color:I = 0x7f0c0062

.field public static final design_error:I = 0x7f0c010d

.field public static final design_fab_shadow_end_color:I = 0x7f0c0063

.field public static final design_fab_shadow_mid_color:I = 0x7f0c0064

.field public static final design_fab_shadow_start_color:I = 0x7f0c0065

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0c0066

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0c0067

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0c0068

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0c0069

.field public static final design_snackbar_background_color:I = 0x7f0c006a

.field public static final design_textinput_error_color_dark:I = 0x7f0c006b

.field public static final design_textinput_error_color_light:I = 0x7f0c006c

.field public static final design_tint_password_toggle:I = 0x7f0c010e

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c006d

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c006e

.field public static final dim_foreground_material_dark:I = 0x7f0c006f

.field public static final dim_foreground_material_light:I = 0x7f0c0070

.field public static final dimgray:I = 0x7f0c0071

.field public static final displayable_rate_and_review_background:I = 0x7f0c0072

.field public static final facebook:I = 0x7f0c0073

.field public static final followed_stores_store_name_color:I = 0x7f0c0074

.field public static final foreground_material_dark:I = 0x7f0c0075

.field public static final foreground_material_light:I = 0x7f0c0076

.field public static final gold:I = 0x7f0c0077

.field public static final gold_700:I = 0x7f0c0078

.field public static final green:I = 0x7f0c0079

.field public static final green_700:I = 0x7f0c007a

.field public static final green_tint_default:I = 0x7f0c007b

.field public static final greenapple:I = 0x7f0c007c

.field public static final grey:I = 0x7f0c007d

.field public static final grey_700:I = 0x7f0c007e

.field public static final happyblue:I = 0x7f0c007f

.field public static final highlighted_text_material_dark:I = 0x7f0c0080

.field public static final highlighted_text_material_light:I = 0x7f0c0081

.field public static final indigo:I = 0x7f0c0082

.field public static final indigo_700:I = 0x7f0c0083

.field public static final light_custom_gray:I = 0x7f0c0084

.field public static final lightblue:I = 0x7f0c0085

.field public static final lightblue_700:I = 0x7f0c0086

.field public static final lighter_custom_gray:I = 0x7f0c0087

.field public static final lightgreen:I = 0x7f0c0088

.field public static final lightgreen_700:I = 0x7f0c0089

.field public static final lightsky:I = 0x7f0c008a

.field public static final lightsky_700:I = 0x7f0c008b

.field public static final lime:I = 0x7f0c008c

.field public static final lime_700:I = 0x7f0c008d

.field public static final link:I = 0x7f0c008e

.field public static final magenta:I = 0x7f0c008f

.field public static final magenta_700:I = 0x7f0c0090

.field public static final maroon:I = 0x7f0c0091

.field public static final maroon_700:I = 0x7f0c0092

.field public static final material_blue_grey_800:I = 0x7f0c0093

.field public static final material_blue_grey_900:I = 0x7f0c0094

.field public static final material_blue_grey_950:I = 0x7f0c0095

.field public static final material_deep_teal_200:I = 0x7f0c0096

.field public static final material_deep_teal_500:I = 0x7f0c0097

.field public static final material_grey_100:I = 0x7f0c0098

.field public static final material_grey_300:I = 0x7f0c0099

.field public static final material_grey_50:I = 0x7f0c009a

.field public static final material_grey_600:I = 0x7f0c009b

.field public static final material_grey_800:I = 0x7f0c009c

.field public static final material_grey_850:I = 0x7f0c009d

.field public static final material_grey_900:I = 0x7f0c009e

.field public static final medium_custom_gray:I = 0x7f0c009f

.field public static final medium_gray:I = 0x7f0c00a0

.field public static final midnight:I = 0x7f0c00a1

.field public static final midnight_700:I = 0x7f0c00a2

.field public static final navdrawer_body_text_1_inverse:I = 0x7f0c00a3

.field public static final navdrawer_body_text_2_inverse:I = 0x7f0c00a4

.field public static final notification_action_color_filter:I = 0x7f0c0000

.field public static final notification_icon_bg_color:I = 0x7f0c00a5

.field public static final notification_material_background_media_default_color:I = 0x7f0c00a6

.field public static final orange:I = 0x7f0c00a7

.field public static final orange_700:I = 0x7f0c00a8

.field public static final overlay_black:I = 0x7f0c00a9

.field public static final pink:I = 0x7f0c00aa

.field public static final pink_700:I = 0x7f0c00ab

.field public static final primary_dark_material_dark:I = 0x7f0c00ac

.field public static final primary_dark_material_light:I = 0x7f0c00ad

.field public static final primary_material_dark:I = 0x7f0c00ae

.field public static final primary_material_light:I = 0x7f0c00af

.field public static final primary_text_default_material_dark:I = 0x7f0c00b0

.field public static final primary_text_default_material_light:I = 0x7f0c00b1

.field public static final primary_text_disabled_material_dark:I = 0x7f0c00b2

.field public static final primary_text_disabled_material_light:I = 0x7f0c00b3

.field public static final psts_background_tab_pressed:I = 0x7f0c00b4

.field public static final psts_background_tab_pressed_ripple:I = 0x7f0c00b5

.field public static final recommended_stores_numbers_color:I = 0x7f0c00b6

.field public static final red:I = 0x7f0c00b7

.field public static final red_700:I = 0x7f0c00b8

.field public static final reviews_helpful_stroke_button:I = 0x7f0c00b9

.field public static final ripple_material_dark:I = 0x7f0c00ba

.field public static final ripple_material_light:I = 0x7f0c00bb

.field public static final screenshots_background_color:I = 0x7f0c00bc

.field public static final seagreen:I = 0x7f0c00bd

.field public static final seagreen_700:I = 0x7f0c00be

.field public static final search_sponspored_label_background:I = 0x7f0c00bf

.field public static final secondary_text_default_material_dark:I = 0x7f0c00c0

.field public static final secondary_text_default_material_light:I = 0x7f0c00c1

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c00c2

.field public static final secondary_text_disabled_material_light:I = 0x7f0c00c3

.field public static final semi_transparent_black:I = 0x7f0c00c4

.field public static final separators_grey:I = 0x7f0c00c5

.field public static final silver:I = 0x7f0c00c6

.field public static final silver_dark:I = 0x7f0c00c7

.field public static final slategray:I = 0x7f0c00c8

.field public static final slategray_700:I = 0x7f0c00c9

.field public static final springgreen:I = 0x7f0c00ca

.field public static final springgreen_700:I = 0x7f0c00cb

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c00cc

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c00cd

.field public static final switch_thumb_material_dark:I = 0x7f0c010f

.field public static final switch_thumb_material_light:I = 0x7f0c0110

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c00ce

.field public static final switch_thumb_normal_material_light:I = 0x7f0c00cf

.field public static final teal:I = 0x7f0c00d0

.field public static final teal_700:I = 0x7f0c00d1

.field public static final timeline_separator:I = 0x7f0c00d2

.field public static final transparent_amber:I = 0x7f0c00d3

.field public static final transparent_black:I = 0x7f0c00d4

.field public static final transparent_blue:I = 0x7f0c00d5

.field public static final transparent_bluegrey:I = 0x7f0c00d6

.field public static final transparent_brown:I = 0x7f0c00d7

.field public static final transparent_deeppurple:I = 0x7f0c00d8

.field public static final transparent_digitallydifferent:I = 0x7f0c00d9

.field public static final transparent_dimgray:I = 0x7f0c00da

.field public static final transparent_eocean:I = 0x7f0c00db

.field public static final transparent_gold:I = 0x7f0c00dc

.field public static final transparent_green:I = 0x7f0c00dd

.field public static final transparent_grey:I = 0x7f0c00de

.field public static final transparent_indigo:I = 0x7f0c00df

.field public static final transparent_lightblue:I = 0x7f0c00e0

.field public static final transparent_lightgreen:I = 0x7f0c00e1

.field public static final transparent_lightsky:I = 0x7f0c00e2

.field public static final transparent_lime:I = 0x7f0c00e3

.field public static final transparent_magalhaes:I = 0x7f0c00e4

.field public static final transparent_magenta:I = 0x7f0c00e5

.field public static final transparent_maroon:I = 0x7f0c00e6

.field public static final transparent_midnight:I = 0x7f0c00e7

.field public static final transparent_orange:I = 0x7f0c00e8

.field public static final transparent_pink:I = 0x7f0c00e9

.field public static final transparent_red:I = 0x7f0c00ea

.field public static final transparent_seagreen:I = 0x7f0c00eb

.field public static final transparent_silver:I = 0x7f0c00ec

.field public static final transparent_slategray:I = 0x7f0c00ed

.field public static final transparent_springgreen:I = 0x7f0c00ee

.field public static final transparent_teal:I = 0x7f0c00ef

.field public static final transparent_timwe:I = 0x7f0c00f0

.field public static final transparent_white:I = 0x7f0c00f1

.field public static final white:I = 0x7f0c00f2

.field public static final working_well_text_color:I = 0x7f0c00f3

.field public static final yellow:I = 0x7f0c00f4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
