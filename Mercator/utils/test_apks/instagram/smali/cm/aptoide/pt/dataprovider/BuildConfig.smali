.class public final Lcm/aptoide/pt/dataprovider/BuildConfig;
.super Ljava/lang/Object;
.source "BuildConfig.java"


# static fields
.field public static final APPLICATION_ID:Ljava/lang/String; = "cm.aptoide.pt.dataprovider"

.field public static final APTOIDE_WEB_SERVICES_APTWORDS_HOST:Ljava/lang/String; = "webservices.aptwords.net"

.field public static final APTOIDE_WEB_SERVICES_APTWORDS_SCHEME:Ljava/lang/String; = "http"

.field public static final APTOIDE_WEB_SERVICES_HOST:Ljava/lang/String; = "webservices.aptoide.com"

.field public static final APTOIDE_WEB_SERVICES_SCHEME:Ljava/lang/String; = "https"

.field public static final APTOIDE_WEB_SERVICES_V7_HOST:Ljava/lang/String; = "ws75.aptoide.com"

.field public static final APTOIDE_WEB_SERVICES_WRITE_V7_HOST:Ljava/lang/String; = "ws75-primary.aptoide.com"

.field public static final BUILD_TYPE:Ljava/lang/String; = "release"

.field public static final DEBUG:Z = false

.field public static final FLAVOR:Ljava/lang/String; = "prod"

.field public static final VERSION_CODE:I = -0x1

.field public static final VERSION_NAME:Ljava/lang/String; = ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
