.class public Lcm/aptoide/pt/preferences/managed/ManagedKeys;
.super Ljava/lang/Object;
.source "ManagedKeys.java"


# static fields
.field public static final ACCESS:Ljava/lang/String; = "access"

.field public static final ACCESS_CONFIRMED:Ljava/lang/String; = "access_confirmed"

.field public static final ALLOW_ROOT_INSTALATION:Ljava/lang/String; = "allowRoot"

.field public static final ANIMATIONS_ENABLED:Ljava/lang/String; = "animationsEnabled"

.field public static final CHECK_AUTO_UPDATE:Ljava/lang/String; = "checkautoupdate"

.field public static final DEBUG:Ljava/lang/String; = "debugmode"

.field public static final FIRST_RUN_V7:Ljava/lang/String; = "firstrun"

.field public static final FORCE_COUNTRY:Ljava/lang/String; = "forcecountry"

.field public static final FORCE_SERVER_REFRESH_FLAG:Ljava/lang/String; = "forceServerRefreshFlag"

.field public static final GENERAL_DOWNLOADS_MOBILE:Ljava/lang/String; = "generalnetworkmobile"

.field public static final GENERAL_DOWNLOADS_WIFI:Ljava/lang/String; = "generalnetworkwifi"

.field public static final HWSPECS_FILTER:Ljava/lang/String; = "hwspecsChkBox"

.field public static final LAST_PUSH_NOTIFICATION_ID:Ljava/lang/String; = "lastPushNotificationId"

.field public static final LAST_UPDATES_KEY:Ljava/lang/String; = "last_updates_key"

.field public static final MATURE_CHECK_BOX:Ljava/lang/String; = "matureChkBox"

.field public static final MAX_FILE_CACHE:Ljava/lang/String; = "maxFileCache"

.field public static final NOTIFICATION_TYPE:Ljava/lang/String; = "notificationtype"

.field public static final PREF_ALWAYS_UPDATE:Ljava/lang/String; = "dev_mode_always_update"

.field public static final PREF_NEEDS_SQLITE_DB_MIGRATION:Ljava/lang/String; = "needsSqliteDbMigration"

.field public static final PREF_SHOW_UPDATE_NOTIFICATION:Ljava/lang/String; = "showUpdatesNotification"

.field public static final PRIVACY_CONFIRMATION:Ljava/lang/String; = "privacyconfirmation"

.field public static final SCHEDULE_DOWNLOAD_SETTING:Ljava/lang/String; = "schDwnBox"

.field public static final SHOW_SHARE_PREVIEW:Ljava/lang/String; = "donotshowmeagain"

.field public static final UPDATES_FILTER_ALPHA_BETA_KEY:Ljava/lang/String; = "updatesFilterAlphaBetaKey"

.field public static final UPDATES_SYSTEM_APPS_KEY:Ljava/lang/String; = "updatesSystemAppsKey"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
