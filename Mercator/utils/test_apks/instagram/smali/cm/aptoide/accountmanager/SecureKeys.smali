.class Lcm/aptoide/accountmanager/SecureKeys;
.super Ljava/lang/Object;
.source "SecureKeys.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final LOGIN_MODE:Ljava/lang/String; = "aptoide_account_manager_login_mode"

.field public static final MATURE_SWITCH:Ljava/lang/String; = "aptoide_account_manager_mature_switch"

.field public static final QUEUE_NAME:Ljava/lang/String; = "queueName"

.field public static final REFRESH_TOKEN:Ljava/lang/String; = "refresh_token"

.field public static final REPO_AVATAR:Ljava/lang/String; = "storeAvatar"

.field public static final REPO_THEME:Ljava/lang/String; = "storeTheme"

.field public static final USER_AVATAR:Ljava/lang/String; = "useravatar"

.field public static final USER_EMAIL:Ljava/lang/String; = "usernameLogin"

.field public static final USER_ID:Ljava/lang/String; = "userId"

.field public static final USER_NICK_NAME:Ljava/lang/String; = "username"

.field public static final USER_REPO:Ljava/lang/String; = "userRepo"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
