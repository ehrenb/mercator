.class public final Lcm/aptoide/accountmanager/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcm/aptoide/accountmanager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c00f5

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c00f6

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0c00f7

.field public static final abc_color_highlight_material:I = 0x7f0c00f8

.field public static final abc_hint_foreground_material_dark:I = 0x7f0c00f9

.field public static final abc_hint_foreground_material_light:I = 0x7f0c00fa

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0001

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c00fb

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c00fc

.field public static final abc_primary_text_material_dark:I = 0x7f0c00fd

.field public static final abc_primary_text_material_light:I = 0x7f0c00fe

.field public static final abc_search_url_text:I = 0x7f0c00ff

.field public static final abc_search_url_text_normal:I = 0x7f0c0002

.field public static final abc_search_url_text_pressed:I = 0x7f0c0003

.field public static final abc_search_url_text_selected:I = 0x7f0c0004

.field public static final abc_secondary_text_material_dark:I = 0x7f0c0100

.field public static final abc_secondary_text_material_light:I = 0x7f0c0101

.field public static final abc_tint_btn_checkable:I = 0x7f0c0102

.field public static final abc_tint_default:I = 0x7f0c0103

.field public static final abc_tint_edittext:I = 0x7f0c0104

.field public static final abc_tint_seek_thumb:I = 0x7f0c0105

.field public static final abc_tint_spinner:I = 0x7f0c0106

.field public static final abc_tint_switch_thumb:I = 0x7f0c0107

.field public static final abc_tint_switch_track:I = 0x7f0c0108

.field public static final accent_material_dark:I = 0x7f0c0005

.field public static final accent_material_light:I = 0x7f0c0006

.field public static final amber:I = 0x7f0c000c

.field public static final background_floating_material_dark:I = 0x7f0c0019

.field public static final background_floating_material_light:I = 0x7f0c001a

.field public static final background_material_dark:I = 0x7f0c001c

.field public static final background_material_light:I = 0x7f0c001d

.field public static final black:I = 0x7f0c001f

.field public static final bluegrey:I = 0x7f0c0026

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c0028

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c0029

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c002a

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c002b

.field public static final bright_foreground_material_dark:I = 0x7f0c002c

.field public static final bright_foreground_material_light:I = 0x7f0c002d

.field public static final brown:I = 0x7f0c002e

.field public static final button_material_dark:I = 0x7f0c0030

.field public static final button_material_light:I = 0x7f0c0031

.field public static final cardview_dark_background:I = 0x7f0c0033

.field public static final cardview_light_background:I = 0x7f0c0034

.field public static final cardview_shadow_end_color:I = 0x7f0c0035

.field public static final cardview_shadow_start_color:I = 0x7f0c0036

.field public static final com_facebook_blue:I = 0x7f0c0037

.field public static final com_facebook_button_background_color:I = 0x7f0c0038

.field public static final com_facebook_button_background_color_disabled:I = 0x7f0c0039

.field public static final com_facebook_button_background_color_focused:I = 0x7f0c003a

.field public static final com_facebook_button_background_color_focused_disabled:I = 0x7f0c003b

.field public static final com_facebook_button_background_color_pressed:I = 0x7f0c003c

.field public static final com_facebook_button_background_color_selected:I = 0x7f0c003d

.field public static final com_facebook_button_border_color_focused:I = 0x7f0c003e

.field public static final com_facebook_button_login_silver_background_color:I = 0x7f0c003f

.field public static final com_facebook_button_login_silver_background_color_pressed:I = 0x7f0c0040

.field public static final com_facebook_button_send_background_color:I = 0x7f0c0041

.field public static final com_facebook_button_send_background_color_pressed:I = 0x7f0c0042

.field public static final com_facebook_button_text_color:I = 0x7f0c0109

.field public static final com_facebook_device_auth_text:I = 0x7f0c0043

.field public static final com_facebook_likeboxcountview_border_color:I = 0x7f0c0044

.field public static final com_facebook_likeboxcountview_text_color:I = 0x7f0c0045

.field public static final com_facebook_likeview_text_color:I = 0x7f0c0046

.field public static final com_facebook_messenger_blue:I = 0x7f0c0047

.field public static final com_facebook_send_button_text_color:I = 0x7f0c010a

.field public static final com_facebook_share_button_text_color:I = 0x7f0c0048

.field public static final com_smart_login_code:I = 0x7f0c0049

.field public static final common_google_signin_btn_text_dark:I = 0x7f0c010b

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f0c004c

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0c004d

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0c004e

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0c004f

.field public static final common_google_signin_btn_text_light:I = 0x7f0c010c

.field public static final common_google_signin_btn_text_light_default:I = 0x7f0c0050

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0c0051

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0c0052

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0c0053

.field public static final create_store_edit_text_background:I = 0x7f0c0054

.field public static final create_store_edit_text_color:I = 0x7f0c0055

.field public static final create_store_hint_color:I = 0x7f0c0056

.field public static final create_store_title_color:I = 0x7f0c0057

.field public static final deeppurple:I = 0x7f0c005c

.field public static final default_color:I = 0x7f0c005e

.field public static final design_bottom_navigation_shadow_color:I = 0x7f0c0062

.field public static final design_error:I = 0x7f0c010d

.field public static final design_fab_shadow_end_color:I = 0x7f0c0063

.field public static final design_fab_shadow_mid_color:I = 0x7f0c0064

.field public static final design_fab_shadow_start_color:I = 0x7f0c0065

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0c0066

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0c0067

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0c0068

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0c0069

.field public static final design_snackbar_background_color:I = 0x7f0c006a

.field public static final design_textinput_error_color_dark:I = 0x7f0c006b

.field public static final design_textinput_error_color_light:I = 0x7f0c006c

.field public static final design_tint_password_toggle:I = 0x7f0c010e

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c006d

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c006e

.field public static final dim_foreground_material_dark:I = 0x7f0c006f

.field public static final dim_foreground_material_light:I = 0x7f0c0070

.field public static final followed_stores_store_name_color:I = 0x7f0c0074

.field public static final foreground_material_dark:I = 0x7f0c0075

.field public static final foreground_material_light:I = 0x7f0c0076

.field public static final green:I = 0x7f0c0079

.field public static final grey:I = 0x7f0c007d

.field public static final highlighted_text_material_dark:I = 0x7f0c0080

.field public static final highlighted_text_material_light:I = 0x7f0c0081

.field public static final indigo:I = 0x7f0c0082

.field public static final lightblue:I = 0x7f0c0085

.field public static final lightgreen:I = 0x7f0c0088

.field public static final lime:I = 0x7f0c008c

.field public static final material_blue_grey_800:I = 0x7f0c0093

.field public static final material_blue_grey_900:I = 0x7f0c0094

.field public static final material_blue_grey_950:I = 0x7f0c0095

.field public static final material_deep_teal_200:I = 0x7f0c0096

.field public static final material_deep_teal_500:I = 0x7f0c0097

.field public static final material_grey_100:I = 0x7f0c0098

.field public static final material_grey_300:I = 0x7f0c0099

.field public static final material_grey_50:I = 0x7f0c009a

.field public static final material_grey_600:I = 0x7f0c009b

.field public static final material_grey_800:I = 0x7f0c009c

.field public static final material_grey_850:I = 0x7f0c009d

.field public static final material_grey_900:I = 0x7f0c009e

.field public static final notification_action_color_filter:I = 0x7f0c0000

.field public static final notification_icon_bg_color:I = 0x7f0c00a5

.field public static final notification_material_background_media_default_color:I = 0x7f0c00a6

.field public static final orange:I = 0x7f0c00a7

.field public static final orange_700:I = 0x7f0c00a8

.field public static final pink:I = 0x7f0c00aa

.field public static final primary_dark_material_dark:I = 0x7f0c00ac

.field public static final primary_dark_material_light:I = 0x7f0c00ad

.field public static final primary_material_dark:I = 0x7f0c00ae

.field public static final primary_material_light:I = 0x7f0c00af

.field public static final primary_text_default_material_dark:I = 0x7f0c00b0

.field public static final primary_text_default_material_light:I = 0x7f0c00b1

.field public static final primary_text_disabled_material_dark:I = 0x7f0c00b2

.field public static final primary_text_disabled_material_light:I = 0x7f0c00b3

.field public static final recommended_stores_numbers_color:I = 0x7f0c00b6

.field public static final red:I = 0x7f0c00b7

.field public static final ripple_material_dark:I = 0x7f0c00ba

.field public static final ripple_material_light:I = 0x7f0c00bb

.field public static final secondary_text_default_material_dark:I = 0x7f0c00c0

.field public static final secondary_text_default_material_light:I = 0x7f0c00c1

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c00c2

.field public static final secondary_text_disabled_material_light:I = 0x7f0c00c3

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c00cc

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c00cd

.field public static final switch_thumb_material_dark:I = 0x7f0c010f

.field public static final switch_thumb_material_light:I = 0x7f0c0110

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c00ce

.field public static final switch_thumb_normal_material_light:I = 0x7f0c00cf

.field public static final teal:I = 0x7f0c00d0

.field public static final white:I = 0x7f0c00f2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
