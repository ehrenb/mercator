.class public interface abstract Lio/realm/k;
.super Ljava/lang/Object;
.source "FileToDownloadRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$altLink()Ljava/lang/String;
.end method

.method public abstract realmGet$downloadId()I
.end method

.method public abstract realmGet$fileName()Ljava/lang/String;
.end method

.method public abstract realmGet$fileType()I
.end method

.method public abstract realmGet$link()Ljava/lang/String;
.end method

.method public abstract realmGet$md5()Ljava/lang/String;
.end method

.method public abstract realmGet$packageName()Ljava/lang/String;
.end method

.method public abstract realmGet$path()Ljava/lang/String;
.end method

.method public abstract realmGet$progress()I
.end method

.method public abstract realmGet$status()I
.end method

.method public abstract realmGet$versionCode()I
.end method

.method public abstract realmGet$versionName()Ljava/lang/String;
.end method

.method public abstract realmSet$altLink(Ljava/lang/String;)V
.end method

.method public abstract realmSet$downloadId(I)V
.end method

.method public abstract realmSet$fileName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$fileType(I)V
.end method

.method public abstract realmSet$link(Ljava/lang/String;)V
.end method

.method public abstract realmSet$md5(Ljava/lang/String;)V
.end method

.method public abstract realmSet$packageName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$path(Ljava/lang/String;)V
.end method

.method public abstract realmSet$progress(I)V
.end method

.method public abstract realmSet$status(I)V
.end method

.method public abstract realmSet$versionCode(I)V
.end method

.method public abstract realmSet$versionName(Ljava/lang/String;)V
.end method
