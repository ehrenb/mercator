.class public interface abstract Lio/realm/aq;
.super Ljava/lang/Object;
.source "StoreRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$downloads()J
.end method

.method public abstract realmGet$iconPath()Ljava/lang/String;
.end method

.method public abstract realmGet$passwordSha1()Ljava/lang/String;
.end method

.method public abstract realmGet$storeId()J
.end method

.method public abstract realmGet$storeName()Ljava/lang/String;
.end method

.method public abstract realmGet$theme()Ljava/lang/String;
.end method

.method public abstract realmGet$username()Ljava/lang/String;
.end method

.method public abstract realmSet$downloads(J)V
.end method

.method public abstract realmSet$iconPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$passwordSha1(Ljava/lang/String;)V
.end method

.method public abstract realmSet$storeId(J)V
.end method

.method public abstract realmSet$storeName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$theme(Ljava/lang/String;)V
.end method

.method public abstract realmSet$username(Ljava/lang/String;)V
.end method
