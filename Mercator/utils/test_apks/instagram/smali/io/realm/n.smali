.class public interface abstract Lio/realm/n;
.super Ljava/lang/Object;
.source "InstalledRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$icon()Ljava/lang/String;
.end method

.method public abstract realmGet$name()Ljava/lang/String;
.end method

.method public abstract realmGet$packageName()Ljava/lang/String;
.end method

.method public abstract realmGet$signature()Ljava/lang/String;
.end method

.method public abstract realmGet$storeName()Ljava/lang/String;
.end method

.method public abstract realmGet$systemApp()Z
.end method

.method public abstract realmGet$versionCode()I
.end method

.method public abstract realmGet$versionName()Ljava/lang/String;
.end method

.method public abstract realmSet$icon(Ljava/lang/String;)V
.end method

.method public abstract realmSet$name(Ljava/lang/String;)V
.end method

.method public abstract realmSet$packageName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$signature(Ljava/lang/String;)V
.end method

.method public abstract realmSet$storeName(Ljava/lang/String;)V
.end method

.method public abstract realmSet$systemApp(Z)V
.end method

.method public abstract realmSet$versionCode(I)V
.end method

.method public abstract realmSet$versionName(Ljava/lang/String;)V
.end method
