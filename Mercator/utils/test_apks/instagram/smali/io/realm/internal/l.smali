.class public interface abstract Lio/realm/internal/l;
.super Ljava/lang/Object;
.source "Row.java"


# static fields
.field public static final b:Lio/realm/internal/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lio/realm/internal/l$1;

    invoke-direct {v0}, Lio/realm/internal/l$1;-><init>()V

    sput-object v0, Lio/realm/internal/l;->b:Lio/realm/internal/l;

    return-void
.end method


# virtual methods
.method public abstract a()J
.end method

.method public abstract a(Ljava/lang/String;)J
.end method

.method public abstract a(JD)V
.end method

.method public abstract a(JF)V
.end method

.method public abstract a(JJ)V
.end method

.method public abstract a(JLjava/lang/String;)V
.end method

.method public abstract a(JLjava/util/Date;)V
.end method

.method public abstract a(JZ)V
.end method

.method public abstract a(J[B)V
.end method

.method public abstract a(J)Z
.end method

.method public abstract b()Lio/realm/internal/Table;
.end method

.method public abstract b(JJ)V
.end method

.method public abstract b(J)Z
.end method

.method public abstract c()J
.end method

.method public abstract c(J)V
.end method

.method public abstract d(J)Ljava/lang/String;
.end method

.method public abstract d()Z
.end method

.method public abstract e(J)Lio/realm/RealmFieldType;
.end method

.method public abstract f(J)J
.end method

.method public abstract g(J)Z
.end method

.method public abstract h(J)F
.end method

.method public abstract i(J)D
.end method

.method public abstract j(J)Ljava/util/Date;
.end method

.method public abstract k(J)Ljava/lang/String;
.end method

.method public abstract l(J)[B
.end method

.method public abstract m(J)Lio/realm/internal/LinkView;
.end method

.method public abstract n(J)V
.end method
