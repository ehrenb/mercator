.class public interface abstract Lio/realm/o;
.super Ljava/lang/Object;
.source "MinimalAdRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$adId()Ljava/lang/Long;
.end method

.method public abstract realmGet$appId()Ljava/lang/Long;
.end method

.method public abstract realmGet$clickUrl()Ljava/lang/String;
.end method

.method public abstract realmGet$cpcUrl()Ljava/lang/String;
.end method

.method public abstract realmGet$cpdUrl()Ljava/lang/String;
.end method

.method public abstract realmGet$cpiUrl()Ljava/lang/String;
.end method

.method public abstract realmGet$description()Ljava/lang/String;
.end method

.method public abstract realmGet$iconPath()Ljava/lang/String;
.end method

.method public abstract realmGet$name()Ljava/lang/String;
.end method

.method public abstract realmGet$networkId()Ljava/lang/Long;
.end method

.method public abstract realmGet$packageName()Ljava/lang/String;
.end method

.method public abstract realmSet$adId(Ljava/lang/Long;)V
.end method

.method public abstract realmSet$appId(Ljava/lang/Long;)V
.end method

.method public abstract realmSet$clickUrl(Ljava/lang/String;)V
.end method

.method public abstract realmSet$cpcUrl(Ljava/lang/String;)V
.end method

.method public abstract realmSet$cpdUrl(Ljava/lang/String;)V
.end method

.method public abstract realmSet$cpiUrl(Ljava/lang/String;)V
.end method

.method public abstract realmSet$description(Ljava/lang/String;)V
.end method

.method public abstract realmSet$iconPath(Ljava/lang/String;)V
.end method

.method public abstract realmSet$name(Ljava/lang/String;)V
.end method

.method public abstract realmSet$networkId(Ljava/lang/Long;)V
.end method

.method public abstract realmSet$packageName(Ljava/lang/String;)V
.end method
