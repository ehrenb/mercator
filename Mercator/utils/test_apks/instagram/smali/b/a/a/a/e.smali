.class public final enum Lb/a/a/a/e;
.super Ljava/lang/Enum;
.source "DNSRecordType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lb/a/a/a/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lb/a/a/a/e;

.field public static final enum B:Lb/a/a/a/e;

.field public static final enum C:Lb/a/a/a/e;

.field public static final enum D:Lb/a/a/a/e;

.field public static final enum E:Lb/a/a/a/e;

.field public static final enum F:Lb/a/a/a/e;

.field public static final enum G:Lb/a/a/a/e;

.field public static final enum H:Lb/a/a/a/e;

.field public static final enum I:Lb/a/a/a/e;

.field public static final enum J:Lb/a/a/a/e;

.field public static final enum K:Lb/a/a/a/e;

.field public static final enum L:Lb/a/a/a/e;

.field public static final enum M:Lb/a/a/a/e;

.field public static final enum N:Lb/a/a/a/e;

.field public static final enum O:Lb/a/a/a/e;

.field public static final enum P:Lb/a/a/a/e;

.field public static final enum Q:Lb/a/a/a/e;

.field public static final enum R:Lb/a/a/a/e;

.field public static final enum S:Lb/a/a/a/e;

.field public static final enum T:Lb/a/a/a/e;

.field public static final enum U:Lb/a/a/a/e;

.field public static final enum V:Lb/a/a/a/e;

.field public static final enum W:Lb/a/a/a/e;

.field public static final enum X:Lb/a/a/a/e;

.field public static final enum Y:Lb/a/a/a/e;

.field public static final enum Z:Lb/a/a/a/e;

.field public static final enum a:Lb/a/a/a/e;

.field public static final enum aa:Lb/a/a/a/e;

.field public static final enum ab:Lb/a/a/a/e;

.field public static final enum ac:Lb/a/a/a/e;

.field public static final enum ad:Lb/a/a/a/e;

.field public static final enum ae:Lb/a/a/a/e;

.field public static final enum af:Lb/a/a/a/e;

.field public static final enum ag:Lb/a/a/a/e;

.field private static ah:Ld/a/b;

.field private static final synthetic ak:[Lb/a/a/a/e;

.field public static final enum b:Lb/a/a/a/e;

.field public static final enum c:Lb/a/a/a/e;

.field public static final enum d:Lb/a/a/a/e;

.field public static final enum e:Lb/a/a/a/e;

.field public static final enum f:Lb/a/a/a/e;

.field public static final enum g:Lb/a/a/a/e;

.field public static final enum h:Lb/a/a/a/e;

.field public static final enum i:Lb/a/a/a/e;

.field public static final enum j:Lb/a/a/a/e;

.field public static final enum k:Lb/a/a/a/e;

.field public static final enum l:Lb/a/a/a/e;

.field public static final enum m:Lb/a/a/a/e;

.field public static final enum n:Lb/a/a/a/e;

.field public static final enum o:Lb/a/a/a/e;

.field public static final enum p:Lb/a/a/a/e;

.field public static final enum q:Lb/a/a/a/e;

.field public static final enum r:Lb/a/a/a/e;

.field public static final enum s:Lb/a/a/a/e;

.field public static final enum t:Lb/a/a/a/e;

.field public static final enum u:Lb/a/a/a/e;

.field public static final enum v:Lb/a/a/a/e;

.field public static final enum w:Lb/a/a/a/e;

.field public static final enum x:Lb/a/a/a/e;

.field public static final enum y:Lb/a/a/a/e;

.field public static final enum z:Lb/a/a/a/e;


# instance fields
.field private final ai:Ljava/lang/String;

.field private final aj:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 18
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_IGNORE"

    const-string v2, "ignore"

    invoke-direct {v0, v1, v5, v2, v5}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->a:Lb/a/a/a/e;

    .line 22
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_A"

    const-string v2, "a"

    invoke-direct {v0, v1, v6, v2, v6}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->b:Lb/a/a/a/e;

    .line 26
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NS"

    const-string v2, "ns"

    invoke-direct {v0, v1, v7, v2, v7}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->c:Lb/a/a/a/e;

    .line 30
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MD"

    const-string v2, "md"

    invoke-direct {v0, v1, v8, v2, v8}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->d:Lb/a/a/a/e;

    .line 34
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MF"

    const-string v2, "mf"

    invoke-direct {v0, v1, v9, v2, v9}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->e:Lb/a/a/a/e;

    .line 38
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_CNAME"

    const/4 v2, 0x5

    const-string v3, "cname"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->f:Lb/a/a/a/e;

    .line 42
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_SOA"

    const/4 v2, 0x6

    const-string v3, "soa"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->g:Lb/a/a/a/e;

    .line 46
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MB"

    const/4 v2, 0x7

    const-string v3, "mb"

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->h:Lb/a/a/a/e;

    .line 50
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MG"

    const/16 v2, 0x8

    const-string v3, "mg"

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->i:Lb/a/a/a/e;

    .line 54
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MR"

    const/16 v2, 0x9

    const-string v3, "mr"

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->j:Lb/a/a/a/e;

    .line 58
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NULL"

    const/16 v2, 0xa

    const-string v3, "null"

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->k:Lb/a/a/a/e;

    .line 62
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_WKS"

    const/16 v2, 0xb

    const-string v3, "wks"

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->l:Lb/a/a/a/e;

    .line 66
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_PTR"

    const/16 v2, 0xc

    const-string v3, "ptr"

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->m:Lb/a/a/a/e;

    .line 70
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_HINFO"

    const/16 v2, 0xd

    const-string v3, "hinfo"

    const/16 v4, 0xd

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->n:Lb/a/a/a/e;

    .line 74
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MINFO"

    const/16 v2, 0xe

    const-string v3, "minfo"

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->o:Lb/a/a/a/e;

    .line 78
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MX"

    const/16 v2, 0xf

    const-string v3, "mx"

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->p:Lb/a/a/a/e;

    .line 82
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_TXT"

    const/16 v2, 0x10

    const-string v3, "txt"

    const/16 v4, 0x10

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->q:Lb/a/a/a/e;

    .line 86
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_RP"

    const/16 v2, 0x11

    const-string v3, "rp"

    const/16 v4, 0x11

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->r:Lb/a/a/a/e;

    .line 90
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_AFSDB"

    const/16 v2, 0x12

    const-string v3, "afsdb"

    const/16 v4, 0x12

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->s:Lb/a/a/a/e;

    .line 94
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_X25"

    const/16 v2, 0x13

    const-string v3, "x25"

    const/16 v4, 0x13

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->t:Lb/a/a/a/e;

    .line 98
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_ISDN"

    const/16 v2, 0x14

    const-string v3, "isdn"

    const/16 v4, 0x14

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->u:Lb/a/a/a/e;

    .line 102
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_RT"

    const/16 v2, 0x15

    const-string v3, "rt"

    const/16 v4, 0x15

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->v:Lb/a/a/a/e;

    .line 106
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NSAP"

    const/16 v2, 0x16

    const-string v3, "nsap"

    const/16 v4, 0x16

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->w:Lb/a/a/a/e;

    .line 110
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NSAP_PTR"

    const/16 v2, 0x17

    const-string v3, "nsap-otr"

    const/16 v4, 0x17

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->x:Lb/a/a/a/e;

    .line 114
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_SIG"

    const/16 v2, 0x18

    const-string v3, "sig"

    const/16 v4, 0x18

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->y:Lb/a/a/a/e;

    .line 118
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_KEY"

    const/16 v2, 0x19

    const-string v3, "key"

    const/16 v4, 0x19

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->z:Lb/a/a/a/e;

    .line 122
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_PX"

    const/16 v2, 0x1a

    const-string v3, "px"

    const/16 v4, 0x1a

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->A:Lb/a/a/a/e;

    .line 126
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_GPOS"

    const/16 v2, 0x1b

    const-string v3, "gpos"

    const/16 v4, 0x1b

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->B:Lb/a/a/a/e;

    .line 130
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_AAAA"

    const/16 v2, 0x1c

    const-string v3, "aaaa"

    const/16 v4, 0x1c

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->C:Lb/a/a/a/e;

    .line 134
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_LOC"

    const/16 v2, 0x1d

    const-string v3, "loc"

    const/16 v4, 0x1d

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->D:Lb/a/a/a/e;

    .line 138
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NXT"

    const/16 v2, 0x1e

    const-string v3, "nxt"

    const/16 v4, 0x1e

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->E:Lb/a/a/a/e;

    .line 142
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_EID"

    const/16 v2, 0x1f

    const-string v3, "eid"

    const/16 v4, 0x1f

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->F:Lb/a/a/a/e;

    .line 146
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NIMLOC"

    const/16 v2, 0x20

    const-string v3, "nimloc"

    const/16 v4, 0x20

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->G:Lb/a/a/a/e;

    .line 150
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_SRV"

    const/16 v2, 0x21

    const-string v3, "srv"

    const/16 v4, 0x21

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->H:Lb/a/a/a/e;

    .line 154
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_ATMA"

    const/16 v2, 0x22

    const-string v3, "atma"

    const/16 v4, 0x22

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->I:Lb/a/a/a/e;

    .line 158
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NAPTR"

    const/16 v2, 0x23

    const-string v3, "naptr"

    const/16 v4, 0x23

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->J:Lb/a/a/a/e;

    .line 162
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_KX"

    const/16 v2, 0x24

    const-string v3, "kx"

    const/16 v4, 0x24

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->K:Lb/a/a/a/e;

    .line 166
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_CERT"

    const/16 v2, 0x25

    const-string v3, "cert"

    const/16 v4, 0x25

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->L:Lb/a/a/a/e;

    .line 170
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_A6"

    const/16 v2, 0x26

    const-string v3, "a6"

    const/16 v4, 0x26

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->M:Lb/a/a/a/e;

    .line 174
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_DNAME"

    const/16 v2, 0x27

    const-string v3, "dname"

    const/16 v4, 0x27

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->N:Lb/a/a/a/e;

    .line 178
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_SINK"

    const/16 v2, 0x28

    const-string v3, "sink"

    const/16 v4, 0x28

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->O:Lb/a/a/a/e;

    .line 182
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_OPT"

    const/16 v2, 0x29

    const-string v3, "opt"

    const/16 v4, 0x29

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->P:Lb/a/a/a/e;

    .line 186
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_APL"

    const/16 v2, 0x2a

    const-string v3, "apl"

    const/16 v4, 0x2a

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->Q:Lb/a/a/a/e;

    .line 190
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_DS"

    const/16 v2, 0x2b

    const-string v3, "ds"

    const/16 v4, 0x2b

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->R:Lb/a/a/a/e;

    .line 194
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_SSHFP"

    const/16 v2, 0x2c

    const-string v3, "sshfp"

    const/16 v4, 0x2c

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->S:Lb/a/a/a/e;

    .line 198
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_RRSIG"

    const/16 v2, 0x2d

    const-string v3, "rrsig"

    const/16 v4, 0x2e

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->T:Lb/a/a/a/e;

    .line 202
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_NSEC"

    const/16 v2, 0x2e

    const-string v3, "nsec"

    const/16 v4, 0x2f

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->U:Lb/a/a/a/e;

    .line 206
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_DNSKEY"

    const/16 v2, 0x2f

    const-string v3, "dnskey"

    const/16 v4, 0x30

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->V:Lb/a/a/a/e;

    .line 210
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_UINFO"

    const/16 v2, 0x30

    const-string v3, "uinfo"

    const/16 v4, 0x64

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->W:Lb/a/a/a/e;

    .line 214
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_UID"

    const/16 v2, 0x31

    const-string v3, "uid"

    const/16 v4, 0x65

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->X:Lb/a/a/a/e;

    .line 218
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_GID"

    const/16 v2, 0x32

    const-string v3, "gid"

    const/16 v4, 0x66

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->Y:Lb/a/a/a/e;

    .line 222
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_UNSPEC"

    const/16 v2, 0x33

    const-string v3, "unspec"

    const/16 v4, 0x67

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->Z:Lb/a/a/a/e;

    .line 226
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_TKEY"

    const/16 v2, 0x34

    const-string v3, "tkey"

    const/16 v4, 0xf9

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->aa:Lb/a/a/a/e;

    .line 230
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_TSIG"

    const/16 v2, 0x35

    const-string v3, "tsig"

    const/16 v4, 0xfa

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->ab:Lb/a/a/a/e;

    .line 234
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_IXFR"

    const/16 v2, 0x36

    const-string v3, "ixfr"

    const/16 v4, 0xfb

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->ac:Lb/a/a/a/e;

    .line 238
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_AXFR"

    const/16 v2, 0x37

    const-string v3, "axfr"

    const/16 v4, 0xfc

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->ad:Lb/a/a/a/e;

    .line 242
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MAILA"

    const/16 v2, 0x38

    const-string v3, "mails"

    const/16 v4, 0xfd

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->ae:Lb/a/a/a/e;

    .line 246
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_MAILB"

    const/16 v2, 0x39

    const-string v3, "mailb"

    const/16 v4, 0xfe

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->af:Lb/a/a/a/e;

    .line 250
    new-instance v0, Lb/a/a/a/e;

    const-string v1, "TYPE_ANY"

    const/16 v2, 0x3a

    const-string v3, "any"

    const/16 v4, 0xff

    invoke-direct {v0, v1, v2, v3, v4}, Lb/a/a/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lb/a/a/a/e;->ag:Lb/a/a/a/e;

    .line 14
    const/16 v0, 0x3b

    new-array v0, v0, [Lb/a/a/a/e;

    sget-object v1, Lb/a/a/a/e;->a:Lb/a/a/a/e;

    aput-object v1, v0, v5

    sget-object v1, Lb/a/a/a/e;->b:Lb/a/a/a/e;

    aput-object v1, v0, v6

    sget-object v1, Lb/a/a/a/e;->c:Lb/a/a/a/e;

    aput-object v1, v0, v7

    sget-object v1, Lb/a/a/a/e;->d:Lb/a/a/a/e;

    aput-object v1, v0, v8

    sget-object v1, Lb/a/a/a/e;->e:Lb/a/a/a/e;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lb/a/a/a/e;->f:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lb/a/a/a/e;->g:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lb/a/a/a/e;->h:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lb/a/a/a/e;->i:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lb/a/a/a/e;->j:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lb/a/a/a/e;->k:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lb/a/a/a/e;->l:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lb/a/a/a/e;->m:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lb/a/a/a/e;->n:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lb/a/a/a/e;->o:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lb/a/a/a/e;->p:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lb/a/a/a/e;->q:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lb/a/a/a/e;->r:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lb/a/a/a/e;->s:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lb/a/a/a/e;->t:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lb/a/a/a/e;->u:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lb/a/a/a/e;->v:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lb/a/a/a/e;->w:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lb/a/a/a/e;->x:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lb/a/a/a/e;->y:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lb/a/a/a/e;->z:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lb/a/a/a/e;->A:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lb/a/a/a/e;->B:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lb/a/a/a/e;->C:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lb/a/a/a/e;->D:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lb/a/a/a/e;->E:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lb/a/a/a/e;->F:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lb/a/a/a/e;->G:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lb/a/a/a/e;->H:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lb/a/a/a/e;->I:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lb/a/a/a/e;->J:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lb/a/a/a/e;->K:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lb/a/a/a/e;->L:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lb/a/a/a/e;->M:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lb/a/a/a/e;->N:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lb/a/a/a/e;->O:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lb/a/a/a/e;->P:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lb/a/a/a/e;->Q:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lb/a/a/a/e;->R:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lb/a/a/a/e;->S:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lb/a/a/a/e;->T:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lb/a/a/a/e;->U:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lb/a/a/a/e;->V:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lb/a/a/a/e;->W:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lb/a/a/a/e;->X:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lb/a/a/a/e;->Y:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lb/a/a/a/e;->Z:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lb/a/a/a/e;->aa:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lb/a/a/a/e;->ab:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lb/a/a/a/e;->ac:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lb/a/a/a/e;->ad:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lb/a/a/a/e;->ae:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lb/a/a/a/e;->af:Lb/a/a/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lb/a/a/a/e;->ag:Lb/a/a/a/e;

    aput-object v2, v0, v1

    sput-object v0, Lb/a/a/a/e;->ak:[Lb/a/a/a/e;

    .line 252
    const-class v0, Lb/a/a/a/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ld/a/c;->a(Ljava/lang/String;)Ld/a/b;

    move-result-object v0

    sput-object v0, Lb/a/a/a/e;->ah:Ld/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 258
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 259
    iput-object p3, p0, Lb/a/a/a/e;->ai:Ljava/lang/String;

    .line 260
    iput p4, p0, Lb/a/a/a/e;->aj:I

    .line 261
    return-void
.end method

.method public static a(I)Lb/a/a/a/e;
    .locals 5

    .prologue
    .line 301
    invoke-static {}, Lb/a/a/a/e;->values()[Lb/a/a/a/e;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 302
    iget v4, v0, Lb/a/a/a/e;->aj:I

    if-ne v4, p0, :cond_0

    .line 305
    :goto_1
    return-object v0

    .line 301
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 304
    :cond_1
    sget-object v0, Lb/a/a/a/e;->ah:Ld/a/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not find record type for index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ld/a/b;->c(Ljava/lang/String;)V

    .line 305
    sget-object v0, Lb/a/a/a/e;->a:Lb/a/a/a/e;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lb/a/a/a/e;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lb/a/a/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lb/a/a/a/e;

    return-object v0
.end method

.method public static values()[Lb/a/a/a/e;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lb/a/a/a/e;->ak:[Lb/a/a/a/e;

    invoke-virtual {v0}, [Lb/a/a/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb/a/a/a/e;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lb/a/a/a/e;->aj:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lb/a/a/a/e;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lb/a/a/a/e;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
