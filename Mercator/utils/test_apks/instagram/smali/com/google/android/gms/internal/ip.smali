.class public Lcom/google/android/gms/internal/ip;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/in;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/internal/iu;

.field public final b:Lcom/google/android/gms/ads/internal/cache/e;

.field public final c:Lcom/google/android/gms/internal/jq;

.field public final d:Lcom/google/android/gms/internal/cr;

.field public final e:Lcom/google/android/gms/internal/iy;

.field public final f:Lcom/google/android/gms/internal/fr;

.field public final g:Lcom/google/android/gms/internal/iz;

.field public final h:Lcom/google/android/gms/internal/ja;

.field public final i:Lcom/google/android/gms/internal/hh;

.field public final j:Lcom/google/android/gms/internal/jt;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/iu;Lcom/google/android/gms/ads/internal/cache/e;Lcom/google/android/gms/internal/jq;Lcom/google/android/gms/internal/cr;Lcom/google/android/gms/internal/iy;Lcom/google/android/gms/internal/fr;Lcom/google/android/gms/internal/iz;Lcom/google/android/gms/internal/ja;Lcom/google/android/gms/internal/hh;Lcom/google/android/gms/internal/jt;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ip;->a:Lcom/google/android/gms/internal/iu;

    iput-object p2, p0, Lcom/google/android/gms/internal/ip;->b:Lcom/google/android/gms/ads/internal/cache/e;

    iput-object p3, p0, Lcom/google/android/gms/internal/ip;->c:Lcom/google/android/gms/internal/jq;

    iput-object p4, p0, Lcom/google/android/gms/internal/ip;->d:Lcom/google/android/gms/internal/cr;

    iput-object p5, p0, Lcom/google/android/gms/internal/ip;->e:Lcom/google/android/gms/internal/iy;

    iput-object p6, p0, Lcom/google/android/gms/internal/ip;->f:Lcom/google/android/gms/internal/fr;

    iput-object p7, p0, Lcom/google/android/gms/internal/ip;->g:Lcom/google/android/gms/internal/iz;

    iput-object p8, p0, Lcom/google/android/gms/internal/ip;->h:Lcom/google/android/gms/internal/ja;

    iput-object p9, p0, Lcom/google/android/gms/internal/ip;->i:Lcom/google/android/gms/internal/hh;

    iput-object p10, p0, Lcom/google/android/gms/internal/ip;->j:Lcom/google/android/gms/internal/jt;

    return-void
.end method

.method public static a()Lcom/google/android/gms/internal/ip;
    .locals 11

    new-instance v0, Lcom/google/android/gms/internal/ip;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/ads/internal/cache/g;

    invoke-direct {v2}, Lcom/google/android/gms/ads/internal/cache/g;-><init>()V

    new-instance v3, Lcom/google/android/gms/internal/jr;

    invoke-direct {v3}, Lcom/google/android/gms/internal/jr;-><init>()V

    new-instance v4, Lcom/google/android/gms/internal/cq;

    invoke-direct {v4}, Lcom/google/android/gms/internal/cq;-><init>()V

    new-instance v5, Lcom/google/android/gms/internal/jb;

    invoke-direct {v5}, Lcom/google/android/gms/internal/jb;-><init>()V

    new-instance v6, Lcom/google/android/gms/internal/fs;

    invoke-direct {v6}, Lcom/google/android/gms/internal/fs;-><init>()V

    new-instance v7, Lcom/google/android/gms/internal/jc;

    invoke-direct {v7}, Lcom/google/android/gms/internal/jc;-><init>()V

    new-instance v8, Lcom/google/android/gms/internal/jd;

    invoke-direct {v8}, Lcom/google/android/gms/internal/jd;-><init>()V

    new-instance v9, Lcom/google/android/gms/internal/hg;

    invoke-direct {v9}, Lcom/google/android/gms/internal/hg;-><init>()V

    new-instance v10, Lcom/google/android/gms/internal/js;

    invoke-direct {v10}, Lcom/google/android/gms/internal/js;-><init>()V

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/internal/ip;-><init>(Lcom/google/android/gms/internal/iu;Lcom/google/android/gms/ads/internal/cache/e;Lcom/google/android/gms/internal/jq;Lcom/google/android/gms/internal/cr;Lcom/google/android/gms/internal/iy;Lcom/google/android/gms/internal/fr;Lcom/google/android/gms/internal/iz;Lcom/google/android/gms/internal/ja;Lcom/google/android/gms/internal/hh;Lcom/google/android/gms/internal/jt;)V

    return-object v0
.end method
