.class Lcom/a/a/a/g;
.super Lio/fabric/sdk/android/a$b;
.source "AnswersLifecycleCallbacks.java"


# instance fields
.field private final a:Lcom/a/a/a/u;

.field private final b:Lcom/a/a/a/j;


# direct methods
.method public constructor <init>(Lcom/a/a/a/u;Lcom/a/a/a/j;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lio/fabric/sdk/android/a$b;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/a/a/a/g;->a:Lcom/a/a/a/u;

    .line 16
    iput-object p2, p0, Lcom/a/a/a/g;->b:Lcom/a/a/a/j;

    .line 17
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/a/a/a/g;->a:Lcom/a/a/a/u;

    sget-object v1, Lcom/a/a/a/w$b;->a:Lcom/a/a/a/w$b;

    invoke-virtual {v0, p1, v1}, Lcom/a/a/a/u;->a(Landroid/app/Activity;Lcom/a/a/a/w$b;)V

    .line 27
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/a/a/a/g;->a:Lcom/a/a/a/u;

    sget-object v1, Lcom/a/a/a/w$b;->b:Lcom/a/a/a/w$b;

    invoke-virtual {v0, p1, v1}, Lcom/a/a/a/u;->a(Landroid/app/Activity;Lcom/a/a/a/w$b;)V

    .line 32
    iget-object v0, p0, Lcom/a/a/a/g;->b:Lcom/a/a/a/j;

    invoke-virtual {v0}, Lcom/a/a/a/j;->a()V

    .line 33
    return-void
.end method

.method public b(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public c(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/a/a/a/g;->a:Lcom/a/a/a/u;

    sget-object v1, Lcom/a/a/a/w$b;->c:Lcom/a/a/a/w$b;

    invoke-virtual {v0, p1, v1}, Lcom/a/a/a/u;->a(Landroid/app/Activity;Lcom/a/a/a/w$b;)V

    .line 38
    iget-object v0, p0, Lcom/a/a/a/g;->b:Lcom/a/a/a/j;

    invoke-virtual {v0}, Lcom/a/a/a/j;->b()V

    .line 39
    return-void
.end method

.method public d(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/a/a/a/g;->a:Lcom/a/a/a/u;

    sget-object v1, Lcom/a/a/a/w$b;->d:Lcom/a/a/a/w$b;

    invoke-virtual {v0, p1, v1}, Lcom/a/a/a/u;->a(Landroid/app/Activity;Lcom/a/a/a/w$b;)V

    .line 44
    return-void
.end method

.method public e(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method
