class ComponentType:
    ACTIVITY = 'activity'
    SERVICE = 'service'
    PROVIDER = 'provider'
    RECEIVER = 'receiver'